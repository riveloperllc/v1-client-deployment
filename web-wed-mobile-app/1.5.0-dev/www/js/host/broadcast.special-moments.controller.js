angular.module('WebWedApp')

.controller('SpecialMomentsBroadcastCtrl',
['$rootScope', '$scope', '$state', '$ionicHistory', '$localStorage', '$ionicPopup', '$cordovaCalendar', '$cordovaInAppBrowser', 'ENDPOINT_LIST', '$ionicSideMenuDelegate',
function($rootScope, $scope, $state, $ionicHistory, $localStorage, $ionicPopup, $cordovaCalendar, $cordovaInAppBrowser, ENDPOINT_LIST, $ionicSideMenuDelegate) {

  $localStorage['broadcast'] = true;

  $scope.$on("$ionicView.beforeLeave", function(event, data){
    $scope.session.disconnect();
  });

  $scope.Properties = {
    title: $localStorage['name']
  }

  $scope.account = {
    name: $localStorage['name'],
    holdersName: $localStorage['special_moment']['holdersName'],
    eventName: $localStorage['special_moment']['name'],
    code: $localStorage['special_moment']['code'],
    longDate: $localStorage['special_moment']['date']['longDate'],
    longTime: $localStorage['special_moment']['date']['longTime'],
    computed: $localStorage['special_moment']['date']['computed'],
    giftlink: $localStorage['special_moment']['giftlink'],
    ondemand: $localStorage['special_moment']['ondemand'],
    public: ($localStorage['special_moment']['settings']['public']),
    chat: ($localStorage['special_moment']['settings']['chat']),
    gifts: ($localStorage['special_moment']['settings']['gifts'])
  }

  $scope.name = $scope.account.name;
  $scope.eventCode = $scope.account.code;

  $scope.icon_blue = '';
  $scope.icon_yellow = '';
  $scope.icon_fav = '';

  $scope.readyToEnd = false;

  $scope.publishing = false;

  $scope.clickHeartButton = function() {

    if ($scope.icon_blue == '') {
      $scope.icon_blue = 'active';
      $scope.icon_yellow = 'active';
    }
    else {
      $scope.icon_blue = '';
      $scope.icon_yellow = '';
    }

    if ($scope.icon_fav == 'active') {
      $scope.icon_fav = '';
    }
    else {
      $scope.icon_fav = 'active';
    }

    // if ($scope.readyToEnd == false){
    //   $rootScope.showPopup('Hold up!', 'By tapping this button you are about to end your special moment, please tap again to complete your moment.');
    //   $scope.readyToEnd = true;
    // }else{
    //   // $scope.completeWedding(); //Moment is actually completed, not left.
    // }
    // $ionicHistory.goBack();
    if (!$scope.publishing){
      if ($scope.role == 'publisher'){
        $scope.session.publish( $scope.publisher );
        TB.updateViews();
        $scope.publishing = true;
      }
    }else{
      $scope.session.unpublish($scope.publisher);
      TB.updateViews();
      $scope.publishing = false;
    }
  }

  $rootScope.showPopup = function(title, content){

    if (window.popupOK != undefined){
      window.popupOK();
    }

      $rootScope.alertPopup = $ionicPopup.alert({
          template: '' +
              '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
              '<h1>' + title + '</h1>' +
              '<p>' + content + '</p>' +
              '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
          cssClass: 'customPopup',
          scope: this
      });

      $rootScope.alertPopup.then(function(res) {
          console.log('alertPopup');
      });

      $rootScope.popupOK = function() {
          console.log('ok');
          $rootScope.alertPopup.close();
      }

      window.popupOK = $rootScope.popupOK;
  }

  
  keepscreenon.enable();

  $scope.apikey = $localStorage['apikey'];
  $scope.session_id = $localStorage['special_moment']['session_id'];
  $scope.token_id = $localStorage['special_moment']['token_id'];
  $scope.role = $localStorage['special_moment']['role'];


  $scope.session = TB.initSession( $scope.apikey, $scope.session_id );
  
  if ($scope.role != 'publisher'){
    $scope.session.on({
      'streamCreated': function( event ){
        var videoElement = document.getElementById('fullscreen_video');
        $scope.session.subscribe( event.stream, videoElement.id, {subscribeToAudio: true, height: '75vh', width: '100vw'} );
        TB.updateViews();
      },

      'streamDestroyed': function( event ){
        console.log('Special Moment has been interupted or ended.');
      }
    });
  }

  if (ionic.Platform.isAndroid()){
    cordova.plugins.diagnostic.requestRuntimePermission(function(status){
      switch(status){
          case cordova.plugins.diagnostic.runtimePermissionStatus.GRANTED:
              console.log("Permission granted to use the camera");
              $scope.publisher = TB.initPublisher($scope.apikey, 'fullscreen_video', {height: '100vh', width: '100vw', insertMode: 'replace'});
              TB.updateViews();
              break;
          case cordova.plugins.diagnostic.runtimePermissionStatus.NOT_REQUESTED:
              console.log("Permission to use the camera has not been requested yet");
              break;
          case cordova.plugins.diagnostic.runtimePermissionStatus.DENIED:
              console.log("Permission denied to use the camera - ask again?");
              $rootScope.showPopup('Permission Denied', 'We do not have permission to access your camera, please go into your settings on your phone and grant this app camera permissions.');
              $ionicHistory.goBack();
              break;
          case cordova.plugins.diagnostic.runtimePermissionStatus.DENIED_ALWAYS:
              console.log("Permission permanently denied to use the camera - guess we won't be using it then!");
              $rootScope.showPopup('Permission Denied', 'We do not have permission to access your camera, please go into your settings on your phone and grant this app camera permissions.');
              $ionicHistory.goBack();
              break;
      }
    }, function(error){
        console.error("The following error occurred: "+error);
    }, cordova.plugins.diagnostic.runtimePermission.CAMERA);
  }else{
    $scope.publisher = TB.initPublisher($scope.apikey, 'fullscreen_video', {height: '100vh', width: '100vw', insertMode: 'replace', cameraName: 'back'});
    TB.updateViews();
  }

  $scope.session.connect($scope.token_id, function(){
    console.log('Connected to Opentok services.');
    if ($scope.role == 'publisher'){
      console.log('Attmepting to publish to Opentok services.');
      
    }
  });

  $scope.goBack = function(){
    $scope.session.unpublish($scope.publisher);
    $ionicHistory.goBack();
  }


} // end of controller function
]
);
