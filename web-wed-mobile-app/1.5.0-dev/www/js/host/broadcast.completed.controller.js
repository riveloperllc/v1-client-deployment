angular.module('WebWedApp')

    .controller('HomeBroadcastCompletedCtrl',
        ['$scope', '$state', '$ionicHistory', 'ENDPOINT_LIST', 'AuthService', '$ionicPopup', '$http', '$localStorage',
            function($scope, $state, $ionicHistory, ENDPOINT_LIST, AuthService, $ionicPopup, $http, $localStorage) {

                $scope.showPopup = function(title, content){
                    $scope.alertPopup = $ionicPopup.alert({
                        template: '' +
                            '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
                            '<h1>' + title + '</h1>' +
                            '<p>' + content + '</p>' +
                            '<button class="button button-full button-positive" ng-click="popupOK()">OK</button>',
                        cssClass: 'customPopup',
                        scope: this

                    });

                    $scope.alertPopup.then(function(res) {
                        console.log('alertPopup');
                    });

                    $scope.popupOK = function() {
                        console.log('ok');
                        $scope.alertPopup.close();
                    }

                    window.popupOK = $scope.popupOK;
                }

            } // end of controller function
        ]
    );
