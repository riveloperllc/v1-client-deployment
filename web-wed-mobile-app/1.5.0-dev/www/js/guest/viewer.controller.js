angular.module('WebWedApp')

    .controller('GuestViewerCtrl',
        ['$rootScope', '$scope', '$state', '$ionicHistory', '$cordovaSocialSharing', '$localStorage', '$ionicPopup', '$cordovaCalendar', '$cordovaInAppBrowser', 'ENDPOINT_LIST', '$ionicSideMenuDelegate', '$ionicModal',
            function($rootScope, $scope, $state, $ionicHistory, $cordovaSocialSharing, $localStorage, $ionicPopup, $cordovaCalendar, $cordovaInAppBrowser, ENDPOINT_LIST, $ionicSideMenuDelegate, $ionicModal) {

                $ionicHistory.nextViewOptions({
                    disableAnimate: false
                });

                $scope.messages = {
                    text: ''
                };

                $localStorage['broadcast'] = true;
                $ionicSideMenuDelegate.canDragContent(false);

                // Clear history
                $ionicHistory.clearHistory();

                $rootScope.showPopup = function(title, content){

                  if (window.popupOK != undefined){
                    window.popupOK();
                  }

                    $rootScope.alertPopup = $ionicPopup.alert({
                        template: '' +
                            '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
                            '<h1>' + title + '</h1>' +
                            '<p>' + content + '</p>' +
                            '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
                        cssClass: 'customPopup',
                        scope: this
                    });

                    $rootScope.alertPopup.then(function(res) {
                        console.log('alertPopup');
                    });

                    $rootScope.popupOK = function() {
                        console.log('ok');
                        $rootScope.alertPopup.close();
                    }

                    window.popupOK = $rootScope.popupOK;
                }

                $scope.Properties = {
                    title: $localStorage['name']
                }

                $scope.account = {
                    name: $localStorage['name'],
                    yours: $localStorage['wedding']['yours1']['name'],
                    yours2: $localStorage['wedding']['yours2']['name'],
                    officiant: $localStorage['wedding']['officiant'],
                    witness1: $localStorage['wedding']['witness']['name'],
                    witness2: $localStorage['wedding']['witness2']['name'],
                    longDate: $localStorage['wedding']['date']['longDate'],
                    longTime: $localStorage['wedding']['date']['longTime'],
                    code: $localStorage['wedding']['code'],
                    public: ($localStorage['wedding']['settings']['public']),
                    chat: ($localStorage['wedding']['settings']['chat']),
                    gifts: ($localStorage['wedding']['settings']['gifts']),
                    giftlink: ($localStorage['wedding']['giftlink'])
                }

                if ($scope.account.public){ $scope.account.public = false; }else{ $scope.account.public = true; }
                if ($scope.account.chat){ $scope.account.chat = false; }else{ $scope.account.chat = true; }
                if ($scope.account.gifts){ $scope.account.gifts = false; if ($localStorage['wedding']['giftlink'] == ''){ $scope.account.gifts = true; } }else{ $scope.account.gifts = true; }

                $scope.icon_blue = '';
                $scope.icon_yellow = '';
                $scope.icon_fav = '';

                $scope.clickHeartButton = function() {

                    if ($scope.icon_blue == '') {
                        $scope.icon_blue = 'active';
                        $scope.icon_yellow = 'active';
                    }
                    else {
                        $scope.icon_blue = '';
                        $scope.icon_yellow = '';
                    }

                    if ($scope.icon_fav == 'active') {
                        $scope.icon_fav = '';
                    }
                    else {
                        $scope.icon_fav = 'active';
                    }
                }

                $scope.name = $localStorage['name'];
                $scope.token = $localStorage['token'];
                $scope.weddingCode = $localStorage['wedding']['code'];



                  $scope.goGift = function(){
                    var options = {
                      location: 'no',
                      clearcache: 'yes',
                      toolbar: 'yes'
                    };

                    if ($scope.account.giftlink != ''){
                      $cordovaInAppBrowser.open($scope.account.giftlink, '_blank', options).then(
                        function(event) {
                          // success
                        }
                      ).catch(
                        function(event) {
                          // error
                        }
                      );
                    }
                  };

                $scope.addToCalendar = function(){
                    var title = '';
                    if ($scope.Properties.active == 'wedding'){
                      $scope.notes = $scope.account.yours + " & " + $scope.account.yours2 + " is getting married using WebWedMobile.";
                      title = 'Web Wed Marriage';
                    }else{
                      $scope.notes = 'WebWedMobile Special Moment scheduled.';
                      title = 'Web Wed Special Moments';
                    }
                    var MS_PER_MINUTE = 60000;
                    var startDate = new Date($scope.account.computed);
                    if ($scope.Properties.active == 'wedding'){
                      var endDate = new Date(startDate.getTime() + 55 * MS_PER_MINUTE);
                    }else{
                      var endDate = new Date(startDate.getTime() + 30 * MS_PER_MINUTE);
                    }

                    $cordovaCalendar.createEvent({
                      title: title,
                      location: 'WebWedMobile App',
                      notes: $scope.notes,
                      startDate: startDate,
                      endDate: endDate
                    }).then(function (result) {
                      $rootScope.showPopup('Awesome!', 'We have added this event to your event calendar.');
                    }, function (err) {
                      $rootScope.showPopup('Hmm', 'We could not add this event to your calendar, please try again later.');
                    });
                  };

                $scope.share = function(){
                    if ($scope.Properties.active == 'wedding'){
                      var shareText = $scope.account.yours + ' and ' + $scope.account.yours2 + ' are tying the knot. Join in for their live  wedding ' + $scope.account.longDate + ' at ' + $scope.account.longTime + '. ' + $scope.account.ceremonylink;
                    }else{
                      var shareText = 'A WebWedMobile Special Moment is taking place. Tune into this special moment by following this link ' + $scope.account.ceremonylink;
                    }
                    $cordovaSocialSharing
                    .share(shareText) // Share via native share sheet
                    .then(function(result) {
                      //$rootScope.showPopup('Yay!', 'We have successfully shared your ceremony link!');
                    }, function(err) {
                      $rootScope.showPopup('Hmm', 'We were unable to share your ceremony link.');
                    });
                  }

                $scope.addToCalendarAction = false;

                  if ($scope.Properties.active == 'wedding' || ($scope.Properties.active == 'special_moment' && $scope.account.ondemand == true)){
                    var MS_PER_MINUTE = 60000;
                    var weddingTime = Date.parse($scope.account.computed);
                    var thirtyMinutesAfterWeddingStartTime = new Date(weddingTime + 30 * MS_PER_MINUTE).getTime();
                    var currentTime = Date.now();

                    if (currentTime >= thirtyMinutesAfterWeddingStartTime){
                      $scope.addToCalendarAction = true;
                    }
                  }
                

                $scope.goBack = function(){
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true
                    });
                    for (var i = 0; i < $scope.subscribers.length; i++){
                      var subscriber = $scope.subscribers[i];
                      $scope.session.unsubscribe(subscriber);
                    }
                    $scope.session.disconnect();
                    $state.go('app.home-guest');
                };

                $scope.name = $localStorage['name'];
                $scope.token = $localStorage['token'];
                $scope.role = $localStorage['wedding']['role'];
                $scope.weddingCode = $localStorage['wedding']['code'];
                $scope.typeOfConnection = "presenter";

                if ($scope.role == "yours" || $scope.role == "yours2" || $scope.role == "official"){
                  $scope.typeOfConnection = "presenter";
                }else if ($scope.role == "witness"){
                  $scope.typeOfConnection = "viewer";
                }else{
                  $scope.typeOfConnection = "viewer";
                }

                console.log($scope.role);

                jQuery.loadScript = function(url, callback){
                  jQuery.ajax({
                    url: url,
                    dataType: 'script',
                    success: callback,
                    async: true
                  });
                }

                keepscreenon.enable();
                  

                  $scope.positions = {
                    your: false,
                    yours: false,
                    witness1: false,
                    witness2: false,
                    officiant: false
                  };

                  $scope.handlers = {
                        'streamCreated': function( event ){
                          var div = $scope.getSubscriberElement(event);

                          if (($scope.role == 'guest' && event.stream.connection.data == 'officiant') || (event.stream.connection.data == 'witness1') || (event.stream.connection.data == 'witness2')){
                            if(event.stream.connection.data == 'officiant'){
                              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '33vh', width: '33vw'} );
                            }else{
                              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '45vh', width: '50vw'} );
                            }
                          }else{
                            if(event.stream.connection.data == 'yours'){
                              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '55vh', width: '50vw'} ); 
                            }else{
                              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '55vh', width: '50vw'} ); 
                            }
                          }

                          $scope.subscribers.push(subscriber);

                          TB.updateViews();
                        },

                        'streamDestroyed': function( event ){
                          window.e = event;
                          console.log(event.stream.connection.data);
                          if (event.stream.connection.data == 'yours'){
                            $('#videoContainer2').append('<div id="yourVideoElement2"></div>');
                          }else if (event.stream.connection.data == 'yours2'){
                            $('#videoContainer2').append('<div id="yoursVideoElement2"></div>');
                          }else if (event.stream.connection.data == 'witness1'){
                            $('#bottombar2').append('<div id="witnessVideoElement2"></div>');
                          }else if (event.stream.connection.data == 'witness2'){
                            $('#bottombar2').append('<div id="witness2VideoElement2"></div>');
                          }else if (event.stream.connection.data == 'officiant'){
                            $('#bottombar2').append('<div id="officialVideoElement2"></div>');
                            // $state.go('app.home-broadcast-completed');
                            // $scope.session.disconnect();
                          }

                          if (!$scope.positions.your || !$scope.positions.yours || !$scope.positions.witness1 || !$scope.positions.witness2 || !$scope.positions.officiant){
                            document.getElementById('beatingheart').style.display = 'block';
                            document.getElementById('beatingheart').style.position = 'absolute';
                            // document.getElementById('beatingheart').style.display = 'block';
                          }

                          console.log('destroyed');
                        }
                      };

                    $scope.getPublisherElement = function(){
                      var id = 'yourVideoElement2';
                      if ($scope.role == 'yours'){
                        $scope.positions.your = true;
                      }else if ($scope.role == 'yours2'){
                        id = 'yoursVideoElement2';
                        $scope.positions.yours = true;
                      }else if ($scope.role == 'witness1'){
                        id = 'witnessVideoElement2';
                        $scope.positions.witness1 = true;
                      }else if ($scope.role == 'witness2'){
                        id = 'witness2VideoElement2';
                        $scope.positions.witness2 = true;
                      }else if ($scope.role == 'officiant'){
                        id = 'officialVideoElement2';
                        $scope.positions.officiant = true;
                      }
                      return id;
                    };

                    $scope.getPublisherToken = function(){
                      return $localStorage['wedding']['token_id'];
                    };

                    $scope.getSubscriberElement = function(subscriber){
                      window.e = subscriber;
                      var event = subscriber;
                      if (event.stream.connection.data == 'yours'){
                        //Bride
                        var div = document.getElementById('yourVideoElement2');
                        $scope.positions.your = true;
                      }else if (event.stream.connection.data == 'yours2'){
                        //Groom
                        var div = document.getElementById('yoursVideoElement2');
                        $scope.positions.yours = true;
                      }else if (event.stream.connection.data == 'witness1'){
                        //Witness1
                        var div = document.getElementById('witnessVideoElement2');
                        $scope.positions.witness1 = true;
                      }else if (event.stream.connection.data == 'witness2'){
                        //Witness2
                        var div = document.getElementById('witness2VideoElement2');
                        $scope.positions.witness2 = true;
                      }else if (event.stream.connection.data == 'officiant'){
                        //Officiant
                        var div = document.getElementById('officialVideoElement2');
                        $scope.positions.officiant = true;
                      }else{
                        //Apple TV
                      }

                      return div;
                    };

                    var apiKey = $localStorage['apikey']; // INSERT YOUR API Key

                      //secret d2329b40d5b69006701f047d2b7b83814a5c5676

                    var sessionId = $localStorage['wedding']['session_id']; // INSERT YOUR SESSION ID
                      
                      

                      // window.s = [];
                      $scope.session = TB.initSession( apiKey, sessionId ); 
                      $scope.session.on('signal:weddingCompleted', function(event){
                        for (var i = 0; i < $scope.subscribers.length; i++){
                          var subscriber = $scope.subscribers[i];
                          $scope.session.unsubscribe(subscriber);
                        }
                        $scope.session.disconnect();
                        $state.go('app.home-broadcast-completed');
                      });

                      $scope.subscribers = [];

                      $scope.session.on($scope.handlers);

                    $scope.session.connect($scope.getPublisherToken(), function(){ });

                      if (!ionic.Platform.isIPad()){
                        document.getElementById('beatingheart').style.position = 'absolute';
                        document.getElementById('beatingheart').style.top = '10%';
                      }

            } // end of controller function
        ]
    );
