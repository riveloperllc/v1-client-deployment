angular.module('WebWedApp')

.controller('ContactCtrl', ['$scope', '$state', '$ionicHistory', 'ENDPOINT_LIST', '$ionicPopup', '$http', '$localStorage',
function($scope, $state, $ionicHistory, ENDPOINT_LIST, $ionicPopup, $http, $localStorage) {

  if (window.socket != null){
    window.socket.disconnect();
  }

  $scope.contact = {
    name: '',
    email: '',
    message: ''
  };

  if ($localStorage['token'] != null && $localStorage['token'] != ''){
    $scope.route = 'app';
  }else{
    $scope.route = 'login';
  }

  $scope.goLegal = function() {
    if ($localStorage['token'] != null){
      window.location.href = '#/app/legal';
    }else{
      window.location.href = '#/login/legal';
    }
  };

  $scope.showContactError = function() {
    $scope.alertPopup = $ionicPopup.alert({
      template: '' +
      '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
      '<h1>Oh No!</h1>' +
      '<p>It seems an error occured please try again later!</p>' +
      '<button class="button button-full button-positive" ng-click="popupOK()">OK</button>',
      cssClass: 'customPopup',
      scope: this

    });

    $scope.alertPopup.then(function(res) {
      console.log('alertPopup');
    });

    $scope.popupOK = function() {
      console.log('ok');
      $scope.alertPopup.close();
    }

    window.popupOK = $scope.popupOK;
  }

  $scope.showContactSuccess = function() {
    $scope.alertPopup = $ionicPopup.alert({
      template: '' +
      '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
      '<h1>Awesome!</h1>' +
      '<p>The WebWed Team has been notified and will get back with you shortly.</p>' +
      '<button class="button button-full button-positive" ng-click="popupOK()">OK</button>',
      cssClass: 'customPopup',
      scope: this

    });

    $scope.alertPopup.then(function(res) {
      console.log('alertPopup');
    });

    $scope.popupOK = function() {
      console.log('ok');
      $scope.alertPopup.close();
    }
  }

  $scope.doSubmit = function() {
    var apiURL = ENDPOINT_LIST.CONTACT_API;
    var postData = 'token=' + $localStorage['token'] + '&' + 'name=' + encodeURIComponent($scope.contact.name) + '&' + 'email=' + encodeURIComponent($scope.contact.email) + '&' + 'message=' + encodeURIComponent($scope.contact.message);

    var config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      }
    }

    $http.post(apiURL, postData, config).success(function(response){
      console.log(response);
      if (response['success'] == undefined){
        $scope.showContactError();
      }else{
        $scope.showContactSuccess();
      }
    }).error(function(error){
      $scope.showContactError();
    });
  }
} // end of controller function
]);
