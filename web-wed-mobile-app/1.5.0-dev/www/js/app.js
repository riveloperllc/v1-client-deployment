angular.module('WebWedApp', ['ionic', 'ngCordova', 'ngStorage', 'WebWedApp.constant'])

    .run(function($rootScope, $ionicPlatform, $ionicModal, $cordovaNetwork, $localStorage, $state, ENDPOINT_LIST, $cordovaInAppBrowser, $ionicPopup) {

        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)

            // if (ionic.Platform.platform() == 'iOS' || ionic.Platform.platform() == 'android'){
              if (navigator.splashscreen != undefined){
                navigator.splashscreen.hide();
              }

            if (window.cordova && window.cordova.plugins.Keyboard) {
                window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                window.cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                // window.StatusBar.hide();
            }

            window.addEventListener('native.keyboardshow', keyboardShowHandler);

            function keyboardShowHandler(e){
              if (!ionic.Platform.isAndroid()){
                $('.login-view').css('top', '-125px');
                $('.contact-view').css('top', '-125px');
              }
            }

            window.addEventListener('native.keyboardhide', keyboardHideHandler);

            function keyboardHideHandler(e){
              if (!ionic.Platform.isAndroid()){
                $('.login-view').css('top', '0px');
                $('.contact-view').css('top', '0px');
              }
            }

            var googleanalyticsApp = angular.module('googleanalytics', ['ionic'])
                .run(function($ionicPlatform, $ionicPopup) {
                    $ionicPlatform.ready(function() {
                        if(typeof analytics !== undefined) {
                            analytics.startTrackerWithId("UA-74133839-1");
                        } else {
                            console.log("Google Analytics Unavailable");
                        }
                    });
                });

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Check first time open
            //
            // /////////////////////////////////////////////////////////////////////////////////////////////////////////
            var storage = $localStorage.$default({
                first_run: true
            });

            window.open = cordova.InAppBrowser.open;


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Dialog : Unsupported Device
            //
            // /////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ionicModal.fromTemplateUrl('templates/unsupported/unsupported-device.html', {
                scope: $rootScope
            }).then(function(modal) {
                $rootScope.unsupportedDeviceModal = modal;
            });

            $rootScope.showUnsupportedDevice = function() {
                $rootScope.unsupportedDeviceModal.show();
            }

            $rootScope.hideUnsupportedDevice = function() {
                $rootScope.unsupportedDeviceModal.hide();
            }

            if (document.height < 568){
              setTimeout(function(){
                $rootScope.showUnsupportedDevice();
              }, 500);
            }else{
              if (storage.first_run) {
                  $localStorage['first_run'] = false;
                  $state.go('intro');
              }
              else {
                  $state.go('login.startup');
              }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Dialog : No Internet Connection
            //
            // /////////////////////////////////////////////////////////////////////////////////////////////////////////

            // No Internet Connection dialog
            $ionicModal.fromTemplateUrl('templates/no-connection/no-connection.html', {
                scope: $rootScope
            }).then(function(modal) {
                $rootScope.noConnectionModal = modal;
            });

            $rootScope.showNoConnectionDialog = function() {
                $('video').hide();
                if (ionic.Platform.isIOS()){
                  cordova.plugins.iosrtc.refreshVideos();
                }
                $rootScope.noConnectionModal.show();
            }

            $rootScope.hideNoConnectionDialog = function() {
                $('video').show();
                if (ionic.Platform.isIOS()){
                  cordova.plugins.iosrtc.refreshVideos();
                }
                $rootScope.noConnectionModal.hide();
            }

            $rootScope.version = ENDPOINT_LIST.VERSION;
            $rootScope.currentYear = ((new Date()).getFullYear());

            $rootScope.showPopup = function(title, content){
                $rootScope.alertPopup = $ionicPopup.alert({
                    template: '' +
                        '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
                        '<h1>' + title + '</h1>' +
                        '<p>' + content + '</p>' +
                        '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
                    cssClass: 'customPopup',
                    scope: this
                });

                $rootScope.alertPopup.then(function(res) {
                    console.log('alertPopup');
                });

                $rootScope.popupOK = function() {
                    console.log('ok');
                    $rootScope.alertPopup.close();
                }

                window.popupOK = $rootScope.popupOK;
            }

            $rootScope.showPopupWithCallback = function(title, content, callback){
                $rootScope.alertPopup = $ionicPopup.alert({
                    template: '' +
                        '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
                        '<h1>' + title + '</h1>' +
                        '<p>' + content + '</p>' +
                        '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
                    cssClass: 'customPopup',
                    scope: this
                });

                $rootScope.alertPopup.then(function(res) {
                    console.log('alertPopup');
                });

                $rootScope.popupOK = function() {
                    console.log('ok');
                    $rootScope.alertPopup.close();
                    callback();
                }

                window.popupOK = $rootScope.popupOK;
            }

            $rootScope.needAccount = function(){
              var options = {
                  location: 'no',
                  clearcache: 'yes',
                  toolbar: 'yes'
              };

              var ref = window.open(ENDPOINT_LIST.SIGNUP_PAGE, '_blank', 'location=no');

              ref.addEventListener('loadstop', function(event) {
                if (event.url.indexOf('login') > -1){
                  ref.close();
                  $rootScope.showPopup('Awesome', 'If you have completed the sign up process you may now login using the email and password you provided.');
                }
              } );
            }

            $rootScope.watchDemo = function(){
              var options = {
                  location: 'no',
                  clearcache: 'yes',
                  toolbar: 'yes'
              };

              $cordovaInAppBrowser.open(ENDPOINT_LIST.WATCH_DEMO_VIDEO, '_blank', options).then(
                  function(event) {
                      // success
                  }
              ).catch(
                  function(event) {
                      // error
                      $rootScope.showPopup('Hmm', 'An error occurred, please try again later.');
                  }
              );
            }

            $rootScope.openSystemBrowser = function(url){
              var options = {
                  location: 'no',
                  clearcache: 'yes',
                  toolbar: 'yes'
              };

              $cordovaInAppBrowser.open(url, '_system', options).then(
                  function(event) {
                      // success
                  }
              ).catch(
                  function(event) {
                      // error
                      $rootScope.showPopup('Hmm', 'An error occurred, please try again later.');
                  }
              );
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Check internet connection status
            //
            // /////////////////////////////////////////////////////////////////////////////////////////////////////////

            // listen for Online event
            $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                console.log('cordovaNetwork:online => ' + networkState);

                $rootScope.hideNoConnectionDialog();
            });

            // listen for Offline event
            $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                console.log('cordovaNetwork:offline => ' + networkState);

                $rootScope.showNoConnectionDialog();
            });

            $rootScope.getParameterByName = function(name, url){
              if (!url) url = window.location.href;
              name = name.replace(/[\[\]]/g, "\\$&");
              var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                  results = regex.exec(url);
              if (!results) return null;
              if (!results[2]) return '';
              return decodeURIComponent(results[2].replace(/\+/g, " "));
            }

            $rootScope.handleOpenURL = function(url){
              setTimeout(function(){
                var code = $rootScope.getParameterByName('code', url);

                if (code != '' && code != null){
                  $localStorage['code'] = code;
                  $state.go('login.invited-guest');
                }
              }, 0);
            };

            window.handleOpenURL = $rootScope.handleOpenURL;

            universalLinks.subscribe(null, function (eventData) {
              console.log('Did launch application from the link: ' + eventData.url);
              window.handleOpenURL(eventData.url);
            });
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

        $ionicConfigProvider.backButton.text('');
        $ionicConfigProvider.backButton.previousTitleText(false);
        $ionicConfigProvider.views.swipeBackEnabled(false);
        //$ionicConfigProvider.scrolling.jsScrolling(true);

        $stateProvider

            .state('intro', {
                url: '/intro',
                templateUrl: 'templates/intro/intro.html',
                controller: 'IntroCtrl',
                cache: false
            })

            .state('unsupported-device', {
                url: '/unsupported',
                templateUrl: 'templates/unsupported/unsupported-device.html',
                cache: false
            })

            .state('login', {
                url: '/login',
                abstract: true,
                templateUrl: 'templates/sidemenu/login_menu.html',
                controller: 'AppLoggedOutCtrl',
                cache: false
            })

            .state('login.startup', {
                url: '/startup',
                views: {
                    'menuContent': {
                      templateUrl: 'templates/auth/login.startup.html',
                      controller: 'LoginStartupCtrl'
                    }
                },
                cache: false
            })

            .state('login.account-choice', {
                url: '/account-choice',
                views: {
                    'menuContent': {
                      templateUrl: 'templates/auth/login.account-choice.html',
                      controller: 'LoginChoiceCtrl'
                    }
                },
                cache: false
            })

            .state('login.has-account', {
                url: '/has-account',
                views: {
                    'menuContent': {
                      templateUrl: 'templates/auth/login.hasaccount.html',
                      controller: 'LoginHasAccountCtrl'
                    }
                },
                cache: false
            })

            .state('login.invited-guest', {
                url: '/invited-guest',
                views: {
                    'menuContent': {
                      templateUrl: 'templates/auth/login.invited-guest.html',
                      controller: 'LoginInvitedGuestCtrl'
                    }
                },
                cache: false
            })

            .state('login.about', {
                url: '/about',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/about/about.html'
                    }
                },
                cache: false
            })

            .state('login.legal', {
                url: '/legal',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/legal/legal.html'
                    }
                },
                cache: false
            })

            .state('login.contact', {
                url: '/contact',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/contact/contact.html',
                        controller: 'ContactCtrl'
                    }
                },
                cache: false
            })

            .state('login.terms-of-service', {
                url: '/terms-of-service/:navButton',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/legal/terms-of-service.html',
                        controller: 'TermsOfServiceCtrl',
                    }
                },
                cache: false
            })

            .state('login.privacy-policy', {
                url: '/privacy-policy/:navButton',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/legal/privacy-policy.html',
                        controller: 'PrivacyPolicyCtrl',
                    }
                },
                cache: false
            })

            .state('terms-of-service', {
                url: '/terms-of-service/:navButton',
                templateUrl: 'templates/legal/terms-of-service.html',
                controller: 'TermsOfServiceCtrl',
                cache: false
            })

            .state('privacy-policy', {
                url: '/privacy-policy/:navButton',
                templateUrl: 'templates/legal/privacy-policy.html',
                controller: 'PrivacyPolicyCtrl',
                cache: false
            })

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/sidemenu/menu.html',
                controller: 'AppCtrl',
                cache: false
            })

            .state('app.terms-of-service', {
                url: '/terms-of-service/:navButton',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/legal/terms-of-service.html',
                        controller: 'TermsOfServiceCtrl'
                    }
                },
                cache: false
            })

            .state('app.privacy-policy', {
                url: '/privacy-policy/:navButton',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/legal/privacy-policy.html',
                        controller: 'PrivacyPolicyCtrl'
                    }
                },
                cache: false
            })

            .state('app.about', {
                url: '/about',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/about/about.html'
                    }
                },
                cache: false
            })

            .state('app.legal', {
                url: '/legal',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/legal/legal.html'
                    }
                },
                cache: false
            })

            .state('app.account', {
                url: '/account',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/account/account.html',
                        controller: 'AccountCtrl'
                    }
                },
                cache: false
            })

            .state('app.contact', {
                url: '/contact',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/contact/contact.html',
                        controller: 'ContactCtrl'
                    }
                },
                cache: false
            })

            .state('app.home-guest', {
                url: '/home-guest',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.guest.html',
                        controller: 'HomeGuestCtrl'
                    }
                }
            })

            .state('app.home-host', {
                url: '/home-host',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.host.html',
                        controller: 'HomeHostCtrl'
                    }
                }
            })

            .state('app.home-broadcast', {
                url: '/home-broadcast',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.broadcast.html',
                        controller: 'WeddingsBroadcastCtrl'
                    }
                }
            })

            .state('app.home-broadcast-special-moments', {
                url: '/home-broadcast-special-moments',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.special-moments.broadcast.html',
                        controller: 'SpecialMomentsBroadcastCtrl'
                    }
                }
            })

            .state('app.home-viewer', {
                url: '/home-viewer',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.viewer.html',
                        controller: 'GuestViewerCtrl'
                    }
                }
            })

            .state('app.home-special_moments-viewer', {
                url: '/home-special_moments-viewer',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.special-moments.viewer.html',
                        controller: 'SpecialMomentsGuestViewerCtrl'
                    }
                }
            })

            .state('app.home-broadcast-completed', {
                url: '/home-broadcast-completed',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.broadcast.completed.html',
                        controller: 'HomeBroadcastCompletedCtrl'
                    }
                }
            })

        ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/intro');
    });
