angular.module('WebWedApp')

.controller('AccountCtrl',
['$rootScope', '$scope', '$state', '$ionicHistory', '$localStorage', '$http', '$ionicPopup', 'ENDPOINT_LIST', '$cordovaEmailComposer', '$cordovaSocialSharing',
function($rootScope, $scope, $state, $ionicHistory, $localStorage, $http, $ionicPopup, ENDPOINT_LIST, $cordovaEmailComposer, $cordovaSocialSharing) {

  if (window.socket != null){
    window.socket.disconnect();
  }

  // Clear history
  //$ionicHistory.clearHistory();

  $scope.Properties = {
    active: $localStorage['active']
  };

  if ($localStorage['active'] == 'wedding'){
    $scope.account = {
      name: $localStorage['name'],
      yours: $localStorage['wedding']['yours1']['name'],
      yours2: $localStorage['wedding']['yours2']['name'],
      officiant: $localStorage['wedding']['officiant']['name'],
      witness1: $localStorage['wedding']['witness']['name'],
      witness2: $localStorage['wedding']['witness2']['name'],
      longDate: $localStorage['wedding']['date']['longDate'],
      longTime: $localStorage['wedding']['date']['longTime'],
      computed: $localStorage['wedding']['date']['computed'],
      giftlink: $localStorage['wedding']['giftlink'],
      ceremonylink: $localStorage['wedding']['ceremonylink'],
      code: $localStorage['wedding']['code'],
      public: ($localStorage['wedding']['settings']['public']),
      chat: ($localStorage['wedding']['settings']['chat']),
      gifts: ($localStorage['wedding']['settings']['gifts']),
      ondemand: false
    }
  }else{
    $scope.account = {
      name: $localStorage['name'],
      eventName: $localStorage['special_moment']['name'],
      code: $localStorage['special_moment']['code'],
      longDate: $localStorage['special_moment']['date']['longDate'],
      longTime: $localStorage['special_moment']['date']['longTime'],
      computed: $localStorage['special_moment']['date']['computed'],
      giftlink: $localStorage['special_moment']['giftlink'],
      ondemand: $localStorage['special_moment']['ondemand'],
      public: ($localStorage['special_moment']['settings']['public']),
      chat: ($localStorage['special_moment']['settings']['chat']),
      gifts: ($localStorage['special_moment']['settings']['gifts'])
    }
  }

  $scope.disableElements = false;

  if ($localStorage['active'] == 'wedding' && ($scope.account.role == 'witness' || $scope.account.role == 'witness2' || $scope.account.role == 'official')){

    $scope.disableElements = true;

    $('input[ng-model="setting.giftlink"]').attr('disabled', 'disabled');
  }

  $scope.loginMode = $localStorage['loginMode'];

  $scope.setting = {
    agree: false,
    email: $localStorage['email'],
    password: "",
    confirm: "",
    giftlink: $scope.account.giftlink,
    public: $scope.account.public,
    chat: $scope.account.chat,
    gifts: $scope.account.gifts
  }

  $scope.publicBroadcast = Boolean($scope.setting.public);
  $scope.allowChat = Boolean($scope.setting.chat);
  $scope.acceptGifts = Boolean($scope.setting.gifts);

  $rootScope.showPopup = function(title, content){

    if (window.popupOK != undefined){
      window.popupOK();
    }

      $rootScope.alertPopup = $ionicPopup.alert({
          template: '' +
              '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
              '<h1>' + title + '</h1>' +
              '<p>' + content + '</p>' +
              '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
          cssClass: 'customPopup',
          scope: this
      });

      $rootScope.alertPopup.then(function(res) {
          console.log('alertPopup');
      });

      $rootScope.popupOK = function() {
          console.log('ok');
          $rootScope.alertPopup.close();
      }

      window.popupOK = $rootScope.popupOK;
  }

  $scope.update = function(){
    $scope.publicBroadcastValue2 = document.getElementById('publicBroadcast').checked;
    $scope.allowChatValue2 = document.getElementById('allowChat').checked;
    $scope.acceptGiftsValue2 = document.getElementById('acceptGifts').checked;

    if ($localStorage['active'] == 'wedding'){
      $localStorage['wedding']['settings']['public'] = $scope.publicBroadcastValue2;
      $localStorage['wedding']['settings']['chat'] = $scope.allowChatValue2;
      $localStorage['wedding']['settings']['gifts'] = $scope.acceptGiftsValue2;
    }else{
      $localStorage['special_moment']['settings']['public'] = $scope.publicBroadcastValue2;
      $localStorage['special_moment']['settings']['chat'] = $scope.allowChatValue2;
      $localStorage['special_moment']['settings']['gifts'] = $scope.acceptGiftsValue2;
    }

    if ($scope.publicBroadcastValue2){
      $scope.publicBroadcastValue2 = 1;
    }else{
      $scope.publicBroadcastValue2 = 0;
    }

    if ($scope.allowChatValue2){
      $scope.allowChatValue2 = 1;
    }else{
      $scope.allowChatValue2 = 0;
    }

    if ($scope.acceptGiftsValue2){
      $scope.acceptGiftsValue2 = 1;
    }else{
      $scope.acceptGiftsValue2 = 0;
    }

    if ($scope.setting.password != $scope.setting.confirm){
      $rootScope.showPopup('Oh no!', 'Please double check that your passwords match and try again!');
      return;
    }

    var apiURL = ENDPOINT_LIST.UPDATE_API;
    var postData = 'type=updatesettings' + '&' +
    'email=' + encodeURIComponent($scope.setting.email) + '&' +
    'password=' + encodeURIComponent($scope.setting.password) + '&' +
    'confirm=' + encodeURIComponent($scope.setting.confirm) + '&' +
    'weddingregistrylink=' + encodeURIComponent($scope.setting.giftlink) + '&' +
    'publicbroadcast=' + encodeURIComponent($scope.publicBroadcastValue2) + '&' +
    'acceptgifts=' + encodeURIComponent($scope.acceptGiftsValue2) + '&' +
    'allowchat=' + encodeURIComponent($scope.allowChatValue2) + '&' +
    'token=' + encodeURIComponent($localStorage['token']);


    console.log(postData);

    var config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      }
    }
    if ($scope.setting.agree){
      if ($localStorage['active'] == 'wedding'){
        $http.post(apiURL, postData, config).success(function(response){
          $localStorage['wedding']['yours1']['name'] = $scope.account.yours;
          $localStorage['wedding']['yours2']['name'] = $scope.account.yours2;
          $localStorage['wedding']['officiant']['name'] = $scope.account.officiant;
          $localStorage['wedding']['witness']['name'] = $scope.account.witness1;
          $localStorage['wedding']['witness2']['name'] = $scope.account.witness2;
          $localStorage['wedding']['date']['longDate'] = $scope.account.longDate;
          $localStorage['wedding']['date']['longTime'] = $scope.account.longTime;
          $localStorage['wedding']['ceremonylink'] = $scope.account.ceremonylink;
          $localStorage['password'] = $scope.setting.password;
          $localStorage['wedding']['settings']['public'] = $scope.publicBroadcastValue2;
          $localStorage['wedding']['settings']['chat'] = $scope.allowChatValue2;
          $localStorage['wedding']['settings']['gifts'] = $scope.allowGiftsValue2;
          $rootScope.showPopup('Yay!', 'We have updated your settings successfully.');
        }).error(function(error){
          $rootScope.showPopup('Hmm', 'An error occurred when updating your settings, please try again later.');
        });
      }else{
        $http.post(apiURL, postData, config).success(function(response){
          console.log(response);
          $localStorage['special_moment']['name'] = $scope.account.eventName;
          $localStorage['special_moment']['code'] = $scope.account.code;
          $localStorage['special_moment']['date']['longDate'] = $scope.account.longDate;
          $localStorage['special_moment']['date']['longTime'] = $scope.account.longTime;
          $localStorage['password'] = $scope.setting.password;
          $localStorage['special_moment']['settings']['public'] = $scope.publicBroadcastValue2;
          $localStorage['special_moment']['settings']['chat'] = $scope.allowChatValue2;
          $localStorage['special_moment']['settings']['gifts'] = $scope.allowGiftsValue2;
          $rootScope.showPopup('Yay!', 'We have updated your settings successfully.');
        }).error(function(error){
          $rootScope.showPopup('Hmm', 'An error occurred when updating your settings, please try again later.');
        });
      }
    }else{
      $rootScope.showPopup('Hmm', 'You must agree to the above user agreement and try again.');
    }
  }

  $scope.share = function(){
    if ($localStorage['active'] == 'wedding'){
      var shareText = $localStorage['wedding']['yours1']['name'] + ' and ' + $localStorage['wedding']['yours2']['name'] + ' are tying the knot. Join in for their live  wedding ' + $localStorage['wedding']['date']['longDate'] + ' at ' + $localStorage['wedding']['date']['longTime'] + '. ' + $localStorage['wedding']['ceremonylink'];
    }else{
      var shareText = "A WebWedMobile Special Moment is taking place, join in now!";
    }
    $cordovaSocialSharing
    .share(shareText) // Share via native share sheet
    .then(function(result) {
      //$rootScope.showPopup('Yay!', 'We have successfully shared your ceremony link!');
    }, function(err) {
      $rootScope.showPopup('Hmm', 'We were unable to share your ceremony link.');
    });
  }

} // end of controller function
]
);
