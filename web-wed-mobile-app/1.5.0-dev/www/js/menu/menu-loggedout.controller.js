angular.module('WebWedApp')

.controller('AppLoggedOutCtrl',
['$scope', '$state', '$rootScope', '$ionicHistory', 'AuthService', '$localStorage', '$ionicLoading',
function($scope, $state, $rootScope, $ionicHistory, AuthService, $localStorage, $ionicLoading) {
  $scope.gotoHome = function(){
    $state.go('login.startup');
  };

  $scope.gotoSignup = function(){
    $rootScope.needAccount();
  };

  $scope.gotoAbout = function(){
    $state.go('login.about');
  };

  $scope.gotoLegalResources = function(){
    $state.go('login.legal');
  };

  $scope.gotoContactUs = function(){
    $state.go('login.contact');
  };

  $scope.gotoTermsOfService = function(){
    $state.go('terms-of-service');
  };

  $scope.gotoPrivacyPolicy = function(){
    $state.go('privacy-policy');
  };
} // end of controller function
]
);
