angular.module('WebWedApp')

.controller('AppCtrl',
['$scope', '$state', '$rootScope', '$ionicHistory', 'AuthService', '$localStorage', '$ionicLoading',
function($scope, $state, $rootScope, $ionicHistory, AuthService, $localStorage, $ionicLoading) {

  $scope.loginMode = $localStorage['loginMode'];
  $scope.broadcasting = $localStorage['broadcast'];

  if ($scope.broadcasting == false && $scope.loginMode == 'host'){
    $scope.type = 2;
  }else if ($scope.loginMode == 'guest'){
    $scope.type = 3;
  }else{
    $scope.type = 4;
  }

  $scope.goLogout = function() {
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    if (window.socket != null){
      window.socket.disconnect();
    }

    // Clear logged infomation
    AuthService.logout();

    // Clear history
    $ionicHistory.clearHistory();

    $ionicLoading.hide();

    $state.go('login.startup');
  }

  $scope.account = {
    name: $localStorage['name'],
    active: $localStorage['_active']
  }

  if ($localStorage['active'] != $localStorage['_active']){
    $scope.account.active = $localStorage['active'];
  }

  if ($scope.account.active == 'wedding' || $scope.account.active == '"wedding"'){
    if ($localStorage['wedding'].length == 0){
      $scope.account.active = 'special_moment';
    }

    if ($localStorage['wedding']['yours1'] != null){
      $scope.account.yours1 = $localStorage['wedding']['yours1']['name'];
    }

    if ($localStorage['wedding']['yours2'] != null){
      $scope.account.yours2 = $localStorage['wedding']['yours2']['name'];
    }

    if ($localStorage['wedding']['date'] != null){
      $scope.account.longDate = $localStorage['wedding']['date']['longDate'];
      $scope.account.longTime = $localStorage['wedding']['date']['longTime'];
    }
  }

  if ($scope.account.active == 'special_moment' || $scope.account.active == '"special_moment"'){
    $scope.account.code = $localStorage['special_moment']['event_code'];
    $scope.account.ondemand = $localStorage['special_moment']['ondemand'];

    if ($localStorage['special_moment']['date'] != null){
      $scope.account.longDate = $localStorage['special_moment']['date']['longDate'];
      $scope.account.longTime = $localStorage['special_moment']['date']['longTime'];
    }
  }

  $scope.goHome = function(){
    // Clear history
    $ionicHistory.clearHistory();

    if (window.socket != null){
      window.socket.disconnect();
    }

    if ($scope.loginMode == 'guest'){
      $state.go('app.home-guest');
    }else{
      $state.go('app.home-host');
    }
  }

  if ($scope.loginMode == 'host'){
    $scope.my_account_or_profile = 'My Account';
  }else{
    $scope.my_account_or_profile = 'My Profile';
  }

} // end of controller function
]
);
