angular.module('WebWedApp')

    .factory('AuthService',
        ['$http', '$q', '$localStorage', 'ENDPOINT_LIST',
            function($http, $q, $localStorage, ENDPOINT_LIST) {

                var service = {};

                service.loginMode = '';

                // Reset before login
                service.loginMode = '';
                window.socket = null;

                // Reset storage
                service.reset = function() {
                    service.loginMode = '';
                    $localStorage['email'] = '';
                    $localStorage['code'] = '';
                    $localStorage['token'] = '';
                    $localStorage['active'] = '';
                    $localStorage['name'] = '';
                    $localStorage['loginMode'] = '';
                    $localStorage['first_run'] = false;
                };

                // Login - Wedding party
                service.login = function(email, password) {

                    service.reset();

                    // Set email
                    $localStorage['email'] = email;
                    $localStorage['password'] = password;
                    $localStorage['broadcast'] = false;

                    var deferred = $q.defer();
                    var apiURL = ENDPOINT_LIST.LOGIN_HOST_API;
                    var postData = 'type=has_account' + '&' + 'email=' + encodeURIComponent(email) + '&' + 'password=' + encodeURIComponent(password);

                    var config = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Accept': 'application/json'
                        }
                    }

                    // if (email == 'bride@webwedmobile.com'){
                    //     var apiURL = "bride.json";
                    // }

                    // if (email == 'groom@webwedmobile.com'){
                    //     var apiURL = 'groom.json';
                    // }

                    // if (email == 'witness1@webwedmobile.com'){
                    //     var apiURL = 'witness1.json';
                    // }

                    // if (email == 'witness2@webwedmobile.com'){
                    //     var apiURL = 'witness2.json';
                    // }

                    // if (email == 'officiant@webwedmobile.com'){
                    //     var apiURL = 'officiant.json';
                    // }

                    // if (email == 'appletv@webwedmobile.com'){
                    //     var apiURL = 'appletv.json';
                    // }

                    $http.post(apiURL, postData, config)
                        .success(function(response) {
                            console.log(response);
                            if (response['errorCode'] != null){
                                deferred.resolve(response);
                            }else{
                                if (response['data'] == undefined || response['data'] == null){
                                    response = { error:'', errorCode:0 };
                                    deferred.reject(response);
                                }

                                console.log('token: ' + response['data']['token']);

                                //Save email
                                $localStorage['email'] = email;

                                // Save name
                                $localStorage['name'] = response['data']['name'];

                                // Save token
                                $localStorage['token'] = response['data']['token'];

                                // Save Wedding Instance
                                $localStorage['wedding'] = response['data']['wedding'];

                                // Save Special Moment Instance
                                $localStorage['special_moment'] = response['data']['special_moment'];

                                // Save settings
                                // $localStorage['settings'] = response['data']['settings'];
                                $localStorage['settings'] = response['data']['settings'];
                                
                                $localStorage['apikey'] = response['data']['apikey'];

                                // Save login mode
                                service.loginMode = 'host';

                                $localStorage['loginMode'] = 'host';

                                deferred.resolve(response);
                            }
                        })
                        .error(function(error) {
                            console.log(error);

                            deferred.reject(error);
                        });

                    return deferred.promise;
                }

                service.forgotPassword = function(email) {
                  var deferred = $q.defer();
                  var apiURL = ENDPOINT_LIST.FORGOT_PASSWORD_API;
                  var postData = 'email=' + encodeURIComponent(email);

                  console.log(postData);

                  var config = {
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded',
                          'Accept': 'application/json'
                      }
                  }

                  $http.post(apiURL, postData, config)
                      .success(function(response){
                        console.log(response);
                        deferred.resolve(response);
                      })
                      .error(function(error){
                        console.log(JSON.stringify(error));

                        deferred.reject(error);
                      });

                  return deferred.promise;
                }

                // Login - Invited guest
                service.loginGuest = function(email, eventcode) {

                    eventcode = eventcode.toUpperCase(); //HotFix

                    service.reset();

                    // Set email
                    $localStorage['email'] = email;
                    $localStorage['code'] = eventcode;
                    $localStorage['broadcast'] = false;

                    var deferred = $q.defer();
                    var apiURL = ENDPOINT_LIST.LOGIN_GUEST_API;
                    var postData = 'type=guest' + '&' + 'email=' + encodeURIComponent(email) + '&' + 'eventcode=' + encodeURIComponent(eventcode);

                    console.log(postData);

                    var config = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Accept': 'application/json'
                        }
                    }

                    $http.post(apiURL, postData, config)
                        .success(function(response) {
                            console.log(response);
                            if (response['errorCode'] != null){
                                deferred.reject(response);
                            }else{
                                if (response['data'] == undefined || response['data'] == null){
                                    response = { error:'', errorCode:0 };
                                    deferred.resolve(response);
                                }

                                console.log('token: ' + response['data']['token']);

                                // Save name
                                $localStorage['name'] = response['data']['name'];

                                if ($localStorage['name'] == null){
                                  $localStorage['name'] = 'Anonymous';
                                }

                                // Save token
                                $localStorage['token'] = response['data']['token'];

                                // Save Type Of event
                                if (response['data']['wedding'].length == 0){
                                  $localStorage['type_of_event'] = 'special_moment';
                                }else{
                                  $localStorage['type_of_event'] = 'wedding';
                                }

                                // Save Wedding Instance
                                $localStorage['wedding'] = response['data']['wedding'];

                                // Save Special Moment Instance
                                $localStorage['special_moment'] = response['data']['special_moment'];

                                // Save login mode
                                service.loginMode = 'guest';

                                $localStorage['loginMode'] = 'guest';

                                deferred.resolve(response);
                            }
                        })
                        .error(function(error) {
                            console.log(JSON.stringify(error));

                            deferred.reject(error);
                        });

                    return deferred.promise;
                }

                service.getLoginMode = function() {
                    return service.loginMode;
                }

                // Logout
                service.logout = function() {
                    service.reset();
                }

                return service;
            } // end of factory function
        ]
    );
