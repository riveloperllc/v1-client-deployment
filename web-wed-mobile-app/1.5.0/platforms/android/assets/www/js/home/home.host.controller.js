angular.module('WebWedApp')

    .controller('HomeHostCtrl',
        ['$rootScope', '$scope', '$state', '$ionicHistory', '$localStorage', '$ionicPopup', '$cordovaCalendar', '$cordovaInAppBrowser', 'ENDPOINT_LIST', '$cordovaSocialSharing',
            function($rootScope, $scope, $state, $ionicHistory, $localStorage, $ionicPopup, $cordovaCalendar, $cordovaInAppBrowser, ENDPOINT_LIST, $cordovaSocialSharing) {

                if (window.socket != null){
                    window.socket.disconnect();
                }

                // Clear history
                $ionicHistory.clearHistory();

                $rootScope.showPopup = function(title, content){

                  if (window.popupOK != undefined){
                    window.popupOK();
                  }

                    $rootScope.alertPopup = $ionicPopup.alert({
                        template: '' +
                            '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
                            '<h1>' + title + '</h1>' +
                            '<p>' + content + '</p>' +
                            '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
                        cssClass: 'customPopup',
                        scope: this
                    });

                    $rootScope.alertPopup.then(function(res) {
                        console.log('alertPopup');
                    });

                    $rootScope.popupOK = function() {
                        console.log('ok');
                        $rootScope.alertPopup.close();
                    }

                    window.popupOK = $rootScope.popupOK;
                }

                $scope.Properties = {
                    title: '',
                    active: $localStorage['active']
                }

                if ($scope.Properties.active == ''){
                  $scope.Properties.active = $localStorage['type_of_event'];
                }

                if ($scope.Properties.active == 'wedding'){
                  $scope.account = {
                      name: $localStorage['name'],
                      yours: $localStorage['wedding']['yours1']['name'],
                      yours2: $localStorage['wedding']['yours2']['name'],
                      officiant: $localStorage['wedding']['officiant']['name'],
                      witness1: $localStorage['wedding']['witness']['name'],
                      witness2: $localStorage['wedding']['witness2']['name'],
                      longDate: $localStorage['wedding']['date']['longDate'],
                      longTime: $localStorage['wedding']['date']['longTime'],
                      computed: $localStorage['wedding']['date']['computed'],
                      giftlink: $localStorage['wedding']['giftlink'],
                      ceremonylink: $localStorage['wedding']['ceremonylink'],
                      code: $localStorage['wedding']['code'],
                      public: ($localStorage['wedding']['settings']['public']),
                      chat: ($localStorage['wedding']['settings']['chat']),
                      gifts: ($localStorage['wedding']['settings']['gifts'])
                  }
                }else{
                  $scope.account = {
                    name: $localStorage['name'],
                    holdersName: $localStorage['special_moment']['holdersName'],
                    eventName: $localStorage['special_moment']['name'],
                    code: $localStorage['special_moment']['code'],
                    longDate: $localStorage['special_moment']['date']['longDate'],
                    longTime: $localStorage['special_moment']['date']['longTime'],
                    computed: $localStorage['special_moment']['date']['computed'],
                    giftlink: $localStorage['special_moment']['giftlink'],
                    ondemand: $localStorage['special_moment']['ondemand'],
                    ceremonylink: $localStorage['special_moment']['ceremonylink'],
                    public: ($localStorage['special_moment']['settings']['public']),
                    chat: ($localStorage['special_moment']['settings']['chat']),
                    gifts: ($localStorage['special_moment']['settings']['gifts'])
                  }
                }

                if ($scope.account.public){ $scope.account.public = false; }else{ $scope.account.public = true; }
                if ($scope.account.chat){ $scope.account.chat = false; }else{ $scope.account.chat = true; }
                if ($scope.account.gifts){ $scope.account.gifts = false; if ($scope.account.gitlink == ''){ $scope.account.gifts = true; } }else{ $scope.account.gifts = true; }

                $scope.icon_blue = '';
                $scope.icon_yellow = '';
                $scope.icon_fav = '';

                $scope.clickHeartButton = function() {

                    if ($scope.icon_blue == '') {
                        $scope.icon_blue = 'active';
                        $scope.icon_yellow = 'active';
                    }
                    else {
                        $scope.icon_blue = '';
                        $scope.icon_yellow = '';
                    }

                    if ($scope.icon_fav == 'active') {
                        $scope.icon_fav = '';
                    }
                    else {
                        $scope.icon_fav = 'active';
                    }
                }

                $scope.addToCalendar = function() {
                  if ($scope.Properties.active == 'wedding'){
                    $scope.notes = $scope.account.yours + " & " + $scope.account.yours2 + " is getting hitched using WebWedMobile.";
                  }else{
                    $scope.notes = 'WebWedMobile Special Moment scheduled.';
                  }

                    var MS_PER_MINUTE = 60000;
                    var startDate = new Date($scope.account.computed);
                    if ($scope.Properties.active == 'wedding'){
                      var endDate = new Date(startDate.getTime() + 55 * MS_PER_MINUTE);
                    }else{
                      var endDate = new Date(startDate.getTime() + 30 * MS_PER_MINUTE);
                    }

                    $cordovaCalendar.createEvent({
                        title: 'WebWedMobile Event',
                        location: 'WebWedMobile App',
                        notes: $scope.notes,
                        startDate: startDate,
                        endDate: endDate
                    }).then(function (result) {
                        $rootScope.showPopup('Awesome!', 'We have added this event to your calendar.');
                    }, function (err) {
                        $rootScope.showPopup('Oh No!', 'WebWed needs permission to access your calendar. Go to Settings > Privacy > Calendar.');
                    });
                }

                $scope.share = function(){
                  if ($scope.Properties.active == 'wedding'){
                    var shareText = $scope.account.yours + ' and ' + $scope.account.yours2 + ' are tying the knot. Join in for their live  wedding ' + $scope.account.longDate + ' at ' + $scope.account.longTime + '. ' + $scope.account.ceremonylink;
                  }else{
                    var shareText = 'A WebWedMobile Special Moment is taking place. Tune into this special moment by following this link ' + $scope.account.ceremonylink;
                  }
                  $cordovaSocialSharing
                  .share(shareText) // Share via native share sheet
                  .then(function(result) {
                    //$rootScope.showPopup('Yay!', 'We have successfully shared your ceremony link!');
                  }, function(err) {
                    $rootScope.showPopup('Hmm', 'We were unable to share your ceremony link.');
                  });
                }

                $scope.inviteGuests = function() {
                    var options = {
                        location: 'no',
                        clearcache: 'yes',
                        toolbar: 'yes'
                    };

                    $cordovaInAppBrowser.open(ENDPOINT_LIST.INVITEGUEST_PAGE + "app&token=" + $localStorage['token'], '_blank', options).then(
                        function(event) {
                            // success
                        }
                    ).catch(
                        function(event) {
                            // error
                            $rootScope.showPopup('Hmm', 'An error occurred when trying to invite guests, please try again later.');
                        }
                    );
                }

                $scope.myAccount = function() {
                    $state.go('app.account');
                }

                $scope.legalResources = function() {
                    var options = {
                        location: 'no',
                        clearcache: 'yes',
                        toolbar: 'yes'
                    };

                    $cordovaInAppBrowser.open(ENDPOINT_LIST.LEGALRESOURCES_PAGE, '_blank', options).then(
                        function(event) {
                            // success
                        }
                    ).catch(
                        function(event) {
                            // error
                        }
                    );
                };

                $scope.goGift = function(){
                    var options = {
                        location: 'no',
                        clearcache: 'yes',
                        toolbar: 'yes'
                    };

                    if ($scope.account.giftlink != ''){
                        $cordovaInAppBrowser.open($scope.account.giftlink, '_blank', options).then(
                            function(event) {
                                // success
                            }
                        ).catch(
                            function(event) {
                                // error
                            }
                        );
                    }
                };

                $scope.showBroadcast = function(){
                  var MS_PER_MINUTE = 60000;
                  var eventTime = Date.parse($scope.account.computed);
                  if ($localStorage['active'] == 'wedding'){
                    // var thirtyMinutesBeforeWeddingStartTime = new Date(eventTime - 30 * MS_PER_MINUTE).getTime();
                    // var thirtyMinutesAfterWeddingStartTime = new Date(eventTime + 30 * MS_PER_MINUTE).getTime();
                    // var currentTime = Date.now();
                    // if (currentTime >= thirtyMinutesBeforeWeddingStartTime && currentTime <= thirtyMinutesAfterWeddingStartTime){
                        $ionicHistory.nextViewOptions({
                           disableAnimate: true
                        });
                        $localStorage['broadcast'] = true;
                        $state.go('app.home-broadcast');
                    // }else{
                    //     $rootScope.showPopup('Oh no!', 'Looks like this wedding is no longer available or is not available yet.');
                    // }
                  }else{
                    //Goto special moments broadcast screen
                    if ($scope.account.ondemand){
                      $state.go('app.home-broadcast-special-moments');
                    }else{
                      var thirtyMinutesBeforeWeddingStartTime = new Date(eventTime - 30 * MS_PER_MINUTE).getTime();
                      var thirtyMinutesAfterWeddingStartTime = new Date(eventTime + 30 * MS_PER_MINUTE).getTime();
                      var currentTime = Date.now();
                      if (currentTime >= thirtyMinutesBeforeWeddingStartTime && currentTime <= thirtyMinutesAfterWeddingStartTime){
                        $state.go('app.home-broadcast-special-moments');
                      }else{
                        $rootScope.showPopup('Oh no!', 'Looks like your special moment is no longer available or is not available yet. Please wait until 10 minutes prior to the event time.');
                      }
                    }
                  }
                };


                $scope.addToCalendarAction = false;

                var MS_PER_MINUTE = 60000;
                var weddingTime = Date.parse($scope.account.computed);
                var thirtyMinutesAfterWeddingStartTime = new Date(weddingTime + 30 * MS_PER_MINUTE).getTime();
                var currentTime = Date.now();

                if (currentTime >= thirtyMinutesAfterWeddingStartTime){
                  $scope.addToCalendarAction = true;
                }

                if ($scope.Properties.active == 'special_moment' && $scope.account.ondemand == true){
                  $scope.addToCalendarAction = true;
                }

                if ($scope.Properties.active == 'wedding'){
                  $rootScope.showPopup('Quick Note', 'Please tap the heart below when you are ready to join the wedding.');
                }else{
                  if ($scope.account.ondemand){
                    $rootScope.showPopup('Quick Note', 'Your event is on-demand. Simply tap the heart button below to begin streaming your special moment for 30 minutes.');
                  }else{
                    $rootScope.showPopup('Quick Note', 'Your event is scheduled for ' + $scope.account.computed + '. You may begin 5 minutes prior to your events scheduled time.');
                  }
                }

            } // end of controller function
        ]
    );
