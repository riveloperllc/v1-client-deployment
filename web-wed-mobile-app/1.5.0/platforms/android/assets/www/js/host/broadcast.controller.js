angular.module('WebWedApp')

.controller('WeddingsBroadcastCtrl',
['$rootScope', '$scope', '$state', '$ionicHistory', '$localStorage', '$ionicPopup', '$cordovaCalendar', '$cordovaInAppBrowser', 'ENDPOINT_LIST', '$ionicSideMenuDelegate',
function($rootScope, $scope, $state, $ionicHistory, $localStorage, $ionicPopup, $cordovaCalendar, $cordovaInAppBrowser, ENDPOINT_LIST, $ionicSideMenuDelegate) {

  $localStorage['broadcast'] = true;
  $ionicSideMenuDelegate.canDragContent(false);

  // Clear history
  $ionicHistory.clearHistory();


  $scope.$on("$ionicView.beforeLeave", function(event, data){
    $scope.session.unpublish($scope.publisher);
    $scope.session.disconnect();
  });

  //window.device = {};
  //window.device.platform = 'browser';

  /*if (window.device.platform == undefined){
  window.device = {};
  window.device.platform = 'browser';
}*/

$rootScope.showPopup = function(title, content){

  if (window.popupOK != undefined){
    window.popupOK();
  }

    $rootScope.alertPopup = $ionicPopup.alert({
        template: '' +
            '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
            '<h1>' + title + '</h1>' +
            '<p>' + content + '</p>' +
            '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
        cssClass: 'customPopup',
        scope: this
    });

    $rootScope.alertPopup.then(function(res) {
        console.log('alertPopup');
    });

    $rootScope.popupOK = function() {
        console.log('ok');
        $rootScope.alertPopup.close();
    }

    window.popupOK = $rootScope.popupOK;
}

$scope.Properties = {
  title: $localStorage['name']
}

$scope.account = {
  name: $localStorage['name'],
  yours: $localStorage['wedding']['yours1']['name'],
  yours2: $localStorage['wedding']['yours2']['name'],
  officiant: $localStorage['wedding']['officiant'],
  witness1: $localStorage['wedding']['witness']['name'],
  witness2: $localStorage['wedding']['witness2']['name'],
  longDate: $localStorage['wedding']['date']['longDate'],
  longTime: $localStorage['wedding']['date']['longTime'],
  code: $localStorage['wedding']['code']
}

$scope.icon_blue = '';
$scope.icon_yellow = '';
$scope.icon_fav = '';

$scope.readyToEnd = false;
$scope.weddingIsActive = true;

$scope.clickHeartButton = function() {

  // if ($scope.icon_blue == '') {
  //   $scope.icon_blue = 'active';
  //   $scope.icon_yellow = 'active';
  // }
  // else {
  //   $scope.icon_blue = '';
  //   $scope.icon_yellow = '';
  // }

  // if ($scope.icon_fav == 'active') {
  //   $scope.icon_fav = '';
  // }
  // else {
  //   $scope.icon_fav = 'active';
  // }

  if ($scope.role == 'officiant'){
    if ($scope.readyToEnd == false){
      // $rootScope.showPopup('Hold up!', 'By clicking this button you are certifying this wedding is completed, please click the heart button again to confirm.');
      // $scope.readyToEnd = true;
    }else{
      // $scope.completeWedding(); //Wedding is actually completed, not left.
      $scope.weddingIsActive = false;
      $scope.session.signal({
        type: 'weddingCompleted',
        data: ''
      }, function(error){
        $('#bottombar').append('<div id="officialVideoElement" style="float:left;" class="people-block people-3"></div>');
        $state.go('app.home-broadcast-completed');
      });
    }
  }
}


$scope.name = $localStorage['name'];
$scope.token = $localStorage['token'];
$scope.role = $localStorage['wedding']['role'];
$scope.weddingCode = $localStorage['wedding']['code'];
$scope.typeOfConnection = "presenter";

if ($scope.role == "yours" || $scope.role == "yours2" || $scope.role == "official"){
  $scope.typeOfConnection = "presenter";
}else if ($scope.role == "witness"){
  $scope.typeOfConnection = "viewer";
}else{
  $scope.typeOfConnection = "viewer";
}

console.log($scope.role);

jQuery.loadScript = function(url, callback){
  jQuery.ajax({
    url: url,
    dataType: 'script',
    success: callback,
    async: true
  });
}

keepscreenon.enable();
  

  $scope.positions = {
    your: false,
    yours: false,
    witness1: false,
    witness2: false,
    officiant: false
  };

  $scope.handlers = {
        'streamCreated': function( event ){
          var div = $scope.getSubscriberElement(event);

          if (($scope.role == 'guest' && event.stream.connection.data == 'officiant') || (event.stream.connection.data == 'witness1') || (event.stream.connection.data == 'witness2')){
            if(event.stream.connection.data == 'officiant'){
              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '33vh', width: '33vw'} );
            }else{
              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '45vh', width: '50vw'} );
            }
          }else{
            if(event.stream.connection.data == 'yours'){
              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '55vh', width: '50vw'} ); 
            }else{
              var subscriber = $scope.session.subscribe( event.stream, div.id, {subscribeToAudio: true, height: '55vh', width: '50vw'} ); 
            }
          }

          $scope.subscribers.push(subscriber);

          TB.updateViews();

          if ($scope.role == 'guest'){
            if ($scope.positions.your && $scope.positions.yours && $scope.positions.witness1 && $scope.positions.witness2 && $scope.positions.officiant){
              document.getElementById('yourVideoElement2').style.display = 'block';
              document.getElementById('yoursVideoElement2').style.display = 'block';
              document.getElementById('witnessVideoElement2').style.display = 'block';
              document.getElementById('witness2VideoElement2').style.display = 'block';
              document.getElementById('officialVideoElement2').style.display = 'block';
              $scope.session.signal({
                type: 'wedding_started',
                data: ''
              }, function(error){
                console.log('Sent start signal.');  
              });
              TB.updateViews();
            }else{
              document.getElementById('yourVideoElement2').style.display = 'none';
              document.getElementById('yoursVideoElement2').style.display = 'none';
              document.getElementById('witnessVideoElement2').style.display = 'none';
              document.getElementById('witness2VideoElement2').style.display = 'none';
              document.getElementById('officialVideoElement2').style.display = 'none';
              TB.updateViews();
            }
          }
        },

        'streamDestroyed': function( event ){
          window.e = event;
          if (event.stream.connection.data == 'yours'){
            $('#videoContainer2').append('<div id="yourVideoElement2"></div>');
          }else if (event.stream.connection.data == 'yours2'){
            $('#videoContainer2').append('<div id="yoursVideoElement2"></div>');
          }else if (event.stream.connection.data == 'witness1'){
            $('#bottombar2').append('<div id="witnessVideoElement2"></div>');
          }else if (event.stream.connection.data == 'witness2'){
            $('#bottombar2').append('<div id="witness2VideoElement2"></div>');
          }else if (event.stream.connection.data == 'officiant'){
            $('#bottombar2').append('<div id="officialVideoElement2"></div>');
            // $state.go('app.home-broadcast-completed');
            // $scope.session.disconnect();
          }

          if (!$scope.positions.your || !$scope.positions.yours || !$scope.positions.witness1 || !$scope.positions.witness2 || !$scope.positions.officiant){
            document.getElementById('beatingheart').style.display = 'block';
            document.getElementById('beatingheart').style.position = 'absolute';
            // document.getElementById('beatingheart').style.display = 'block';
          }

          if ($scope.role == 'guest'){
            if ($scope.positions.your && $scope.positions.yours && $scope.positions.witness1 && $scope.positions.witness2 && $scope.positions.officiant){
              document.getElementById('yourVideoElement2').style.display = 'block';
              document.getElementById('yoursVideoElement2').style.display = 'block';
              document.getElementById('witnessVideoElement2').style.display = 'block';
              document.getElementById('witness2VideoElement2').style.display = 'block';
              document.getElementById('officialVideoElement2').style.display = 'block';
              TB.updateViews();
            }else{
              document.getElementById('yourVideoElement2').style.display = 'none';
              document.getElementById('yoursVideoElement2').style.display = 'none';
              document.getElementById('witnessVideoElement2').style.display = 'none';
              document.getElementById('witness2VideoElement2').style.display = 'none';
              document.getElementById('officialVideoElement2').style.display = 'none';
              TB.updateViews();
            }
          }

          console.log('destroyed');
        }
      };

    $scope.getPublisherElement = function(){
      var id = 'yourVideoElement2';
      if ($scope.role == 'yours'){
        $scope.positions.your = true;
      }else if ($scope.role == 'yours2'){
        id = 'yoursVideoElement2';
        $scope.positions.yours = true;
      }else if ($scope.role == 'witness1'){
        id = 'witnessVideoElement2';
        $scope.positions.witness1 = true;
      }else if ($scope.role == 'witness2'){
        id = 'witness2VideoElement2';
        $scope.positions.witness2 = true;
      }else if ($scope.role == 'officiant'){
        id = 'officialVideoElement2';
        $scope.positions.officiant = true;
      }
      return id;
    };

    $scope.getPublisherToken = function(){
      return $localStorage['wedding']['token_id'];
    };

    $scope.getSubscriberElement = function(subscriber){
      var event = subscriber;
      if (event.stream.connection.data == 'yours'){
        //Bride
        var div = document.getElementById('yourVideoElement2');
        $scope.positions.your = true;
      }else if (event.stream.connection.data == 'yours2'){
        //Groom
        var div = document.getElementById('yoursVideoElement2');
        $scope.positions.yours = true;
      }else if (event.stream.connection.data == 'witness1'){
        //Witness1
        var div = document.getElementById('witnessVideoElement2');
        $scope.positions.witness1 = true;
      }else if (event.stream.connection.data == 'witness2'){
        //Witness2
        var div = document.getElementById('witness2VideoElement2');
        $scope.positions.witness2 = true;
      }else if (event.stream.connection.data == 'officiant'){
        //Officiant
        var div = document.getElementById('officialVideoElement2');
        $scope.positions.officiant = true;
      }else{
        //Apple TV
      }

      return div;
    };

      var apiKey = $localStorage['apikey']; // INSERT YOUR API Key

      //secret d2329b40d5b69006701f047d2b7b83814a5c5676

      var sessionId = $localStorage['wedding']['session_id']; // INSERT YOUR SESSION ID
      
      // Very simple OpenTok Code for group video chat
      if ($scope.role != 'guest' && $scope.role != 'officiant'){
        $scope.publisher = TB.initPublisher(apiKey,$scope.getPublisherElement(), {insertMode: 'append'});
      }

      if ($scope.role == 'guest'){
        if ($scope.positions.your && $scope.positions.yours && $scope.positions.witness1 && $scope.positions.witness2 && $scope.positions.officiant){
          document.getElementById('yourVideoElement2').style.display = 'block';
          document.getElementById('yoursVideoElement2').style.display = 'block';
          document.getElementById('witnessVideoElement2').style.display = 'block';
          document.getElementById('witness2VideoElement2').style.display = 'block';
          document.getElementById('officialVideoElement2').style.display = 'block';
          TB.updateViews();
        }else{
          document.getElementById('yourVideoElement2').style.display = 'none';
          document.getElementById('yoursVideoElement2').style.display = 'none';
          document.getElementById('witnessVideoElement2').style.display = 'none';
          document.getElementById('witness2VideoElement2').style.display = 'none';
          document.getElementById('officialVideoElement2').style.display = 'none';
          TB.updateViews();
        }
      }

      // window.s = [];
      $scope.session = TB.initSession( apiKey, sessionId ); 
      $scope.session.on('signal:weddingCompleted', function(event){
        $scope.session.unpublish($scope.publisher);
        for (var i = 0; i < $scope.subscribers.length; i++){
          var subscriber = $scope.subscribers[i];
          $scope.session.unsubscribe(subscriber);
        }
        $scope.session.disconnect();
        $state.go('app.home-broadcast-completed');
      });

      $scope.subscribers = [];

      $scope.session.on($scope.handlers);

      if ($scope.role != 'officiant'){
        $scope.session.connect($scope.getPublisherToken(), function(){
          if ($scope.role != 'guest'){
            $scope.session.publish( $scope.publisher );
          }

          if ($scope.positions.your && $scope.positions.yours && $scope.positions.witness1 && $scope.positions.witness2 && $scope.positions.officiant){
            document.getElementById('beatingheart').style.display = 'none';
          }
        });
      }

      if ($scope.role == 'officiant'){
        //$scope.publisher = TB.initPublisher(apiKey,$scope.getPublisherElement(), {insertMode: 'append'});
        $rootScope.showPopupWithCallback('Finished with ceremony?', 'When you are finished with the ceremony you can complete the wedding by tapping the red heart in the top right corner. This will mark the ceremony as completed, and all parties will be disconnected.', function(){
          $scope.readyToEnd = true;
          $scope.publisher = TB.initPublisher(apiKey,'officialVideoElement2', {insertMode: 'append'});
          $scope.session.on($scope.handlers);
          $scope.session.connect($scope.getPublisherToken(), function(){
            if ($scope.role != 'guest'){
              $scope.session.publish( $scope.publisher );

              $scope.session.signal({
                type: 'wedding_started',
                data: ''
              }, function(error){
                console.log('Sent start signal.');  
              });
            }

            if ($scope.positions.your && $scope.positions.yours && $scope.positions.witness1 && $scope.positions.witness2 && $scope.positions.officiant){
              document.getElementById('beatingheart').style.display = 'none';
            }

          });
          $scope.$apply();
        });
      }

      if (!ionic.Platform.isIPad()){
        document.getElementById('beatingheart').style.position = 'absolute';
        document.getElementById('beatingheart').style.top = '10%';
      }

} // end of controller function
]
);
