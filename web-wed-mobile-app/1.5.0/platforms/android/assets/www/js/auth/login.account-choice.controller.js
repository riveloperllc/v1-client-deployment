angular.module('WebWedApp')

    .controller('LoginChoiceCtrl',
        ['$rootScope', '$scope', '$state', '$ionicPopup', '$ionicHistory', '$cordovaInAppBrowser', 'ENDPOINT_LIST', '$localStorage', '$ionicLoading', 'AuthService', '$http',
            function($rootScope, $scope, $state, $ionicPopup, $ionicHistory, $cordovaInAppBrowser, ENDPOINT_LIST, $localStorage, $ionicLoading, AuthService, $http) {
              
              $(document).ready(function(){
                $('button[ng-click="goSpecialMoment()"]').addClass('hideAndroid');
                $('button[ng-click="goWeddingPartyLogin()"]').addClass('hideAndroid');
                $('b').addClass('hideAndroid');
                $('h1').addClass('hideAndroid');
                $('img[src="img/heart-ring.svg"]').addClass('hideAndroid');
              });

              $rootScope.showPopup = function(title, content){

                if (window.popupOK != undefined){
                  window.popupOK();
                }

                  $rootScope.alertPopup = $ionicPopup.alert({
                      template: '' +
                          '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
                          '<h1>' + title + '</h1>' +
                          '<p>' + content + '</p>' +
                          '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
                      cssClass: 'customPopup',
                      scope: this
                  });

                  $rootScope.alertPopup.then(function(res) {
                      console.log('alertPopup');
                  });

                  $rootScope.popupOK = function() {
                      console.log('ok');
                      $rootScope.alertPopup.close();
                  }

                  window.popupOK = $rootScope.popupOK;
              }

                $scope.showLoader = function(){
                  $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0,
                    hideOnStateChange: true
                  });
                }

                $scope.goWeddingPartyLogin = function() {
                  $scope.showLoader();
                  $localStorage['active'] = "wedding";
                  $localStorage['_active'] = "wedding";
                  $state.go('app.home-host');
                }

                $scope.goInvitedGuest = function() {
                  $scope.showLoader();
                  $state.go('login.invited-guest');
                }

                $scope.goSpecialMoment = function() {
                  $scope.showLoader();
                  $localStorage['active'] = "special_moment";
                  $localStorage['_active'] = "special_moment";
                  $state.go('app.home-host');
                }

                $scope.attendEvent = function() {
                  $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                  });

                  AuthService.loginGuest($scope.login.email, $scope.login.pincode).then(
                    function(response) {
                      console.log(response);
                      $ionicLoading.hide();
                      if (response['errorCode'] == null || response['errorCode'] == undefined){
                        $state.go('app.home-guest');
                      }else{
                        $rootScope.showPopup('Hmm', 'That email or password did not match our records. Please double check your email, and password then try again.');
                      }
                    },
                    function(error) {
                      $ionicLoading.hide();
                      $rootScope.showPopup('Hmm', 'That email or password did not match our records. Please double check your email, and password then try again.');
                    }
                  );
                }

                $scope.login = {
                  wedding_party : false,
                  special_moment : false,
                  pincode: ''
                }

                if ($localStorage['email'] != undefined && $localStorage['email'] != null){
                  $scope.login.email = $localStorage['email'];
                }

                if ($localStorage['wedding'] != undefined && $localStorage['wedding'] != null){
                  if ($localStorage['wedding']['code'] != undefined && $localStorage['wedding']['code'] != null){
                    $scope.login.wedding_party = true;
                  }
                }

                if ($localStorage['special_moment'] != undefined && $localStorage['special_moment'] != null){
                  if ($localStorage['special_moment']['code'] != undefined && $localStorage['special_moment']['code'] != null){
                    $scope.login.special_moment = true;
                  }
                }

                // var accountTypes = $localStorage['account_types'];
                // for (var i = 0; i < accountTypes.length; i++) {
                //   var type = accountTypes[i];
                //   switch(type){
                //     case 'wedding_party':
                //       $scope.login.wedding_party = true;
                //       break;
                //
                //     case 'guest':
                //       $scope.login.guest = true;
                //       break;
                //
                //     case 'special_moment':
                //       $scope.login.special_moment = true;
                //       break;
                //   }
                // }
            } // end of controller function
        ]
    );
