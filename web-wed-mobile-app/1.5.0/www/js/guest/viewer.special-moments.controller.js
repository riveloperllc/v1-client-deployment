angular.module('WebWedApp')

.controller('SpecialMomentsGuestViewerCtrl',
['$rootScope', '$scope', '$state', '$ionicHistory', '$localStorage', '$ionicPopup', '$cordovaCalendar', '$cordovaInAppBrowser', 'ENDPOINT_LIST', '$ionicSideMenuDelegate', '$ionicModal', '$cordovaSocialSharing',
function($rootScope, $scope, $state, $ionicHistory, $localStorage, $ionicPopup, $cordovaCalendar, $cordovaInAppBrowser, ENDPOINT_LIST, $ionicSideMenuDelegate, $ionicModal, $cordovaSocialSharing) {

  $ionicHistory.nextViewOptions({
    disableAnimate: false
  });

  $scope.$on("$ionicView.beforeLeave", function(event, data){
    // $scope.session.disconnect();
    for (var i = 0; i < $scope.subscribers.length; i++){
      var subscriber = $scope.subscribers[i];
      $scope.session.unsubscribe(subscriber);
    }
  });

  $rootScope.showPopup = function(title, content){

    if (window.popupOK != undefined){
      window.popupOK();
    }

      $rootScope.alertPopup = $ionicPopup.alert({
          template: '' +
              '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
              '<h1>' + title + '</h1>' +
              '<p>' + content + '</p>' +
              '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
          cssClass: 'customPopup',
          scope: this
      });

      $rootScope.alertPopup.then(function(res) {
          console.log('alertPopup');
      });

      $rootScope.popupOK = function() {
          console.log('ok');
          $rootScope.alertPopup.close();
      }

      window.popupOK = $rootScope.popupOK;
  }

  $scope.messages = {
    text: ''
  };

  $localStorage['broadcast'] = true;
  $ionicSideMenuDelegate.canDragContent(false);

  $scope.Properties = {
    title: $localStorage['special_moment']['name']
  }

  $scope.account = {
    name: $localStorage['name'],
    holdersName: $localStorage['special_moment']['holdersName'],
    eventName: $localStorage['special_moment']['name'],
    code: $localStorage['special_moment']['event_code'],
    longDate: $localStorage['special_moment']['date']['longDate'],
    longTime: $localStorage['special_moment']['date']['longTime'],
    computed: $localStorage['special_moment']['date']['computed'],
    ceremonylink: $localStorage['special_moment']['ceremonylink'],
    giftlink: $localStorage['special_moment']['giftlink'],
    ondemand: $localStorage['special_moment']['ondemand'],
    public: ($localStorage['special_moment']['settings']['public']),
    chat: ($localStorage['special_moment']['settings']['chat']),
    gifts: ($localStorage['special_moment']['settings']['gifts'])
  }

  if ($scope.account.public){ $scope.account.public = false; }else{ $scope.account.public = true; }
  if ($scope.account.chat){ $scope.account.chat = false; }else{ $scope.account.chat = true; }
  if ($scope.account.gifts){ $scope.account.gifts = false; if ($localStorage['special_moment']['giftlink'] == ''){ $scope.account.gifts = true; } }else{ $scope.account.gifts = true; }

  $scope.icon_blue = '';
  $scope.icon_yellow = '';
  $scope.icon_fav = '';

  $scope.clickHeartButton = function() {

    if ($scope.icon_blue == '') {
      $scope.icon_blue = 'active';
      $scope.icon_yellow = 'active';
    }
    else {
      $scope.icon_blue = '';
      $scope.icon_yellow = '';
    }

    if ($scope.icon_fav == 'active') {
      $scope.icon_fav = '';
    }
    else {
      $scope.icon_fav = 'active';
    }
  }

  // $scope.socket = window.socket;
  // $scope.sessionId = window.sessionId;
  // $scope.streams = window.streams;

  // $scope.init = function(callback){

  //   if (window.socket == null){
  //     $scope.socket = io.connect('http://endpoint.webwedmobile.com:8082/');
  //   }else{
  //     $scope.socket = window.socket;
  //   }

  //   window.onbeforeunload = function(){
  //     window.socket.disconnect();
  //   };

  //   window.socket = $scope.socket;

  //   callback();
  // };

  // $scope.sendMessage = function(message){
  //   window.socket.emit('message', message);
  // };

  // $scope.sendChatMessage = function(){
  //   var body = $scope.messages.text;
  //   $('.messages').append('<li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
  //   $scope.messages.text = '';
  //   $scope.sendMessage({
  //     id: 'post',
  //     text: body
  //   });
  // };

  // $("#message_text").on("keypress", function(event){
  //   if (event.keyCode === 13) {
  //     $scope.sendChatMessage();
  //     $scope.messages.text = '';
  //   }
  // });

  $scope.goBack = function(){
    $ionicHistory.goBack();
  };

  $scope.share = function(){
    if ($scope.Properties.active == 'wedding'){
      var shareText = $scope.account.yours + ' and ' + $scope.account.yours2 + ' are tying the knot. Join in for their live  wedding ' + $scope.account.longDate + ' at ' + $scope.account.longTime + '. ' + $scope.account.ceremonylink;
    }else{
      var shareText = 'A WebWedMobile Special Moment is taking place. Tune into this special moment by following this link ' + $scope.account.ceremonylink;
    }
    $cordovaSocialSharing
    .share(shareText) // Share via native share sheet
    .then(function(result) {
      //$rootScope.showPopup('Yay!', 'We have successfully shared your ceremony link!');
    }, function(err) {
      $rootScope.showPopup('Hmm', 'We were unable to share your ceremony link.');
    });
  }

  $scope.goGift = function(){
    var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'yes'
    };

    if ($scope.account.giftlink != ''){
      $cordovaInAppBrowser.open($scope.account.giftlink, '_blank', options).then(
        function(event) {
          // success
        }
      ).catch(
        function(event) {
          // error
        }
      );
    }
  };

  $scope.addToCalendar = function(){
    var title = '';
    if ($scope.Properties.active == 'wedding'){
      $scope.notes = $scope.account.yours + " & " + $scope.account.yours2 + " is getting married using WebWedMobile.";
      title = 'Web Wed Marriage';
    }else{
      $scope.notes = 'WebWedMobile Special Moment scheduled.';
      title = 'Web Wed Special Moments';
    }
    var MS_PER_MINUTE = 60000;
    var startDate = new Date($scope.account.computed);
    if ($scope.Properties.active == 'wedding'){
      var endDate = new Date(startDate.getTime() + 55 * MS_PER_MINUTE);
    }else{
      var endDate = new Date(startDate.getTime() + 30 * MS_PER_MINUTE);
    }

    $cordovaCalendar.createEvent({
      title: title,
      location: 'WebWedMobile App',
      notes: $scope.notes,
      startDate: startDate,
      endDate: endDate
    }).then(function (result) {
      $rootScope.showPopup('Awesome!', 'We have added this event to your event calendar.');
    }, function (err) {
      $rootScope.showPopup('Hmm', 'We could not add this event to your calendar, please try again later.');
    });
  };

  jQuery.loadScript = function(url, callback){
    jQuery.ajax({
      url: url,
      dataType: 'script',
      success: callback,
      async: true
    });
  }

  keepscreenon.enable();

  $scope.apikey = $localStorage['apikey'];
  $scope.session_id = $localStorage['special_moment']['session_id'];
  $scope.token_id = $localStorage['special_moment']['token_id'];
  $scope.role = $localStorage['special_moment']['role'];


  $scope.session = TB.initSession( $scope.apikey, $scope.session_id );
  
  $scope.subscribers = [];

  if ($scope.role != 'publisher'){
   $scope.session.on({
      'streamCreated': function( event ){
        var videoElement = document.getElementById('fullscreen_video');
        var subscriber = $scope.session.subscribe( event.stream, videoElement.id, {subscribeToAudio: true, height: '75vh', width: '100vw'} );
        $scope.subscribers.push(subscriber);
        TB.updateViews();
      },

      'streamDestroyed': function( event ){
        console.log('Special Moment has been interupted or ended.');
       // $scope.session.disconnect();
        $ionicHistory.goBack();
      }
    });
  }

  // $scope.$on('$ionicView.loaded', function(){
   $scope.session.connect($scope.token_id, function(){
      if ($scope.role == 'publisher'){
        var publisher = TB.initPublisher($scope.apikey, 'fullscreen_video', {insertMode: 'replace', cameraName: 'back'});
        $scope.session.publish( publisher );
      }
    });
  // });

} // end of controller function
]
);
