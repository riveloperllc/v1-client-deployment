angular.module('WebWedApp.constant', [])

    .constant('ENDPOINT_LIST', (function() {
        var web_server = 'http://webwedmobile.com/admin';
        var api_root = '/api/v1/api.php';

        return {
            VERSION: '1.5.1',
            DOMAIN: web_server,
            WATCH_DEMO_VIDEO: 'https://www.youtube-nocookie.com/embed/mTMQqgrLU9Y?rel=0&amp;showinfo=0&amp;autoplay=true',
            LOGIN_CHECK_API: web_server + api_root + '?cmd=loginCheck',
            LOGIN_EVENT_CHECK_API: web_server + api_root + '?cmd=loginEventCheck',
            LOGIN_SPECIAL_MOMENTS_API: web_server + api_root + '?cmd=loginSpecialMoments',
            LOGIN_HOST_API: web_server + api_root + '?cmd=login',
            LOGIN_GUEST_API: web_server + api_root + '?cmd=login',
            ACCOUNT_SETTINGS_API: web_server + api_root + '?cmd=accountdetails',
            CONTACT_API: web_server + api_root + '?cmd=contactus',
            UPDATE_API: web_server + api_root + '?cmd=update',
            RESEND_CODE_API: web_server + api_root + '?cmd=resend_code',
            FORGOT_PASSWORD_API: web_server + api_root + '?cmd=forgotpassword',
            SIGNUP_PAGE: web_server + '/user_admin/signup.php?app',
            LEGALRESOURCES_PAGE: web_server + '/sign_up.php',
            INVITEGUEST_PAGE: web_server + '/user_admin/add_contacts.php?'
        }
    })())
;
