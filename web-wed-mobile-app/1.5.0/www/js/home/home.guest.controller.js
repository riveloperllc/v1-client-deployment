angular.module('WebWedApp')

.controller('HomeGuestCtrl',
['$rootScope', '$scope', '$state', '$cordovaInAppBrowser', '$ionicHistory', 'ENDPOINT_LIST', '$localStorage', '$ionicPopup', '$cordovaCalendar', '$cordovaSocialSharing', '$ionicModal',
function($rootScope, $scope, $state, $cordovaInAppBrowser, $ionicHistory, ENDPOINT_LIST, $localStorage, $ionicPopup, $cordovaCalendar, $cordovaSocialSharing, $ionicModal) {
  $scope.Properties = {
    event: $localStorage['type_of_event']
  };

  $rootScope.showPopup = function(title, content){
      $rootScope.alertPopup = $ionicPopup.alert({
          template: '' +
              '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
              '<h1>' + title + '</h1>' +
              '<p>' + content + '</p>' +
              '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
          cssClass: 'customPopup',
          scope: this
      });

      $rootScope.alertPopup.then(function(res) {
          console.log('alertPopup');
      });

      $rootScope.popupOK = function() {
          console.log('ok');
          $rootScope.alertPopup.close();
      }

      window.popupOK = $rootScope.popupOK;
  }

  if ($localStorage['wedding'].length == 0){
    $scope.Properties.event = 'special_moment';
  }

  $scope.Properties.active = $scope.Properties.event;

  if ($scope.Properties.active == 'wedding'){
    $scope.account = {
      name: $localStorage['name'],
      yours: $localStorage['wedding']['yours1']['name'],
      yours2: $localStorage['wedding']['yours2']['name'],
      longDate: $localStorage['wedding']['date']['longDate'],
      longTime: $localStorage['wedding']['date']['longTime'],
      computed: $localStorage['wedding']['date']['computed'],
      ceremonylink: $localStorage['wedding']['ceremonylink'],
      code: $localStorage['wedding']['code'],
      public: ($localStorage['wedding']['settings']['public']),
      chat: ($localStorage['wedding']['settings']['chat']),
      gifts: ($localStorage['wedding']['settings']['gifts']),
      giftlink: ($localStorage['wedding']['giftlink'])
    }
  }else{
    $scope.account = {
      name: $localStorage['name'],
      holdersName: $localStorage['special_moment']['holdersName'],
      eventName: $localStorage['special_moment']['name'],
      code: $localStorage['special_moment']['event_code'],
      longDate: $localStorage['special_moment']['date']['longDate'],
      longTime: $localStorage['special_moment']['date']['longTime'],
      computed: $localStorage['special_moment']['date']['computed'],
      ceremonylink: $localStorage['special_moment']['ceremonylink'],
      giftlink: $localStorage['special_moment']['giftlink'],
      ondemand: $localStorage['special_moment']['ondemand'],
      public: ($localStorage['special_moment']['settings']['public']),
      chat: ($localStorage['special_moment']['settings']['chat']),
      gifts: ($localStorage['special_moment']['settings']['gifts'])
    }

    if ($scope.account.ondemand){
      $(document).ready(function(){
        $($('button[ng-click="addToCalendar();"]')[0]).css('display', 'none');
      });
    }
  }

  $scope.messages = {
    text: ''
  };

  //Add Special Moment Logic
  $scope.addToCalendarAction = false;

  if ($scope.Properties.active == 'wedding' || ($scope.Properties.active == 'special_moment' && $scope.account.ondemand == true)){
    var MS_PER_MINUTE = 60000;
    var weddingTime = Date.parse($scope.account.computed);
    var thirtyMinutesAfterWeddingStartTime = new Date(weddingTime + 30 * MS_PER_MINUTE).getTime();
    var currentTime = Date.now();

    if (currentTime >= thirtyMinutesAfterWeddingStartTime){
      $scope.addToCalendarAction = true;
    }
  }

  // Clear history
  $ionicHistory.clearHistory();

  $scope.bubbles = [
    {
      src: 'img/bubble-chat.svg',
      pos: 0
    },
    {
      src: 'img/bubble-gift.svg',
      pos: 15
    },
    {
      src: 'img/bubble-heart.svg',
      pos: 22
    },
    {
      src: 'img/bubble-clock.svg',
      pos: 43
    },
    {
      src: 'img/bubble-link.svg',
      pos: 60
    }
  ];

  if ($scope.account.public){
    $scope.account.public = false;
  }else{
    $scope.account.public = true;
  }

  if ($scope.account.chat){
    $scope.account.chat = false;
  }else{
    $scope.account.chat = true;
  }

  if ($scope.account.gifts){
    $scope.account.gifts = false;
    if ($scope.account.giftlink == ''){
      $scope.account.gifts = true;
    }
  }else{
    $scope.account.gifts = true;
  }

  $scope.showBubble = false;
  $scope.bubbleIndex = -1;

  $scope.doBubble = function() {
    if (! $scope.showBubble) {
      $scope.showBubble = true;
      $scope.bubbleIndex = 0;

      $scope.bubble = $scope.bubbles[$scope.bubbleIndex].src;
      $scope.marginLeft = $scope.bubbles[$scope.bubbleIndex].pos + '%';
    }
  }

  $scope.nextBubble = function() {
    if ($scope.bubbleIndex < 4) {
      $scope.bubbleIndex++;

      $scope.bubble = $scope.bubbles[$scope.bubbleIndex].src;
      $scope.marginLeft = $scope.bubbles[$scope.bubbleIndex].pos + '%';
    }
    else {
      $scope.showBubble = false;
      $scope.bubbleIndex = -1;
    }
  }

  $scope.displayHowItWorks = function(){
    $localStorage['first_help'] = false;
    $scope.doBubble();
  }

  $scope.doBubble();

  $scope.chat = {
    shown: false
  };

  $scope.goChat = function() {
    console.log('called');
    if ($scope.chat.shown){
      $scope.chat.shown = false;
    }else{
      $scope.chat.shown = true;
    }
  }


  window.interval = setInterval(function(){
    if (document.getElementsByClassName('messages')[0] == null) { clearInterval(window.interval); return; }
    document.getElementsByClassName('messages')[0].scrollTop = document.getElementsByClassName('messages')[0].scrollHeight;
  }, 500);

  // This event fires when the keyboard will hide
  window.addEventListener('native.keyboardshow', keyboardShowHandler);

  function keyboardShowHandler(e){
    $(".chat_window").addClass("keyboardOn");
  }

  // This event fires when the keyboard will show
  window.addEventListener('native.keyboardhide', keyboardHideHandler);

  function keyboardHideHandler(e){
    $(".chat_window").removeClass("keyboardOn");
  }

  $scope.goGift = function(){
    var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'yes'
    };

    if ($scope.account.giftlink != ''){
      $cordovaInAppBrowser.open($scope.account.giftlink, '_blank', options).then(
        function(event) {
          // success
        }
      ).catch(
        function(event) {
          // error
        }
      );
    }
  };

  // $localStorage['first_help'] = true;

  if ($localStorage['first_help']) {
    $localStorage['first_help'] = false;
    $scope.doBubble();
  }

  $scope.addToCalendar = function(){
    var title = '';
    if ($scope.Properties.active == 'wedding'){
      $scope.notes = $scope.account.yours + " & " + $scope.account.yours2 + " is getting married using WebWedMobile.";
      title = 'Web Wed Marriage';
    }else{
      $scope.notes = 'WebWedMobile Special Moment scheduled.';
      title = 'Web Wed Special Moments';
    }
    var MS_PER_MINUTE = 60000;
    var startDate = new Date($scope.account.computed);
    if ($scope.Properties.active == 'wedding'){
      var endDate = new Date(startDate.getTime() + 55 * MS_PER_MINUTE);
    }else{
      var endDate = new Date(startDate.getTime() + 30 * MS_PER_MINUTE);
    }

    $cordovaCalendar.createEvent({
      title: title,
      location: 'WebWedMobile App',
      notes: $scope.notes,
      startDate: startDate,
      endDate: endDate
    }).then(function (result) {
      $rootScope.showPopup('Awesome!', 'We have added this event to your event calendar.');
    }, function (err) {
      $rootScope.showPopup('Hmm', 'We could not add this event to your calendar, please try again later.');
    });
  };

  $scope.showBroadcast = function(){
    $ionicHistory.nextViewOptions({
      disableAnimate: true
    });

    if ($scope.Properties.active == 'wedding'){
      $state.go('app.home-viewer');
    }else{
      $state.go('app.home-special_moments-viewer');
    }
  }

  // if (window.socket != null){
  //   window.socket.disconnect();
  // }

  

  $scope.sendChatMessage = function(){
    var body = $scope.messages.text;
    console.log('Sending: ' + body);
    $scope.messages.text = '';
    if (body != ''){
      $('.messages').append('<li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
      $scope.session.signal({
        type: 'chat',
        data: body
      }, function(error){
        if (!error){
          $scope.messages.text = '';
        }
      });
    }
  };

  $scope.sendChatMessageViaEnter = function(body){
    console.log('Sending: ' + body);
    $scope.messages.text = '';
    if (body != ''){
      $('.messages').append('<li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
      $scope.session.signal({
        type: 'chat',
        data: body
      }, function(error){
        if (!error){
          $scope.messages.text = '';
        }
      });
    }
  };

  $("#message_text").on("keypress", function(event){
    if (event.keyCode === 13) {
      var body = $scope.messages.text;
      $scope.messages.text = '';
      $scope.sendChatMessageViaEnter(body);
    }
  });

  keepscreenon.enable();

  $scope.apikey = $localStorage['apikey'];

  if ($scope.Properties.active == 'wedding'){
    $scope.event = $localStorage['wedding'];
  }else{
    $scope.event = $localStorage['special_moment'];
  }

  $scope.session_id = $scope.event['session_id'];
  $scope.token_id = $scope.event['token_id'];
  $scope.role = $scope.event['role'];

  $scope.session = TB.initSession( $scope.apikey, $scope.session_id );

  $scope.session.on('signal:chat', function(event){
    var body = event.data;
    if (event.from.connectionId != $scope.session.connection.connectionId){
      $('.messages').append('<li class="message left appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
    }
  });

  $scope.event = {
    eventReady: false
  };

  $scope.session.on('signal:moment_stopped', function(event){
    // $('#weddingButton').attr('disabled', 'disabled');
    var scope = angular.element($('#weddingButton')).scope();
    scope.event.eventReady = false;
    scope.$apply();
  });

  $scope.session.on('signal:wedding_stopped', function(event){
    // $('#weddingButton').attr('disabled', 'disabled');
    var scope = angular.element($('#weddingButton')).scope();
    scope.event.eventReady = false;
    scope.$apply();
  });

  $scope.session.on('signal:wedding_started', function(event){
    // $('#weddingButton').attr('disabled', 'disabled');
    console.log('received event');
    var scope = angular.element($('#weddingButton')).scope();
    scope.event.eventReady = true;
    scope.$apply();
  });

  $scope.session.on({
    'streamCreated': function( event ){
      if ($scope.Properties.active != 'wedding'){ 
        var scope = angular.element($('#weddingButton')).scope();
        scope.event.eventReady = true;
        scope.$apply();
      }else{
        if (event.stream.connection.data == 'officiant'){
          console.log('received event');
          var scope = angular.element($('#weddingButton')).scope();
          scope.event.eventReady = true;
          scope.$apply();
        }
      }
    },

    'streamDestroyed': function( event ){
      if ($scope.Properties.active != 'wedding'){ 
        var scope = angular.element($('#weddingButton')).scope();
        scope.event.eventReady = false;
        scope.$apply();
      }
    }
  });
  
  $scope.$on('$ionicView.enter', function(){
    // Anything you can think of
    // $scope.session.disconnect();
    // $scope.session.connect($scope.token_id, function(){ });
  });

  $scope.session.connect($scope.token_id, function(){ });

  // $scope.$on('$ionicView.beforeLeave', function(){
  //   $scope.session.disconnect();
  // });

  $scope.share = function(){
    if ($scope.Properties.active == 'wedding'){
      var shareText = $scope.account.yours + ' and ' + $scope.account.yours2 + ' are tying the knot. Join in for their live  wedding ' + $scope.account.longDate + ' at ' + $scope.account.longTime + '. ' + $scope.account.ceremonylink;
    }else{
      var shareText = 'A WebWedMobile Special Moment is taking place. Tune into this special moment by following this link ' + $scope.account.ceremonylink;
    }
    $cordovaSocialSharing
    .share(shareText) // Share via native share sheet
    .then(function(result) {
      //$rootScope.showPopup('Yay!', 'We have successfully shared your ceremony link!');
    }, function(err) {
      $rootScope.showPopup('Hmm', 'We were unable to share your ceremony link.');
    });
  }

  $scope.joinWeddingViaButton = function(){
    if ($scope.event.eventReady){
      $scope.showBroadcast();
    }else{
      $rootScope.showPopup('Hold on', 'The event does not seem to be available just yet, the heart will turn white when the event is available.');
    }
  };

} // end of controller function
]
);
