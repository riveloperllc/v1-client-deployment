angular.module('WebWedApp')

.controller('LoginHasAccountCtrl',
['$rootScope', '$scope', '$state', '$ionicHistory', '$ionicPopup', '$cordovaInAppBrowser', 'ENDPOINT_LIST', 'AuthService', '$localStorage', '$ionicLoading',
function($rootScope, $scope, $state, $ionicHistory, $ionicPopup, $cordovaInAppBrowser, ENDPOINT_LIST, AuthService, $localStorage, $ionicLoading) {

  $scope.login = {
    email: '',
    password: ''
  };

  $scope.once = false;
  $scope.twice = false;

  if ($localStorage['email'] != '' && $localStorage['email'] != undefined){
    $scope.login.email = $localStorage['email'];
    $scope.once = true;
  }

  if ($localStorage['password'] != undefined && $localStorage['password'] != ''){
    $scope.login.password = $localStorage['password'];
    $scope.twice = true;
  }

  $rootScope.showPopup = function(title, content){

    if (window.popupOK != undefined){
      window.popupOK();
    }

      $rootScope.alertPopup = $ionicPopup.alert({
          template: '' +
              '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
              '<h1>' + title + '</h1>' +
              '<p>' + content + '</p>' +
              '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
          cssClass: 'customPopup',
          scope: this
      });

      $rootScope.alertPopup.then(function(res) {
          console.log('alertPopup');
      });

      $rootScope.popupOK = function() {
          console.log('ok');
          $rootScope.alertPopup.close();
      }

      window.popupOK = $rootScope.popupOK;
  }

  $scope.doLogin = function() {
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0,
      hideOnStateChange: true
    });
    AuthService.login($scope.login.email, $scope.login.password).then(
      function(response) {
        if (response['errorCode'] == null || response['errorCode'] == undefined){
          $localStorage['email'] = $scope.login.email;
          $state.go('login.account-choice');
        }else{
          $ionicLoading.hide();
          $rootScope.showPopup('Hmm', 'That email or password did not match our records. Please double check your email, and password then try again.');
        }
      },
      function(error) {
        $ionicLoading.hide();
        console.log('error');
        $rootScope.showPopup('Hmm', 'That email or password did not match our records. Please double check your email, and password then try again.');
      }
    );
  };

  $scope.forgotPassword = function(){
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    AuthService.forgotPassword($scope.login.email).then(
      function(response){
        $ionicLoading.hide();
        $rootScope.showPopup('Awesome', 'Please check your email for instructions on resetting your password.');
      },
      function(error){
        $rootScope.showPopup('Hmm', 'An error occurred, please try again later.');
      }
    );
  };

  if ($scope.once && $scope.twice && $localStorage['deeplink'] == true){
    $scope.doLogin();
  }

  $scope.goNeedAccount = function() {
    var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'yes'
    };

    $cordovaInAppBrowser.open(ENDPOINT_LIST.SIGNUP_PAGE, '_blank', options).then(
      function(event) {
        // success
      }
    ).catch(
      function(event) {
        // error
        $rootScope.showPopup('Hmm', 'An error occurred, please try again later.');
      }
    );
  };

  $scope.goBack = function() {
    $ionicHistory.goBack();
  };

} // end of controller function
]
);
