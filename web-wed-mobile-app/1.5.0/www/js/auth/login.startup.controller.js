angular.module('WebWedApp')

    .controller('LoginStartupCtrl',
        ['$rootScope', '$scope', '$state', '$ionicPopup', '$ionicHistory', '$cordovaInAppBrowser', 'ENDPOINT_LIST', '$localStorage',
            function($rootScope, $scope, $state, $ionicPopup, $ionicHistory, $cordovaInAppBrowser, ENDPOINT_LIST, $localStorage) {
                // Clear navigation history
                $ionicHistory.clearHistory();

                $rootScope.showPopup = function(title, content){

                  if (window.popupOK != undefined){
                    window.popupOK();
                  }

                    $rootScope.alertPopup = $ionicPopup.alert({
                        template: '' +
                            '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
                            '<h1>' + title + '</h1>' +
                            '<p>' + content + '</p>' +
                            '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
                        cssClass: 'customPopup',
                        scope: this
                    });

                    $rootScope.alertPopup.then(function(res) {
                        console.log('alertPopup');
                    });

                    $rootScope.popupOK = function() {
                        console.log('ok');
                        $rootScope.alertPopup.close();
                    }

                    window.popupOK = $rootScope.popupOK;
                }

                $scope.goWeddingPartyLogin = function() {
                    $state.go('login.has-account');
                }

                $scope.goInvitedGuest = function() {
                    $state.go('login.invited-guest');
                }

                if ($localStorage['email'] == null || $localStorage['email'] == undefined){
                    $localStorage['email'] = '';
                }

                if ($localStorage['code'] == null || $localStorage['code'] == undefined){
                    $localStorage['code'] = '';
                }
            } // end of controller function
        ]
    );
