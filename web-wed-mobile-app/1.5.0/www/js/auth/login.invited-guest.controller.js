angular.module('WebWedApp')

.controller('LoginInvitedGuestCtrl',
['$rootScope', '$scope', '$state', '$ionicHistory', 'ENDPOINT_LIST', 'AuthService', '$ionicPopup', '$http', '$localStorage', '$ionicLoading',
function($rootScope, $scope, $state, $ionicHistory, ENDPOINT_LIST, AuthService, $ionicPopup, $http, $localStorage, $ionicLoading) {

  $rootScope.showPopup = function(title, content){

    if (window.popupOK != undefined){
      window.popupOK();
    }

      $rootScope.alertPopup = $ionicPopup.alert({
          template: '' +
              '<button class="button button-small button-icon icon ion-ios-close-empty" ng-click="popupOK()"></button>' +
              '<h1>' + title + '</h1>' +
              '<p>' + content + '</p>' +
              '<button class="button button-full button-positive" onClick="window.popupOK()">OK</button>',
          cssClass: 'customPopup',
          scope: this
      });

      $rootScope.alertPopup.then(function(res) {
          console.log('alertPopup');
      });

      $rootScope.popupOK = function() {
          console.log('ok');
          $rootScope.alertPopup.close();
      }

      window.popupOK = $rootScope.popupOK;
  }

  $scope.login = {
    email: '',
    pincode: ''
  };

  if ($localStorage['code'] != "" && $localStorage['code'] != '00000' && $localStorage['code'] != undefined){
    $scope.login.pincode = $localStorage['code'];
  }

  $scope.doLogin = function() {
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    AuthService.loginGuest($scope.login.email, $scope.login.pincode).then(
      function(response) {
        $ionicLoading.hide();
        if (response['errorCode'] == null || response['errorCode'] == undefined){
          $localStorage['active'] = $localStorage['type_of_event'];
          $state.go('app.home-guest');
        }else{
          $rootScope.showPopup('Hmm', 'That email or password did not match our records. Please double check your email, and password then try again.');
        }
      },
      function(error) {
        $ionicLoading.hide();
        $rootScope.showPopup('Hmm', 'That email or password did not match our records. Please double check your email, and password then try again.');
      }
    );
  };

  $scope.resendMyCode = function() {
    if ($scope.login.email == null || $scope.login.email == "" || $scope.login.email == undefined){
      $rootScope.showPopup('Missing Email', 'Please make sure you enter your email in order for us to locate you in our records.');
      return;
    }
    var apiURL = ENDPOINT_LIST.RESEND_CODE_API;
    var postData = 'email=' + encodeURIComponent($scope.login.email);

    var config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
      }
    }

    $http.post(apiURL, postData, config).success(function(response){
      if (response['errorCode'] == null || response['errorCode'] == undefined){
        $rootScope.showPopup('Awesome!', 'Please check your email for the Wedding Code.');
      }else{
        $rootScope.showPopup('Incorrect Email or Password', 'That email or password did not match our records. Please double check your email, and password then try again.');
      }
    }).error(function(error){
      $rootScope.showPopup('Error Occurred', 'An error occurred when sending you your wedding code, please try again later.');
    });
  };



  $scope.goBack = function() {
    $localStorage['code'] = '';
    $ionicHistory.goBack();
  };

  window.validate = function(){

  };

  $scope.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

} // end of controller function
]
);
