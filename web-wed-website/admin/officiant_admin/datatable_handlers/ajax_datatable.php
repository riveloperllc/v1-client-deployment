<?php

if (!isset($_GET['basepath']) && $_GET['basepath']){ exit; }
require_once('../../common/connection.php');
require_once('../../common/utility_functions.php');

// SQL server connection information
$sql_details = array(
  'user' => $username_NWHL,
  'pass' => $password_NWHL,
  'db'   => $database_NWHL,
  'host' => $hostname_NWHL
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Easy set variables
*/

// DB table to use
$table = 'weddings';
// $assign_to = $_POST['assign_to'];


// Table's primary key
// $primaryKey = 'user_id';
$primaryKey = 'id';
$where = "assigned_officiant_uid =" . $_GET['wwmid'];


// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  array( 'db' => $primaryKey, 'dt' => 0 ),
  array( 'db' => 'type_of_wedding', 'dt' => 1, 'formatter' => function( $d, $row ){
    $productInstance = getProductInstanceFromId($d);
    if (empty($productInstance)){
      return htmlentities('Unknown Product or Service.');
    }

    return htmlentities($productInstance['name']);
  }),
  array( 'db' => 'event_date', 'dt' => 2 ),
  array( 'db' => 'uid',  'dt' => 3, 'formatter' => function( $d, $row ) {
    $userInstance = getUserInstanceFromUid($d);
    if (empty($userInstance)){
      return htmlentities('Pending Acceptence');
    }
    return htmlentities($userInstance['name']);
  }),
  array( 'db' => 'yours_uid', 'dt' => 4, 'formatter' => function( $d, $row ) {
    $userInstance = getUserInstanceFromUid($d);
    if (empty($userInstance)){
      return htmlentities('Pending Acceptence');
    }
    return htmlentities($userInstance['name']);
  }),
  array( 'db' => 'brief_description', 'dt' => 5, 'formatter' => function( $d, $row ) {
    return htmlentities($d);
  })
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* If you just want to use the basic configuration for DataTables with PHP
* server-side, there is no need to edit below this line.
*/

require( '../../common/ssp.class.php' );

echo json_encode(
SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where)
);

?>
