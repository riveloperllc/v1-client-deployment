<?php
session_start();
require_once '../common/connection.php';
require_once '../common/config.php';

if (!isset($_SESSION['wwm_minister_id'])) {
  //header('Location: index.php');
  exit;
}

if (paymentIsCurrent($pdoDB, $_SESSION['wwm_minister_id'], 1)) {
  //header('Location: index.php');
  exit;
}

if (!minister_signup_completed($pdoDB, $_SESSION['wwm_minister_id'])) {
  //header('Location: continued_signup.php');
  exit;
}

$officiant_data = getProfile($pdoDB, $_SESSION['wwm_minister_id'], 0);
// print_r($_POST);
// var_dump($officiant_data['subscription_id']);

if (isset($_POST['submit'], $_POST['product_id'], $_POST['stripe_token'], $_POST['billing_name'], $_POST['card_number'], $_POST['card_expiration'], $_POST['card_cvc'])) {

  $product_id = intval($_POST['product_id']);
  $amount = floatval(getProductPricing($pdoDB, intval(@$_POST['product_id']), 0) * 100); //Times by 100 for Stripe. E.g $50 = 5000 pennies.
  $stripe_token = $_POST['stripe_token'];
  $billing_name = $_POST['billing_name'];

  $error = '';
  $success = '';

  if($officiant_data['subscription_id'] == 0) {
    $subscription_id = false;
    $amount = floatval(9.99)*100;
  } else {
    $subscription_id = $officiant_data['subscription_id'];
  }

  if( !$subscription_id ) {
    // create a new customer if our current user doesn't have one
    $customer = \Stripe\Customer::create(array(
      "source" => $stripe_token,
      "plan" => "officiant",
      "email" => strip_tags(trim($officiant_data['email'])),
      "trial_end" => strtotime("+90 days")
    ));

    $subscription_id = $customer->id;
    $prepared = $pdoDB->prepare("UPDATE `ministers` SET `subscription_id` = ? WHERE `minister_id` = ?");
    $prepared->execute(array($subscription_id, $_SESSION['wwm_minister_id']));

  }
  if( $subscription_id ) {
      /*try{
        $charge = \Stripe\Charge::create(
          array(
            'amount' => intval($amount),
            'currency' => 'usd',
            'customer' => $subscription_id,
            'metadata' => array(
              'session_info' => json_encode(@$_SESSION),
              'product_id' => $product_id
            )
          )
        );*/

        // $txn_id = ($charge->balance_transaction);
        $txn_id = ('TRIAL PAYMENT');

        $prepared = $pdoDB->prepare("INSERT INTO `payments` VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)");
        $prepared->execute(array($_SESSION['wwm_minister_id'], 0, ($amount / 100), $txn_id, $product_id, strtotime('now'), ''));
        $success = '<div class="alert alert-success">You are successfully subscribed to the officiant plan. You will be billed in 90 days. You will be redirected in just a moment... <script>setTimeout(function(){ location.href="index.php"; }, 2000)</script></div>';

      /*} catch(\Stripe\Error\Card $e) {
        $error = '<div class="alert alert-danger">' . $e->getMessage() . '</div>';
      }*/


  ////}




  ////header('Location: '.$siteurl.'officiant_admin/index.php');
  //exit;
}
}

$page_title = 'Add Minister - ';
include '../header.php';
?>
<style>
.error {
  color: red;
  display:none;
}

.errorField {
  border: 1px solid red;
}
</style>
<div class="form_main">
  <div class="form_main_inside">
    <div class="login">
      <h1>Payment Information</h1>
      <?php echo @$error; echo @$success; ?>
      <label style="width: auto;" class="control-label" for="discount">Discount Code</label>
      <input type="text" id="discount" name="discount" class="form-control" onchange="checkCode(this.value);">
      <p id="isValid"></p>
      <form name="make_payment_form" id="make_payment_form" method="post" action="make_payment.php">
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">
        Stripe.setPublishableKey('<?php echo STRIPE_PUBLIC_TOKEN; ?>');
        </script>
        <p>We do require a credit card to be on file. A $1 authorization fee will be charged at this time. After the 3 month initial period has concluded your current balance will be: $<span id="displayed_amount"><?php echo getProductPricing($pdoDB, intval(@$_GET['product']), 0); ?></span> monthly. This is a subscription based pricing, auto-renewal is activated. Contact WebWed Mobile to disable auto-renewal.</p><br/>
        <div class="form-wrapper">
          <div class="alert-wrapper"></div>
          <div class="card-wrapper"></div>
          <input type="hidden" name="product_id" value="<?php echo intval(@$_GET['product']); ?>" />
          <input type="hidden" name="stripe_token" value="" />

          <div class="alert alert-danger" id="402error" style="display:none;">The card information provided was invalid. Please double check and try again. Contact <a href="mailto:info@WebWedMobile.com">info@webwedmobile.com</a> if you believe this is a mistake. </div>

          <div class="form-group col-xs-12 col-md-12">
            <span id="name_error" class="error">Incorrect billing name:</span>
            <label style="width:auto;" for="billing_name" class="control-label">Billing Name</label>
            <input type="text" class="form-control" required="required" data-stripe="billing_name" placeholder="Billing Name" onClick="if (!window.edited){ if(confirm('Are you sure you want to edit your billing name?')){ event.preventDefault(); $('#billing_name').val(''); window.edited = true; }else{ $('#billing_name').blur(); window.edited = false; } }" name="billing_name" id="billing_name" value="<?php echo htmlentities(getProfile($pdoDB, $_SESSION['wwm_minister_id'], 0)['name']); ?>"/>
          </div>

          <div class="form-group col-xs-12 col-md-12">
            <span id="number_error" class="error">Incorrect card number:</span>
            <label style="width:auto;" for="card_number" class="control-label">Card Number</label>
            <input type="text" class="form-control" size="20" max-length="20" required="required" data-stripe="number" placeholder="Card Number" name="card_number" id="card_number"/>
          </div>

          <div class="form-group col-xs-12 col-md-6">
            <label style="width:auto;" for="card_expiration" class="control-label">Exp</label>
            <input type="text" size="9" max-length="9" min-length="9" required="required" class="form-control" placeholder="Expiration Date (MM/YYYY)" name="card_expiration" id="card_expiration"/>
          </div>
          <div class="form-group col-xs-12 col-md-6">
            <label style="width:auto;" for="card_cvc" class="control-label">CCV</label>
            <input type="number" size="4" max-length="4" min-length="4" required="required" data-stripe="cvc" class="form-control" placeholder="CCV" name="card_cvc" id="card_cvc" value="" placeholder=""/>
          </div>
          <input type="hidden" id="discount_amount" name="discount_amount"/>
          <input type="hidden" id="discount_id" name="discount_id"/>
          <input type="hidden" data-stripe="exp-month" id="exp-month" />
          <input type="hidden" data-stripe="exp-year" id="exp-year" />
          <input type="submit" id="submit" name="submit" value="Submit Payment" class="btn btn-primary" />

        </div>
      </form>
    </div>
  </div>
</div>

<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>
function checkCode(code) {
  $.ajax({
       url: '../api/v1/api.php?cmd=validate_discount',
       type: 'POST',
       data: {'code': code} ,
       cache: false,
       success:function(data) {
         var data = JSON.parse(data);
         if (!data.success) { //If fails
           if (data.error) { //Returned if any error from api.php
             $('#isValid').removeClass('hidden').text(data.error); //Throw relevant error
             $('#discount_amount').val('none');
           }
         }
         else {
             $('#isValid').removeClass('hidden').text(data.success); //If successful, than throw a success message
             $('#discount_amount').val(data.amount);
             $('#discount_id').val(data.discount_id);
             var currentAmount = $('#displayed_amount').text();
             var newAmount = currentAmount - data.amount;
             $('#displayed_amount').html(newAmount+ "(<strong>Discount of $"+data.amount+" applied</strong>)");
           }
         }
   });
};

$(function() {
  $( "#minister_add_form" ).validate({
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email:true
      },
      username: {
        required: true
      },
      password: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name."
      },
      email: {
        required: "Please enter email address.",
        email:"Please enter valid email address."
      },
      username: {
        required: "Please enter username."
      },
      password: {
        required: "Please enter password."
      }
    }

  });

  var card = new Card({
    form: 'form', // *required*
    container: '.card-wrapper', // *required*
    formSelectors: {
      numberInput: 'input#card_number', // optional — default input[name="number"]
      expiryInput: 'input#card_expiration', // optional — default input[name="expiry"]
      cvcInput: 'input#card_cvc', // optional — default input[name="cvc"]
      nameInput: 'input#billing_name' // optional - defaults input[name="name"]
    },
    placeholders: {
      number: '•••• •••• •••• ••••',
      name: '<?php echo htmlentities(getProfile($pdoDB, $_SESSION['wwm_minister_id'], 0)['name']); ?>',
      expiry: '••/••••',
      cvc: '•••'
    },
    width: 200, // optional — default 350px
    debug: true // optional - default false
  });
});
window.gottoken = false;
$(document).ready(function(){
  $("#make_payment_form").submit(function(e){

    if (!Stripe.card.validateCardNumber($("#card_number").val())){
      event.preventDefault();
      alert('Please make sure to enter a valid card number and try again.');
      return;
    }

    var expMonth = $("#card_expiration").val().split(' / ')[0];
    var expYear = $("#card_expiration").val().split(' / ')[1];

    if (!Stripe.card.validateExpiry(expMonth, expYear)){
      event.preventDefault();
      alert('Please make sure to enter a valid expiration date and try again.');
      return;
    }

    if (!Stripe.card.validateCVC($("#card_cvc").val())){
      event.preventDefault();
      alert('Please make sure to enter a valid CVC code and try again.');
      return;
    }

    $("#exp-month").val(expMonth);
    $("#exp-year").val(expYear);

    if (!window.gottoken){

      // Grab the form:
      var $form = $(this);

      // Disable the submit button to prevent repeated clicks:
      $('#submit').prop('disabled', true);
      // Request a token from Stripe:
      e.preventDefault();
      Stripe.card.createToken($form, function(status, response){
        $('#submit').prop('disabled', false);

        if (status == 402){
          $('#402error').show();
        }

        if (response.error){
          $("#" + response.error.param + "_error").show();
          $("input[data-stripe='" + response.error.param + "']").addClass('errorField');
          $("input[data-stripe='" + response.error.param + "']").click(function(){
            $("#" + response.error.param + "_error").hide();
            $("input[data-stripe='" + response.error.param + "']").removeClass('errorField');
          });
        }else{
          $("input[name='stripe_token']").val(response.id);
          window.gottoken = true;
          $('#submit').click();
          return true;
        }
      });

      //Prevent the form from being submitted:
      event.preventDefault();
    }
  });
});

</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
<?php 
include '../footer.php'; 
?>
