<?php
session_start();

if(!isset($_SESSION['wwm_minister_id'])){
	header("location:login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');

$wedding_data = minister_manage_wedding($pdoDB, $_GET['id']);

if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_minister_id'])){
	header("Location: make_payment.php");
	exit;
}

if (!minister_signup_completed($pdoDB, $_SESSION['wwm_minister_id'])){
	header("Location: continued_signup.php");
	exit;
}

$pending_approval = minister_pending_approval($pdoDB, $_SESSION['wwm_minister_id']);

if ( $pending_approval['admin_approved'] == "0"){
	header("Location: manage_account.php?pending=true");
	exit;
}

$page_title = "Manage Wedding - ";
include('../header.php'); ?>
<br/>
<br/>
<br/>
<br/>
<div class="container">
	<div class="form_main_inside">
		<div class="">
			<h1><span>Manage Wedding</span></h1>
			<form action="minister_upload_marriage_cert.php" method="POST">
				<div class="form-group col-xs-12 col-md-6">
					<label for="type_of_wedding" class="control-label">Type of wedding</label>
					<input type="text" class="form-control" id="type_of_wedding" name="type_of_wedding" value="<?php echo getProductNameFromId($wedding_data['type_of_wedding']); ?>" readonly>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="event_code" class="control-label">Event Code</label>
					<input type="text" class="form-control" id="event_code" name="event_code" value="<?php echo $wedding_data['event_code']; ?>" readonly>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="event_date" class="control-label">Scheduled Time</label>
					<input type="text" class="form-control" id="event_date" name="event_date" value="<?php echo $wedding_data['event_date']; ?>" readonly>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="registers_name" class="control-label">Spouse A</label>
					<input type="text" class="form-control" id="registers_name" name="registers_name" value="<?php echo getUsersNameFromUid($wedding_data['uid']); ?>" readonly>
				</div>
				<div class="form-group col-xs-12 col-md-12">
					<label for="spouses_name" class="control-label">Spouse B</label>
					<input type="text" class="form-control" id="spouses_name" name="spouses_name" value="<?php echo getUsersNameFromUid($wedding_data['yours_uid']); ?>" readonly>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="witness_first_name" class="control-label">Witness A</label>
					<input type="text" class="form-control" id="witness_first_name" name="witness_first_name" value="<?php echo getUsersNameFromUid($wedding_data['first_witness_uid']); ?>" readonly>
				</div>
				<div class="form-group col-xs-12 col-md-6">
					<label for="type_of_wedding" class="control-label">Witness B</label>
					<input type="text" class="form-control" id="type_of_wedding" name="type_of_wedding" value="<?php echo getUsersNameFromUid($wedding_data['second_witness_uid']); ?>" readonly>
				</div>
				<div class="form-group col-xs-12 col-md-6 hidden">
					<label for="wedding_id" class="control-label">Hidden Wedding ID</label>
					<input type="text" class="form-control" id="wedding_id" name="wedding_id" value="<?php echo @$_GET['id']; ?>" readonly>
				</div>
				<!-- <div class="form-group col-xs-12 col-md-12">
					<button type="submit" class="btn btn-default">Upload Marriage Certificate</button>
				</div> -->
			</form>
		</div>
	</div>
</div>


<!-- pattern -->

<?php include('../footer.php'); ?>
