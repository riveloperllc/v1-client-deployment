<?php
session_start();
require_once '../common/connection.php';
require_once '../common/config.php';

if (!isset($_SESSION['wwm_minister_id'])) {
    header('Location: index.php');
    exit;
}

if (isset($_POST['submit'], $_POST['religion'], $_POST['bio'], $_POST['performing_states'], $_POST['starttime'], $_POST['endtime'], $_POST['willingness'],$_POST['address'],$_POST['city'],$_POST['zip'],$_POST['birthdate_day'], $_POST['birthdate_month'], $_POST['birthdate_year'])) {
    switch (strtolower($_POST['religion'])){
			case 'christian':
				$_POST['religion'] = 'Christian';
				break;
			case 'catholic':
				$_POST['religion'] = 'Catholic';
				break;
			case 'hindu':
				$_POST['religion'] = 'Hindu';
				break;
			case 'buddhist':
				$_POST['religion'] = 'Buddhist';
				break;
			case 'jewish':
				$_POST['religion'] = 'Jewish';
				break;
			case 'religious':
				$_POST['religion'] = 'Religious';
				break;
			case 'nonreligious':
				$_POST['religion'] = 'Non-Religious';
				break;
			case 'interfaith':
				$_POST['religion'] = 'Interfaith';
				break;
			case 'other':
				$_POST['religion'] = 'Other';
				break;
			default:
				$_POST['religion'] = 'Other';
				break;
		}
		$_POST['bio'] = base64_encode(htmlentities($_POST['bio']));

		$_POST['performing_states'] = json_encode($_POST['performing_states']);


		$_POST['willingness'] = intval($_POST['willingness']);

		if(!boolval(preg_match("/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i", $_POST['starttime']))){
			$_POST['starttime'] = '10:00 AM';
		}

		if(!boolval(preg_match("/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i", $_POST['endtime']))){
			$_POST['endtime'] = '8:00 PM';
		}

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `religious_affiliation` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['religion'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `bio` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['bio'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `gender` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['gender'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `phone` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['phone'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `address` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['address'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `city` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['city'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `state` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['state'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `zip` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['zip'], $_SESSION['wwm_minister_id']));

    $_POST['birthdate'] = @$_POST['birthdate_month'] . '/' . @$_POST['birthdate_day'] . '/' . @$_POST['birthdate_year'];
		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `birthdate` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['birthdate'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `performing_states` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['performing_states'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `perform_willingness` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['willingness'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `available_times` = ? WHERE `minister_id` = ?");
		$prepared->execute(array(json_encode(array("start" => $_POST['starttime'], "end" => $_POST['endtime'])), $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `perform_willingness` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['willingness'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `profile_completed` = 1 WHERE `minister_id` = ?");
		$prepared->execute(array($_SESSION['wwm_minister_id']));

    if(isset($_FILES['file_upload']))
    {
    	$error = false;
    	$files = array();
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
    	$uploaddir = 'upload/';
    	foreach($_FILES as $file)
    	{

    		$t = move_uploaded_file($file['tmp_name'], $uploaddir .basename($timestamp .$file['name']));
    		if($t)
    		{
    			chown(getcwd() . '/'.$uploaddir .basename($timestamp .$file['name']), 'jcbanks');
    			$files[] = $uploaddir . date('Y-m-d') .$file['name'];

                $prepared = $pdoDB->prepare("UPDATE `ministers` SET `photo` = ? WHERE `minister_id` = ?");
                $prepared->execute(array($timestamp.$file['name'], $_SESSION['wwm_minister_id']));

    		}
    		else
    		{
    		    $error = true;
    		}
    	}
    	$data = ($error) ? array('error' => 'There was an error updating your profile picture. Please try again later') : array('files' => $files);
    }
    //print_r($_FILES);
    //print_r($data);
		header("Location: make_payment.php?product=4");
		exit;
}

$page_title = 'Update Account - ';
include '../header.php';

$us_state_abbrevs_names = array(
	'AL'=>'ALABAMA',
	'AZ'=>'ARIZONA',
	'AR'=>'ARKANSAS',
	'CA'=>'CALIFORNIA',
	'CO'=>'COLORADO',
	'CT'=>'CONNECTICUT',
	'DE'=>'DELAWARE',
	'FL'=>'FLORIDA',
	'GA'=>'GEORGIA',
	'ID'=>'IDAHO',
	'IL'=>'ILLINOIS',
	'IN'=>'INDIANA',
	'IA'=>'IOWA',
	'KS'=>'KANSAS',
	'KY'=>'KENTUCKY',
	'LA'=>'LOUISIANA',
	'ME'=>'MAINE',
	'MD'=>'MARYLAND',
	'MA'=>'MASSACHUSETTS',
	'MI'=>'MICHIGAN',
	'MN'=>'MINNESOTA',
	'MS'=>'MISSISSIPPI',
	'MO'=>'MISSOURI',
	'MT'=>'MONTANA',
	'NE'=>'NEBRASKA',
	'NV'=>'NEVADA',
	'NH'=>'NEW HAMPSHIRE',
	'NJ'=>'NEW JERSEY',
	'NM'=>'NEW MEXICO',
	'NY'=>'NEW YORK',
	'NC'=>'NORTH CAROLINA',
	'ND'=>'NORTH DAKOTA',
	'OH'=>'OHIO',
	'OK'=>'OKLAHOMA',
	'OR'=>'OREGON',
	'PA'=>'PENNSYLVANIA',
	'RI'=>'RHODE ISLAND',
	'SC'=>'SOUTH CAROLINA',
	'SD'=>'SOUTH DAKOTA',
	'TN'=>'TENNESSEE',
	'TX'=>'TEXAS',
	'UT'=>'UTAH',
	'VT'=>'VERMONT',
	'VI'=>'VIRGIN ISLANDS',
	'VA'=>'VIRGINIA',
	'WA'=>'WASHINGTON',
	'WV'=>'WEST VIRGINIA',
	'WI'=>'WISCONSIN',
	'WY'=>'WYOMING'
);
?>

<script>
window.states = [

	{	"abbreviation": "AL" },
	{	"abbreviation": "AZ" },
	{	"abbreviation": "AR" },
	{	"abbreviation": "CA" },
	{	"abbreviation": "CO" },
	{	"abbreviation": "CT" },
	{	"abbreviation": "DE" },
	{	"abbreviation": "FL" },
	{	"abbreviation": "GA" },
	{	"abbreviation": "ID" },
	{	"abbreviation": "IL" },
	{	"abbreviation": "IN" },
	{	"abbreviation": "IA" },
	{	"abbreviation": "KS" },
	{	"abbreviation": "KY" },
	{	"abbreviation": "LA" },
	{	"abbreviation": "ME" },
	{	"abbreviation": "MD" },
	{	"abbreviation": "MA" },
	{	"abbreviation": "MI" },
	{	"abbreviation": "MN" },
	{	"abbreviation": "MS" },
	{	"abbreviation": "MO" },
	{	"abbreviation": "MT" },
	{	"abbreviation": "NE" },
	{	"abbreviation": "NV" },
	{ "abbreviation": "NH" },
	{ "abbreviation": "NJ" },
	{ "abbreviation": "NM" },
	{ "abbreviation": "NY" },
	{ "abbreviation": "NC" },
	{ "abbreviation": "ND" },
	{ "abbreviation": "OH" },
	{ "abbreviation": "OK" },
	{ "abbreviation": "OR" },
	{ "abbreviation": "PA" },
	{ "abbreviation": "RI" },
	{ "abbreviation": "SC" },
	{ "abbreviation": "SD" },
	{ "abbreviation": "TN" },
	{ "abbreviation": "TX" },
	{ "abbreviation": "UT" },
	{ "abbreviation": "VT" },
	{ "abbreviation": "VA" },
	{ "abbreviation": "WA" },
	{ "abbreviation": "WV" },
	{ "abbreviation": "WI" },
	{ "abbreviation": "WY" } ];
</script>
<br/>
<br/>
<br/>
<br/>
<style>
  #stateItem {
    width: 5%;
    float:left;
  }
</style>
<div class="container page-content">
	<div class="row">
			<h1>Officiant Profile</h1>
			<p>Please complete your WebWed profile before you can accept weddings.</p><br/>
			<form name="minister_add_form" id="minister_add_form" method="post" action="<?php echo SITEURL; ?>officiant_admin/continued_signup.php" enctype="multipart/form-data">
					<!-- left column -->
					<div class="col-md-3">
						<div class="text-center">
						<h6>Avatar</h6>
						<input type="file" name="file_upload" id="photo"/>
						</div>
					</div>

					<!-- edit form column -->
					<div class="col-md-9 personal-info">

						<div class="form-group col-xs-12 col-md-12">
							<label for="religious_affiliation" class="control-label">What is your religious affiliation?</label>
							<select name="religion" class="form-control">
								<option value="other" selected="seleted">Other – N/A</option>
								<option value="christianity">Christian</option>
								<option value="catholic">Catholic</option>
								<option value="jewish">Jewish</option>
								<option value="hindu">Hindu</option>
								<option value="buddhist">Buddhist</option>
								<option value="religious">Religious</option>
								<option value="nonreligious">Non–Religious</option>
								<option value="interfaith">Interfaith</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-md-12">
							<label for="bio" class="control-label">Enter a short biography about yourself.</label>
							<textarea class="form-control" id="bio" name="bio" required></textarea>
						</div>

						<div class="form-group col-xs-12 col-md-12">
							<label for="phone" class="control-label">What is your phone number?</label>
							<input type="text" class="form-control" id="phone" name="phone" required>
						</div>

						<div class="form-group col-xs-12 col-md-12">
							<label for="gender" class="control-label">What is your gender?</label>
							<select name="gender" class="form-control">
                <option value="0">Male</option>
                <option value="1">Female</option>
              </select>
						</div>

						<div class="form-group col-xs-12 col-md-12">
							<label for="address" class="control-label">What is your address?</label>
							<input type="text" class="form-control" id="address" name="address" required>
						</div>

						<div class="form-group col-xs-12 col-md-4">
							<label for="city" class="control-label">What is your city?</label>
							<input type="text" class="form-control" id="city" name="city" required>
						</div>

						<div class="form-group col-xs-12 col-md-4">
							<label for="state" class="control-label">What is your state?</label>
							<!-- <input type="text" class="form-control" id="state" maxlength="2" name="state" required> -->
              <select class="form-control" name="state" id="state" class="form-control" style="margin:0;">
                <?php
                foreach($us_state_abbrevs_names as $state) { ?>
                  <option value="<?php echo $state; ?>" ><?= $state ?></option>
                  <?php
                }
                ?>
              </select>
						</div>

						<div class="form-group col-xs-12 col-md-4">
							<label for="zip" class="control-label">What is your zip code?</label>
							<input type="text" maxlength="5" class="form-control" id="zip" name="zip" required>
						</div>


						<div class="form-group col-xs-4 col-md-4">
							<label for="birthdate_month" class="control-label">What is your birthday?</label>
							<select class="form-control" id="birthdate_month" name="birthdate_month" required>
                <option value="">Month</option>
                <option value="1">January</option>
                <option value="2">Febuary</option>
                <option value="3">March</option>
                <option value="4">April</option>
                <option value="5">May</option>
                <option value="6">June</option>
                <option value="7">July</option>
                <option value="8">August</option>
                <option value="9">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
              </select>
						</div>
						<div class="form-group col-xs-4 col-md-4">
              <label for="birthdate_day" class="control-label"><span style="opacity:0;">a</span></label>
							<!-- <input type="number" placeholder="01" max-length="2" min-length="1" class="form-control" id="birthdate_day" name="birthdate_day" required /> -->
              <select class="form-control" id="birthdate_day" name="birthdate_day" required="required">
                <option value="">Day</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>

              </select>
						</div>
						<div class="form-group col-xs-4 col-md-4">
              <label for="birthdate_year" class="control-label"><span style="opacity:0;">a</span></label>
							<!-- <input type="number" placeholder="1980" max-length="4" min-length="4" class="form-control" id="birthdate_year" name="birthdate_year" required /> -->
              <select name="birthdate_year" id="birthdate_year" class="form-control" required="required">
                <option value="">Year</option>
                <?php for ($i=1998; $i > 1933; $i--) {
                  echo '<option value="'.$i.'">'.$i.'</option>';
                } ?>
              </select>
						</div>

						<div class="form-group col-xs-12 col-md-12">
							<script>
								function availableInAllStates(){
									$('input[name="performing_states[]"]').each(function(e){ if ($($('input[name="performing_states[]"]')[e]).attr('checked') == 'checked') { $($('input[name="performing_states[]"]')[e]).removeAttr('checked'); return; } $($('input[name="performing_states[]"]')[e]).attr('checked', 'checked'); });
								}

								function availableAllDays(){
									$('input[name="day[]"]').each(function(e){ if ($($('input[name="day[]"]')[e]).attr('checked') == 'checked') { $($('input[name="day[]"]')[e]).removeAttr('checked'); return; } $($('input[name="day[]"]')[e]).attr('checked', 'checked'); });
								}

								$(document).ready(function(){
									availableAllDays();
								});
							</script>

							<label>What states are you licensed to perform in?</label>

							<script>
							$(document).ready(function() {
								for (var i = 0; i < window.states.length; i++) {
									var abbr = window.states[i].abbreviation;
									var value = i;

									var html = '<div id="stateItem"><input type="checkbox"  name="performing_states[]" value="'+abbr+'" id="'+abbr+'"/><span id="stateName">'+abbr+'</span></div>';
									$('#stateItems').append(html);
								}
							});
							</script>
							<div id="stateItems">
								<div id="stateItem">
									<input type="checkbox" id="performInAllStates" onClick="availableInAllStates();"/> <span>All States</span>
								</div>
							</div>
						</div>
						<div class="form-group col-xs-12 col-md-6">
							<label>What time are you available from? *</label>
							<!-- <input type="text" class="form-control" name="starttime" id="starttime" required="required"/> -->
              <select name="starttime" id="starttime" class="form-control" required="required">
							<option value="12:00 AM">12:00 AM</option>
							<option value="12:30 AM">12:30 AM</option>
							<option value="1:00 AM">1:00 AM</option>
							<option value="1:30 AM">1:30 AM</option>
							<option value="2:00 AM">2:00 AM</option>
							<option value="2:30 AM">2:30 AM</option>
							<option value="3:00 AM">3:00 AM</option>
							<option value="3:30 AM">3:30 AM</option>
							<option value="4:00 AM">4:00 AM</option>
							<option value="4:30 AM">4:30 AM</option>
							<option value="5:00 AM">5:00 AM</option>
							<option value="5:30 AM">5:30 AM</option>
							<option value="6:00 AM">6:00 AM</option>
							<option value="6:30 AM">6:30 AM</option>
							<option value="7:00 AM">7:00 AM</option>
							<option value="7:30 AM">7:30 AM</option>
							<option value="8:00 AM" selected="selected">8:00 AM</option>
							<option value="8:30 AM">8:30 AM</option>
							<option value="9:00 AM">9:00 AM</option>
							<option value="9:30 AM">9:30 AM</option>
							<option value="10:00 AM">10:00 AM</option>
							<option value="10:30 AM">10:30 AM</option>
							<option value="11:00 AM">11:00 AM</option>
							<option value="11:30 AM">11:30 AM</option>
							<option value="12:00 PM">12:00 PM</option>
							<option value="12:30 PM">12:30 PM</option>
							<option value="1:00 PM">1:00 PM</option>
							<option value="1:30 PM">1:30 PM</option>
							<option value="2:00 PM">2:00 PM</option>
							<option value="2:30 PM">2:30 PM</option>
							<option value="3:00 PM">3:00 PM</option>
							<option value="3:30 PM">3:30 PM</option>
							<option value="4:00 PM">4:00 PM</option>
							<option value="4:30 PM">4:30 PM</option>
							<option value="5:00 PM">5:00 PM</option>
							<option value="5:30 PM">5:30 PM</option>
							<option value="6:00 PM">6:00 PM</option>
							<option value="6:30 PM">6:30 PM</option>
							<option value="7:00 PM">7:00 PM</option>
							<option value="7:30 PM">7:30 PM</option>
							<option value="8:00 PM">8:00 PM</option>
							<option value="8:30 PM">8:30 PM</option>
							<option value="9:00 PM">9:00 PM</option>
							<option value="9:30 PM">9:30 PM</option>
							<option value="10:00 PM">10:00 PM</option>
							<option value="10:30 PM">10:30 PM</option>
							<option value="11:00 PM">11:00 PM</option>
							<option value="11:30 PM">11:30 PM</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-md-6">
							<label>What time are you available until? *</label>
							<!-- <input type="text" class="form-control" name="endtime" id="endtime" required="required"/> -->
              <select name="endtime" id="endtime" class="form-control" required="required">
							<option value="12:00 AM">12:00 AM</option>
							<option value="12:30 AM">12:30 AM</option>
							<option value="1:00 AM">1:00 AM</option>
							<option value="1:30 AM">1:30 AM</option>
							<option value="2:00 AM">2:00 AM</option>
							<option value="2:30 AM">2:30 AM</option>
							<option value="3:00 AM">3:00 AM</option>
							<option value="3:30 AM">3:30 AM</option>
							<option value="4:00 AM">4:00 AM</option>
							<option value="4:30 AM">4:30 AM</option>
							<option value="5:00 AM">5:00 AM</option>
							<option value="5:30 AM">5:30 AM</option>
							<option value="6:00 AM">6:00 AM</option>
							<option value="6:30 AM">6:30 AM</option>
							<option value="7:00 AM">7:00 AM</option>
							<option value="7:30 AM">7:30 AM</option>
							<option value="8:00 AM">8:00 AM</option>
							<option value="8:30 AM">8:30 AM</option>
							<option value="9:00 AM">9:00 AM</option>
							<option value="9:30 AM">9:30 AM</option>
							<option value="10:00 AM">10:00 AM</option>
							<option value="10:30 AM">10:30 AM</option>
							<option value="11:00 AM">11:00 AM</option>
							<option value="11:30 AM">11:30 AM</option>
							<option value="12:00 PM">12:00 PM</option>
							<option value="12:30 PM">12:30 PM</option>
							<option value="1:00 PM">1:00 PM</option>
							<option value="1:30 PM">1:30 PM</option>
							<option value="2:00 PM">2:00 PM</option>
							<option value="2:30 PM">2:30 PM</option>
							<option value="3:00 PM">3:00 PM</option>
							<option value="3:30 PM">3:30 PM</option>
							<option value="4:00 PM">4:00 PM</option>
							<option value="4:30 PM">4:30 PM</option>
							<option value="5:00 PM">5:00 PM</option>
							<option value="5:30 PM">5:30 PM</option>
							<option value="6:00 PM">6:00 PM</option>
							<option value="6:30 PM">6:30 PM</option>
							<option value="7:00 PM">7:00 PM</option>
							<option value="7:30 PM">7:30 PM</option>
							<option value="8:00 PM" selected="selected">8:00 PM</option>
							<option value="8:30 PM">8:30 PM</option>
							<option value="9:00 PM">9:00 PM</option>
							<option value="9:30 PM">9:30 PM</option>
							<option value="10:00 PM">10:00 PM</option>
							<option value="10:30 PM">10:30 PM</option>
							<option value="11:00 PM">11:00 PM</option>
							<option value="11:30 PM">11:30 PM</option>
							</select>
						</div>
						<div class="form-group col-xs-12 col-md-12" id="startDayItems">


							<div id="startDayItem">

								<input type="checkbox"  name="day[]" id="startDay" value="0"/>
								<span id="startDayTitle">Monday</span>
							</div>

							<div id="startDayItem">

								<input type="checkbox"  name="day[]" id="startDay" value="1"/>
								<span id="startDayTitle">Tuesday</span>
							</div>

							<div id="startDayItem">

								<input type="checkbox"  name="day[]" id="startDay" value="2"/>
								<span id="startDayTitle">Wednesday</span>
							</div>

							<div id="startDayItem">

								<input type="checkbox"  name="day[]" id="startDay" value="3"/>
								<span id="startDayTitle">Thursday</span>
							</div>

							<div id="startDayItem">

								<input type="checkbox"  name="day[]" id="startDay" value="4"/>
								<span id="startDayTitle">Friday</span>
							</div>

							<div id="startDayItem">

								<input type="checkbox"  name="day[]" id="startDay" value="5"/>
								<span id="startDayTitle">Saturday</span>
							</div>

							<div id="startDayItem">

								<input type="checkbox"  name="day[]" id="startDay" value="6"/>
								<span id="startDayTitle">Sunday</span>
							</div>
						</div>
						<div class="form-group col-xs-12 col-md-12">
							<label for="officiant_willing" class="control-label">Are you willing to perform same-sex, and Proxy weddings? *</label>
							<div class="radio willing_radio">
								<label><input type="radio" class="form-control" name="willingness" id="willingness" value="1" checked="checked"/>Yes</label>
								<label><input type="radio" class="form-control" name="willingness" id="willingness" value="0"/>No</label>
							</div>
						</div>
						<div class="form-group col-xs-12 col-md-6 hidden">
							<label for="wwm_minister_id" class="control-label">Hidden Officiant ID</label>
							<input type="text" class="form-control" id="wwm_minister_id" name="wwm_minister_id" value="<?php echo $_SESSION['wwm_minister_id']; ?>" readonly>
						</div>
						<div class="error"></div>

						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-8">
								<input type="submit" name="submit" value="Next" class="btn btn-primary" />
							</div>
						</div>

					</div>
			</form>
	</div>
</div>
<br/>
<br/>
<br/>

<script language="javascript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>


$(function() {

  $.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg != value;
  }, "Please select an item above.");

  $("#phone").mask("(999) 999-9999");
	$("#zip").mask("99999");

	$( "#minister_add_form" ).validate({
		rules: {
			performing_states:{
				required: true,
				minlength: 1
			},
			name: {
				required: true
			},
			email: {
				required: true,
				email:true
			},
      birthdate_day: {
        required: true,
        valueNotEquals: ""
      },
      birthdate_month: {
        required: true,
        valueNotEquals: ""
      },
      birthdate_year: {
        required: true,
        valueNotEquals: ""
      },
			username: {
				required: true
			},
			"day[]": {
				required: true
			},
		},
		messages: {
			name: {
				required: "Please enter name."
			},
			email: {
				required: "Please enter email address.",
				email:"Please enter valid email address."
			},
			username: {
				required: "Please enter username."
			},
			"day[]": {
				required: "Please select days.",
				minlength: 1
			},
			"performing_states[]":{
				required: 'Select the States !!'
			}
		}

	});
});
</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
<?php include '../footer.php'; ?>
