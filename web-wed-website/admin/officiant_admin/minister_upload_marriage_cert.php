<?php
ob_start();
session_start();
require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');
$page_name=basename($_SERVER['PHP_SELF']);
$page_title = "Upload SIGNED MARRIAGE CERTIFICATE - ";
// var_dump($_POST);
if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_minister_id'])){
  header("Location: make_payment.php");
  exit;
}

if (!minister_signup_completed($pdoDB, $_SESSION['wwm_minister_id'])){
  header("Location: continued_signup.php");
  exit;
}

include('../header.php');
?>

<div class="form_main">
  <div class="form_main_inside">


    <div class="login">

      <?php
      if (isset($_POST['wedding_id'])){
        $wedding_id = $_POST['wedding_id'];
      }else if (isset($_GET['wedding_id'])){
        $wedding_id = $_POST['wedding_id'];
      }else{
        header("Location: m_assign_weddings.php?errorNoWedding");
        exit;
      }


      $weddingInstance = getWeddingInstanceFromId($wedding_id);

      if (empty($weddingInstance)){
        header("Location: m_assign_weddings.php?errorEventExist");
        exit;
      }
      $user_id = $weddingInstance['uid'];
      $registersUserInstance = getUserInstanceFromUid($user_id);

      if (empty($registersUserInstance)){
        header("Location: m_assign_weddings.php?errorUserExist");
        exit;
      }

      if (isset($_POST['submit_marriage_certificate']) && $_POST['submit_marriage_certificate']!='')
      {

        if(isset($_FILES['marriage_certificate']['name']) && $_FILES['marriage_certificate']['name']!='')
        {

          $allowedExts = array(
            "pdf",
            "doc",
            "docx"
          );

          $file_name	= $_FILES['marriage_certificate']['name'];
          $org_file_name	= $file_name;
          $ext = end((explode(".", $file_name)));

          if (!in_array($ext, $allowedExts))
          {
            $_SESSION['message'] = "Please upload certificate in pdf, doc or docx format only.";
            $_SESSION['flag'] = "2";
            header("location: minister_upload_marriage_cert.php?wedding_id=".$wedding_id);
            exit();
          }


          $date = strtotime(date("YmdHis"));

          $certpremarriage_path = 'upload/certmarriage';
          $file_location =  $certpremarriage_path.'/'.$date.'_'.$user_id.'.'.$ext;

          if (move_uploaded_file($_FILES['marriage_certificate']['tmp_name'], $file_location))
          {
            $query_string_upmec = "update weddings set marriage_certificate='".$file_location."' where id = ".$wedding_id."";
            mysql_query($query_string_upmec);

            if(isset($_POST['old_certificate']) && $_POST['old_certificate']!='')
            {
              $unlink_file=  $_POST['old_certificate'];
              @unlink($unlink_file);
            }

            $mailto = $registersUserInstance['email'];

            $file = $root_url ."officiant_admin/"."$file_location";

            $file_size = filesize($file);
            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
            $uid = md5(uniqid(time()));
            $separator = md5(time());
            $eol = PHP_EOL;

            $host = $_SERVER['HTTP_HOST'];
            $attch1 = "noreply@$host";

            // main header (multipart mandatory)
            $headers = "From: \" WebWedMobile \"<noreply@$host>\n";
            $headers .= "MIME-Version: 1.0" . $eol;
            $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
            $headers .= "Content-Transfer-Encoding: 7bit";

            $message = "Please find attached SIGNED MARRIAGE CERTIFICATE.";
            // message
            $body = "This is a MIME encoded message." . $eol . $eol;
            $body .= "--" . $separator . $eol;
            $body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
            $body .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
            $body .= $message . $eol . $eol;

            // attachment
            $file = $file_name;
            $body .= "--" . $separator . $eol;
            $body .= "Content-Type: application/octet-stream; name=\"" . $file . "\"" . $eol;
            $body .= "Content-Transfer-Encoding: base64" . $eol;
            $body .= "Content-Disposition: attachment" . $eol . $eol;
            $body .= $content . $eol . $eol;
            $body .= "--" . $separator . "--";

            //SEND Mail
            $subject = "SIGNED MARRIAGE CERTIFICATE";
            email($mailto, $subject, $body, $headers);

            $_SESSION['message'] = "SIGNED MARRIAGE CERTIFICATE uploaded and sent successfully to registered email address of couple.";
            $_SESSION['flag'] = "1";
            //header("location: manage_users.php"); exit;
          }

        }
        else
        {
          $_SESSION['message'] = "Please Upload SIGNED MARRIAGE CERTIFICATE.";
          $_SESSION['flag'] = "2";
        }
      }


      if(isset($_SESSION['message']) && $_SESSION['message']!='')
      {
        if($_SESSION['flag'] == 1)
        {
          echo "<div class='alert-box success1'>".$_SESSION['message']."</div>";
        }
        if($_SESSION['flag'] == 2)
        {
          echo "<div class='alert-box error1'><span>error: </span>".$_SESSION['message']."</div>";
        }
        unset($_SESSION['flag']);
        unset($_SESSION['message']);
      }
      ?>

      <h1>MARRIAGE CERTIFICATE</h1>

      <form name="form_upload_pmec" id="form_upload_pmec" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="user_id" value="<?php echo $_POST['user_id'];?>" />

        <p><strong>Spouses Name:</strong> <?php echo $registersUserInstance['name']; ?>, <?php echo getUsersNameFromUid($weddingInstance['yours_uid']); ?></p>
        <p><strong>Wedding Date:</strong> <?php echo $weddingInstance['event_date'] . ' ' . $weddingInstance['event_time']; ?></p>
        <?php
        if(isset($weddingInstance['marriage_certificate']) && $weddingInstance['marriage_certificate'] != '')
        { ?>
          <input type="hidden" name="old_certificate" value="<?php echo $weddingInstance["marriage_certificate"]; ?>" />
          <a href="download.php?filename=<?php echo $weddingInstance["marriage_certificate"]; ?>" class="btn btn-success col-md-12">
            Download MARRIAGE CERTIFICATE
          </a>
          <?php
        }
        ?>

        <input type="file" name="marriage_certificate" id="marriage_certificate" class="inputfile inputfile-3" />
        <label for="marriage_certificate"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload Signed Marriage Certificate</span></label>

        <input type="submit" name="submit_marriage_certificate"  value="Submit" class="btn btn-blue" />
        <a class="btn col-md-12" href="m_assign_weddings.php">Go Back</a>
      </p>
    </form>
  </div>
</div>
</div>

<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>

// $('#when_married').datetimepicker({minDate: 0, format: 'Y/m/d g:i A',formatTime: 'A g:i'});

// $('#witness_first_dob').datetimepicker({timepicker:false,maxDate: 0, format: 'Y/m/d'});

// $('#witness_second_dob').datetimepicker({timepicker:false,maxDate: 0, format: 'Y/m/d'});


$(function() {

});
</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->

<?php include('../footer.php'); ?>
