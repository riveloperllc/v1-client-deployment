<?php // You need to add server side validation and better error handling here

require_once('../common/connection.php');
require_once('../common/config.php');

$data = array();

if(isset($_GET['files']))
{
	$error = false;
	$files = array();
    $date = new DateTime();
    $timestamp = $date->getTimestamp();
	$uploaddir = 'upload/';
	foreach($_FILES as $file)
	{
		if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($timestamp .$file['name'])))
		{
			$files[] = $uploaddir . date('Y-m-d') .$file['name'];

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `photo` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($timestamp.$file['name'], $_POST['minister_id']));

		}
		else
		{
		    $error = true;
		}
	}
	$data = ($error) ? array('error' => 'There was an error updating your profile picture. Please try again later') : array('files' => $files);
}
else
{
    if (isset($_POST['religion'], $_POST['officiant_bio'], $_POST['performing_states'], $_POST['starttime'], $_POST['endtime'], $_POST['willingness'],$_POST['address'],$_POST['city'],$_POST['zip'],$_POST['birthdate'])) {

        switch (strtolower($_POST['religion'])){
                case 'not listed':
                    $_POST['religion'] = 'Not Listed';
                    break;
                case 'christianity':
                    $_POST['religion'] = 'Christianity';
                    break;
                case 'catholic':
                    $_POST['religion'] = 'Catholic';
                    break;
                case 'judaism':
                    $_POST['religion'] = 'Judaism';
                    break;
                case 'islam':
                    $_POST['religion'] = 'Islam';
                    break;
                case 'other':
                    $_POST['religion'] = 'Other';
                    break;
                default:
                    $_POST['religion'] = 'Not Listed';
                    break;
            }

            $_POST['officiant_bio'] = base64_encode(htmlentities($_POST['officiant_bio']));
            $_POST['performing_states'] = json_encode($_POST['performing_states']);
            $_POST['willingness'] = intval($_POST['willingness']);

            if(!boolval(preg_match("/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i", $_POST['starttime']))){
                $_POST['starttime'] = '10:00 AM';
            }

            if(!boolval(preg_match("/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i", $_POST['endtime']))){
                $_POST['endtime'] = '8:00 PM';
            }

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `religious_affiliation` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['religion'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `bio` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['officiant_bio'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `phone` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['phone'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `gender` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['gender'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `address` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['address'], $_POST['wwm_minister_id']));


            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `city` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['city'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `state` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['state'], $_POST['wwm_minister_id']));


            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `zip` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['zip'], $_POST['wwm_minister_id']));


            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `birthdate` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['birthdate'], $_POST['wwm_minister_id']));


            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `performing_states` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['performing_states'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `perform_willingness` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['willingness'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `available_times` = ? WHERE `minister_id` = ?");
            $prepared->execute(array(json_encode(array("start" => $_POST['starttime'], "end" => $_POST['endtime'])), $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `perform_willingness` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['willingness'], $_POST['wwm_minister_id']));

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `profile_completed` = 1 WHERE `minister_id` = ?");
            $prepared->execute(array($_POST['wwm_minister_id']));

    }
	$data = array('success' => 'Your details have been updated successfully', 'formData' => $_POST);
}

echo json_encode($data);

?>
