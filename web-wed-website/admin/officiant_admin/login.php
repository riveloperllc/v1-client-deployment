<?php
ob_start();
session_start();

if (isset($_SESSION['wwm_minister_id'])){ header("Location: m_assign_weddings.php"); exit; }
require_once('../common/connection.php');
require_once('../common/config.php');

if(isset($_POST['submit']))
{
	$email = $_POST['email'];
	$password = md5($_POST['password']);

	$sql_min = "SELECT * FROM ministers WHERE email='$email' and password='$password' LIMIT 1";
	$resultstatus = mysql_query($sql_min);
	$countstatus = mysql_num_rows($resultstatus);

	if($countstatus > 0)
	{
		$data = mysql_fetch_array($resultstatus);
		$_SESSION['wwm_minister_id'] = $data['minister_id'];
		header("location:m_assign_weddings.php");
	}
	else
	{
		$msg = "<div class='alert alert-danger'>Your username or password is incorrect. Please try again.</br/></br/></div>";
	}

}
?>
<?php  $page_name=basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Login -";
include('../header.php'); ?>
<br/>
<br/>
<br/>
<div class="form_main">
    <div class="login">
        <h1>Officiant Login</h1>
		<?php
			if (isset($msg)) {
				echo $msg;
			}
		?>
        <form class="form-horizontal text-center" role="form" name="min_login_form" id="min_login_form" method="post" action="">
            <input type="email" placeholder="Email" class="input_text" id="email" name="email" value=""/><br/><br/>
            <input type="password" placeholder="Password" class="input_text" id="password" name="password" value=""/><br/><br/>
			<input type="submit" name="submit" value="Login" class="btn btn-primary" id="login-btn" /><br/><br/>
            <a class="btn" href="<?php echo SITEURL; ?>officiant_admin/signup.php">Sign up</a><br/><br/>
            <a class="" href="<?php echo SITEURL; ?>officiant_admin/forgot_password.php">Forgot password</a>
        </form>
    </div>
</div>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>


$(function() {
	$( "#min_login_form" ).validate({
		rules: {
			username: {
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			username: {
				required: "Please enter your username."
			},
			password: {
				required: "Please enter your password."
			}
		}
});
});
</script>
<?php include('../footer.php'); ?>
