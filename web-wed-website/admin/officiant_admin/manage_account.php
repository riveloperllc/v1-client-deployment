<?php
session_start();
if(!isset($_SESSION['wwm_minister_id'])){
	header("location:login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');

if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_minister_id'])){
  header("Location: make_payment.php");
	exit;
}

if (!minister_signup_completed($pdoDB, $_SESSION['wwm_minister_id'])){
	header("Location: continued_signup.php");
	exit;
}

if(isset($_FILES['file_upload']))
{
	$error = false;
	$files = array();
    $date = new DateTime();
    $timestamp = $date->getTimestamp();
	$uploaddir = 'upload/';
	foreach($_FILES as $file)
	{

		$t = move_uploaded_file($file['tmp_name'], $uploaddir .basename($timestamp .$file['name']));
		if($t)
		{
			chown(getcwd() . '/'.$uploaddir .basename($timestamp .$file['name']), 'jcbanks');
			$files[] = $uploaddir . date('Y-m-d') .$file['name'];

            $prepared = $pdoDB->prepare("UPDATE `ministers` SET `photo` = ? WHERE `minister_id` = ?");
            $prepared->execute(array($timestamp.$file['name'], $_SESSION['wwm_minister_id']));

		}
		else
		{
		    $error = true;
		}
	}
	$data = ($error) ? array('error' => 'There was an error updating your profile picture. Please try again later') : array('files' => $files);
}

if (sizeof($_POST) > 0){
	$msg = '<div class="alert alert-success">Your account has been updated successfully.</div>';
}

if (isset($_POST['officiant_pass'], $_POST['officiant_pass_c'])){
	if ($_POST['officiant_pass'] != $_POST['officiant_pass_c']){
		$msg = '<div class="alert alert-danger">Your passwords do not match, please double check and try again.</div>';
	}else{
		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `password` = ? WHERE `minister_id` = ?");
		$prepared->execute(array(md5($_POST['officiant_pass']), $_SESSION['wwm_minister_id']));
	}
}
if (isset($_POST['religion'], $_POST['officiant_bio'], $_POST['performing_states'], $_POST['starttime'], $_POST['endtime'], $_POST['willingness'],$_POST['address'],$_POST['city'],$_POST['zip'],$_POST['birthdate_day'], $_POST['birthdate_month'], $_POST['birthdate_year'])) {

		switch (strtolower($_POST['religion'])){
			case 'christian':
				$_POST['religion'] = 'Christian';
				break;
			case 'catholic':
				$_POST['religion'] = 'Catholic';
				break;
			case 'hindu':
				$_POST['religion'] = 'Hindu';
				break;
			case 'buddhist':
				$_POST['religion'] = 'Buddhist';
				break;
			case 'jewish':
				$_POST['religion'] = 'Jewish';
				break;
			case 'religious':
				$_POST['religion'] = 'Religious';
				break;
			case 'nonreligious':
				$_POST['religion'] = 'Non-Religious';
				break;
			case 'interfaith':
				$_POST['religion'] = 'Interfaith';
				break;
			case 'other':
				$_POST['religion'] = 'Other';
				break;
			default:
				$_POST['religion'] = 'Other';
				break;
		}
		$_POST['bio'] = base64_encode(htmlentities($_POST['officiant_bio']));

		$_POST['performing_states'] = json_encode($_POST['performing_states']);


		$_POST['willingness'] = intval($_POST['willingness']);

		if(!boolval(preg_match("/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i", $_POST['starttime']))){
			$_POST['starttime'] = '10:00 AM';
		}

		if(!boolval(preg_match("/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i", $_POST['endtime']))){
			$_POST['endtime'] = '8:00 PM';
		}

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `religious_affiliation` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['religion'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `bio` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['bio'], $_SESSION['wwm_minister_id']));

		// $prepared = $pdoDB->prepare("UPDATE `ministers` SET `gender` = ? WHERE `minister_id` = ?");
		// $prepared->execute(array($_POST['gender'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `phone` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['officiant_phone'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `address` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['address'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `city` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['city'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `state` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['state'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `zip` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['zip'], $_SESSION['wwm_minister_id']));

		$_POST['birthdate'] = @$_POST['birthdate_month'] . '/' . @$_POST['birthdate_day'] . '/' . @$_POST['birthdate_year'];
		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `birthdate` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['birthdate'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `performing_states` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['performing_states'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `perform_willingness` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['willingness'], $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `available_times` = ? WHERE `minister_id` = ?");
		$prepared->execute(array(json_encode(array("start" => $_POST['starttime'], "end" => $_POST['endtime'])), $_SESSION['wwm_minister_id']));

		$prepared = $pdoDB->prepare("UPDATE `ministers` SET `perform_willingness` = ? WHERE `minister_id` = ?");
		$prepared->execute(array($_POST['willingness'], $_SESSION['wwm_minister_id']));


}

$officiant_data = getProfile($pdoDB, $_SESSION['wwm_minister_id'], 0);

$performing_states_abbr = array("AL","AZ","AR","CA","CO","CT","DE","FL","GA","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");
$performing_states_select = json_decode($officiant_data['performing_states']);


$us_state_abbrevs_names = array(
	'AL'=>'ALABAMA',
	'AZ'=>'ARIZONA',
	'AR'=>'ARKANSAS',
	'CA'=>'CALIFORNIA',
	'CO'=>'COLORADO',
	'CT'=>'CONNECTICUT',
	'DE'=>'DELAWARE',
	'FL'=>'FLORIDA',
	'GA'=>'GEORGIA',
	'ID'=>'IDAHO',
	'IL'=>'ILLINOIS',
	'IN'=>'INDIANA',
	'IA'=>'IOWA',
	'KS'=>'KANSAS',
	'KY'=>'KENTUCKY',
	'LA'=>'LOUISIANA',
	'ME'=>'MAINE',
	'MD'=>'MARYLAND',
	'MA'=>'MASSACHUSETTS',
	'MI'=>'MICHIGAN',
	'MN'=>'MINNESOTA',
	'MS'=>'MISSISSIPPI',
	'MO'=>'MISSOURI',
	'MT'=>'MONTANA',
	'NE'=>'NEBRASKA',
	'NV'=>'NEVADA',
	'NH'=>'NEW HAMPSHIRE',
	'NJ'=>'NEW JERSEY',
	'NM'=>'NEW MEXICO',
	'NY'=>'NEW YORK',
	'NC'=>'NORTH CAROLINA',
	'ND'=>'NORTH DAKOTA',
	'OH'=>'OHIO',
	'OK'=>'OKLAHOMA',
	'OR'=>'OREGON',
	'PA'=>'PENNSYLVANIA',
	'RI'=>'RHODE ISLAND',
	'SC'=>'SOUTH CAROLINA',
	'SD'=>'SOUTH DAKOTA',
	'TN'=>'TENNESSEE',
	'TX'=>'TEXAS',
	'UT'=>'UTAH',
	'VT'=>'VERMONT',
	'VI'=>'VIRGIN ISLANDS',
	'VA'=>'VIRGINIA',
	'WA'=>'WASHINGTON',
	'WV'=>'WEST VIRGINIA',
	'WI'=>'WISCONSIN',
	'WY'=>'WYOMING'
);

$page_title = "Update Account - ";
include('../header.php'); ?>
<br>
<br>
<br>
<style>
	#stateItem {
		width: 5%;
		float:left;
	}
</style>
<div class="container">
	<h1>Manage Account</h1>
	<hr>
	<div class="row">
		<?php
		if(isset($_GET['pending']) && $_GET['pending'] == 'true') {
			?>
			<div class="alert alert-warning alert-dismissable" id="success_alert">
				<a class="panel-close close" data-dismiss="alert">×</a>
				<p id="success">Thank you for registering. After we review your profile we will notify you via email that your profile has been approved and you available in the officiant gallery.</p>
			</div>
			<?php
		}
		?>
		<?php
		if(isset($_GET['submit']) && $_GET['submit'] == 'true') {
			?>
			<div class="alert alert-success alert-dismissable" id="success_alert">
				<a class="panel-close close" data-dismiss="alert">×</a>
				<p id="success">Your details have been updated successfully.</p>
			</div>
			<?php
		}
		?>

		<form method="post" action="<?php echo $siteurl; ?>officiant_admin/manage_account.php" enctype="multipart/form-data">
			<!-- left column -->
			<div class="col-md-3">
				<div class="text-center">
					<?php if($officiant_data['photo'] === '') {
						?>
						<img src="<?php echo $siteurl.'/officiant_admin/upload/default_avatar_male.jpg'?>" class="avatar img-circle img-responsive" alt="avatar">
						<?php
					} else {
						?>
						<img src="<?php echo $siteurl.'/officiant_admin/upload/'.$officiant_data['photo']; ?>" class="avatar img-circle img-responsive" alt="avatar">
						<?php
					}
					?>

					<h6>Change Photo</h6>
					<input type="file" name="file_upload" id="file_upload" class="form-control">
				</div>
				<?php

				$date=getNextPaymentDate($pdoDB, $_SESSION['wwm_minister_id']);
				echo  "<b>Next Billing Date : ".$date."</b>";
				?>
			</div>

			<!-- edit form column -->

			<div class="col-md-9 personal-info">
				<div class="row">
					<h2>Personal Info</h2>
					<?php echo @$msg; ?>
					<div class="form-group col-xs-12 col-md-6">
						<label for="officiant_name" class="control-label">Name</label>
						<input type="text" class="form-control" id="officiant_name" name="officiant_name" value="<?php echo $officiant_data['name']; ?>" readonly>
					</div>
					<div class="form-group col-xs-12 col-md-6">
						<label for="officiant_name" class="control-label">Phone</label>
						<input type="text" class="form-control" id="officiant_phone" name="officiant_phone" value="<?php echo $officiant_data['phone']; ?>" readonly>
					</div>
					<div class="form-group col-xs-12 col-md-12">
						<label for="officiant_email" class="control-label">Email</label>
						<input type="text" class="form-control" id="officiant_email" name="officiant_email" value="<?php echo $officiant_data['email']; ?>" readonly>
					</div>
					<div class="form-group col-xs-12 col-md-6">
						<label for="officiant_pass" class="control-label">New Password</label>
						<input type="password" class="form-control" id="officiant_pass" name="officiant_pass">
					</div>
					<div class="form-group col-xs-12 col-md-6">
						<label for="officiant_pass_c" class="control-label">Confirm Password</label>
						<input type="password" class="form-control" id="officiant_pass_c" name="officiant_pass_c">
					</div>
					<div class="form-group col-xs-12 col-md-12">
						<label for="religious_affiliation" class="control-label">Religious Affiliation</label>
						<select name="religion" class="form-control">
							<option value="other" <?php if($officiant_data['religious_affiliation'] == 'other') {echo 'selected';} ?>>Other – N/A</option>
							<option value="christian" <?php if($officiant_data['religious_affiliation'] == 'Christian') {echo 'selected';} ?>>Christian</option>
							<option value="catholic" <?php if($officiant_data['religious_affiliation'] == 'Catholic') {echo 'selected';} ?>>Catholic</option>
							<option value="jewish" <?php if($officiant_data['religious_affiliation'] == 'Jewish') {echo 'selected';} ?>>Jewish</option>
							<option value="hindu" <?php if($officiant_data['religious_affiliation'] == 'Hindu') {echo 'selected';} ?>>Hindu</option>
							<option value="buddhist" <?php if($officiant_data['religious_affiliation'] == 'Buddhist') {echo 'selected';} ?>>Buddhist</option>
							<option value="religious" <?php if($officiant_data['religious_affiliation'] == 'Religious') {echo 'selected';} ?>>Religious</option>
							<option value="nonreligious" <?php if($officiant_data['religious_affiliation'] == 'NonReligious') {echo 'selected';} ?>>Non–Religious</option>
							<option value="intefaith" <?php if($officiant_data['religious_affiliation'] == 'Interfaith') {echo 'selected';} ?>>Interfaith</option>
						</select>
					</div>
					<div class="form-group col-xs-12 col-md-12">
						<label for="officiant_bio" class="control-label">Bio</label>
						<textarea class="form-control" id="officiant_bio" name="officiant_bio"><?php echo base64_decode($officiant_data['bio']); ?></textarea>
					</div>


					<div class="form-group col-xs-12 col-md-12">
						<label for="address" class="control-label">Address</label>
						<input type="text" class="form-control" id="address" name="address" value="<?php echo $officiant_data['address']; ?>" >
					</div>

					<div class="form-group col-xs-12 col-md-4">
						<label for="city" class="control-label">City</label>
						<input type="text" class="form-control" id="city" name="city" value="<?php echo $officiant_data['city']; ?>">
					</div>

					<div class="form-group col-xs-12 col-md-4">
						<label for="state" class="control-label">State</label>
						<!-- <input type="text" class="form-control" id="state" maxlength="2" name="state" value="<?php echo $officiant_data['state']; ?>"> -->
						<select class="form-control" name="state" id="state" class="form-control" style="margin:0;">
						<?php
						foreach($us_state_abbrevs_names as $state) { ?>
							<option value="<?php echo $state; ?>" <?php if ($state == $officiant_data['state']){ ?> selected="selected" <?php } ?> ><?= $state ?></option>
							<?php
						}
						?>
						</select>
					</div>

					<div class="form-group col-xs-12 col-md-4">
						<label for="zip" class="control-label">Zip</label>
						<input type="text" maxlength="5" class="form-control" id="zip" name="zip" value="<?php echo $officiant_data['zip']; ?>">
					</div>


					<div class="form-group col-xs-4 col-md-4">
						<label for="birthdate_month" class="control-label">Birthday Month</label>
						<select class="form-control" id="birthdate_month" name="birthdate_month">
							<option value="1" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 1){ ?> selected="selected" <?php } ?>>January</option>
							<option value="2" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 2){ ?> selected="selected" <?php } ?>>Febuary</option>
							<option value="3" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 3){ ?> selected="selected" <?php } ?>>March</option>
							<option value="4" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 4){ ?> selected="selected" <?php } ?>>April</option>
							<option value="5" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 5){ ?> selected="selected" <?php } ?>>May</option>
							<option value="6" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 6){ ?> selected="selected" <?php } ?>>June</option>
							<option value="7" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 7){ ?> selected="selected" <?php } ?>>July</option>
							<option value="8" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 8){ ?> selected="selected" <?php } ?>>August</option>
							<option value="9" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 9){ ?> selected="selected" <?php } ?>>September</option>
							<option value="10" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 10){ ?> selected="selected" <?php } ?>>October</option>
							<option value="11" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 11){ ?> selected="selected" <?php } ?>>November</option>
							<option value="12" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 12){ ?> selected="selected" <?php } ?>>December</option>
						</select>
					</div>
					<div class="form-group col-xs-4 col-md-4">
						<label for="birthdate_day" class="control-label">Birthday Day</label>
						<!-- <input type="number" placeholder="01" max-length="2" min-length="1" class="form-control" id="birthdate_day" name="birthdate_day" value="<?php echo intval(@date('d', @strtotime(@$officiant_data['birthdate']))); ?>" /> -->
						<select class="form-control" id="birthdate_day" name="birthdate_day" required="required">
							<option value="1" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 1){ ?> selected="selected" <?php } ?>>1</option>
							<option value="2" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 2){ ?> selected="selected" <?php } ?>>2</option>
							<option value="3" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 3){ ?> selected="selected" <?php } ?>>3</option>
							<option value="4" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 4){ ?> selected="selected" <?php } ?>>4</option>
							<option value="5" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 5){ ?> selected="selected" <?php } ?>>5</option>
							<option value="6" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 6){ ?> selected="selected" <?php } ?>>6</option>
							<option value="7" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 7){ ?> selected="selected" <?php } ?>>7</option>
							<option value="8" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 8){ ?> selected="selected" <?php } ?>>8</option>
							<option value="9" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 9){ ?> selected="selected" <?php } ?>>9</option>
							<option value="10" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 10){ ?> selected="selected" <?php } ?>>10</option>
							<option value="11" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 11){ ?> selected="selected" <?php } ?>>11</option>
							<option value="12" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 12){ ?> selected="selected" <?php } ?>>12</option>
							<option value="13" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 13){ ?> selected="selected" <?php } ?>>13</option>
							<option value="14" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 14){ ?> selected="selected" <?php } ?>>14</option>
							<option value="15" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 15){ ?> selected="selected" <?php } ?>>15</option>
							<option value="16" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 16){ ?> selected="selected" <?php } ?>>16</option>
							<option value="17" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 17){ ?> selected="selected" <?php } ?>>17</option>
							<option value="18" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 18){ ?> selected="selected" <?php } ?>>18</option>
							<option value="19" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 19){ ?> selected="selected" <?php } ?>>19</option>
							<option value="20" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 20){ ?> selected="selected" <?php } ?>>20</option>
							<option value="21" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 21){ ?> selected="selected" <?php } ?>>21</option>
							<option value="22" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 22){ ?> selected="selected" <?php } ?>>22</option>
							<option value="23" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 23){ ?> selected="selected" <?php } ?>>23</option>
							<option value="24" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 24){ ?> selected="selected" <?php } ?>>24</option>
							<option value="25" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 25){ ?> selected="selected" <?php } ?>>25</option>
							<option value="26" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 26){ ?> selected="selected" <?php } ?>>26</option>
							<option value="27" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 27){ ?> selected="selected" <?php } ?>>27</option>
							<option value="28" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 28){ ?> selected="selected" <?php } ?>>28</option>
							<option value="29" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 29){ ?> selected="selected" <?php } ?>>29</option>
							<option value="30" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 30){ ?> selected="selected" <?php } ?>>30</option>
							<option value="31" <?php if (@date('d', @strtotime(@$officiant_data['birthdate'])) == 31){ ?> selected="selected" <?php } ?>>31</option>

						</select>
					</div>
					<div class="form-group col-xs-4 col-md-4">
						<label for="birthdate_year" class="control-label">Birthday Year</label>
						<!-- <input type="number" placeholder="1980" max-length="4" min-length="4" class="form-control" id="birthdate_year" name="birthdate_year" value="<?php echo intval(@date('Y', @strtotime(@$officiant_data['birthdate']))); ?>" /> -->
						<select name="birthdate_year" id="birthdate_year" class="form-control" required="required">
							<?php
							 for ($i=1998; $i > 1933; $i--) {
								$a = intval(@date('Y', @strtotime(@$officiant_data['birthdate'])));
								if ($a == $i){
									$s = 'selected="selected"';
								}else{
									$s = '';
								}
								echo '<option value="'.$i.'" '.$s.'>'.$i.'</option>';
							} ?>
						</select>
					</div>

					<div class="form-group col-xs-12 col-md-12">
						<label for="officiant_states" class="control-label">What states are you licensed to perform in?</label><br><br>
						<?php
						if(!empty($performing_states_abbr)) {
							foreach($performing_states_abbr as $check) {
								if (in_array($check, $performing_states_select)) {
									echo "<div id='stateItem'><input type='checkbox'  name='performing_states[]' value=".$check." id='performing_states'/ checked><span id='stateName'>".$check."</span></div>";
								} else {
									echo "<div id='stateItem'><input type='checkbox'  name='performing_states[]' value=".$check." id='performing_states'/><span id='stateName'>".$check."</span></div>";
								}
							}
						}
						?>
					</div>
					<div class="form-group col-xs-12 col-md-12">
						<label>Available From *</label>
						<!-- <input type="text" class="form-control" name="starttime" id="starttime" /> -->

						<select name="starttime" id="starttime" class="form-control" required="required">
						<option value="12:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '12:00 AM'){ ?> selected="selected" <?php } ?>>12:00 AM</option>
						<option value="12:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '12:30 AM'){ ?> selected="selected" <?php } ?>>12:30 AM</option>
						<option value="1:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '1:00 AM'){ ?> selected="selected" <?php } ?>>1:00 AM</option>
						<option value="1:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '1:30 AM'){ ?> selected="selected" <?php } ?>>1:30 AM</option>
						<option value="2:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '2:00 AM'){ ?> selected="selected" <?php } ?>>2:00 AM</option>
						<option value="2:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '2:30 AM'){ ?> selected="selected" <?php } ?>>2:30 AM</option>
						<option value="3:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '3:00 AM'){ ?> selected="selected" <?php } ?>>3:00 AM</option>
						<option value="3:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '3:30 AM'){ ?> selected="selected" <?php } ?>>3:30 AM</option>
						<option value="4:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '4:00 AM'){ ?> selected="selected" <?php } ?>>4:00 AM</option>
						<option value="4:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '4:30 AM'){ ?> selected="selected" <?php } ?>>4:30 AM</option>
						<option value="5:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '5:00 AM'){ ?> selected="selected" <?php } ?>>5:00 AM</option>
						<option value="5:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '5:30 AM'){ ?> selected="selected" <?php } ?>>5:30 AM</option>
						<option value="6:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '6:00 AM'){ ?> selected="selected" <?php } ?>>6:00 AM</option>
						<option value="6:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '6:30 AM'){ ?> selected="selected" <?php } ?>>6:30 AM</option>
						<option value="7:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '7:00 AM'){ ?> selected="selected" <?php } ?>>7:00 AM</option>
						<option value="7:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '7:30 AM'){ ?> selected="selected" <?php } ?>>7:30 AM</option>
						<option value="8:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '8:00 AM'){ ?> selected="selected" <?php } ?>>8:00 AM</option>
						<option value="8:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '8:30 AM'){ ?> selected="selected" <?php } ?>>8:30 AM</option>
						<option value="9:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '9:00 AM'){ ?> selected="selected" <?php } ?>>9:00 AM</option>
						<option value="9:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '9:30 AM'){ ?> selected="selected" <?php } ?>>9:30 AM</option>
						<option value="10:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '10:00 AM'){ ?> selected="selected" <?php } ?>>10:00 AM</option>
						<option value="10:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '10:30 AM'){ ?> selected="selected" <?php } ?>>10:30 AM</option>
						<option value="11:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>11:00 AM</option>
						<option value="11:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '11:30 AM'){ ?> selected="selected" <?php } ?>>11:30 AM</option>
						<option value="12:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '12:00 PM'){ ?> selected="selected" <?php } ?>>12:00 PM</option>
						<option value="12:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '12:30 PM'){ ?> selected="selected" <?php } ?>>12:30 PM</option>
						<option value="1:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '1:00 PM'){ ?> selected="selected" <?php } ?>>1:00 PM</option>
						<option value="1:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '1:30 PM'){ ?> selected="selected" <?php } ?>>1:30 PM</option>
						<option value="2:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '2:00 PM'){ ?> selected="selected" <?php } ?>>2:00 PM</option>
						<option value="2:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '2:30 PM'){ ?> selected="selected" <?php } ?>>2:30 PM</option>
						<option value="3:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '3:00 PM'){ ?> selected="selected" <?php } ?>>3:00 PM</option>
						<option value="3:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '3:30 PM'){ ?> selected="selected" <?php } ?>>3:30 PM</option>
						<option value="4:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '4:00 PM'){ ?> selected="selected" <?php } ?>>4:00 PM</option>
						<option value="4:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '4:30 PM'){ ?> selected="selected" <?php } ?>>4:30 PM</option>
						<option value="5:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '5:00 PM'){ ?> selected="selected" <?php } ?>>5:00 PM</option>
						<option value="5:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '5:30 PM'){ ?> selected="selected" <?php } ?>>5:30 PM</option>
						<option value="6:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '6:00 PM'){ ?> selected="selected" <?php } ?>>6:00 PM</option>
						<option value="6:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '6:30 PM'){ ?> selected="selected" <?php } ?>>6:30 PM</option>
						<option value="7:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '7:00 PM'){ ?> selected="selected" <?php } ?>>7:00 PM</option>
						<option value="7:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '7:30 PM'){ ?> selected="selected" <?php } ?>>7:30 PM</option>
						<option value="8:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '8:00 PM'){ ?> selected="selected" <?php } ?>>8:00 PM</option>
						<option value="8:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '8:30 PM'){ ?> selected="selected" <?php } ?>>8:30 PM</option>
						<option value="9:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '9:00 PM'){ ?> selected="selected" <?php } ?>>9:00 PM</option>
						<option value="9:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '9:30 PM'){ ?> selected="selected" <?php } ?>>9:30 PM</option>
						<option value="10:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '10:00 PM'){ ?> selected="selected" <?php } ?>>10:00 PM</option>
						<option value="10:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '10:30 PM'){ ?> selected="selected" <?php } ?>>10:30 PM</option>
						<option value="11:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '11:00 PM'){ ?> selected="selected" <?php } ?>>11:00 PM</option>
						<option value="11:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['start'] == '11:30 PM'){ ?> selected="selected" <?php } ?>>11:30 PM</option>
						</select>
						<label>Available To *</label>
						<!-- <input type="text" class="form-control" name="endtime" id="endtime" /> -->
						<select name="endtime" id="endtime" class="form-control" required="required">
						<option value="12:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>12:00 AM</option>
						<option value="12:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>12:30 AM</option>
						<option value="1:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>1:00 AM</option>
						<option value="1:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>1:30 AM</option>
						<option value="2:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>2:00 AM</option>
						<option value="2:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>2:30 AM</option>
						<option value="3:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>3:00 AM</option>
						<option value="3:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>3:30 AM</option>
						<option value="4:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>4:00 AM</option>
						<option value="4:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>4:30 AM</option>
						<option value="5:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>5:00 AM</option>
						<option value="5:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>5:30 AM</option>
						<option value="6:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>6:00 AM</option>
						<option value="6:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>6:30 AM</option>
						<option value="7:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>7:00 AM</option>
						<option value="7:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>7:30 AM</option>
						<option value="8:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>8:00 AM</option>
						<option value="8:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>8:30 AM</option>
						<option value="9:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>9:00 AM</option>
						<option value="9:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>9:30 AM</option>
						<option value="10:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>10:00 AM</option>
						<option value="10:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>10:30 AM</option>
						<option value="11:00 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>11:00 AM</option>
						<option value="11:30 AM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 AM'){ ?> selected="selected" <?php } ?>>11:30 AM</option>
							<option value="12:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '12:00 PM'){ ?> selected="selected" <?php } ?>>12:00 PM</option>
							<option value="12:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '12:30 PM'){ ?> selected="selected" <?php } ?>>12:30 PM</option>
							<option value="1:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '1:00 PM'){ ?> selected="selected" <?php } ?>>1:00 PM</option>
							<option value="1:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '1:30 PM'){ ?> selected="selected" <?php } ?>>1:30 PM</option>
							<option value="2:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '2:00 PM'){ ?> selected="selected" <?php } ?>>2:00 PM</option>
							<option value="2:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '2:30 PM'){ ?> selected="selected" <?php } ?>>2:30 PM</option>
							<option value="3:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '3:00 PM'){ ?> selected="selected" <?php } ?>>3:00 PM</option>
							<option value="3:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '3:30 PM'){ ?> selected="selected" <?php } ?>>3:30 PM</option>
							<option value="4:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '4:00 PM'){ ?> selected="selected" <?php } ?>>4:00 PM</option>
							<option value="4:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '4:30 PM'){ ?> selected="selected" <?php } ?>>4:30 PM</option>
							<option value="5:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '5:00 PM'){ ?> selected="selected" <?php } ?>>5:00 PM</option>
							<option value="5:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '5:30 PM'){ ?> selected="selected" <?php } ?>>5:30 PM</option>
							<option value="6:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '6:00 PM'){ ?> selected="selected" <?php } ?>>6:00 PM</option>
							<option value="6:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '6:30 PM'){ ?> selected="selected" <?php } ?>>6:30 PM</option>
							<option value="7:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '7:00 PM'){ ?> selected="selected" <?php } ?>>7:00 PM</option>
							<option value="7:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '7:30 PM'){ ?> selected="selected" <?php } ?>>7:30 PM</option>
							<option value="8:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '8:00 PM'){ ?> selected="selected" <?php } ?>>8:00 PM</option>
							<option value="8:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '8:30 PM'){ ?> selected="selected" <?php } ?>>8:30 PM</option>
							<option value="9:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '9:00 PM'){ ?> selected="selected" <?php } ?>>9:00 PM</option>
							<option value="9:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '9:30 PM'){ ?> selected="selected" <?php } ?>>9:30 PM</option>
							<option value="10:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '10:00 PM'){ ?> selected="selected" <?php } ?>>10:00 PM</option>
							<option value="10:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '10:30 PM'){ ?> selected="selected" <?php } ?>>10:30 PM</option>
							<option value="11:00 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:00 PM'){ ?> selected="selected" <?php } ?>>11:00 PM</option>
							<option value="11:30 PM" <?php if (json_decode($officiant_data['available_times'], true)['end'] == '11:30 PM'){ ?> selected="selected" <?php } ?>>11:30 PM</option>
						</select>
					</div>
					<div class="form-group col-xs-12 col-md-12">
						<label for="officiant_willing" class="control-label">Are you willing to perform Samesex, and Proxy weddings? *</label>
						<div class="radio willing_radio">
							<label><input type="radio" class="form-control" name="willingness" id="willingness" value="1" <?php if($officiant_data['perform_willingness'] == '1') {echo 'checked="checked"';} ?> />Yes</label>
							<label><input type="radio" class="form-control" name="willingness" id="willingness" value="0" <?php if($officiant_data['perform_willingness'] == '0') {echo 'checked="checked"';} ?>/>No</label>
						</div>
					</div>
					<div class="form-group col-xs-12 col-md-6 hidden">
						<label for="wwm_minister_id" class="control-label">Hidden Officiant ID</label>
						<input type="text" class="form-control" id="wwm_minister_id" name="wwm_minister_id" value="<?php echo $_SESSION['wwm_minister_id']; ?>" readonly>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<input type="submit" class="btn btn-primary" value="Save"/>
					</div>
				</div>
			</div>
		</div>
	</form>


</div>
<hr>
<?php
//  SET PROFILE PICTURE
if($officiant_data['photo'] == null) {
	$photo = 'http://localhost/Client/Riverloper/webwedadmin/upload/default_avatar_male.jpg';
} else {
	$photo = $officiant_data['photo'];
}
?>
<script>
var available_times = '<?php echo $officiant_data['available_times']; ?>';
var minister_id = '<?php echo $_SESSION['wwm_minister_id']; ?>';
available_times = JSON.parse( available_times );
$( document ).ready(function() {


});
</script>
<script language="javascript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>


$(function() {
	$("#phone").mask("(999) 999-9999");
	$("#zip").mask("99999");

	$( "#minister_update_form" ).validate({
		errorLabelContainer: $("#minister_add_form div.error"),
		rules: {
			"performing_states[]":{
				required: true,
				minlength: 1
			},
			name: {
				required: true
			},
			email: {
				required: true,
				email:true
			},
			username: {
				required: true
			},
			"day[]": {
				required: true
			},
			officiant_pass : {
				required: false
			},
			officiant_pass_c: {
				equalTo: '#officiant_pass'
			}
		},
		messages: {
			name: {
				required: "Please enter name."
			},
			email: {
				required: "Please enter email address.",
				email:"Please enter valid email address."
			},
			username: {
				required: "Please enter username."
			},
			"day[]": {
				required: "Please select available days.",
				minlength: 1
			},
			"performing_states[]":{
				required: 'Select the States !!'
			}
		}

	});
});
</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
<!-- pattern -->

<?php include('../footer.php'); ?>
