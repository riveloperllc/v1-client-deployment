<?php if (!defined('BASEPATH')){ exit; } ?>
<ul class="nav navbar-nav">
  <?php if (minister_signup_completed($pdoDB, $_SESSION['wwm_minister_id'])) { ?>
  <li><a href="<?php echo SITEURL; ?>officiant_admin/m_assign_weddings.php">My Schedule</a></li>
  <li><a href="<?php echo SITEURL; ?>officiant_admin/manage_account.php">Manage Account</a></li>
  <?php }else{ ?>
    <li><a href="<?php echo SITEURL; ?>officiant_admin/m_assign_weddings.php">Manage Account</a></li>
  <?php } ?>
</ul>
<ul class="nav navbar-nav navbar-right">
  <li></li>
  <li id="sign-in-li">
    <a href="<?php echo SITEURL; ?>officiant_admin/logout.php" id="sign-in-red">
      <span class="pull-right"><i class="fa fa-heart"></i><span id="accountname">Logout</span></span>
    </a>
  </li>
</ul>
