<?php
if (isset($_SESSION['wwm_minister_id'])){ header("Location: logout.php"); exit; }
session_start();
require_once('../common/connection.php');
require_once('../common/config.php');

if (isset($_POST['submit'])) {
	$name = ucfirst(mysql_real_escape_string($_POST['name']));
	$email = mysql_real_escape_string($_POST['email']);
	//$username = mysql_real_escape_string($_POST['username']);
	$password = md5($_POST['password']);

	if (!alreadyExists($pdoDB, $email)){
		$query_string = "INSERT INTO ministers (name, email, password) VALUES ('$name', '$email', '$password')";
		$result = mysql_query ($query_string);

		$_SESSION['message'] = "";
		$_SESSION['flag'] = "1";

		$sql_min = "SELECT * FROM ministers WHERE email='$email' and password='$password' LIMIT 1";
		$resultstatus = mysql_query($sql_min);
		$countstatus = mysql_num_rows($resultstatus);

		if($countstatus > 0)
		{
			@session_start();
			$data = mysql_fetch_array($resultstatus);
			$_SESSION['wwm_minister_id'] = $data['minister_id'];
		}

		header('Location: '.$siteurl.'officiant_admin/continued_signup.php');
		exit;
	}else{
		$msg = '<div class="alert alert-danger">That email already exists in our system, please follow this link to login <a href="';
		$msg .= SITEURL.'officiant_admin/login.php">Click here to login</a>';
		$msg .= '</div>';
	}

}
?>
<?php
$page_title = "Signup Officiant - ";
include('../header.php');
?>
<br/>
<br/>
<br/>
<div class="form_main">
	<div class="form_main_inside">
		<div class="login">
			<h1>Sign Up</h1>
			<?php echo @$msg; ?>
			<form name="minister_add_form" id="minister_add_form" method="post" action="<?php echo SITEURL; ?>officiant_admin/signup.php">
				<p id="payment_notification">An Officiant account is a $9.99 monthly fee.<br/><br/>The first 3 months are completely free.</p><br/>

				<label>Name</label><br/><br/>
				<input type="text" class="input_text" name="name" id="name"/><br/><br/>

				<label>Email Address</label><br/><br/>
				<input type="text" class="input_text" name="email" id="email"/><br/><br/>

				<label>Password</label><br/><br/>
				<input type="password" class="input_text" name="password" id="password" value="" placeholder=""/><br/><br/>

				<input type="submit" name="submit"  value="Continue" class="btn btn-primary" /><br/><br/>
					<a class="btn" href="login.php">Go Back</a><br/><br/>

			</form>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>


$(function() {
	$( "#minister_add_form" ).validate({
		rules: {
			name: {
				required: true
			},
			email: {
				required: true,
				email:true
			},
			username: {
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			name: {
				required: "Please enter name."
			},
			email: {
				required: "Please enter email address.",
				email:"Please enter valid email address."
			},
			username: {
				required: "Please enter username."
			},
			password: {
				required: "Please enter password."
			}
		}

	});
});
</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
<?php include('../footer.php'); ?>
