<?php
session_start();
if(!isset($_SESSION['wwm_minister_id'])){
	header("location:login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');


if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_minister_id'])){
  header("Location: make_payment.php");
	exit;
}

if (!minister_signup_completed($pdoDB, $_SESSION['wwm_minister_id'])){
	header("Location: continued_signup.php");
	exit;
}

$pending_approval = minister_pending_approval($pdoDB, $_SESSION['wwm_minister_id']);

if ( $pending_approval['admin_approved'] == "0"){
	header("Location: manage_account.php?pending=true");
	exit;
}


$page_title = "Assigned Weddings - ";
include('../header.php'); ?>
<?php

?>
<script>
$(document).ready(function(){
	var table = $('#weddings').DataTable({
		"responsive": true,
		"processing": true,
		"serverSide": true,
		"lengthChange": false,
		"ajax": {
			"url": "datatable_handlers/ajax_datatable.php?wwmid=<?php echo $_SESSION['wwm_minister_id']; ?>",
			"data": function ( d ){
				d.basepath = true;
			}
		},
		"columnDefs": [{
			"visible": false,
			"targets": 0
		}]
	});

	$("#weddings tbody").on('click', 'tr', function(){
		var data = table.row(this).data();
		var id = data[0];
		location.href = '<?php echo SITEURL; ?>officiant_admin/manage_wedding.php?id='+id;
	});
});
</script>
<br/>
<br/>
<br/>
<br/>
<div class="container">
	<div class="form_main_inside">
		<div class="">
			<?php
			if(isset($_SESSION['message']) && $_SESSION['message']!='')
			{
				if($_SESSION['flag'] == 1)
				{
					echo "<div class='alert alert-success'>".$_SESSION['message']."</div>";
				}
				if($_SESSION['flag'] == 2)
				{
					echo "<div class='alert alert-danger'><span>error: </span>".$_SESSION['message']."</div>";
				}
				unset($_SESSION['flag']);
				unset($_SESSION['message']);
			}
			?>

			<h1><span>Scheduled weddings</span></h1>
			<div class="table-responsive">
				<table id="weddings" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<th>ID</th>
						<th>Type</th>
						<th>Time</th>
						<th>Spouse A</th>
						<th>Spouse B</th>
						<th>Desc</th>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<!-- pattern -->

<?php include('../footer.php'); ?>
