<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'])){
  header("location: admin_login.php");
}
?>
<?php
require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');
require_once('../common/utility_functions.php');
?>
<!-- Middle Start -->
<?php  $page_name = basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Weddings - ";
include('../header.php'); ?>
<script>
$(document).ready(function(){
	var table = $('#events').DataTable({
		"responsive": true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "datatable_handlers/events_datatable.php?user_token=<?php echo @$_SESSION['wwm_admin_token']; ?>",
			"data": function ( d ){
				d.basepath = true;
			}
		},
    "lengthChange": false,
    "columnDefs": [{
			"visible": false,
			"targets": 0
		}]
	});

	// $("#weddings tbody").on('click', 'tr', function(){
	// 	var data = table.row(this).data();
	// 	var id = data[0];
	// 	location.href = '<?php echo SITEURL; ?>officiant_admin/manage_wedding.php?id='+id;
	// });
});
</script>
<br/>
<br/>
<br/>
<br/>
<div class="container">
  <div class="form_main_inside">
    <div class="login payment dashboard">
      <h1><span>Events</span></h1>
      <div class="table-responsive">
				<table id="events" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<th>ID</th>
						<th>Event Code</th>
						<th>Accept Gifts</th>
						<th>Public Event</th>
            <th>Allow Chat</th>
            <th>Date of Event</th>
            <th>Time of Event</th>
					</thead>
					<tbody></tbody>
				</table>
			</div>
    </div>
  </div>
</div>


<!-- pattern -->


<?php include('../footer.php'); ?>
