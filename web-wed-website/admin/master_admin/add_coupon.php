<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'], $_SESSION['wwm_admin_token'])){
  header("location: admin_login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');

// if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_admin_id'])){
//   header("Location: make_payment.php");
// 	exit;
// }

if (!isLoggedInAsAdmin($_SESSION['wwm_admin_token'])){
  header("Location: logout.php");
}

if (isset($_POST['name'], $_POST['price'])){

  $prepared = $pdoDB->prepare("INSERT INTO `discount_codes` VALUES (NULL, ?, ?)");
  $prepared->execute(array($_POST['name'], $_POST['price']));

  header("Location: manage_coupons.php");
  exit;

  $msg = '<div class="alert alert-success">Coupon has been successfully added.</div>';
}

$page_title = "Add Coupon - ";
include('../header.php'); ?>
<br>
<br>
<br>
<div class="container">
  <h1>Add Coupon</h1>
  <hr>
  <div class="row">
      <?php
    echo @$msg;
    ?>
    <div class="alert alert-info">The price of the product must be at least $1 for credit card authorization charges.</div>
    <form name="admin_update_form" id="admin_update_form" method="post" action="">

      <!-- edit form column -->

      <div class="col-md-12 personal-info">

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Discount Code</label>
          <input type="text" name="name" class="form-control" />
        </div>

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Discount Amount</label>
          <input type="number" name="price" class="form-control" />
        </div>

        <div class="form-group">
          <div class="col-md-12">
            <input type="submit" class="btn btn-primary" value="Add Coupon">
          </div>
        </div>

      </div>
    </div>
  </form>


</div>
<hr>
<!-- pattern -->

<?php include('../footer.php'); ?>
