<?php

if (!isset($_GET['basepath']) && $_GET['basepath']){ exit; }
if (!isset($_GET['user_token']) && $_GET['user_token']){ exit; }

require_once('../../common/connection.php');
require_once('../../common/functions.php');
require_once('../../common/utility_functions.php');


if (!isLoggedInAsAdmin($_GET['user_token'])){
  echo json_encode(array());
  exit;
}

$where = '';

// SQL server connection information
$sql_details = array(
  'user' => $username_NWHL,
  'pass' => $password_NWHL,
  'db'   => $database_NWHL,
  'host' => $hostname_NWHL
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Easy set variables
*/

// DB table to use
$table = 'ministers';

// Table's primary key
// $primaryKey = 'user_id';
$primaryKey = 'minister_id';


// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  array( 'db' => $primaryKey, 'dt' => 0 ),
  array( 'db' => 'photo', 'dt' => 1, 'formatter' => function ( $d, $row ){
    if ($d == ''){
      return '<img src="http://webwedmobile.com/images/default_avatar_male.jpg" style="width:50px;" class="thumbnail thumbnail-tiny"/>';
    }else{
      return '<img src="http://webwedmobile.com/images/default_avatar_male.jpg" style="width:50px;" class="thumbnail humbnail-tiny"/>';
    }
  }),
  array( 'db' => 'name', 'dt' => 2 ),
  array( 'db' => 'email', 'dt' => 3),
  array( 'db' => 'phone', 'dt' => 4),
  array( 'db' => 'profile_completed', 'dt' => 5, 'formatter' => function ( $d, $row ){
    if ($d == 1){
      return '<i class="glyphicon glyphicon-ok"></i>';
    }else{
      return '<i class="glyphicon glyphicon-remove"></i>';
    }
  }),
  array( 'db' => 'admin_approved', 'dt' => 6, 'formatter' => function ( $d, $row ){
    if ($d == 1){
      return '<i class="glyphicon glyphicon-ok"></i>';
    }else{
      return '<i class="glyphicon glyphicon-remove"></i>';
    }
  }),
  array( 'db' => 'membership_expiry', 'dt' => 7, 'formatter' => function ( $d, $row ){
    if ($d == '0000-00-00'){ return 'No Active Membership'; }
    return date('m/d/Y h:i A (T)', strtotime($d));
  }),
  array( 'db' => 'created_date', 'dt' => 8, 'formatter' => function ( $d, $row ){
    if ($d == '0000-00-00'){ return '<i class="glyphicon glyphicon-remove"></i>'; }
    return date('m/d/Y h:i A (T)', strtotime($d));
  })
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* If you just want to use the basic configuration for DataTables with PHP
* server-side, there is no need to edit below this line.
*/

require( '../../common/ssp.class.php' );

echo json_encode(
SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where)
);

?>
