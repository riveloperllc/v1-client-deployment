<?php

if (!isset($_GET['basepath']) && $_GET['basepath']){ exit; }
if (!isset($_GET['user_token']) && $_GET['user_token']){ exit; }

require_once('../../common/connection.php');
require_once('../../common/functions.php');
require_once('../../common/utility_functions.php');

$where = '';

if (!isLoggedInAsAdmin($_GET['user_token'])){
  echo json_encode(array());
  exit;
}

// SQL server connection information
$sql_details = array(
  'user' => $username_NWHL,
  'pass' => $password_NWHL,
  'db'   => $database_NWHL,
  'host' => $hostname_NWHL
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Easy set variables
*/

// DB table to use
$table = 'discount_codes';

// Table's primary key
// $primaryKey = 'user_id';
$primaryKey = 'id';


// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  array( 'db' => $primaryKey, 'dt' => 0 ),
  array( 'db' => 'code', 'dt' => 1 ),
  array( 'db' => 'amount', 'dt' => 2)
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* If you just want to use the basic configuration for DataTables with PHP
* server-side, there is no need to edit below this line.
*/

require( '../../common/ssp.class.php' );

echo json_encode(
SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where)
);

?>
