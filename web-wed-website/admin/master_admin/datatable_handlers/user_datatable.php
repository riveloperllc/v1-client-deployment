<?php

if (!isset($_GET['basepath']) && $_GET['basepath']){ exit; }
if (!isset($_GET['user_token']) && $_GET['user_token']){ exit; }

require_once('../../common/connection.php');
require_once('../../common/functions.php');
require_once('../../common/utility_functions.php');


if (!isLoggedInAsAdmin($_GET['user_token'])){
  echo json_encode(array());
  exit;
}

$where = '';

// SQL server connection information
$sql_details = array(
  'user' => $username_NWHL,
  'pass' => $password_NWHL,
  'db'   => $database_NWHL,
  'host' => $hostname_NWHL
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Easy set variables
*/

// DB table to use
$table = 'www_users_new';

// Table's primary key
// $primaryKey = 'user_id';
$primaryKey = 'id';


// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  array( 'db' => $primaryKey, 'dt' => 'DT_RowId' ),
  array( 'db' => 'name', 'dt' => 'name'),
  array( 'db' => 'email', 'dt' => 'email'),
  array( 'db' => 'gender', 'dt' => 'gender', 'formatter' => function ( $d, $row ){
    if ($d == 1){
      return 'Male';
    }else{
      return 'Female';
    }
  }),
  array( 'db' => 'birthdate', 'dt' => 'birthdate'),
  array( 'db' => 'city', 'dt' => 'city'),
  array( 'db' => 'state', 'dt' => 'state'),
  array( 'db' => 'phone', 'dt' => 'phone'),
  array( 'db' => 'timezone', 'dt' => 'timezone')
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* If you just want to use the basic configuration for DataTables with PHP
* server-side, there is no need to edit below this line.
*/

require( '../../common/ssp.class.php' );

echo json_encode(
SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where)
);

?>
