<?php

if (!isset($_GET['basepath']) && $_GET['basepath']){ exit; }
if (!isset($_GET['user_token']) && $_GET['user_token']){ exit; }

require_once('../../common/connection.php');
require_once('../../common/functions.php');
require_once('../../common/utility_functions.php');

$where = '';

if (!isLoggedInAsAdmin($_GET['user_token'])){
  echo json_encode(array());
  exit;
}

// SQL server connection information
$sql_details = array(
  'user' => $username_NWHL,
  'pass' => $password_NWHL,
  'db'   => $database_NWHL,
  'host' => $hostname_NWHL
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Easy set variables
*/

// DB table to use
$table = 'special_moments';

// Table's primary key
// $primaryKey = 'user_id';
$primaryKey = 'id';


// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
  array( 'db' => 'id', 'dt' => 0 ),
  array( 'db' => 'event_code', 'dt' => 1 ),
  array( 'db' => 'gifts', 'dt' => 2, 'formatter' => function($d, $row){
    if ($d == 0){
      return 'Disabled';
    }else{
      return 'Enabled';
    }
  } ),
  array( 'db' => 'public', 'dt' => 3 , 'formatter' => function($d, $row){
    if ($d == 0){
      return 'Disabled';
    }else{
      return 'Enabled';
    }
  }),
  array( 'db' => 'chat', 'dt' => 4 , 'formatter' => function($d, $row){
    if ($d == 0){
      return 'Disabled';
    }else{
      return 'Enabled';
    }
  }),
  array( 'db' => 'event_date', 'dt' => 5, 'formatter' => function($d, $row){
    if ($d == ''){
      return 'On-Demand';
    }else{
      return $d;
    }
  }),
  array( 'db' => 'event_time', 'dt' => 6, 'formatter' => function($d, $row){
    if ($d == ''){
      return 'On-Demand';
    }else{
      return $d;
    }
  })
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* If you just want to use the basic configuration for DataTables with PHP
* server-side, there is no need to edit below this line.
*/

require( '../../common/ssp.class.php' );
/*SELECT id, event_code, gifts, public, chat, event_date, event_time
FROM special_moments
GROUP BY event_date, event_time
UNION ALL
SELECT id, event_code, gifts, public, chat, event_date, event_time
FROM weddings
GROUP BY event_date, event_time*/

echo json_encode(
  SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, $where)
);

?>
