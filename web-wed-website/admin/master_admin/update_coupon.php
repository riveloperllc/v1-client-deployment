<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'], $_SESSION['wwm_admin_token'])){
  header("location: admin_login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');

// if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_admin_id'])){
//   header("Location: make_payment.php");
// 	exit;
// }

if (!isLoggedInAsAdmin($_SESSION['wwm_admin_token'])){
  header("Location: logout.php");
}

if (isset($_POST['name'], $_POST['price'], $_POST['id'])){

  $prepared = $pdoDB->prepare("UPDATE `discount_codes` SET `code` = ? WHERE `id` = ?");
  $prepared->execute(array($_POST['name'], $_POST['id']));
  $prepared = $pdoDB->prepare("UPDATE `discount_codes` SET `amount` = ? WHERE `id` = ?");
  $prepared->execute(array($_POST['price'], $_POST['id']));


  $msg = '<div class="alert alert-success">Coupon has been successfully updated.</div>';
}

$prepared = $pdoDB->prepare("SELECT * FROM `discount_codes` WHERE `id` = ?");
$prepared->execute(array($_GET['id']));
if ($prepared->rowCount() == 0){
  header("Location: manage_coupons.php");
  exit;
}

$row = $prepared->fetch(PDO::FETCH_ASSOC);

if (!isset($_GET['id'])){
  header("Location: index.php");
  exit;
}
$page_title = "Update Coupon - ";
include('../header.php'); ?>
<br>
<br>
<br>
<div class="container">
  <h1>Update Coupon</h1>
  <hr>
  <div class="row">
      <?php
    echo @$msg;
    ?>
    <form name="admin_update_form" id="admin_update_form" method="post" action="">

      <!-- edit form column -->

      <div class="col-md-12 personal-info">

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Discount Code</label>
          <input type="text" name="name" class="form-control" value="<?php echo @$row['code']; ?>"/>
        </div>

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Discount Amount</label>
          <input type="number" name="price" class="form-control" value="<?php echo @$row['amount']; ?>"/>
        </div>

        <div class="form-group col-xs-12 col-md-6 hidden">
          <label for="uid" class="control-label">Hidden user ID</label>
          <input type="hidden" class="form-control" id="uid" name="id" value="<?php echo @$_GET['id']; ?>" readonly>
        </div>

        <div class="form-group">
          <div class="col-md-12">
            <input type="submit" class="btn btn-primary" value="Save Changes">
          </div>
        </div>

      </div>
    </div>
  </form>


</div>
<hr>
<!-- pattern -->

<?php include('../footer.php'); ?>
