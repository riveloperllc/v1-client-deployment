<?php if (!defined('BASEPATH')){ exit; } ?>
<ul class="nav navbar-nav">
  <li><a href="<?php echo SITEURL; ?>master_admin/manage_users.php">Users</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/manage_ministers.php">Officiants</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/manage_products.php">Products</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/manage_weddings.php">Weddings</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/manage_special_moments.php">Special Moments</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/manage_coupons.php">Coupons</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/manage_payments.php">Payments</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/manage-account.php">Account</a></li>
  <li><a href="<?php echo SITEURL; ?>master_admin/logout.php">Logout</a></li>
</ul>
