<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'], $_SESSION['wwm_admin_token'])){
  header("location: admin_login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');

// if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_admin_id'])){
//   header("Location: make_payment.php");
// 	exit;
// }

if (!isLoggedInAsAdmin($_SESSION['wwm_admin_token'])){
  header("Location: logout.php");
}

if (isset($_POST['name'], $_POST['description'], $_POST['price'], $_POST['id'])){

  $prepared = $pdoDB->prepare("UPDATE `ceremony_options` SET `name` = ? WHERE `co_id` = ?");
  $prepared->execute(array($_POST['name'], $_POST['id']));
  $prepared = $pdoDB->prepare("UPDATE `ceremony_options` SET `description` = ? WHERE `co_id` = ?");
  $prepared->execute(array($_POST['description'], $_POST['id']));
  $prepared = $pdoDB->prepare("UPDATE `ceremony_options` SET `price` = ? WHERE `co_id` = ?");
  $prepared->execute(array($_POST['price'], $_POST['id']));


  $msg = '<div class="alert alert-success">Product has been successfully updated.</div>';
}

if (!isset($_GET['id'])){
  header("Location: index.php");
  exit;
}
$page_title = "Update Product - ";
include('../header.php'); ?>
<br>
<br>
<br>
<div class="container">
  <h1>Update Product</h1>
  <hr>
  <div class="row">
      <?php
    echo @$msg;
    ?>
    <form name="admin_update_form" id="admin_update_form" method="post" action="">

      <!-- edit form column -->

      <div class="col-md-12 personal-info">

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Product Name</label>
          <input type="text" name="name" class="form-control" value="<?php echo getProductName($pdoDB, @$_GET['id']); ?>"/>
        </div>

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Product Description</label>
          <input type="text" name="description" class="form-control" value="<?php echo getProductDescription($pdoDB, @$_GET['id']); ?>"/>
        </div>

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Product Price</label>
          <input type="number" name="price" class="form-control" value="<?php echo getProductPricing($pdoDB, @$_GET['id']); ?>"/>
        </div>

        <div class="form-group col-xs-12 col-md-6 hidden">
          <label for="uid" class="control-label">Hidden user ID</label>
          <input type="hidden" class="form-control" id="uid" name="id" value="<?php echo @$_GET['id']; ?>" readonly>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label"></label>
          <div class="col-md-8">
            <input type="submit" class="btn btn-primary" value="Save Changes">
          </div>
        </div>

      </div>
    </div>
  </form>


</div>
<hr>
<!-- pattern -->

<?php include('../footer.php'); ?>
