<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'])){
  header("location: admin_login.php");
}
?>
<?php
require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');
require_once('../common/utility_functions.php');
?>
<!-- Middle Start -->
<?php  $page_name = basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Products - ";
include('../header.php'); ?>
<script>
$(document).ready(function(){
	var table = $('#products').DataTable({
		"responsive": true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "datatable_handlers/products_datatable.php?user_token=<?php echo @$_SESSION['wwm_admin_token']; ?>",
			"data": function ( d ){
				d.basepath = true;
			}
		},
    "lengthChange": false,
		columnDefs: [
      {
        targets: [0],
        visible: 0
      },
      {
      	targets: [0,2],
      	className: 'hidden-xs hidden-sm'
    	}
  	]
	});

	$("#products tbody").on('click', 'tr', function(){
		var data = table.row(this).data();
		var id = data[0];
		location.href = '<?php echo SITEURL; ?>master_admin/update_product.php?id='+id;
	});
});
</script>
<br/>
<br/>
<br/>
<br/>
<div class="container">
  <div class="form_main_inside">
    <div class="login payment dashboard">
      <h1><span>Products</span></h1>
      <div class="table-responsive">
				<table id="products" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
					</thead>
					<tbody></tbody>
				</table>
			</div>
    </div>
  </div>
</div>


<!-- pattern -->


<?php include('../footer.php'); ?>
