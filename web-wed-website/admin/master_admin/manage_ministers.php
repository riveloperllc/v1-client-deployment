<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'])){
	header("location:admin_login.php");
}
require_once('../common/connection.php');
require_once('../common/config.php');

$page_title = "Officiants - ";
include_once('../header.php');
?>
<script>
$(document).ready(function(){
	var table = $('#ministers').DataTable({
		"responsive": true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "datatable_handlers/minister_datatable.php?user_token=<?php echo @$_SESSION['wwm_admin_token']; ?>",
			"data": function ( d ){
				d.basepath = true;
			}
		},
		"lengthChange": false,
		columnDefs: [
			{
				"visible": false,
				"targets": 0
			},
    	{
      	targets: [3,7],
      	className: 'hidden-xs hidden-sm'
    	}
  	],
  	"order": [[6, 'desc']]
	});

	$("#ministers tbody").on('click', 'tr', function(){
		var data = table.row(this).data();
		var id = data[0];
		location.href = '<?php echo SITEURL; ?>master_admin/update_minister.php?id='+id;
	});
});
</script>
<br/>
<br/>
<br/>
<br/>
<div class="container">
	<div class="form_main_inside">
		<div class="login payment dashboard">
			<h1><span>Officiants</span></h1>
			<a href="<?php echo SITEURL; ?>officiant_admin/signup.php" class="btn btn-primary">Create Officiant</a>
			<div class="table-responsive">
				<table id="ministers" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<th>Minister ID</th>
						<th></th>
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Profile Completed</th>
						<th>Approved</th>
						<th>Membership Expiration Date</th>
						<th>Created Date</th>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>


<!-- pattern -->

<?php include('../footer.php'); ?>
