<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'], $_SESSION['wwm_admin_token'])){
  header("location: admin_login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');

// if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_admin_id'])){
//   header("Location: make_payment.php");
// 	exit;
// }

if (!isLoggedInAsAdmin($_SESSION['wwm_admin_token'])){
  header("Location: logout.php");
}

if (isset($_POST['name'], $_POST['email'], $_POST['pass'], $_POST['pass_c'], $_POST['street'], $_POST['city'], $_POST['zip'], $_POST['birthdate_day'], $_POST['birthdate_month'], $_POST['birthdate_year'], $_POST['wwm_admin_id'])){
  $uid = $_POST['wwm_admin_id'];

  $name = $_POST['name'];
  $email = $_POST['email'];
  $pass = $_POST['pass'];
  $confirm_pass = $_POST['pass_c'];
  $street = $_POST['street'];
  $city = $_POST['city'];
  $zip = $_POST['zip'];
  // $birthdate = $_POST['birthdate'];

  if (!empty($pass) && !empty($confirm_pass) && $pass == $confirm_pass){
    $pass = md5($pass);
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `password` = ? WHERE `id` = ?");
    $prepared->execute(array($pass, $uid));
  }else{
    if ($pass != $confirm_pass){
      $msg = '<div class="alert alert-danger">Your passwords did not match, please try fill out your password below and try again.</div>';
    }
  }

  if (!empty($email)){
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `email` = ? WHERE `id` = ?");
    $prepared->execute(array($email, $uid));
  }

  if (!empty($name)){
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `name` = ? WHERE `id` = ?");
    $prepared->execute(array($name, $uid));
  }

  if (!empty($street)){
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `street` = ? WHERE `id` = ?");
    $prepared->execute(array($street, $uid));
  }

  if (!empty($city)){
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `city` = ? WHERE `id` = ?");
    $prepared->execute(array($city, $uid));
  }

  if (!empty($state)){
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `state` = ? WHERE `id` = ?");
    $prepared->execute(array($state, $uid));
  }

  if (!empty($zip)){
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `zip` = ? WHERE `id` = ?");
    $prepared->execute(array($zip, $uid));
  }

  $_POST['birthdate'] = @$_POST['birthdate_month'] . '/' . @$_POST['birthdate_day'] . '/' . @$_POST['birthdate_year'];
  $birthdate = $_POST['birthdate'];
  if (!empty($birthdate)){
    $prepared = $pdoDB->prepare("UPDATE `www_users_new` SET `birthdate` = ? WHERE `id` = ?");
    $prepared->execute(array($birthdate, $uid));
  }

  $msg = '<div class="alert alert-success">Your profile has been successfully updated.</div>';
}

$data = getProfile($pdoDB, $_SESSION['wwm_admin_id'], 1);

$performing_states_abbr = array("AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");
//$performing_states_select = json_decode($data['performing_states']);

$page_title = "Update Account - ";
include('../header.php'); ?>
<br>
<br>
<br>
<br>
<div class="container">
  <h1>Profile</h1>
  <hr>
  <div class="row">
    <?php
    if(isset($_GET['pending']) && $_GET['pending'] == 'true') {
      ?>
      <div class="alert alert-warning alert-dismissable" id="success_alert">
        <a class="panel-close close" data-dismiss="alert">×</a>
        <p id="success">Your account is pending admin approval. You will gain access to other sections onces approved.</p>
      </div>
      <?php
    }
    ?>
    <?php
    if(isset($_GET['submit']) && $_GET['submit'] == 'true') {
      ?>
      <div class="alert alert-success alert-dismissable" id="success_alert">
        <a class="panel-close close" data-dismiss="alert">×</a>
        <p id="success">Your details have been updated successfully</p>
      </div>
      <?php
    }

    echo @$msg;
    ?>
    <form name="admin_update_form" id="admin_update_form" method="post" action="<?php echo $siteurl; ?>/master_admin/manage-account.php">

      <!-- edit form column -->

      <div class="col-md-12 personal-info">

        <h3>Personal info</h3>

        <div class="form-group col-xs-12 col-md-6">
          <label for="name" class="control-label">Name</label>
          <input type="text" class="form-control" id="name" name="name" value="<?php echo $data['name']; ?>">
        </div>
        <div class="form-group col-xs-12 col-md-6">
          <label for="email" class="control-label">Email</label>
          <input type="text" class="form-control" id="email" name="email" value="<?php echo $data['email']; ?>">
        </div>
        <div class="form-group col-xs-12 col-md-6">
          <label for="pass" class="control-label">New Password</label>
          <input type="text" class="form-control" id="pass" name="pass">
        </div>
        <div class="form-group col-xs-12 col-md-6">
          <label for="pass_c" class="control-label">Confirm Password</label>
          <input type="text" class="form-control" id="pass_c" name="pass_c">
        </div>

        <div class="form-group col-xs-12 col-md-12">
          <label for="address" class="control-label">Street</label>
          <input type="text" class="form-control" id="street" name="street" value="<?php echo $data['street']; ?>" >
        </div>

        <div class="form-group col-xs-12 col-md-4">
          <label for="city" class="control-label">City</label>
          <input type="text" class="form-control" id="city" name="city" value="<?php echo $data['city']; ?>">
        </div>

        <div class="form-group col-xs-12 col-md-4">
          <label for="state" class="control-label">State</label>
          <input type="text" class="form-control" id="state" maxlength="2" name="state" value="<?php echo $data['state']; ?>">
        </div>

        <div class="form-group col-xs-12 col-md-4">
          <label for="zip" class="control-label">Zip</label>
          <input type="text" maxlength="5" class="form-control" id="zip" name="zip" value="<?php echo $data['zip']; ?>">
        </div>


        <div class="form-group col-xs-4 col-md-4">
          <label for="birthdate_month" class="control-label">Birthday Month</label>
          <select class="form-control" id="birthdate_month" name="birthdate_month">
            <option value="1" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 1){ ?> selected="selected" <?php } ?>>January</option>
            <option value="2" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 2){ ?> selected="selected" <?php } ?>>Febuary</option>
            <option value="3" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 3){ ?> selected="selected" <?php } ?>>March</option>
            <option value="4" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 4){ ?> selected="selected" <?php } ?>>April</option>
            <option value="5" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 5){ ?> selected="selected" <?php } ?>>May</option>
            <option value="6" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 6){ ?> selected="selected" <?php } ?>>June</option>
            <option value="7" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 7){ ?> selected="selected" <?php } ?>>July</option>
            <option value="8" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 8){ ?> selected="selected" <?php } ?>>August</option>
            <option value="9" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 9){ ?> selected="selected" <?php } ?>>September</option>
            <option value="10" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 10){ ?> selected="selected" <?php } ?>>October</option>
            <option value="11" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 11){ ?> selected="selected" <?php } ?>>November</option>
            <option value="12" <?php if (@date('m', @strtotime(@$data['birthdate'])) == 12){ ?> selected="selected" <?php } ?>>December</option>
          </select>
        </div>
        <div class="form-group col-xs-4 col-md-4">
          <label for="birthdate_day" class="control-label">Birthday Day</label>
          <!-- <input type="number" placeholder="01" max-length="2" min-length="1" class="form-control" id="birthdate_day" name="birthdate_day" value="<?php echo intval(@date('d', @strtotime(@$data['birthdate']))); ?>" /> -->
          <select class="form-control" id="birthdate_day" name="birthdate_day" required="required">
            <option value="1">Day</option>
            <option value="1" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 1){ ?> selected="selected" <?php } ?>>1</option>
            <option value="2" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 2){ ?> selected="selected" <?php } ?>>2</option>
            <option value="3" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 3){ ?> selected="selected" <?php } ?>>3</option>
            <option value="4" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 4){ ?> selected="selected" <?php } ?>>4</option>
            <option value="5" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 5){ ?> selected="selected" <?php } ?>>5</option>
            <option value="6" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 6){ ?> selected="selected" <?php } ?>>6</option>
            <option value="7" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 7){ ?> selected="selected" <?php } ?>>7</option>
            <option value="8" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 8){ ?> selected="selected" <?php } ?>>8</option>
            <option value="9" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 9){ ?> selected="selected" <?php } ?>>9</option>
            <option value="10" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 10){ ?> selected="selected" <?php } ?>>10</option>
            <option value="11" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 11){ ?> selected="selected" <?php } ?>>11</option>
            <option value="12" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 12){ ?> selected="selected" <?php } ?>>12</option>
            <option value="13" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 13){ ?> selected="selected" <?php } ?>>13</option>
            <option value="14" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 14){ ?> selected="selected" <?php } ?>>14</option>
            <option value="15" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 15){ ?> selected="selected" <?php } ?>>15</option>
            <option value="16" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 16){ ?> selected="selected" <?php } ?>>16</option>
            <option value="17" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 17){ ?> selected="selected" <?php } ?>>17</option>
            <option value="18" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 18){ ?> selected="selected" <?php } ?>>18</option>
            <option value="19" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 19){ ?> selected="selected" <?php } ?>>19</option>
            <option value="20" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 20){ ?> selected="selected" <?php } ?>>20</option>
            <option value="21" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 21){ ?> selected="selected" <?php } ?>>21</option>
            <option value="22" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 22){ ?> selected="selected" <?php } ?>>22</option>
            <option value="23" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 23){ ?> selected="selected" <?php } ?>>23</option>
            <option value="24" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 24){ ?> selected="selected" <?php } ?>>24</option>
            <option value="25" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 25){ ?> selected="selected" <?php } ?>>25</option>
            <option value="26" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 26){ ?> selected="selected" <?php } ?>>26</option>
            <option value="27" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 27){ ?> selected="selected" <?php } ?>>27</option>
            <option value="28" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 28){ ?> selected="selected" <?php } ?>>28</option>
            <option value="29" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 29){ ?> selected="selected" <?php } ?>>29</option>
            <option value="30" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 30){ ?> selected="selected" <?php } ?>>30</option>
            <option value="31" <?php if (intval(@date('d', @strtotime(@$data['birthdate']))) == 31){ ?> selected="selected" <?php } ?>>31</option>

          </select>
        </div>
        <div class="form-group col-xs-4 col-md-4">
          <label for="birthdate_year" class="control-label">Birthday Year</label>
          <!-- <input type="number" placeholder="1980" max-length="4" min-length="4" class="form-control" id="birthdate_year" name="birthdate_year" value="<?php echo intval(@date('Y', @strtotime(@$data['birthdate']))); ?>" /> -->
          <select id="birthdate_year" name="birthdate_year" class="form-control" required="required">
            <option>Year</option>
            <?php for ($i=1998; $i > 1933; $i--) {
              if (intval(@date('Y', @strtotime(@$data['birthdate']))) == $i){
                $a = 'selected="selected"';
              }else{
                $a = '';
              }
              echo '<option value="'.$i.'" '.$a.'>'.$i.'</option>';
            } ?>
          </select>
        </div>
        <div class="form-group col-xs-12 col-md-6 hidden">
          <label for="wwm_admin_id" class="control-label">Hidden user ID</label>
          <input type="text" class="form-control" id="wwm_admin_id" name="wwm_admin_id" value="<?php echo $_SESSION['wwm_admin_id']; ?>" readonly>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label"></label>
          <div class="col-md-8">
            <input type="submit" class="btn btn-primary" value="Save Changes">
          </div>
        </div>

      </div>
    </div>
  </form>


</div>
<hr>
<script>
var admin_id = '<?php echo $_SESSION['wwm_admin_id']; ?>';
$(document).ready(function(){
	$("#zip").mask("99999");
});
</script>
<!-- pattern -->

<?php include('../footer.php'); ?>
