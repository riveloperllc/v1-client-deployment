<?php
ob_start();
session_start();
require_once('common/connection.php');
require_once('common/config.php');
$page_name=basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Upload Pre-Marriage Education Certificate - ";
include('header.php');
?>

<div class="form_main">
    <div class="form_main_inside">


        <div class="login">

        		<?php

		if (isset($_POST['submit_cofme']) && $_POST['submit_cofme']!='')
		{

		if(isset($_FILES['certificate_of_pre_marriage_education']['name']) && $_FILES['certificate_of_pre_marriage_education']['name']!='')
			{

			$allowedExts = array(
			"pdf",
			"doc",
			"docx"
			);

			$user_id = $_POST['user_id'];
			$file_name	= $_FILES['certificate_of_pre_marriage_education']['name'];
			$org_file_name	= $file_name;
			$ext = end((explode(".", $file_name)));

			if (!in_array($ext, $allowedExts))
				{
				$_SESSION['message'] = "Please upload certificate in pdf, doc or docx format only.";
				$_SESSION['flag'] = "2";
				header("location: admin_upload_premar_edu_cert.php?user_id=".$user_id);
				exit();
				}


			$date = strtotime(date("YmdHis"));

			$certpremarriage_path = 'upload/certpremarriage';
			$file_location =  $certpremarriage_path.'/'.$date.'_'.$user_id.'.'.$ext;

			if (move_uploaded_file($_FILES['certificate_of_pre_marriage_education']['tmp_name'], $file_location))
				{
				$query_string_upmec = "update wwm_users set upload_pre_marriage_education_certificate='".$file_location."', marriage_education_certificate='Upload' where user_id = ".$user_id."";
				mysql_query($query_string_upmec);

				if(isset($_POST['old_certificate']) && $_POST['old_certificate']!='')
					{
					$unlink_file=  $_POST['old_certificate'];
					@unlink($unlink_file);
					}


				$sql_users = "SELECT * FROM wwm_users WHERE user_id = ".$user_id."";
				$result_users = mysql_query($sql_users);
				$row_user = mysql_fetch_array($result_users);
				$mailto = $row_user['email'];

				$file = $root_url ."$file_location";

				$file_size = filesize($file);
				$handle = fopen($file, "r");
				$content = fread($handle, $file_size);
				fclose($handle);
				$content = chunk_split(base64_encode($content));
				$uid = md5(uniqid(time()));
				$separator = md5(time());
				$eol = PHP_EOL;

				$host = $_SERVER['HTTP_HOST'];
				$attch1 = "noreply@$host";

				// main header (multipart mandatory)
				$headers = "From: \" WebWedMobile \"<noreply@$host>\n";
				$headers .= "MIME-Version: 1.0" . $eol;
				$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
				$headers .= "Content-Transfer-Encoding: 7bit";

				$message = "Please find attached Certificate of Completion of Qualifying Premarital Education.";
				// message
				$body = "This is a MIME encoded message." . $eol . $eol;
				$body .= "--" . $separator . $eol;
				$body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
				$body .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
				$body .= $message . $eol . $eol;

				// attachment
				$file = $file_name;
				$body .= "--" . $separator . $eol;
				$body .= "Content-Type: application/octet-stream; name=\"" . $file . "\"" . $eol;
				$body .= "Content-Transfer-Encoding: base64" . $eol;
				$body .= "Content-Disposition: attachment" . $eol . $eol;
				$body .= $content . $eol . $eol;
				$body .= "--" . $separator . "--";

				//SEND Mail
				$subject = "Certificate of Completion of Qualifying Premarital Education";
				email($mailto, $subject, $body, $headers);




				$_SESSION['message'] = "Pre-Marriage Education Certificate uploaded and sent successfully to registered email address of couple.";
				$_SESSION['flag'] = "1";
				//header("location: manage_users.php"); exit;
				}

			}
		else
		{
		$_SESSION['message'] = "Please upload Certificate of Completion for a Qualified Premarital Education Program.";
		$_SESSION['flag'] = "2";
		//header("location: manage_users.php"); exit;
		}
		}


        if(isset($_SESSION['message']) && $_SESSION['message']!='')
        {
        if($_SESSION['flag'] == 1)
        {
        echo "<div class='alert-box success1'>".$_SESSION['message']."</div>";
        }
        if($_SESSION['flag'] == 2)
        {
        echo "<div class='alert-box error1'><span>error: </span>".$_SESSION['message']."</div>";
        }
        unset($_SESSION['flag']);
        unset($_SESSION['message']);
        }
        ?>

            <h1>Pre-Marriage Education Certificate</h1>
            <?php
			$sql = "SELECT * FROM wwm_users WHERE user_id ='" . $_GET['user_id'] . "'";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
			?>

            <form name="form_upload_pmec" id="form_upload_pmec" method="post" action="" enctype="multipart/form-data">
            	<input type="hidden" name="user_id" value="<?php echo $_GET['user_id'];?>" />

                <label>Bride Name: <?php echo $row["brid_government_name"]; ?></label>
                <label>Groom Name: <?php echo $row["groom_government_name"]; ?></label>
                <label>Applied Date: <?php echo $row["request_date_cert_pre_marri_edu"]; ?></label>
                <label>Status: <?php echo $row["marriage_education_certificate"]; ?></label>
                <?php
				if(isset($row['marriage_education_certificate']) && $row['marriage_education_certificate'] == 'Upload')
				{?>
                 <p>&nbsp;</p>
                 <input type="hidden" name="old_certificate" value="<?php echo $row["upload_pre_marriage_education_certificate"]; ?>" />
                <p>
                <a href="download.php?filename=<?php echo $row["upload_pre_marriage_education_certificate"]; ?>">Download Certificate of Completion of Qualifying Premarital Education</a>
                </p>
                <?php
				}
				?>

               <p>&nbsp;</p>
                <label>Upload Certificate of Completion of Qualifying Premarital Education</label>
                <input type="file" class="input_text" name="certificate_of_pre_marriage_education" id="certificate_of_pre_marriage_education" />


                <input type="submit" name="submit_cofme"  value="Submit" class="btn" />
                 <a class="btn" href="manage_users.php">Go Back</a>
                </p>
            </form>
        </div>
    </div>
</div>

<?php include('bottom_link.php'); ?>


<?php include('footer.php'); ?>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>

$('#when_married').datetimepicker({minDate: 0, format: 'Y/m/d g:i A',formatTime: 'A g:i'});

$('#witness_first_dob').datetimepicker({timepicker:false,maxDate: 0, format: 'Y/m/d'});

$('#witness_second_dob').datetimepicker({timepicker:false,maxDate: 0, format: 'Y/m/d'});


$(function() {
   $( "#edit_user_profile_form" ).validate({
           rules: {
		   			type_of_wedding: {
							required: true
					},
					state_located: {
							required: true
					},
					when_married: {
							required: true
					},
					marriage_counseling: {
							required: true
					},
					people_view_wedding: {
							required: true
					},
					email: {
							required: true,

							email: true,
							remote: "checkEmail.php"
					},
					password: {
							required: true,
							minlength:6
					},
					cpassword: {
							required: true,
							minlength:6,
							equalTo:"#password"
					},


					brid_over_eighteen: {
							required: true
					},
					brid_government_name: {
							required: true
					},
					brid_state: {
							required: true
					},
					brid_city: {
							required: true
					},
					brid_zipcode: {
							required: true,
							number:true,
							minlength:5,
							maxlength:5
					},
					groom_over_eighteen: {
							required: true
					},
					groom_government_name: {
							required: true
					},
					groom_state: {
							required: true
					},
					groom_city: {
							required: true
					},
					groom_zipcode: {
							required: true,
							number:true,
							minlength:5,
							maxlength:5
					},

					witness_first_name:{
						required:true
					},
					witness_first_dob:{
						required:true
					},
					witness_second_name:{
						required:true
					},
					witness_second_dob:{
						required:true
					}
           },
           messages: {

		   			type_of_wedding: {
							required: "Please select type of wedding."
					},
					state_located: {
							required: "Please select state are you located in."
					},
					when_married: {
							required: "Please select date for married."
					},
					marriage_counseling: {
							required: "Please select your answer."
					},
					people_view_wedding: {
							required: "How many people will be viewing the wedding?"
					},
					email: {
							required: "Please enter email address.",
							email: "Please enter valid email address.",
							remote: "Please enter another email address."
					},
					password: {
							required: "Please enter password."
					},
					cpassword: {
							required: "Please enter confirm password.",
							equalTo:"Password does not match with confirm password."
					},

					brid_over_eighteen: {
							required: "Please select your answer."
					},
					brid_government_name: {
							required: "Please enter bride name."
					},
					brid_state: {
							required: "Please select bride state."
					},
					brid_city: {
							required: "Please enter bride city."
					},
					brid_zipcode: {
							required: "Please enter bride zipcode."
					},
					groom_over_eighteen: {
							required: "Please select your answer."
					},
					groom_government_name: {
								required: "Please enter groom name."
					},
					groom_state: {
							required: "Please select groom state."
					},
					groom_city: {
							required: "Please enter groom city."
					},
					groom_zipcode: {
							required: "Please enter groom zipcode."
					},

					witness_first_name:{
						required:"Please enter witness name."
					},
					witness_first_dob:{
						required:"Please enter witness date of birth."
					},
					witness_second_name:{
						required:"Please enter witness name."
					},
					witness_second_dob:{
						required:"Please enter witness date of birth."
					}

		   }

   });
});
</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
