<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'])){
  header("location: admin_login.php");
}
?>
<?php
require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');
require_once('../common/utility_functions.php');
?>
<!-- Middle Start -->
<?php  $page_name = basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Users - ";
include('../header.php'); ?>

<script>
var editor;
$(document).ready(function(){
  window.table = $('#table').DataTable( {
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "datatable_handlers/user_datatable.php?user_token=<?php echo @$_SESSION['wwm_admin_token']; ?>",
      "data": function ( d ){
        d.basepath = true;
      }
    },
    "lengthChange": false,
    columnDefs: [
      {
        targets: [0],
        visible: 0
      },
      {
        targets: [3,4,5,6,8],
        className: 'hidden-xs hidden-sm'
      }
    ],
    columns: [
      { data: 'DT_RowId' },
      { data: 'name' },
      { data: 'email' },
      { data: 'gender' },
      { data: 'birthdate' },
      { data: 'city' },
      { data: 'state' },
      { data: 'phone' },
      { data: 'timezone' }
    ]
  } );
} );
</script>
<br/>
<br/>
<br/>
<br/>
<div class="container">
  <div class="form_main_inside">
    <div class="login payment dashboard">
      <h1><span>Users</span></h1><br/>

      <div class="table-responsive">
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Email Address</th>
            <th>Gender</th>
            <th>Birthdate</th>
            <th>City</th>
            <th>State</th>
            <th>Phone</th>
            <th>Timezone</th>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<!-- pattern -->


<?php include('../footer.php'); ?>
