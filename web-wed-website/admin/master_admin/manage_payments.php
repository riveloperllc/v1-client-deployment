<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'])){
  header("location: admin_login.php");
}
?>
<?php
require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');
require_once('../common/utility_functions.php');
?>
<!-- Middle Start -->
<?php  $page_name = basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Payments - ";
include('../header.php'); ?>
<script>
$(document).ready(function(){
	var table = $('#payments').DataTable({
		"responsive": true,
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "datatable_handlers/payment_datatable.php?user_token=<?php echo @$_SESSION['wwm_admin_token']; ?>",
			"data": function ( d ){
				d.basepath = true;
			}
		},
    "lengthChange": false,
		columnDefs: [
      {
        targets: [0],
        visible: 0
      },
      {
      	targets: [0,4,5,6],
      	className: 'hidden-xs hidden-sm'
    	}
  	]
	});

	// $("#weddings tbody").on('click', 'tr', function(){
	// 	var data = table.row(this).data();
	// 	var id = data[0];
	// 	location.href = '<?php echo SITEURL; ?>officiant_admin/manage_wedding.php?id='+id;
	// });
});
</script>
<br/>
<br/>
<br/>
<br/>
<div class="container">
  <div class="form_main_inside">
    <div class="">
      <h1><span>Payments</span></h1>
      <div class="table-responsive">
				<table id="payments" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<th>ID</th>
						<th>Name</th>
            <th>Product</th>
						<th>Payment</th>
						<th>Transaction ID</th>
            <th>Discount Code</th>
						<th>Date and Time</th>
					</thead>
					<tbody></tbody>
				</table>
			</div>
    </div>
  </div>
</div>


<!-- pattern -->


<?php include('../footer.php'); ?>
