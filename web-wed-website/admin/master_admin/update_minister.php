<?php
session_start();
if(!isset($_SESSION['wwm_admin_id'], $_SESSION['wwm_admin_token'])){
  header("location: admin_login.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');

// if (!paymentIsCurrent($pdoDB, $_SESSION['wwm_admin_id'])){
//   header("Location: make_payment.php");
// 	exit;
// }

if (!isLoggedInAsAdmin($_SESSION['wwm_admin_token'])){
  header("Location: logout.php");
}

if (isset($_POST['accountstatus'], $_POST['uid'])){
  $uid = $_POST['uid'];
  $status = $_POST['accountstatus'];

  $prepared = $pdoDB->prepare("UPDATE `ministers` SET `admin_approved` = ? WHERE `minister_id` = ?");
  $prepared->execute(array($status, $uid));


  $msg = '<div class="alert alert-success">Officiant has been successfully updated.</div>';
}

if (!isset($_GET['id'])){
  header("Location: index.php");
  exit;
}
$officiant_data = getProfile($pdoDB, @$_GET['id'], 0);

$performing_states_abbr = array("AL","AZ","AR","CA","CO","CT","DE","FL","GA","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");
$performing_states_select = json_decode($officiant_data['performing_states']);

$page_title = "Update Officiant - ";
include('../header.php'); ?>
<br>
<br>
<br>
<style>
#stateItem {
  width: 5%;
  float:left;
}
</style>
<div class="container">
  <h1>Update Officiant</h1>
  <hr>
  <div class="row">
    <?php
    echo @$msg;
    ?>
    <form name="minister_update_form2" id="minister_update_form2" method="post">
      <!-- left column -->
      <div class="col-md-3">
        <div class="text-center">
          <?php if($officiant_data['photo'] === '') {
            ?>
            <img src="<?php echo $siteurl.'/officiant_admin/upload/default_avatar_male.jpg'?>" class="avatar img-circle img-responsive" alt="avatar">
            <?php
          } else {
            ?>
            <img src="<?php echo $siteurl.'/officiant_admin/upload/'.$officiant_data['photo']; ?>" class="avatar img-circle img-responsive" alt="avatar">
            <?php
          }
          ?>

        </div>
      </div>

      <!-- edit form column -->

      <div class="col-md-9 personal-info">
        <div class="row">
          <h3>Admin Actions</h3>

          <div class="form-group col-xs-12 col-md-6">
            <label for="name" class="control-label">Officiant Account Status</label>
            <select name="accountstatus" class="form-control">
              <option value="0" <?php if ($officiant_data['admin_approved'] == 0){ ?> selected="selected" <?php } ?>>Not Activated</option>
              <option value="1" <?php if ($officiant_data['admin_approved'] == 1){ ?> selected="selected" <?php } ?>>Activated</option>
            </select>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="submit" class="btn btn-primary" onClick="$('form').submit();" value="Save">
            </div>
          </div>


          <div class="form-group col-xs-12 col-md-6 hidden">
            <label for="uid" class="control-label">Hidden user ID</label>
            <input type="hidden" class="form-control" id="uid" name="uid" value="<?php echo @$_GET['id']; ?>" readonly>
          </div>
        </div>
        <br/>
        <div class="row">

          <h3>Personal Info</h3>

          <div class="form-group col-xs-12 col-md-6">
            <label for="officiant_name" class="control-label">Name</label>
            <input type="text" class="form-control" id="officiant_name" name="officiant_name" value="<?php echo $officiant_data['name']; ?>" readonly>
          </div>
          <div class="form-group col-xs-12 col-md-6">
            <label for="officiant_email" class="control-label">Phone</label>
            <input type="text" class="form-control" id="officiant_phone" name="officiant_phone" value="<?php echo $officiant_data['phone']; ?>" readonly>
          </div>
          <div class="form-group col-xs-12 col-md-6">
            <label for="officiant_email" class="control-label">Email</label>
            <input type="text" class="form-control" id="officiant_email" name="officiant_email" value="<?php echo $officiant_data['email']; ?>" readonly>
          </div>
          <div class="form-group col-xs-12 col-md-12">
            <label for="religious_affiliation" class="control-label">Religious Affiliation</label>
            <select name="religion" class="form-control" disabled="disabled" readonly>
              <option value="other" <?php if($officiant_data['religious_affiliation'] == 'other') {echo 'selected';} ?>>Other – N/A</option>
              <option value="christian" <?php if($officiant_data['religious_affiliation'] == 'Christian') {echo 'selected';} ?>>Christian</option>
              <option value="catholic" <?php if($officiant_data['religious_affiliation'] == 'Catholic') {echo 'selected';} ?>>Catholic</option>
              <option value="jewish" <?php if($officiant_data['religious_affiliation'] == 'Jewish') {echo 'selected';} ?>>Jewish</option>
              <option value="hindu" <?php if($officiant_data['religious_affiliation'] == 'Hindu') {echo 'selected';} ?>>Hindu</option>
              <option value="buddhist" <?php if($officiant_data['religious_affiliation'] == 'Buddhist') {echo 'selected';} ?>>Buddhist</option>
              <option value="religious" <?php if($officiant_data['religious_affiliation'] == 'Religious') {echo 'selected';} ?>>Religious</option>
              <option value="nonreligious" <?php if($officiant_data['religious_affiliation'] == 'NonReligious') {echo 'selected';} ?>>Non–Religious</option>
              <option value="intefaith" <?php if($officiant_data['religious_affiliation'] == 'Interfaith') {echo 'selected';} ?>>Interfaith</option>
            </select>
          </div>
          <div class="form-group col-xs-12 col-md-12">
            <label for="officiant_bio" class="control-label">Bio</label>
            <textarea class="form-control" id="officiant_bio" name="officiant_bio" readonly><?php echo base64_decode($officiant_data['bio']); ?></textarea>
          </div>


          <div class="form-group col-xs-12 col-md-12">
						<label for="address" class="control-label">Address</label>
						<input type="text" class="form-control" id="address" name="address" disabled="disabled" value="<?php echo $officiant_data['address']; ?>" >
					</div>

					<div class="form-group col-xs-12 col-md-4">
						<label for="city" class="control-label">City</label>
						<input type="text" class="form-control" id="city" name="city" disabled="disabled" value="<?php echo $officiant_data['city']; ?>">
					</div>

					<div class="form-group col-xs-12 col-md-4">
						<label for="state" class="control-label">State</label>
						<input type="text" class="form-control" id="state" maxlength="2" name="state" disabled="disabled" value="<?php echo $officiant_data['state']; ?>">
					</div>

					<div class="form-group col-xs-12 col-md-4">
						<label for="zip" class="control-label">Zip</label>
						<input type="number" maxlength="5" class="form-control" id="zip" name="zip" disabled="disabled" value="<?php echo $officiant_data['zip']; ?>">
					</div>


					<div class="form-group col-xs-4 col-md-4">
						<label for="birthdate_month" class="control-label">Birthday Month</label>
						<select class="form-control" id="birthdate_month" name="birthdate_month" disabled="disabled">
							<option value="1" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 1){ ?> selected="selected" <?php } ?>>January</option>
							<option value="2" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 2){ ?> selected="selected" <?php } ?>>Febuary</option>
							<option value="3" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 3){ ?> selected="selected" <?php } ?>>March</option>
							<option value="4" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 4){ ?> selected="selected" <?php } ?>>April</option>
							<option value="5" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 5){ ?> selected="selected" <?php } ?>>May</option>
							<option value="6" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 6){ ?> selected="selected" <?php } ?>>June</option>
							<option value="7" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 7){ ?> selected="selected" <?php } ?>>July</option>
							<option value="8" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 8){ ?> selected="selected" <?php } ?>>August</option>
							<option value="9" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 9){ ?> selected="selected" <?php } ?>>September</option>
							<option value="10" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 10){ ?> selected="selected" <?php } ?>>October</option>
							<option value="11" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 11){ ?> selected="selected" <?php } ?>>November</option>
							<option value="12" <?php if (@date('m', @strtotime(@$officiant_data['birthdate'])) == 12){ ?> selected="selected" <?php } ?>>December</option>
						</select>
					</div>
					<div class="form-group col-xs-4 col-md-4">
						<label for="birthdate_day" class="control-label">Birthday Day</label>
						<input type="number" placeholder="01" max-length="2" min-length="1" class="form-control" id="birthdate_day" name="birthdate_day" disabled="disabled" value="<?php echo intval(@date('d', @strtotime(@$officiant_data['birthdate']))); ?>" />
					</div>
					<div class="form-group col-xs-4 col-md-4">
						<label for="birthdate_year" class="control-label">Birthday Year</label>
						<input type="number" placeholder="1980" max-length="4" min-length="4" class="form-control" id="birthdate_year" name="birthdate_year" disabled="disabled" value="<?php echo intval(@date('Y', @strtotime(@$officiant_data['birthdate']))); ?>" />
					</div>

          <div class="form-group col-xs-12 col-md-12">
            <label for="officiant_states" class="control-label">What states are you licensed to perform in?</label><br><br>
            <?php
            if(!empty($performing_states_abbr)) {
              foreach($performing_states_abbr as $check) {
                if (@in_array($check, $performing_states_select)) {
                  echo "<div id='stateItem'><input type='checkbox' disabled='disabled'  name='performing_states[]' value=".$check." id='performing_states'/ checked><span id='stateName'>".$check."</span></div>";
                } else {
                  echo "<div id='stateItem'><input type='checkbox' disabled='disabled'  name='performing_states[]' value=".$check." id='performing_states'/><span id='stateName'>".$check."</span></div>";
                }
              }
            }
            ?>
          </div>
          <div class="form-group col-xs-6 col-md-6">
            <label>Available From *</label>
            <input type="text" class="form-control" name="starttime" id="starttime" readonly/>
          </div>
          <div class="form-group col-xs-6 col-md-6">
            <label>Available To *</label>
            <input type="text" class="form-control" name="endtime" id="endtime" readonly/>
          </div>
          <div class="form-group col-xs-12 col-md-12">
            <label for="officiant_willing" class="control-label">Are you willing to perform Samesex, and Proxy weddings? *</label>
            <div class="radio willing_radio">
              <label><input type="radio" class="form-control" name="willingness" id="willingness" value="1" disabled='disabled' <?php if($officiant_data['perform_willingness'] == '1') {echo 'checked="checked"';} ?> />Yes</label>
              <label><input type="radio" class="form-control" name="willingness" id="willingness" value="0" disabled='disabled' <?php if($officiant_data['perform_willingness'] == '0') {echo 'checked="checked"';} ?>/>No</label>
            </div>
          </div>
          <div class="form-group col-xs-12 col-md-6 hidden">
            <label for="wwm_minister_id" class="control-label">Hidden Officiant ID</label>
            <input type="text" class="form-control" id="wwm_minister_id" name="wwm_minister_id" value="<?php echo $_SESSION['wwm_minister_id']; ?>" readonly>
          </div>


        </div>
      </div>
    </div>
  </form>


</div>
<hr>
<!-- pattern -->

<?php include('../footer.php'); ?>
