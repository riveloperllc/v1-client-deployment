<?php
ob_start();
session_start();

if(isset($_SESSION['wwm_admin_id'])){
	header("location:manage_users.php");
}
require_once('../common/connection.php');
require_once('../common/config.php');

if(isset($_POST['submit'], $_POST['email'], $_POST['password']))
{
	$email = $_POST['email'];
	$password = md5($_POST['password']);

	$prepared = $pdoDB->prepare("SELECT * FROM www_users_new WHERE (`email` = ? AND `password` = ?) AND (`permission_level` = 5) LIMIT 1");
	$prepared->execute(array($email, $password));

	if($prepared->rowCount() > 0)
	{
		$data = $prepared->fetch(PDO::FETCH_ASSOC);
		$uid = $data['id'];
		$_SESSION['wwm_admin_id'] = $uid;

		$token = hash_hmac('SHA256', md5(mt_rand(9999,99999)), 'WEBWEDMOBILEKEYFILE123!');

		//Close other active sessions.
		$prepared = $pdoDB->prepare("DELETE FROM `tokens` WHERE `uid` = ? AND `type` = 5");
		$prepared->execute(array($uid));

		$prepared = $pdoDB->prepare("INSERT INTO `tokens` VALUES (NULL, ?, ?, ?)");
		$prepared->execute(array($token, $uid, 5));

		$_SESSION['wwm_admin_token'] = $token;
		header("Location: manage_users.php");
		exit;
	}
	else
	{
		$msg = "<div class='alert alert-danger'>Your email or password is incorrect. Please try again.</br/></br/></div>";
	}

}
?>
<?php  $page_name=basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Admin Login -";
include('../header.php'); ?>
<br/>
<br/>
<br/>
<div class="form_main">
	<div class="login">
		<h1>Admin Login</h1>
		<?php echo @$msg; ?>
		<form class="form-horizontal" role="form" name="admin_login_form" id="admin_login_form" method="post" action="<?php echo SITEURL; ?>master_admin/admin_login.php">
			<input type="text" placeholder="Email" class="input_text" id="email" name="email" value=""/><br/><br/>
			<input type="password" placeholder="Password" class="input_text" id="password" name="password" value=""/><br/><br/>
			<input  type="submit" name="submit" value="Login" class="btn btn-primary" />
		</form>
	</div>
</div>
<?php include('../footer.php'); ?>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>


$(function() {
	$( "#admin_login_form" ).validate({
		rules: {
			email: {
				required: true
			},
			password: {
				required: true
			}
		},
		messages: {
			email: {
				required: "Please enter your email."
			},
			password: {
				required: "Please enter your password."
			}
		}
	});
});
</script>
