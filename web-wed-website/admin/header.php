<?php
if (isset($_SERVER['HTTPS']) || @$_SERVER['HTTPS'] == 'on') {
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: ' . $redirect);
  exit();
}
require_once("common/config.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo @$page_title . ' WebWedMobile'; ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!--Custom CSS-->
  <!-- <link type="text/css" rel="stylesheet" media="screen" href="<?php echo ROOTSITEURL; ?>/css/style.css" /> -->
  <!-- <link type="text/css" rel="stylesheet" media="screen" href="<?php echo ROOTSITEURL; ?>/css/custom.css" /> -->
  <link rel="stylesheet" type="text/css" href="<?php echo ROOTSITEURL; ?>/css/main.css"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

  <!--Boostrap and Responsive CSS-->
  <link rel="stylesheet" href="<?php echo ROOTSITEURL; ?>/css/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="<?php echo ROOTSITEURL; ?>/css/responsive.bootstrap.min.css"/>

  <!--DataTables-->
  <link type="text/css" rel="stylesheet" href="<?php echo ROOTSITEURL; ?>/css/dataTables.min.css"/>
  <link type="text/css" rel="stylesheet" href="<?php echo ROOTSITEURL; ?>/css/dataTables.bootstrap.min.css"/>

  <!--Timepicker-->
  <link type="text/css" rel="stylesheet" href="<?php echo ROOTSITEURL; ?>/css/bootstrap-timepicker.min.css"/>
  <link type="text/css" rel="stylesheet" href="<?php echo ROOTSITEURL; ?>/css/jquery.timepicker.css"/>

  <!--File Input-->
  <link type="text/css" rel="stylesheet" href="<?php echo ROOTSITEURL; ?>/css/fileinput.min.css"/>

  <link rel="stylesheet" type="text/css" href="<?php echo ROOTSITEURL; ?>/css/Web_Wed_BackOffice_Custom.css"/>

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.standalone.css">
  <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>

<body>
  <?php

  @session_start();
  if (isset($_GET['app'])){
    $_SESSION['app'] = true;
  }

  ?>
  <?php if (!isset($_GET['app']) && !isset($_SESSION['app'])){ ?>
  <nav class="navbar navbar-default navbar-fixed-top" id="header" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span> </button>
        <a class="navbar-brand" href="index.php" data-original-title="" title=""> <img class="img-responsive" src="<?php echo ROOTSITEURL; ?>/images/icon/ring_logo.svg" alt="Web Wed Logo"> </a> </div>
        <div class="collapse navbar-collapse" id="navbar">
          <?php
          @session_start();
          define('BASEPATH', true);

          if (isset($_SESSION['wwm_minister_id'])){
            require_once("officiant_admin/nav_loggedin.php");
          }else if(strpos($_SERVER['SCRIPT_NAME'], 'officiant_admin') !== false){
            require_once("officiant_admin/nav.php");
          }else if (isset($_SESSION['wwm_admin_id'], $_SESSION['wwm_admin_token'])){
            require_once("master_admin/nav_loggedin.php");
          }else if (strpos($_SERVER['SCRIPT_NAME'], 'master_admin') !== false){
            require_once("master_admin/nav.php");
          }else if (isset($_SESSION['wwm_user_id'])){
            require_once('user_admin/nav_loggedin.php');
          }else{
            require_once('user_admin/nav.php');
          }
          ?>
        </div>
        <!--/.nav-collapse -->
      </div>
    </nav>
    <?php } ?>
