<?php

function findUserById($db, $id){
  $prepared = $db->prepare("SELECT * FROM www_users_new WHERE id = ? LIMIT 1");
  $prepared->execute(array($id));
  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }else{
    return array('name' => 'Not Available');
  }
}

function findWeddingById($db, $id){
  $prepared = $db->prepare("SELECT * FROM weddings WHERE id = ? LIMIT 1");
  $prepared->execute(array($id));
  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }else{
    return array();
  }
}

function findWeddingByCode($db, $id){
  $prepared = $db->prepare("SELECT * FROM weddings WHERE event_code = ? LIMIT 1");
  $prepared->execute(array($id));
  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }else{
    return array();
  }
}

function findSpecialMomentByCode($db, $id){
  $prepared = $db->prepare("SELECT * FROM special_moments WHERE event_code = ? LIMIT 1");
  $prepared->execute(array($id));
  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }else{
    return array();
  }
}

function findWeddingByMinisterId($db, $id){
  $prepared = $db->prepare("SELECT * FROM weddings WHERE assigned_officiant_uid = ? ORDER BY uid ASC LIMIT 1");
  $prepared->execute(array($id));
  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }else{
    return array();
  }
}

function findOrAddSession($db, $code){
  $openTokAPIKey = "45692092";
  $openTokAPISecret = "d2329b40d5b69006701f047d2b7b83814a5c5676";

  require_once(getcwd() . "/Opentok/vendor/autoload.php");
  $prepared = $db->prepare("SELECT `session_id` FROM `sessions` WHERE `event_code` = ?");
  $prepared->execute(array($code));
  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    $session_id = $row['session_id'];
    return $session_id;
  }else{
    //Create session_id then return it.
    $opentok = new OpenTok\OpenTok($openTokAPIKey, $openTokAPISecret);
    $session = $opentok->createSession(array( 'mediaMode' => 'disabled' ));
    $session_id = $session->getSessionId();

    $prepared = $db->prepare("INSERT INTO `sessions` VALUES (NULL, ?, ?)");
    $prepared->execute(array($code, $session_id));

    return $session_id;
  }
}

function fixRole($role){
  if ($role == 'special_moment'){
    return 'special_moment';
  }

  if ($role == 'witness' || $role == 'witness1'){
    return 'witness1';
  }

  if ($role == 'witness2'){
    return 'witness2';
  }

  if ($role == 'official' || $role == 'officiant'){
    return 'officiant';
  }

  if ($role == 'yours' || $role == 'yours1'){
    return 'yours';
  }

  if ($role == 'yours2'){
    return 'yours2';
  }

  return 'guest';
}

function createTokenForRoleAndSession($session_id, $role){
  $openTokAPIKey = "45692092";
  $openTokAPISecret = "d2329b40d5b69006701f047d2b7b83814a5c5676";
  $role = fixRole($role);

  $roleCode = 'subscriber';
  if ($role == 'official' || $role == 'officiant'){
    $roleCode = 'moderator';
  }else if ($role != 'guest'){
    $roleCode = 'publisher';
  }

  require_once(getcwd() . "/Opentok/vendor/autoload.php");
  $opentok = new OpenTok\OpenTok($openTokAPIKey, $openTokAPISecret);
  $token = $opentok->generateToken($session_id, array(
    'role'       => $roleCode,
    'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
    'data'       => $role
  ));

  return $token;
};


function findWeddingMemberByEmail($db, $email, $type){
  if ($type == 'wedding_party'){
    $prepared = $db->prepare("SELECT * FROM www_users_new WHERE email = ? LIMIT 1");
    $prepared->execute(array($email));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);
      return $row;
    }else{
      return array();
    }
  }else if ($type == 'official'){
    $prepared = $db->prepare("SELECT * FROM ministers WHERE email = ? LIMIT 1");
    $prepared->execute(array($email));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);
      return $row;
    }else{
      return array();
    }
  }else{
    $prepared = $db->prepare("SELECT * FROM guests WHERE email = ? LIMIT 1");
    $prepared->execute(array($email));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);
      return $row;
    }else{
      return array();
    }
  }
}

function getRoleInWedding($db, $wedding_id, $uid){
  $prepared = $db->prepare("SELECT * FROM `weddings` WHERE `id` = ?");
  $prepared->execute(array($wedding_id));

  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    if ($row['uid'] == $uid){
      return 'yours';
    }else if ($row['yours_uid'] == $uid){
      return 'yours2';
    }else if ($row['first_witness_uid'] == $uid){
      return 'witness';
    }else if ($row['second_witness_uid'] == $uid){
      return 'witness2';
    }

    return '';
  }else{
    return '';
  }
}

function findWeddingMemberRoleByEmail($db, $email, $type){
  if ($type == 'wedding_party'){
    $prepared = $db->prepare("SELECT * FROM www_users_new WHERE email = ? LIMIT 1");
    $prepared->execute(array($email));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);

      $prepared = $db->prepare("SELECT * FROM `weddings` WHERE `uid` = :id OR `yours_uid` = :id OR `first_witness_uid` = :id OR `second_witness_uid` = :id");
      $prepared->execute(array(":id" => $row['id']));

      $uid = $row['id'];

      if ($prepared->rowCount() > 0){
        $row = $prepared->fetch(PDO::FETCH_ASSOC);
        if ($row['uid'] == $uid){
          return 'yours';
        }else if ($row['yours_uid'] == $uid){
          return 'yours2';
        }else if ($row['first_witness_uid'] == $uid){
          return 'witness1';
        }else if ($row['second_witness_uid'] == $uid){
          return 'witness2';
        }
      }else{
        return '';
      }
    }else if ($type == 'official'){
      $prepared = $db->prepare("SELECT * FROM ministers WHERE email = ? LIMIT 1");
      $prepared->execute(array($email));
      if ($prepared->rowCount() > 0){
        $row = $prepared->fetch(PDO::FETCH_ASSOC);
        return 'officiant';
      }else{
        return '';
      }
    }

    return '';
  }
}

  function findMinisterById($db, $id){
    $prepared = $db->prepare("SELECT * FROM ministers WHERE minister_id = ? LIMIT 1");
    $prepared->execute(array($id));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);
      return $row;
    }else{
      return array();
    }
  }

  function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  function createToken($db, $id, $type){
    if ($type == 'official'){
      $type = 1;
    }else if ($type == 'wedding_party'){
      $type = 2;
    }else if ($type == 'guest'){
      $type = 3;
    }else{
      $type = 3;
    }

    $token = md5(generateRandomString(10));

    $prepared = $db->prepare("DELETE FROM `tokens` WHERE `uid` = ?");
    $prepared->execute(array($id));

    $prepared = $db->prepare("INSERT INTO `tokens` VALUES (NULL, ?, ?, ?)");
    $prepared->execute(array($token, $id, $type));

    return $token;
  }


  function findWeddingByEventCode($db, $event_code){
  	$prepared = $db->prepare("SELECT * FROM weddings WHERE event_code = ? LIMIT 1");
  	$prepared->execute(array($event_code));
  	if ($prepared->rowCount() > 0){
  		$row = $prepared->fetch(PDO::FETCH_ASSOC);
  		return $row;
  	}else{
  		return array();
  	}
  }


  function login($db, $email, $pass_or_code, $type = 'wedding_party'){
    if ($type == 'wedding_party'){
      $prepared = $db->prepare("SELECT * FROM `www_users_new` WHERE `email` = ? AND `password` = ?");
      $prepared->execute(array($email, md5($pass_or_code)));

      if ($prepared->rowCount() > 0){
        $user = $prepared->fetch(PDO::FETCH_ASSOC);
        $token = createToken($db, $user['id'], 'wedding_party');
        $uid = $user['id'];
        $prepared = $db->prepare("SELECT * FROM `weddings` WHERE `uid` = ? OR `yours_uid` = ? OR `first_witness_uid` = ? OR `second_witness_uid` = ? ORDER BY `id` DESC LIMIT 1");
        $prepared->execute(array($uid, $uid, $uid, $uid));
        if ($prepared->rowCount() > 0){
          $wedding = $prepared->fetch(PDO::FETCH_ASSOC);
          $wedding = array_merge($wedding, array('code' => $wedding['event_code']));
          $wedding = array_merge($wedding, array('session_id' => findOrAddSession($db, $wedding['event_code'])));
          $wedding = array_merge($wedding, array('role' => fixRole(findWeddingMemberRoleByEmail($db, $user['email'], 'wedding_party'))));
          $wedding = array_merge($wedding, array('token_id' => createTokenForRoleAndSession($wedding['session_id'], $wedding['role'])));
          $wedding = array_merge($wedding, array('yours1' => array("name" => @findUserById($db, $wedding['uid'])['name'])));
          $wedding = array_merge($wedding, array('yours2' => array("name" => @findUserById($db, $wedding['yours_uid'])['name'])));
          $wedding = array_merge($wedding, array('officiant' => array("name" => @findMinisterById($db, $wedding['assigned_officiant_uid'])['name'])));
          $wedding = array_merge($wedding, array('witness' => array("name" => @findUserById($db, $wedding['first_witness_uid'])['name'])));
          $wedding = array_merge($wedding, array('witness2' => array("name" => @findUserById($db, $wedding['second_witness_uid'])['name'])));
          $wedding = array_merge($wedding, array('ceremonylink' => 'http://webwedmobile.com/join.php?code='.$wedding['event_code']));
          $wedding = array_merge($wedding, array('settings' => array("public" => $wedding['public'], "gifts" => $wedding['gifts'], "chat" => $wedding['chat'])));
          $wedding = array_merge($wedding, array('date' => array("longDate" => @date('l d Y', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])), 'longTime' => @date('h:i A e', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])), 'computed' => @date('m/d/Y h:i:s A e', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])))));
        }else{
          $wedding = array();
        }

        $user['wedding'] = $wedding;

        $prepared = $db->prepare("SELECT * FROM `special_moments` WHERE `uid` = ? ORDER BY `id` DESC LIMIT 1");
        $prepared->execute(array($uid));
        if ($prepared->rowCount() > 0){
          $special_moment = $prepared->fetch(PDO::FETCH_ASSOC);
          $special_moment = array_merge($special_moment, array('code' => $special_moment['event_code']));
          $special_moment = array_merge($special_moment, array('session_id' => findOrAddSession($db, $special_moment['event_code'])));
          $special_moment = array_merge($special_moment, array('token_id' => createTokenForRoleAndSession($special_moment['session_id'], 'special_moment')));
          $special_moment = array_merge($special_moment, array('role' => 'publisher'));
          if ($special_moment['event_date'] == '' && $special_moment['event_time'] == ''){
            $special_moment = array_merge($special_moment, array('ondemand' => true));
          }else{
            $special_moment = array_merge($special_moment, array('ondemand' => false));
          }
          $special_moment = array_merge($special_moment, array('name' => $special_moment['event_name']));
          $special_moment = array_merge($special_moment, array('ceremonylink' => 'http://webwedmobile.com/join.php?code='.$special_moment['event_code']));
          $special_moment = array_merge($special_moment, array('settings' => array("public" => $special_moment['public'], "gifts" => $special_moment['gifts'], "chat" => $special_moment['chat'])));
          $special_moment = array_merge($special_moment, array('date' => array("longDate" => @date('l d Y', strtotime(@$special_moment['event_date'] . " " . @$special_moment['event_time'])), 'longTime' => @date('h:i A e', strtotime(@$special_moment['event_date'] . " " . @$special_moment['event_time'])), 'computed' => @date('m/d/Y h:i:s A', strtotime(@$special_moment['event_date'] . " " . @$special_moment['event_time'])))));
        }else{
          $special_moment = array();
        }

        $user['special_moment'] = $special_moment;

        return array('success' => true, "token" => $token, "row" => $user);
      }

      $weddingOfficial = findWeddingMemberByEmail($db, $email, 'official');
      if (!empty($weddingOfficial)){
        if (md5($pass_or_code) == $weddingOfficial['password']){
          $token = createToken($db, $weddingOfficial['minister_id'], 'official');
          $wedding = findWeddingByMinisterId($db, $weddingOfficial['minister_id']);
          $wedding = array_merge($wedding, array('code' => $wedding['event_code']));
          $wedding = array_merge($wedding, array('session_id' => findOrAddSession($db, $wedding['event_code'])));
          $wedding = array_merge($wedding, array('role' => 'officiant'));
          $wedding = array_merge($wedding, array('token_id' => createTokenForRoleAndSession($wedding['session_id'], $wedding['role'])));
          $wedding = array_merge($wedding, array('yours1' => array("name" => @findUserById($db, $wedding['uid'])['name'])));
          $wedding = array_merge($wedding, array('yours2' => array("name" => @findUserById($db, $wedding['yours_uid'])['name'])));
          $wedding = array_merge($wedding, array('officiant' => array("name" => @findMinisterById($db, $wedding['assigned_officiant_uid'])['name'])));
          $wedding = array_merge($wedding, array('witness' => array("name" => @findUserById($db, $wedding['first_witness_uid'])['name'])));
          $wedding = array_merge($wedding, array('witness2' => array("name" => @findUserById($db, $wedding['second_witness_uid'])['name'])));
          $wedding = array_merge($wedding, array('ceremonylink' => 'http://webwedmobile.com/join.php?code='.$wedding['event_code']));
          $wedding = array_merge($wedding, array('settings' => array("public" => $wedding['public'], "gifts" => $wedding['gifts'], "chat" => $wedding['chat'])));
          $wedding = array_merge($wedding, array('date' => array("longDate" => @date('l d Y', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])), 'longTime' => @date('h:i A', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])), 'computed' => @date('m/d/Y h:i:s A', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])))));

          return array('success' => true, "token" => $token, "row" => array("wedding" => $wedding));
        }else{
          return array("success" => false, "token" => "", "row" => array());
        }
      }else{
        return array("success" => false, "token" => "", "row" => array());
      }

    }else{
      $guestMember = findWeddingMemberByEmail($db, $email, 'guest');

      $wedding = findWeddingByCode($db, $pass_or_code);
      $specialMoment = findSpecialMomentByCode($db, $pass_or_code);

      if (!empty($wedding)){
        // if (!$wedding['public'] && empty($guestMember)){
        //   return array('success' => false, "token" => "", "row" => array());
        // }

        $wedding = array_merge($wedding, array('code' => $wedding['event_code']));
        $wedding = array_merge($wedding, array('session_id' => findOrAddSession($db, $wedding['event_code'])));
        $wedding = array_merge($wedding, array('role' => 'guest'));
        $wedding = array_merge($wedding, array('token_id' => createTokenForRoleAndSession($wedding['session_id'], 'guest')));
        $wedding = array_merge($wedding, array('yours1' => array("name" => @findUserById($db, $wedding['uid'])['name'])));
        $wedding = array_merge($wedding, array('yours2' => array("name" => @findUserById($db, $wedding['yours_uid'])['name'])));
        $wedding = array_merge($wedding, array('officiant' => array("name" => @findMinisterById($db, $wedding['assigned_officiant_uid'])['name'])));
        $wedding = array_merge($wedding, array('witness' => array("name" => @findUserById($db, $wedding['first_witness_uid'])['name'])));
        $wedding = array_merge($wedding, array('witness2' => array("name" => @findUserById($db, $wedding['second_witness_uid'])['name'])));
        $wedding = array_merge($wedding, array('ceremonylink' => 'http://webwedmobile.com/join.php?code='.$wedding['event_code']));
        $wedding = array_merge($wedding, array('settings' => array("public" => $wedding['public'], "gifts" => $wedding['gifts'], "chat" => $wedding['chat'])));
        $wedding = array_merge($wedding, array('date' => array("longDate" => @date('l d Y', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])), 'longTime' => @date('h:i A', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])), 'computed' => @date('m/d/Y h:i:s A', strtotime(@$wedding['event_date'] . " " . @$wedding['event_time'])))));

        if ($wedding['event_code'] == $pass_or_code){
          $token = $pass_or_code;//createToken($db, $guestMember['guest_id'], 'guest');
          return array('success' => true, "token" => $token, "row" => array("guestInstance" => $guestMember, "specialMomentInstance" => array(), "weddingInstance" => $wedding));
        }else{
          return array('success' => false, "token" => "", "row" => array());
        }
      }

      if (!empty($specialMoment)){
        // if (!$specialMoment['public'] && empty($guestMember)){
        //   return array('success' => false, "token" => "", "row" => array());
        // }

        $special_moment = $specialMoment;

        $special_moment = array_merge($special_moment, array('code' => $special_moment['event_code']));
        $special_moment = array_merge($special_moment, array('session_id' => findOrAddSession($db, $special_moment['event_code'])));
        $special_moment = array_merge($special_moment, array('token_id' => createTokenForRoleAndSession($special_moment['session_id'], 'guest')));
        if ($special_moment['event_date'] == '' && $special_moment['event_time'] == ''){
          $special_moment = array_merge($special_moment, array('ondemand' => true));
        }else{
          $special_moment = array_merge($special_moment, array('ondemand' => false));
        }
        $special_moment = array_merge($special_moment, array('name' => $special_moment['event_name']));
        $special_moment = array_merge($special_moment, array('ceremonylink' => 'http://webwedmobile.com/join.php?code='.$special_moment['event_code']));
        $special_moment = array_merge($special_moment, array('settings' => array("public" => $special_moment['public'], "gifts" => $special_moment['gifts'], "chat" => $special_moment['chat'])));
        $special_moment = array_merge($special_moment, array('date' => array("longDate" => @date('F d Y', strtotime(@$special_moment['event_date'] . " " . @$special_moment['event_time'])), 'longTime' => @date('h:i A', strtotime(@$special_moment['event_date'] . " " . @$special_moment['event_time'])), 'computed' => @date('m/d/Y h:i:s A', strtotime(@$special_moment['event_date'] . " " . @$special_moment['event_time'])))));
        if (empty($guestMember)){
          $guestMember['id'] = $email;
        }
        if ($special_moment['event_code'] == $pass_or_code){
          $token = createToken($db, $guestMember['id'], 'guest');
          return array('success' => true, "token" => $token, "row" => array("guestInstance" => $guestMember, "specialMomentInstance" => $special_moment, "weddingInstance" => array()));
        }else{
          return array('success' => false, "token" => "", "row" => array());
        }
      }
    }
  }

  function generateError($errorCode){
    $errors = array(
      1000 => "Please provide the type of login.",
      1001 => "Please provide an email, and password.",
      1002 => "Login was incorrect, please double check the fields above and try again.",
      1003 => "Please provide an email, and weddingcode.",
      1004 => "Please provide a command, and try again!",
      1005 => "Please provide a type of code.",
      1006 => "Please provide an email.",
      1007 => "Please provide a memberRole.",
      1008 => "Please provide a token.",
      1009 => "Please provide a key.",
      1010 => "Please provide all fields and try again.",
      1011 => "You are currently not in a wedding, please try again later.",
      1012 => "You must be part of the wedding party to alter settings.",
      1013 => "You must enter a valid token in order to process this request.",
      1014 => "An error occurred when attempting to process your request. Please try again later.",
      1015 => "Missing product type",
      1017 => "This wedding has already been accepted by another officiant.",
      1019 => "Invalid coupon code, please check your coupon code and try again."
    );

    return json_encode(array("errorCode" => $errorCode, "error" => $errors[$errorCode], "post" => $_POST));
  }

  function getNameFromName($db, $email, $weddingInstance, $role){
    if ($role == 'official' || $role == 'officiant'){
      $prepared = $db->prepare("SELECT * FROM ministers WHERE email = ? LIMIT 1");
      $prepared->execute(array($email));
      if ($prepared->rowCount() > 0){
        $row = $prepared->fetch(PDO::FETCH_ASSOC);
        return $row['name'];
      }else{
        return 'Not Available';
      }
    }else{
      $prepared = $db->prepare("SELECT * FROM www_users_new WHERE email = ? LIMIT 1");
      $prepared->execute(array($email));
      if ($prepared->rowCount() > 0){
        $row = $prepared->fetch(PDO::FETCH_ASSOC);
        return $row['name'];
      }else{
        return 'Not Available';
      }
    }
  }

  function checkUser($db, $email){
    $prepared = $db->prepare("SELECT * FROM www_users_new WHERE email = ? LIMIT 1");
    $prepared->execute(array($email));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);
      return true;
    } else {
      return false;
    }
  }

  function getUserByEmail($db, $email){
    $prepared = $db->prepare("SELECT * FROM www_users_new WHERE email = ? LIMIT 1");
    $prepared->execute(array($email));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);
      return $row;
    } else {
      return array();
    }
  }

  function checkDiscount($db, $code){

    $prepared = $db->prepare("SELECT * FROM discount_codes WHERE code = ? LIMIT 1");
    $prepared->execute(array($code));
    if ($prepared->rowCount() > 0){
      $row = $prepared->fetch(PDO::FETCH_ASSOC);
      return $row;
    } else {
      return false;
    }

  }


  function addUser($db, $name, $email, $password, $gender, $birthdate, $street, $city, $state, $zip, $timezone, $phone) {
    if($password != '') {
      $hash = md5($password);
    } else {
      $hash = '';
    }

    $prepared = $db->prepare("SELECT * FROM www_users_new WHERE email = ?");
    $prepared->execute(array($email));

    if ($prepared->rowCount() > 0){
      $prepared = $db->prepare("DELETE FROM www_users_new WHERE email = ?");
      $prepared->execute(array($email));
    }

    $prepared = $db->prepare("INSERT INTO www_users_new VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $prepared->execute(array($name, $email, $hash, $gender, $birthdate, $street, $city, $state, $zip, $timezone, $phone, '1'));
    $id = $db->lastInsertId();
    return $id;
  }

  function addMoment($db, $uid, $event_code, $event_name, $event_date, $event_time, $gift_url, $gifts, $event_public, $chat) {
    $prepared = $db->prepare("INSERT INTO special_moments VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $prepared->execute(array($uid, $event_code, $event_name, $event_date, $event_time, $gift_url, $gifts, $event_public, $chat));
    $id = $db->lastInsertId();
    return $id;
  }

  function createWedding($db, $uid, $wedding_type, $event_state, $event_date, $event_time, $marriage_counseling, $yours_uid, $first_witness_uid, $second_witness_uid, $activation_code, $activation_status, $marriage_education_certificate, $pre_marriage_request_date, $upload_pre_marriage_education_certificate, $brief_description, $officiant_picks, $religious_affliation, $assigned_officiant_uid, $event_code, $marriage_certificate, $giftlink, $public, $chat, $gifts) {
    $prepared = $db->prepare("INSERT INTO weddings VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $prepared->execute(array($uid, $wedding_type, $event_state, $event_date, $event_time, $marriage_counseling, $yours_uid, $first_witness_uid, $second_witness_uid, $activation_code, $activation_status, $marriage_education_certificate, $pre_marriage_request_date, $upload_pre_marriage_education_certificate, $brief_description, $officiant_picks, $religious_affliation, $assigned_officiant_uid, $event_code, $marriage_certificate, $giftlink, $public, $chat, $gifts));
    $id = $db->lastInsertId();
    return $id;
  }

  function updateWedding($db, $uid, $type, $event_code) {
    if($type == 'yours') {
      $prepared = $db->prepare("UPDATE weddings SET yours_uid = ? WHERE event_code = ?");
    }
    if($type == 'w1') {
      $prepared = $db->prepare("UPDATE weddings SET first_witness_uid = ? WHERE event_code = ?");
    }
    if($type == 'w2') {
      $prepared = $db->prepare("UPDATE weddings SET second_witness_uid = ? WHERE event_code = ?");
    }
    if($type == 'minister') {
      $prepared = $db->prepare("UPDATE weddings SET assigned_officiant_uid = ? WHERE event_code = ?");
    }
    $prepared->execute(array($uid, $event_code));
    $id = $db->lastInsertId();
    return $id;
  }

  $db = $pdoDB;
  require_once('../../common/config.php');
  require_once('../../common/connection.php');
  if (isset($_GET['cmd'])){
    switch($_GET['cmd']){
      case "sign_up":
      $userExists = checkUser($db, $_POST['email1']);
      $userExists = false;
      if(!$userExists) {
        if (isset($_POST['product_type'])) {
          // echo 'here';
          $productid = '1';
          if ($_POST['product_type'] == 'product1'){
            $productid = '1';
          }
          if ($_POST['product_type'] == 'product2'){
            $productid = '2';
          }
          if ($_POST['product_type'] == 'product3'){
            $productid = '3';
          }
          if ($_POST['product_type'] == 'product4'){
            $productid = '4';
          }
          if ($_POST['product_type'] == 'product5'){
            $productid = '5';
          }

          if($_POST['product_type'] == 'product5' || $_POST['product_type'] == 'product1') {
            $wedding_type = '1';

            $_POST['birthdate1'] = @$_POST['birthdate_month1'].'/'.@$_POST['birthdate_day1'].'/'.@$_POST['birthdate_year1'];

            $userInstance = addUser($db, $_POST['name1'], $_POST['email1'], $_POST['password1'],$_POST['gender1'], $_POST['birthdate1'], $_POST['street1'], $_POST['city1'], $_POST['state1'], $_POST['zip1'], $_POST['timezone1'], $_POST['phone1']);
            $event_code = strtoupper(generateRandomString(5));
            if (isset($_POST['gift_url'])) {
              $gift_url = $_POST['gift_url'];
            } else {
              $gift_url = '';
            }

            $_POST['event_date'] = @$_POST['event_date1'];
            $momentInstance = addMoment($db, $userInstance, $event_code, $_POST['event_name'], $_POST['event_date'], $_POST['event_time'], $gift_url, $_POST['gifts'], $_POST['event_public'], $_POST['chat'] );
            echo json_encode(
              array(
                "success" => "Account and special moment event created successfully", "user_id" => $userInstance, "product" => $productid
              )
            );
          } else if($_POST['product_type'] == 'product2' || $_POST['product_type'] == 'product3') {

            if($_POST['product_type'] == 'product2') {
              $wedding_type = '2';
            }

            if($_POST['product_type'] == 'product3') {
              $wedding_type = '3';
            }

            $userIDS = array();
            // var_dump($_POST);
            // $officiantPicks = $_POST['topofficiants0'].','.$_POST['topofficiants1'].','.$_POST['topofficiants2'].','.$_POST['topofficiants3'].','.$_POST['topofficiants4'];
            // ADD ACCOUNT CREATORS DETAILS
            $_POST['birthdate1'] = @$_POST['birthdate_month1'].'/'.@$_POST['birthdate_day1'].'/'.@$_POST['birthdate_year1'];
            $_POST['yours_email'] = @$_POST['email1'];
            //$_POST['event_date'] = @$_POST['event_month1'].'/'.@$_POST['event_day1'].'/'.@$_POST['event_year1'];


            $userInstance = addUser($db, $_POST['name1'], $_POST['email1'], $_POST['password1'],$_POST['gender1'], $_POST['birthdate1'], $_POST['street1'], $_POST['city1'], $_POST['state1'], $_POST['zip1'], $_POST['timezone1'], $_POST['phone1']);
            // print_r($userInstance);
            // Generate Event Code
            //$event_code = $userInstance * rand(5, 15);
            $event_code = strtoupper(generateRandomString(5));
            // SEND NOTIFICATION TO USERS
            $yourURL = $siteurl.'user_admin/wizard_form.php?event_code=' . $event_code . '&user_type=yours&email='. $_POST['yours_email'];
            $to = $_POST['yours_email'];
          	$email_recipient = 'no-reply@webwedmobile.com';
          	$subject  = "WebWedMobile – Wedding Activation Mail";
          	$headers  = 'MIME-Version: 1.0' . "\r\n";
          	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
          	$headers .= 'From:"No–Reply WebWedMobile"<'.$email_recipient.'>' . "\r\n";
          	$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
          	$automessage='<html xmlns="http://www.w3.org/1999/xhtml">
          	<head>
          	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          	<title>Welcome To WebWedMobile</title>
          	</head>
          	<body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
          	<div style="padding:0 15px;">
          	<div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
          	<img src="http://webwedmobile.com/images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
          	<div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
          		<p style="font-size:16px;margin-bottom:15px;">You are invited by : ' . $_POST['name1'] . '. Please visit the following link to activate your account. '.$yourURL.'</p>
          		</div>
          		</div>
          		</div>
          		</body>
          		</html>';
          	@email($to,$subject,$automessage,$headers);

            $w1URL = $siteurl.'user_admin/wizard_form.php?event_code=' . $event_code . '&user_type=w1&email='. $_POST['witness1_email'];
            $to = $_POST['witness1_email'];
            $email_recipient = 'no-reply@webwedmobile.com';
            $subject  = "WebWedMobile – Wedding Activation Mail";
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= 'From:"No–Reply WebWedMobile"<'.$email_recipient.'>' . "\r\n";
            $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
            $automessage='<html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Welcome To WebWedMobile</title>
            </head>
            <body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
            <div style="padding:0 15px;">
            <div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
            <img src="http://webwedmobile.com/images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
            <div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
              <p style="font-size:16px;margin-bottom:15px;">You are invited by : ' . $_POST['name1'] . '. Please visit the following link to activate your account. '.$w1URL.'</p>
              </div>
              </div>
              </div>
              </body>
              </html>';
            @email($to,$subject,$automessage,$headers);

            $w2URL = $siteurl.'user_admin/wizard_form.php?event_code=' . $event_code . '&user_type=w2&email='. $_POST['witness2_email'];
            $to = $_POST['witness2_email'];
            $email_recipient = 'no-reply@webwedmobile.com';
            $subject  = "WebWedMobile – Wedding Activation Mail";
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= 'From:"No–Reply WebWedMobile"<'.$email_recipient.'>' . "\r\n";
            $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
            $automessage='<html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Welcome To WebWedMobile</title>
            </head>
            <body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
            <div style="padding:0 15px;">
            <div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
            <img src="http://webwedmobile.com/images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
            <div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
              <p style="font-size:16px;margin-bottom:15px;">You are invited by : ' . $_POST['name1'] . '. Please visit the following link to activate your account. '.$w2URL.'</p>
              </div>
              </div>
              </div>
              </body>
              </html>';
            @email($to,$subject,$automessage,$headers);

            foreach($_POST['topofficiants'] as $data) {
              $officiant_data = findMinisterById($pdoDB, $data);
              $yourURL = $siteurl.'user_admin/wizard_form.php?event_code=' . $event_code . '&user_type=officiant&email='. $officiant_data['email'];
              $to = $officiant_data['email'];
              $email_recipient = 'no-reply@webwedmobile.com';
              $subject  = "WebWedMobile – Wedding Invitation Mail";
              $headers  = 'MIME-Version: 1.0' . "\r\n";
              $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
              $headers .= 'From:"No–Reply WebWedMobile"<'.$email_recipient.'>' . "\r\n";
              $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
              $automessage='<html xmlns="http://www.w3.org/1999/xhtml">
              <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
              <title>Welcome To WebWedMobile</title>
              </head>
              <body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
              <div style="padding:0 15px;">
              <div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
              <img src="http://webwedmobile.com/images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
              <div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
                <p style="font-size:16px;margin-bottom:15px;">Your services are requested by ' . $_POST['name1'] . ' for their wedding. Please click the following link to check and approve. '.$yourURL.'</p>
                </div>
                </div>
                </div>
                </body>
                </html>';
              @email($to,$subject,$automessage,$headers);
            }

            $topofficiants = implode(", ",$_POST['topofficiants']);
            if (!isset($_POST['event_date']) && isset($_POST['event_date1'])){
              $_POST['event_date'] = $_POST['event_date1'];
            }
            $newWeddingInstance = createWedding($db, $userInstance, $wedding_type, $_POST['state1'], $_POST['event_date'], $_POST['event_time'], '', '', '', '', 'activationcode', 'inactive', '', '', '', $_POST['event_brief'], $topofficiants, $_POST['religious_affiliation'], '', $event_code, '', '','','','');
            //print_r($newWeddingInstance);
            // $_SESSION['user_id'] = $userInstance;
            echo json_encode(
              array(
                "success" => "New Wedding event created successfully", "user_id" => $userInstance, "product" => $productid
              )
            );
          }
        } else { echo generateError(1015); }
      } else {
        echo generateError(1014);
      }
      break;

      case "sign_up_invite":
        $userExists = checkUser($db, $_POST['email1']);
        if(!$userExists) {
          if (isset($_POST['user_type'])) {
            $userInstance = addUser($db, $_POST['name1'], $_POST['email1'], $_POST['password1'],$_POST['gender1'], $_POST['birthdate1'], $_POST['street1'], $_POST['city1'], $_POST['state1'], $_POST['zip1'], $_POST['timezone1'], $_POST['phone1']);
            if($_POST['user_type'] == 'yours') {
              $type = 'yours';
              $updateInstance = updateWedding($db, $userInstance, $type, $_POST['event_code']);
            } else if($_POST['user_type'] == 'w1') {
              $type = 'w1';
              $updateInstance = updateWedding($db, $userInstance, $type, $_POST['event_code']);
            } else if($_POST['user_type'] == 'w2') {
              $type = 'w2';
              $updateInstance = updateWedding($db, $userInstance, $type, $_POST['event_code']);
            }
            echo json_encode(
              array(
                "success" => "You are successfully assigned to the marriage", "user_id" => $userInstance
              )
            );
          } else { echo generateError(1015); }
        } else {
          echo generateError(1016);
        }
        break;

      case "assign_minister":
        $isAssigned = findWeddingByEventCode($db, $_POST['event_code']);
        // var_dump($isAssigned);
        if($isAssigned['assigned_officiant_uid'] == 0) {
          $updateInstance = updateWedding($db, $_POST['officiant_id'], 'minister', $_POST['event_code']);
          echo json_encode(
            array(
              "success" => "You are successfully assigned to the marriage."
            )
          );
        } else if ($isAssigned['assigned_officiant_uid'] == $_POST['officiant_id']) {
          echo generateError(1017);
        } else {
          echo generateError(1018);
        }

        break;

      case "validate_discount":
        if(isset($_POST["code"])) {
          $isValid = checkDiscount($db, $_POST["code"]);
          if(!$isValid) {
            echo generateError(1019);
          } else {
            echo json_encode(
              array(
                "success" => "Discount Applied.", "amount" => $isValid['amount'], "discount_id" => $isValid['id']
              )
            );
          }
        }
        break;

      case "checkemail":
        if (isset($_GET['email'])){
          $email = $_GET['email'];

          echo json_encode(
            array(
              "success" => "Email does not exist."
            )
          );



          // if (!checkUser($db, $email)){
          //   echo json_encode(
          //     array(
          //       "success" => "Email does not exist."
          //     )
          //   );
          // }else{
          //   echo json_encode(
          //     array(
          //       "error" => "You already have an account registered with this email. "
          //     )
          //   );
          // }
        }
        break;

        case "resend_code":
        if (isset($_POST['email'])){
          $email = $_POST['email'];
          $user = checkUser($db, $email);
          if (!$user){
            echo json_encode(
              array(
                "error" => "No account exists with this email address. Please signup in order to receive an event code: <a href='http://webwedmobile.com/admin/user_admin/signup.php'>http://webwedmobile.com/admin/user_admin/signup.php</a>"
              )
            );
          }else{
            $user = getUserByEmail($db, $email);
            $uid = $user['id'];

            $events = array();

            $prepared = $db->prepare("SELECT * FROM `special_moments` WHERE `uid` = ? ORDER BY id ASC");
            $prepared->execute(array($uid));
            if ($prepared->rowCount() > 0){
              $row = $prepared->fetch(PDO::FETCH_ASSOC);
              $event_code = $row['event_code'];
              $events = array_merge($events, array($event_code));
            }

            $prepared = $db->prepare("SELECT * FROM `weddings` WHERE `uid` = ? ORDER BY id ASC");
            $prepared->execute(array($uid));
            if ($prepared->rowCount() > 0){
              $row = $prepared->fetch(PDO::FETCH_ASSOC);
              $event_code = $row['event_code'];
              $events = array_merge($events, array($event_code));
            }
            $to = $email;
            $email_recipient = 'no-reply@webwedmobile.com';
            $subject  = "WebWedMobile, Your Event Codes";
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= 'From:"No Reply WebWedMobile"<'.$email_recipient.'>' . "\r\n";
            $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
            $automessage='<html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Welcome To WebWedMobile</title>
            </head>
            <body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
            <div style="padding:0 15px;">
            <div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
            <img src="http://webwedmobile.com/images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
            <div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
            <p style="font-size:16px;margin-bottom:15px;">Thank you for contacting at <a href="http://www.webwedmobile.com" style="color:#5b9bd5;text-decoration:none;">WEBWEDMOBILE.COM</a>.</p>
            <p style="font-size:16px;margin-bottom:15px;">You have the following events linked to your account:</p>
            <p style="font-size:16px;margin-bottom:15px;"></p>';

            foreach($events as $event){
              $automessage .= '<p style="font-size:16px;margin-bottom:15px;">Event Code: '.$event.'</p>';
            }

            $automessage .= '
            <p style="font-size:16px;margin-bottom:15px;text-align:center;">Thank You!</p>
            </div>
            </div>
            </div>
            </body>
            </html>';
            @email($to,$subject,$automessage,$headers);
            echo json_encode(
              array(
                "success" => "We have sent your event code(s) to the email address you provided."
              )
            );
          }
        }
        break;
      }
    }
?>
