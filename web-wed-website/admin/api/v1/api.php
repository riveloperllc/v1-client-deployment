<?php

header('Access-Control-Allow-Origin: *');
require_once('../../common/connection.php');
require_once('../../common/config.php');
require_once('functions.php');
$db = $pdoDB;


ini_set('display_errors', 1);
error_reporting(E_ALL);

if (isset($_GET['cmd'])){
	switch($_GET['cmd']){
		case "login":
		if (isset($_POST['type'])){
			switch (strtolower($_POST['type'])){
				case "has_account":
				if (isset($_POST['email'], $_POST['password'])){
					$email = $_POST['email'];
					$password = $_POST['password'];

					$loginInstance = login($db, $email, $password);

					if ($loginInstance['success']){
						echo json_encode(
						array(
							"success" => "Login was successful.",
							"data" => array(
								"name" => getNameFromName($db, $email, $loginInstance['row'], @$loginInstance['row']['wedding']['role']),
								"token" => $loginInstance['token'],
								"wedding" => @$loginInstance['row']['wedding'],
								"special_moment" => @$loginInstance['row']['special_moment'],
								"apikey" => "45692092"
							)
							)
						);
					}else{ echo generateError(1002); }
				}else{ echo generateError(1001); }
				break;

				case "guest":
				if (isset($_POST['email'], $_POST['eventcode'])){
					$email = $_POST['email'];
					$eventcode = $_POST['eventcode'];
					$loginInstance = login($db, $email, $eventcode, 'guest');
					if ($loginInstance['success']){
						echo json_encode(
						array(
							"success" => "Login was successful.",
							"data" => array(
								"name" => @$loginInstance['row']['guestInstance']['guest_name'],
								"token" => $loginInstance['token'],
								"wedding" => $loginInstance['row']['weddingInstance'],
								"special_moment" => $loginInstance['row']['specialMomentInstance'],
								"apikey" => "45692092"
							)
						)
					);
				}else{ echo generateError(1002); }
			}else{ echo generateError(1003); }
			break;
		}
	}
	break;

	case "update":
	if (isset($_POST['token'])){
		if (isset($_POST['password'], $_POST['email'], $_POST['confirm'], $_POST['weddingregistrylink'], $_POST['publicbroadcast'], $_POST['acceptgifts'], $_POST['allowchat'], $_POST['token'])){

			$newPassword = md5($_POST['password']);
			// $tokenRow = getByToken($db, $_POST['token']);

			$prepared = $db->prepare("SELECT * FROM `tokens` WHERE `token` = ?");
			$prepared->execute(array($_POST['token']));

			if ($prepared->rowCount() > 0){
				$tokenRow = $prepared->fetch(PDO::FETCH_ASSOC);
			}else{
				$tokenRow = array();
			}

			if (!empty($tokenRow)){
				$uid = $tokenRow['uid'];
				$type = $tokenRow['type'];
				if ($type == 2){

					if (!empty($_POST['weddingregistrylink'])){
						$prepared = $db->prepare("UPDATE weddings SET giftlink = ? WHERE uid = ?");
						$prepared->execute(array($_POST['weddingregistrylink'], $uid));
						$prepared = $db->prepare("UPDATE special_moments SET giftlink = ? WHERE uid = ?");
						$prepared->execute(array($_POST['weddingregistrylink'], $uid));
					}

					if (isset($_POST['publicbroadcast'])){
						$prepared = $db->prepare("UPDATE weddings SET public = ? WHERE uid = ?");
						$prepared->execute(array(($_POST['publicbroadcast']), $uid));
						$prepared = $db->prepare("UPDATE special_moments SET public = ? WHERE uid = ?");
						$prepared->execute(array(convertToInt($_POST['publicbroadcast']), $uid));
					}

					if (isset($_POST['allowchat'])){
						$prepared = $db->prepare("UPDATE weddings SET chat = ? WHERE uid = ?");
						$prepared->execute(array(($_POST['allowchat']), $uid));
						$prepared = $db->prepare("UPDATE special_moments SET chat = ? WHERE uid = ?");
						$prepared->execute(array(convertToInt($_POST['allowchat']), $uid));
					}

					if (isset($_POST['acceptgifts'])){
						$prepared = $db->prepare("UPDATE weddings SET gifts = ? WHERE uid = ?");
						$prepared->execute(array(($_POST['acceptgifts']), $uid));
						$prepared = $db->prepare("UPDATE special_moments SET gifts = ? WHERE uid = ?");
						$prepared->execute(array(convertToInt($_POST['acceptgifts']), $uid));
					}

					if ($_POST['password'] != '' && $_POST['confirm'] != '' && ($_POST['password'] == $_POST['confirm'])){
						$prepared = $db->prepare("UPDATE www_users_new SET password = ? WHERE id = ?");
						$prepared->execute(array(md5($_POST['password']), $uid));

					}

					echo json_encode(array(
						"success" => "Your settings have been updated.",
						"data" => array(
							"updated" => true
						)
					)
				);

			}else{ echo generateError(1012); }
		}else{ echo generateError(1013); }
	}
}else{ echo generateError(1008); }
break;

case "forgotpassword" :

	if (isset($_POST['email'])){

		$email = $_POST['email'];
		$token = md5(mt_rand(999,9999));

		$prepared = $db->prepare("SELECT * FROM `www_users_new` WHERE `email` = ? LIMIT 1");
		$prepared->execute(array($email));

		if ($prepared->rowCount() > 0){
			$row = $prepared->fetch(PDO::FETCH_ASSOC);

			$prepared = $db->prepare("DELETE FROM `reset_tokens` WHERE `uid` = ?");
			$prepared->execute(array($row['id']));

			$prepared = $db->prepare("INSERT INTO `reset_tokens` VALUES (NULL, ?, ?)");
			$prepared->execute(array($row['id'], $token));

			$resetpassword = "http://webwedmobile.com/admin/user_admin/resetpassword.php?token=$token";
			$to = $_POST['email'];
			$email_recipient = 'no-reply@webwedmobile.com';
			$subject  = "WebWedMobile Forgot Password";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= 'From:"No Reply WebWedMobile"<'.$email_recipient.'>' . "\r\n";
			$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
			$automessage='<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Welcome To WebWedMobile</title>
			</head>
			<body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
			<div style="padding:0 15px;">
			<div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
			<img src="http://webwedmobile.com/images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
			<div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
			<p style="font-size:16px;margin-bottom:15px;">Thank you for contacting at <a href="http://www.webwedmobile.com" style="color:#5b9bd5;text-decoration:none;">WEBWEDMOBILE.COM</a>.</p>
			<p style="font-size:16px;margin-bottom:15px;">You need to reset your password</p>
			<p style="font-size:16px;margin-bottom:15px;">If you have any questions, please email us at <a href="mailto:info@webwedmobile.com" style="color:#5b9bd5;text-decoration:none;">info@webwedmobile.com</a></p>
			<p style="font-size:16px;margin-bottom:15px;">To reset your password please <a href="'.$resetpassword.'">click here</a></p>
			<p style="font-size:16px;margin-bottom:15px;text-align:center;">Thank You!</p>
			</div>
			</div>
			</div>
			</body>
			</html>';
			email($to,$subject,$automessage,$headers);
			echo json_encode(array(
				"success" => "Please follow the directions provided to your email address."
			));
		}else{
			echo json_encode(array(
				"error" => "We could not locate an account with that email address."
			));
		}
	}
	break;

case "contactus":
if (isset($_POST['name'], $_POST['email'], $_POST['message'])){

	//Disabled.
	//mail('webwedmobile@gmail.com', 'WebWedMobile Contact Us', 'Name: ' . $_POST['name'] . '\nEmail: ' . $_POST['email'] . '\nMessage: ' . $_POST['message']);
	$to = 'sweat.austin@riveloper.com';
	$email_recipient = 'no-reply@webwedmobile.com';
	$subject  = "WebWedMobile Contact Us";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= 'From:"No Reply WebWedMobile"<'.$email_recipient.'>' . "\r\n";
	$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
	$automessage='<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Welcome To WebWedMobile</title>
	</head>
	<body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
	<div style="padding:0 15px;">
	<div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
	<img src="http://webwedmobile.com/images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
	<div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
		<p style="font-size:16px;margin-bottom:15px;">A Contact Us request has been submitted.</p>
		<p style="font-size:16px;margin-bottom:15px;">Name: '.htmlentities($_POST['name']).' </p>
		<p style="font-size:16px;margin-bottom:15px;">Email: '.htmlentities($_POST['email']).' </p>
		<p style="font-size:16px;margin-bottom:15px;">Message: '.htmlentities($_POST['message']).' </p>
		</div>
		</div>
		</div>
		</body>
		</html>';
	email($to,$subject,$automessage,$headers);

	echo json_encode(array(
		"success" => "We have sent your message to the WebWed Team and will be in contact shortly!"
	));
}else{ echo generateError(1010); }
break;
}
}

function convertToInt($int){
	return intval($int);
}
?>
