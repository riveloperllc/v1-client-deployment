<?php
ini_set('display_errors',1);
ob_start();
session_start();
if(isset($_SESSION['wwm_user_id'])){
	header("location:user_dashboard.php");
}
require_once('../common/connection.php');
require_once('../common/config.php');
$page_title = "User Login - ";
include('../header.php');
?>
<br/>
<div class="form_main">
	<div class="login">
		<h1>Guest Login</h1><br/>
		<div class="alert alert-info">Enter your email address and event code.</div>
		<p id="success" class="alert alert-success text-center hidden"></p>
		<p class="alert alert-danger text-center hidden"></p>
		<div class="alert alert-danger" id="error" style="display:none;"></div>
		<form class="form-horizontal" name="guestLogin" id="guestLogin" method="post" enctype="multipart/form-data">
			<input type="text" placeholder="Email" class="input_text" id="email" name="email" /><br/><br/>
			<input type="text" placeholder="Event Code" value="<?php if(isset($_GET['code'])){ echo htmlentities(@$_GET['code']); } ?>" class="input_text" id="eventCode" name="eventCode"/><br/><br/>
			<input type="submit" name="submit" value="Attend Event" class="btn btn-primary" /><br/><br/>
			<p class="text-center"><a href="resend_wedding_code.php">Resend Event Code </a> </p><br/>
		</form>
	</div>
</div>
<script>
var Base64 = {


    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",


    encode: function(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },


    decode: function(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    _utf8_encode: function(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    _utf8_decode: function(utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}
window.mobileAndTabletcheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};
    $( document ).ready(function(){
        $( "#guestLogin" ).validate({
            rules: {
                email: {
                required: true,
                email: true
                },
                eventCode: {				// compound rule
                required: true
                }
            },
            messages: {
                email: {
                required: "Please enter the email",
                email: "Please enter a valid email address."
                },
                eventCode: {
                required: "Please enter the event code"
                }
            },
            submitHandler: function(form) {
                // do other things for a valid form
                // $('#guestLogin').on('submit', function(e){
                //     e.preventDefault();

                    var Email = $('#email').val();
                    var eventCode = $('#eventCode').val();

										if (mobileAndTabletcheck()){
											location.href="http://webwedmobile.com/join.php?code="+eventCode;
										}
                    $.ajax({
                        url: '../api/v1/api.php?cmd=login',
                        type: 'POST',
                        data:{type:"guest",email:Email,weddingcode:eventCode,eventcode:eventCode},
                        cache: false,
                        success:function(data) {
                            // console.log(data);
													var raw_data = data;
                           var data = JSON.parse(data);
                           console.log(data);
													 $('#error').text(data.error);
													 $('#error').css('display', 'block');
                           if (!data.success) { //If fails
                               if (data.error) { //Returned if any error from api.php
                                   $('.throw_error').removeClass('hidden').fadeIn(1000).html(data.error); //Throw relevant error
                                   $('#success').addClass('hidden'); //Throw relevant error
                               }
                           } else {
                                    $('#success').removeClass('hidden').fadeIn(1000).text(data.success); //If successful, than throw a success message
                                    $('.throw_error').addClass('hidden'); //Throw relevant error
                                    //send token value to php file
                                    var form = $('<form action="temp_guest_login.php" method="post">' +
                                    '<input type="hidden" name="token" value="' + data.data.token + '" />' +
                                    '<input type="hidden" name="special_moment" value="' + Base64.encode(JSON.stringify(data.data.special_moment)) + '" />' +
                                    '<input type="hidden" name="wedding" value="' + Base64.encode(JSON.stringify(data.data.wedding)) + '" />' +
                                    '</form>');
                                    $('body').append(form);
                                    form.submit();

                               }
                           }
                        });
                    return false;

            }
        });




});
</script>
<?php include('../footer.php'); ?>
