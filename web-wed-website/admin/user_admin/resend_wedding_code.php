<?php
ini_set('display_errors',1);
ob_start();
session_start();
// print_r($_SESSION);
if(isset($_SESSION['wwm_user_id'])){
	header("location:user_dashboard.php");
}
require_once('../common/connection.php');
require_once('../common/config.php');
// require_once('../api/v1/api.php');

 	// $page_name=basename($_SERVER['PHP_SELF']);
	$page_title = "Resend Code - ";
include('../header.php');

?>
<br/>
<br/>
<br/>
<div class="form_main">
	<div class="login">
		<h1>Resend Event Code</h1>
		<p id="success" class="alert alert-success text-center hidden"></p>
		<p class="alert alert-danger text-center hidden"></p>
		<div class="alert alert-success" id="result_success" style="display:none;">We have sent your event code(s) to the email you provided.</div>
		<div class="alert alert-danger" id="result_error" style="display:none;">We could not locate any events connected to that email address. Please follow this link to signup: <a href="http://webwedmobile.com/admin/user_admin/signup.php">http://webwedmobile.com/admin/user_admin/signup.php</a></div>
		<form class="form-horizontal" name="guestResend" id="guestResend" method="post" enctype="multipart/form-data">
			<input type="text" placeholder="Email" class="input_text" id="email" name="email" /><br/><br/>
			<input  type="submit" name="submit" value="Submit" class="btn btn-primary" />
		</form>
	</div>
</div>
<script>


    $( document ).ready(function(){
        $( "#guestResend" ).validate({
            rules: {
                email: {
                required: true,
                email: true
                }
            },
            messages: {
                email: {
                required: "Please enter the email",
                email: "Please enter a valid email address."
                }
            },
            submitHandler: function(form) {
                // do other things for a valid form
                // $('#guestResend').on('submit', function(e){
                //     e.preventDefault();
                    var email = $('#email').val();
                    // var eventCode = $('#eventCode').val();
                    $.ajax({
                        url: '../api/v1/api.php?cmd=resend_code',
                        type: 'POST',
                        data:{ email: email },
                        cache: false,
                        // dataType: 'json',
                        success: function(data)
                        {
													console.log(data);
                               var data = JSON.parse(data);
                        //    console.log("value"+data.error);
                           if (!data.success) { //If fails
                               if (data.error) { //Returned if any error from api.php
                                  //  $('.throw_error').removeClass('hidden').fadeIn(1000).html(data.error); //Throw relevant error
                                  //  $('#success').addClass('hidden'); //Throw relevant error
																	$('#result_error').show();
																	$('#result_success').hide();
                               }
                           }
                           else {
                                //  console.log("values"+data.success);
                                    // $('#success').removeClass('hidden').fadeIn(1000).text( data.success); //If successful, than throw a success message
                                    // $('.throw_error').addClass('hidden'); //Throw relevant error
																		$('#result_success').show();
																		$('#result_error').hide();
                                    $('#email').val('');
                               }
                        },

                    });
                    return false;
                // });

            }
        });




});
</script>
<?php include('../footer.php'); ?>
