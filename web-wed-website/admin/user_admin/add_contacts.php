<?php
session_start();
require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');
require_once('../common/functions.php');

function findUserById($db, $id){
  $prepared = $db->prepare("SELECT * FROM www_users_new WHERE id = ? LIMIT 1");
  $prepared->execute(array($id));
  if ($prepared->rowCount() > 0){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }else{
    return array('name' => 'Not Available');
  }
}

if (isset($_GET['?token'])){
  $_GET['token'] = $_GET['?token'];
}

if (isset($_GET['token'])){
  $prepared = $pdoDB->prepare("SELECT * FROM tokens WHERE token = ?");
  $prepared->execute(array($_GET['token']));

  if ($prepared->rowCount() > 0){
    $userRow = $prepared->fetch(PDO::FETCH_ASSOC);
    $user_id = $userRow['uid'];
    $_SESSION['wwm_user_id'] = $user_id;
  }else{
    echo 'Your session token is invalid, please logout and try again.';
    exit;
  }
}else{
  $user_id = @$_SESSION['wwm_user_id'];
}

$db = $pdoDB;
$prepared = $db->prepare("SELECT * FROM `weddings` WHERE `uid` = ? ORDER BY `id` DESC LIMIT 1");
$prepared->execute(array($user_id));
if ($prepared->rowCount() > 0){
  $event = $prepared->fetch(PDO::FETCH_ASSOC);
}else{
  $prepared = $db->prepare("SELECT * FROM `special_moments` WHERE `uid` = ? ORDER BY `id` DESC LIMIT 1");
  $prepared->execute(array($user_id));
  if ($prepared->rowCount() > 0){
    $event = $prepared->fetch(PDO::FETCH_ASSOC);
  }else{
    //header("Location: index.php");
    echo $user_id;
    exit;
  }
}

$eventcode = $event['event_code'];

if (isset($_POST['submit'])) {
  if(isset($_POST['guest_email']) && trim($_POST['guest_email']) != '')
  {
    $guest_email = $_POST['guest_email'];



    $query_guest_check = "select * from guests where guest_email = '".$guest_email."' and event_code='$eventcode'";
    $result_guest_check = mysql_query($query_guest_check);
    mysql_num_rows($result_guest_check);

    if(mysql_num_rows($result_guest_check))
    {
      $_SESSION['message'] = "The email address you entered already belongs to a Guest.";
      $_SESSION['flag'] = "2";
      $msg = '<div class="alert alert-danger">The email address you entered already belongs to a Guest.</div>';
      //header("location: add_contacts.php"); exit;
    }else{
      $s_array = explode("@",$guest_email);
      array_pop($s_array); #remove last element.
      $guest_name = ucfirst(implode("@",$s_array));

      $added_date = date('Y-m-d H:i:s');
      $status = 'sent';

      $query_string = "INSERT INTO guests (guest_email, guest_name, added_date, status, event_code ) VALUES ('$guest_email', '$guest_name', '$added_date', '$status', '$eventcode')";
      $result = mysql_query ($query_string);

      $message = '<html><body>';
      $message .= "<p>Dear ".$guest_name.",</p>";
      $message .= "<p>You are cordially e-vited to a WebWed Event.</p>";
      //$message .= "<p>".findUserById($db, $event['uid'])['name']." & ".findUserById($db, $event['yours_uid'])['name']." are getting hitched via live video broadcast.</p>";
      $message .= "<p>Event Date and Time: ".$event['event_date']. ' '. $event['event_time']." </p>";
      $message .= "<p></p>";
      $message .= "<p><a href='".$siteurl."user_admin/event_redirect.php?event_code=".$eventcode."'>Watch This Event Online (Event Code: ".$eventcode.")</a></p>";
      $message .= "</body></html>";
      $subject = 'You Are Cordially E-vited';

      $host = $_SERVER['HTTP_HOST'];
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= "From: \" WebWedMobile \"<noreply@$host>\n";
      email($guest_email,$subject,$message,$headers);
      $msg = '<div class="alert alert-success">This guest has been sucessfully e-vited.</div>';
      //header('Location: add_contacts.php?success');
      //exit;
    }
  }
  else{

    $_SESSION['message'] = "Please enter email address.";
    $_SESSION['flag'] = "2";
    $msg = '<div class="alert alert-danger">Please enter an email address.</div>';
    //header('Location: add_contacts.php');
    //exit;
  }

}

if(isset($_GET['action']) == "delete" && $_GET['deleteid'] != ""){
  $id = $_GET['deleteid'];

  $sqldstore = "DELETE FROM guests WHERE 	guest_id = ".$id."";
  $result_delete = mysql_query($sqldstore);

  if($result_delete){
    $_SESSION['message'] = "Guest has been deleted sucessfully.";
    $_SESSION['flag'] = "1";
    $msg = '<div class="alert alert-success">Guest has been removed from your event.</div>';
  }}

  if (isset($_GET['success'])){
    $msg = '<div class="alert alert-success">This guest has been e-vited to your event.</div>';
  }
  ?>
  <?php
  $page_title = "Manage Guests - ";
  include('../header.php');

  $sql = "SELECT * FROM guests where event_code = '$eventcode' ";
  $result = mysql_query($sql);
  $total_rows = mysql_num_rows($result);

  ?>
  <br>
  <br>
  <br>
  <br>
  <div class="container">
    <div class="form_main_inside">
      <div class="">
        <?php
        echo @$msg;
        ?>
        <h1>Add Guests</h1>
        <form name="frm_guest_add" id="frm_guest_add" method="post" action="<?php echo SITEURL; ?>user_admin/add_contacts.php">




            <label>Guest's email</label><br/><br/>
            <input type="text" class="input_text" name="guest_email" id="guest_email"/><br/><br/>
            <input type="submit" name="submit"  value="Add Guest" class="btn btn-primary" /><br/><br/>


        </form>

        <div class="clear"></div>
        <br>

        <table id="customers" class="table table-stripped">
          <thead>
            <th>Name</th>
            <th>Email</th>
            <th></th>
          </thead>
          <?php

          while($data = mysql_fetch_array($result))
          {
            ?>
            <tr>
              <td><?php echo $data['guest_name'];?></td>
              <td><?php echo $data['guest_email'];?></td>
              <td><a  onclick="return checkDel()" href="add_contacts.php?action=delete&deleteid=<?php echo $data['guest_id'];?>">Delete</a></td>
            </tr>
            <?php
          }
          if($total_rows < 1){?>
            <tr><td colspan="3">No Records Found</td></tr>
            <?php }	?>
          </table>
        </div>
      </div>
    </div>



    <?php include('../footer.php'); ?>
    <script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
    <script>


    $(function() {
      $( "#frm_guest_add" ).validate({
        rules: {
          guest_email: {
            required: true,
            email:true
          }
        },
        messages: {
          guest_email: {
            required: "Please enter email address.",
            email:"Please enter valid email address."
          }
        }
      });
    });
    </script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
