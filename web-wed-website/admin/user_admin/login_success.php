<?php
session_start();
if(!isset($_SESSION['wwm_user_id'])){
  header("Location: user_login.php");
  exit;
}
header("Location: user_dashboard.php");
exit;
?>
<?php
require_once('../common/connection.php');
require_once('../common/config.php');
?>
<?php $page_name=basename($_SERVER['PHP_SELF']);?>
<?php include('../header.php'); ?>

<div class="form_main">
  <div class="form_main_inside">
    <div class="login">
      <h1>Login Successful</h1>
      <p style="text-align:center;">Login is successful, you are being redirected to your profile page.</p>
      <script>
      setTimeout(function () {
        window.location.href= '<?php echo $siteurl.'user_admin/user_dashboard.php'; ?>'; // the redirect goes here

      },4000); // 4 seconds
      </script>
    </div>
  </div>
</div>

<?php include('../footer.php'); ?>
