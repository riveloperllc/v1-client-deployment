<?php
session_start();

require_once('../common/connection.php');
require_once('../common/config.php');
$page_name=basename($_SERVER['PHP_SELF']);?>
<?php

$error = "";
if(isset($_POST['submit'], $_POST['token']))
{
	if(trim($_POST['newpassword']) !='' && trim($_POST['repassword']) !='')
	{
		$prepared = $pdoDB->prepare("SELECT `uid` FROM reset_tokens WHERE token = ?");
		$prepared->execute(array($_POST['token']));

		if ($prepared->rowCount() == 0){
			header("Location: forgot_password.php");
			exit;
		}else{
			$row = $prepared->fetch(PDO::FETCH_ASSOC);
			$uid = $row['uid'];
			$newpassword = $_POST['newpassword'];
			$passencode = md5($newpassword);
			$repassword = $_POST['repassword'];
			$repassword = md5($repassword);

			$prepared = $pdoDB->prepare("SELECT * FROM www_users_new WHERE id = ?");
			$prepared->execute(array($uid));

			$userRow = $prepared->fetch(PDO::FETCH_ASSOC);

			$query = "UPDATE www_users_new SET password='$passencode' WHERE id='$uid'";
			$data = mysql_query ($query)or die(mysql_error());

			$prepared = $pdoDB->prepare("DELETE FROM reset_tokens WHERE token = ?");
			$prepared->execute(array($_POST['token']));

			if($data)
			{		$to = @$userRow['email'];
				$email_recipient = $siteemail;
				$subject  = "WebWedMobile Reset Password";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
				$headers .= 'From:"WebWedMobile"<'.$email_recipient.'>' . "\r\n";
				$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
				$automessage='<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Welcome To Web Wed Mobile</title>
				</head>
				<body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
				<div style="padding:0 15px;">
				<div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
				<img src="'.$siteurl.'images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
				<div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
				<p style="font-size:16px;margin-bottom:15px;">Thank you for contacting at <a href="http://www.webwedmobile.com" style="color:#5b9bd5;text-decoration:none;">WEBWEDMOBILE.COM</a>.</p>
					<p style="font-size:16px;margin-bottom:15px;">Your password has been reset successfully</p>
					<p style="font-size:16px;margin-bottom:15px;">If you have any questions, please email us at <a href="mailto:info@webwedmobile.com" style="color:#5b9bd5;text-decoration:none;">info@webwedmobile.com</a></p>
					<p style="font-size:16px;margin-bottom:15px;text-align:center;">Thank You!</p>
					</div>
					</div>
					</div>
					</body>
					</html>';
					if(email($to,$subject,$automessage,$headers))
					{
						$msg = '<div class="alert alert-success">Your password has been successfully reset.</div>';
					}
				}
			}
		}
		else
		{
			$error = "<div class='alert alert-danger'>Please enter new password and confirm password.</div>";
		}
	}
	?>
	<?php include('../header.php'); ?>

	<div class="form_main">
		<div class="form_main_inside">
			<div class="login">
				<?php echo @$error; echo @$msg; ?>
				<h1>Reset Password</h1>
				<form role="form" method="post" id="reset-password-form" action="<?php echo SITEURL; ?>user_admin/resetpassword.php?token=<?php echo @$_GET['token']; ?>">
					<label>New Password</label><br/><br/>
					<input type="password" name="newpassword" id="newpassword" class="input_text" placeholder="Password"><br/><br/>
					<label>Confirm New Password</label><br/><br/>
					<input type="hidden" name="token" id="token" value="<?php echo @$_GET['token']; ?>">
					<input type="password" name="repassword" id="repassword" class="input_text" placeholder="Retype Password"><br/><br/>
					<input type="submit" name="submit" value="SUBMIT" class="btn btn-primary" />
				</form>
			</div>
		</div>
	</div>

	<?php include('../footer.php'); ?>



	<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
	<script>
	$(function() {
		$( "#reset-password-form" ).validate({
			rules: {
				newpassword: {
					required: true,
					minlength:6
				},
				repassword: {
					required: true,
					minlength:6,
					equalTo:"#newpassword"
				}
			},
			messages: {
				newpassword: {
					required: "Please enter new password."
				},
				repassword: {
					required: "Please enter confirm password.",
					equalTo:"Passwords do not match."
				}
			}

		});
	});
	</script>
