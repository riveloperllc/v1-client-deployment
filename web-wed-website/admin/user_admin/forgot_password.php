<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
require_once('../common/connection.php');
require_once('../common/config.php');

if(isset($_POST['submit'])){
	$email = $_POST['email'];
	$token = md5(mt_rand(999,9999));

	$prepared = $pdoDB->prepare("SELECT * FROM `www_users_new` WHERE `email` = ? LIMIT 1");
	$prepared->execute(array($email));

	if ($prepared->rowCount() > 0){
		$row = $prepared->fetch(PDO::FETCH_ASSOC);

		$prepared = $pdoDB->prepare("DELETE FROM `reset_tokens` WHERE `uid` = ?");
		$prepared->execute(array($row['id']));

		$prepared = $pdoDB->prepare("INSERT INTO `reset_tokens` VALUES (NULL, ?, ?)");
		$prepared->execute(array($row['id'], $token));

		$resetpassword = $siteurl."user_admin/resetpassword.php?token=$token";
		$to = $email;
		$email_recipient = $siteemail;
		$subject  = "WebWedMobile Forgot Password";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= 'From:"WebWedMobile"<'.$email_recipient.'>' . "\r\n";
		$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
		$automessage='<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Welcome To WebWedMobile</title>
		</head>
		<body style="margin:0;padding:0;font-size:12px;font-family:Arial, Helvetica, sans-serif;">
		<div style="padding:0 15px;">
		<div style="border:4px solid #5b9bd5;max-width:630px;margin:50px auto;padding:10px;background:#f2f2f2;border-radius:5px;">
		<img src="'.$siteurl.'images/logo.png" alt="Logo" title="Logo" style="display:block;margin:15px auto;max-width:100%;" />
		<div style="border:2px solid #a5a5a5;margin:10px auto 0 auto;padding:10px 20px;background:#fff;border-radius:5px;">
		<p style="font-size:16px;margin-bottom:15px;">Thank you for contacting at <a href="http://www.webwedmobile.com" style="color:#5b9bd5;text-decoration:none;">WEBWEDMOBILE.COM</a>.</p>
			<p style="font-size:16px;margin-bottom:15px;">You need to reset your password</p>
			<p style="font-size:16px;margin-bottom:15px;">If you have any questions, please email us at <a href="mailto:info@webwedmobile.com" style="color:#5b9bd5;text-decoration:none;">info@webwedmobile.com</a></p>
			<p style="font-size:16px;margin-bottom:15px;">To reset your password please <a href="'.$resetpassword.'">click here</a></p>
			<p style="font-size:16px;margin-bottom:15px;text-align:center;">Thank You!</p>
			</div>
			</div>
			</div>
			</body>
			</html>';
			if(email($to,$subject,$automessage,$headers)){
				$msg = "<div class='alert alert-success'>We have sent you a password reset. Please check your email to continue this process.</div>";
			}
		} else {
			$error = "<div class='alert alert-danger'><span style='color: #CB0E0E;'>The Email Address was not found in our records.<br/><br/></span></div>";
		}
	}
	?>
	<?php  $page_name=basename($_SERVER['PHP_SELF']);?>
	<?php
	$page_title = "Forgot Password - ";
	include('../header.php'); ?>
	<div class="container">
		<div class="form_main">
		<div class="login wrapper">
			<?php
			if(isset($error)){
				echo $error;
			}
			if(isset($msg)){
				echo $msg;
			}

			?>
			<h1>Forgot Password</h1>
			<form role="form" method="post" id="forgot-password-form" action="forgot_password.php">
				<p><span>Please enter your email address below. You will receive a link to reset your password.</span></p>
				<input type="text" placeholder="Email" class="input_text" name="email" id="email" /><br/><br/>
				<input type="submit" name="submit" value="SUBMIT" class="btn btn-primary" />
			</form>
		</div>
	</div>
		<br/>
		<br/>
		<br/>
	</div>


	<?php include('../footer.php'); ?>
	<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
	<script>


	$(function() {
		$( "#forgot-password-form" ).validate({
			rules: {
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				email: {
					required: "Please enter your email."
				}
			}
		});
	});
	</script>

	<!--action="php echo SITEURL; user_forgot_password.php" -->
