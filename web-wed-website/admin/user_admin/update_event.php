<?php
session_start();
if(!isset($_SESSION['wwm_user_id'])){
  header("location: logout.php");
}

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/utility_functions.php');

// if (!paymentIsCurrent($pdoDB, $_SESSION['user_id'])){
//   header("Location: make_payment.php");
// 	exit;
// }

$mostRecentEvent = getMostRecentEventFromUid($_SESSION['wwm_user_id']);

if (isset($_POST['enable_public'], $_POST['enable_chat'], $_POST['enable_gifts'], $_POST['gift_link'])){
  $event_id = $mostRecentEvent['data']['id'];
  $public = $_POST['enable_public'];
  $chat = $_POST['enable_chat'];
  $gifts = $_POST['enable_gifts'];
  $giftslink = $_POST['gift_link'];

  $table = 'weddings';

  if ($mostRecentEvent['type'] == 'special_moment'){
    $table = 'special_moments';
    $prepared = $pdoDB->prepare("UPDATE `special_moments` SET `gifts` = ? WHERE `id` = ?");
    $prepared->execute(array($gifts, $event_id));

    $prepared = $pdoDB->prepare("UPDATE `special_moments` SET `public` = ? WHERE `id` = ?");
    $prepared->execute(array($public, $event_id));

    $prepared = $pdoDB->prepare("UPDATE `special_moments` SET `chat` = ? WHERE `id` = ?");
    $prepared->execute(array($chat, $event_id));

    $prepared = $pdoDB->prepare("UPDATE `special_moments` SET `giftlink` = ? WHERE `id` = ?");
    $prepared->execute(array($giftslink, $event_id));
  }else{
    $prepared = $pdoDB->prepare("UPDATE `weddings` SET `gifts` = ? WHERE `id` = ?");
    $prepared->execute(array($gifts, $event_id));

    $prepared = $pdoDB->prepare("UPDATE `weddings` SET `public` = ? WHERE `id` = ?");
    $prepared->execute(array($public, $event_id));

    $prepared = $pdoDB->prepare("UPDATE `weddings` SET `chat` = ? WHERE `id` = ?");
    $prepared->execute(array($chat, $event_id));

    $prepared = $pdoDB->prepare("UPDATE `weddings` SET `giftlink` = ? WHERE `id` = ?");
    $prepared->execute(array($giftslink, $event_id));
  }

  $mostRecentEvent = getMostRecentEventFromUid($_SESSION['wwm_user_id']);

  $msg = '<div class="alert alert-success">Your event has been successfully updated.</div>';
}

$page_title = "Update Event - ";
include('../header.php'); ?>
<br>
<br>
<br>
<div class="container">
  <h1>Edit Event</h1>
  <hr>
  <div class="row">
    <?php
    if(isset($_GET['pending']) && $_GET['pending'] == 'true') {
      ?>
      <div class="alert alert-warning alert-dismissable" id="success_alert">
        <a class="panel-close close" data-dismiss="alert">×</a>
        <p id="success">Your account is pending admin approval. You will gain access to other sections onces approved.</p>
      </div>
      <?php
    }
    ?>
    <?php
    if(isset($_GET['submit']) && $_GET['submit'] == 'true') {
      ?>
      <div class="alert alert-success alert-dismissable" id="success_alert">
        <a class="panel-close close" data-dismiss="alert">×</a>
        <p id="success">Your details have been updated successfully</p>
      </div>
      <?php
    }

    echo @$msg;
    ?>
    <form name="admin_update_form" id="admin_update_form" method="post" action="<?php echo $siteurl; ?>/user_admin/update_event.php">

      <!-- edit form column -->

      <div class="col-md-12 personal-info">

        <h2>Configure Event</h2>

        <div class="form-group col-xs-12 col-md-6">
          <label for="email" class="control-label">Is your event public?</label>
          <select class="form-control" name="enable_public">
            <option value='0' <?php if ($mostRecentEvent['data']['public'] == 0){ ?> selected="selected" <?php } ?>>Disabled</option>
            <option value='1' <?php if ($mostRecentEvent['data']['public'] == 1){ ?> selected="selected" <?php } ?>>Enabled</option>
          </select>
        </div>
        <div class="form-group col-xs-12 col-md-6">
          <label for="pass_c" class="control-label">Are guests allowed to chat?</label>
          <select class="form-control" name="enable_chat">
            <option value='0' <?php if ($mostRecentEvent['data']['chat'] == 0){ ?> selected="selected" <?php } ?>>Disabled</option>
            <option value='1' <?php if ($mostRecentEvent['data']['chat'] == 1){ ?> selected="selected" <?php } ?>>Enabled</option>
          </select>
        </div>
        <div class="form-group col-xs-12 col-md-6">
          <label for="pass" class="control-label">Are you accepting gifts?</label>
          <select class="form-control" name="enable_gifts">
            <option value='0' <?php if ($mostRecentEvent['data']['gifts'] == 0){ ?> selected="selected" <?php } ?>>Disabled</option>
            <option value='1' <?php if ($mostRecentEvent['data']['gifts'] == 1){ ?> selected="selected" <?php } ?>>Enabled</option>
          </select>
        </div>
        <div class="form-group col-xs-12 col-md-6">
          <label for="pass_c" class="control-label">What is your registry url?</label>
          <input type="text" class="form-control" id="pass_c" name="gift_link"><br>
          <p class="small">Access your registry using this link. Then copy the web address into the text field provided.</p><a href="https://www.myregistry.com"><img src="https://upload.wikimedia.org/wikipedia/en/b/b8/MyRegistry.com_logo.png" width='250px'/></a></div>
        </div>

        <div class="form-group">
          <div class="col-md-12">
            <input type="submit" class="btn btn-primary" value="Save Changes" style="width:100%;">
          </div>
        </div>

      </div>
    </div>
  </form>


</div>
<hr>
<script>
var admin_id = '<?php echo $_SESSION['user_id']; ?>';
available_times = JSON.parse( available_times );
$( document ).ready(function() {


  $('#starttime').timepicker({
    'defaultTime': available_times['start']
  });

  $('#endtime').timepicker({
    'defaultTime': available_times['end']
  });

});
</script>
<!-- pattern -->

<?php include('../footer.php'); ?>
