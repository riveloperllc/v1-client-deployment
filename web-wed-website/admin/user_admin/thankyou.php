<?php
session_start();
?>
<?php 
require_once('common/connection.php'); 
require_once('common/config.php');
?>
<?php  $page_name=basename($_SERVER['PHP_SELF']);?>
<?php include('header.php'); ?>

<div class="form_main">
    <div class="form_main_inside">
        <div class="login" style="text-align:justify;">
            <h1>Thank You</h1>
          <p>Thank you for registering with WEBWEDMOBILE. </p>
            <p>Your account has been created and a verification email has been sent to your registered email address. Please click on the verification link included in the email to activate your account.</p>
            <p>Your account will not be activated until you verify your email address. </p>
            <p>Having trouble accessing your account? Please contact Online Customer Support.</p>
        </div>
    </div>
</div>


<!-- pattern -->
<!-- pattern -->
<div id="pattern"></div><!-- pattern end --><!-- contents --><!-- content section 1 -->
<div class="slide" id="slide-section1"><span class="close" onclick="closeSlide('section1')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>Web Wed</h1>
                    <p>Getting married is as easy as logging into to your computer or mobile device. With Web Wed and Web Wed Mobile, 
                    marriage by proxy is here to stay.</p>
                </div>
                <div class="post-left-bottom last">
                    <ul id="mycarousel" class="jcarousel-skin-one"><!-- news 1-->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-news"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="News 1" /></a>
                            <span class="titles">Marriage Counseling || <a href="javascript:show('section1-1')">Read More</a></span>
                            <p>Marital or Couples therapy helps partners by creating a context where emotionally charged issues 
                            in the relationship.</p>
                        </li><!-- news 1 end --><!-- news 2 -->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-news"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="News 2" /></a>
                            <span class="titles">Pre-marriage Consults || <a href="javascript:show('section1-1')">Read More</a></span>
                            <p>A successful marriage can be one of the greatest experiences of life. What's interesting is that those 
                            partnerships that endure are not without conflict.</p>
                        </li><!-- news 2 end --><!-- news 3 -->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-news"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="News 3" /></a>
                            <span class="titles">Legality || <a href="javascript:show('section1-1')">Read More</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- news 3 end --><!-- news 4 -->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-news"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="News 4" /></a>
                            <span class="titles">Mobile App || <a href="javascript:show('section1-1')">Read More</a></span>
                            <p>COMING SOON.</p>
                        </li><!-- news 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 1 end --><!-- content section 1-1 -->


<div class="slide" id="slide-section1-1"><span class="close" onclick="closeSlide('section1-1')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>News</h1>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut 
                    laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                    suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="post-left-bottom last">
                    <ul id="mycarousel-1-1" class="jcarousel-skin-one"><!-- news more 1 -->
                        <li>
                            <span class="titles-more">News Title 1 || <a href="javascript:show('section1')">Read Less</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- news more 1 end --><!-- news more 2 -->
                        <li>
                            <span class="titles-more">News Title 2 || <a href="javascript:show('section1')">Read Less</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- news more 2 end --><!-- news more 3 -->
                        <li>
                            <span class="titles-more">News Title 3 || <a href="javascript:show('section1')">Read Less</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- news more 3 end --><!-- news more 4 -->
                        <li>
                            <span class="titles-more">News Title 4 || <a href="javascript:show('section1')">Read Less</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- news more 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 1-1 end --><!-- content section 2 -->


<div class="slide" id="slide-section2"><span class="close" onclick="closeSlide('section2')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>Our Company</h1>
                    <p>The Web Wed team consists of lawyers, ministers, techies, and volunteers to help bring each and every couple the proxy 
                    wedding they desire. Our dedication to forward thinking and a love for technology allows to stay ahead of the curve.</p>
                </div>
                <div class="post-left-bottom last"><!-- smartTab -->
                    <div id="tabs" class="stContainer">
                        <ul>
                            <li><a href="#tabs-1">Who we are</a></li>
                            <li><a href="#tabs-2">What we do</a></li>
                            <li><a href="#tabs-3">Why we do it</a></li>
                        </ul><!-- who -->
                        <div id="tabs-1">
                            <p>All Web Wed ministers, officants, reverends, and clergy are licensed and/or ordained. Web Wed has scoured to 
                            globe to locate equality friendly offiants to wed you anytime and anywhere you choose.</p>
                        </div><!-- who end --><!-- what -->
                        <div id="tabs-2">
                            <p>Web Wed is dedicated to providing access to marriage equality to everyone all over the world. To help achieve 
                            this goal, the founders of Web Wed are pleased to announce to future debut of Web Wed Mobile; a cross-platform 
                            mobile application that will allow people to be wed from anywhere in the world, legally.</p>
                        </div><!-- what end --><!-- why -->
                    	<div id="tabs-3">
                            <p>Web Wed has defined what it means to be wed on YOUR TIME. Your schedule dictates where, when, and what takes place. 
                            No fears of being turned down by churches, banquet halls, or other venues of the like.</p>
                        </div><!-- why end -->
                    </div><!-- smartTab end -->
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 2 end --><!-- content section 3 -->


<div class="slide" id="slide-section3"><span class="close" onclick="closeSlide('section3')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>Our Team</h1>
                    <p>The Web Wed team consists of lawyers, ministers, techies, and volunteers to help bring each and every couple the 
                    proxy wedding they desire. Our dedication to forward thinking and a love for technology allows to stay ahead of the curve.</p>
                </div>
                <div class="post-left-bottom last">
                    <ul id="mycarousel-3" class="jcarousel-skin-one"><!-- team member 1 -->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-team"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="Team Member 1" /></a>
                            <span class="titles">Ministers || <a href="javascript:show('section3-1')">More Info</a></span><!--social media team -->
                            <ul class="social" id="social-team-member-1">
                                <li class="facebook"><a href="#"><strong>Facebook</strong></a></li>
                                <li class="google-plus"><a href="#"><strong>Google+</strong></a></li>
                                <li class="linkedin"><a href="#"><strong>LinkedIn</strong></a></li>
                                <li class="twitter"><a href="#"><strong>Twitter</strong></a></li>
                                <li class="youtube"><a href="#"><strong>YouTube</strong></a></li>
                                <li class="skype"><a href="#"><strong>Skype</strong></a></li>
                            </ul><!-- social media team end -->
                        </li><!-- team member 1 end --><!-- team member 2 -->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-team"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="Team Member 2" /></a><span class="titles">Team Member 2 || 
                            <a href="javascript:show('section3-1')">More Info</a></span><!-- social media team -->
                            <ul class="social" id="social-team-member-2">
                                <li class="facebook"><a href="#"><strong>Facebook</strong></a></li>
                                <li class="google-plus"><a href="#"><strong>Google+</strong></a></li>
                                <li class="linkedin"><a href="#"><strong>LinkedIn</strong></a></li>
                                <li class="twitter"><a href="#"><strong>Twitter</strong></a></li>
                                <li class="youtube"><a href="#"><strong>YouTube</strong></a></li>
                                <li class="skype"><a href="#"><strong>Skype</strong></a></li>
                            </ul><!-- social media team end -->
                        </li><!-- team member 2 end --><!-- team member 3 -->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-team"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="Team Member 3" /></a><span class="titles">
                            Team Member 3 || <a href="javascript:show('section3-1')">More Info</a></span><!-- social media team -->
                            <ul class="social" id="social-team-member-3">
                                <li class="facebook"><a href="#"><strong>Facebook</strong></a></li>
                                <li class="google-plus"><a href="#"><strong>Google+</strong></a></li>
                                <li class="linkedin"><a href="#"><strong>LinkedIn</strong></a></li>
                                <li class="twitter"><a href="#"><strong>Twitter</strong></a></li>
                                <li class="youtube"><a href="#"><strong>YouTube</strong></a></li>
                                <li class="skype"><a href="#"><strong>Skype</strong></a></li>
                            </ul><!-- social media team end -->
                        </li><!-- team member 3 end --><!-- team member 4 -->
                        <li>
                            <a href="images/rollover/rollover-big.jpg" rel="prettyPhoto"><span class="roll-team"></span>
                            <img src="images/rollover/rollover-thumb.jpg" alt="Team Member 4" />
                            </a><span class="titles">Team Member 4 || <a href="javascript:show('section3-1')">More Info</a>
                            </span><!--social media team -->
                            <ul class="social" id="social-team-member-4">
                                <li class="facebook"><a href="#"><strong>Facebook</strong></a></li>
                                <li class="google-plus"><a href="#"><strong>Google+</strong></a></li>
                                <li class="linkedin"><a href="#"><strong>LinkedIn</strong></a></li>
                                <li class="twitter"><a href="#"><strong>Twitter</strong></a></li>
                                <li class="youtube"><a href="#"><strong>YouTube</strong></a></li>
                                <li class="skype"><a href="#"><strong>Skype</strong></a></li>
                            </ul><!-- social media team end -->
                        </li><!-- team member 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 3 end --><!-- content section 3-1 -->


<div class="slide" id="slide-section3-1"><span class="close" onclick="closeSlide('section3-1')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>Our Team</h1>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet 
                    dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
                    lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="post-left-bottom last">
                    <ul id="mycarousel-3-1" class="jcarousel-skin-one"><!-- team member more 1 -->
                        <li>
                            <span class="titles-more">Team Member 1 || <a href="javascript:show('section3')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- team member more 1 end --><!-- team member more 2 -->
                        <li>
                            <span class="titles-more">Team Member 2 || <a href="javascript:show('section3')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus ac 
                            facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- team member more 2 end --><!-- team member more 3 -->
                        <li>
                            <span class="titles-more">Team Member 3 || <a href="javascript:show('section3')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- team member more 3 end --><!-- team member more 4 -->
                        <li>
                            <span class="titles-more">Team Member 4 || <a href="javascript:show('section3')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p></li><!-- team member more 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 3-1 end --><!-- content section 4 -->


<div class="slide" id="slide-section4"><span class="close" onclick="closeSlide('section4')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>Upcoming Events</h1>
                    <p>Coming Soon</p>
                </div>
                <div class="post-left-bottom last" style="display: none">
                    <ul id="mycarousel-4" class="jcarousel-skin-one"><!-- upcoming events 1 -->
                        <li><!-- events date -->
                            <div class="place-date">23</div>
                            <div class="place-month">March</div>
                            <div class="place-year">2013</div><!-- events date end --><!-- container full -->
                            <span class="titles">Event Title 1 || <a href="javascript:show('section4-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- upcoming events 1 end --><!-- upcoming events 2 -->
                        <li><!-- events date -->
                            <div class="place-date">24</div>
                            <div class="place-month">March</div>
                            <div class="place-year">2013</div><!-- events date end -->
                            <span class="titles">Event Title 2 || <a href="javascript:show('section4-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- upcoming events 2 end --><!-- upcoming events 3 -->
                        <li><!-- events date -->
                            <div class="place-date">25</div>
                            <div class="place-month">March</div>
                            <div class="place-year">2013</div><!-- events date end -->
                            <span class="titles">Event Title 3 || <a href="javascript:show('section4-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- upcoming events 3 end --><!-- upcoming events 4 -->
                        <li><!-- events date -->
                            <div class="place-date">26</div>
                            <div class="place-month">March</div>
                            <div class="place-year">2013</div><!-- events date end -->
                            <span class="titles">Event Title 4 || <a href="javascript:show('section4-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- upcoming events 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 4 end --><!-- content section 4-1 -->


<div class="slide" id="slide-section4-1"><span class="close" onclick="closeSlide('section4-1')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>Upcoming Events</h1>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut 
                    laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                    suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="post-left-bottom last">
                    <ul id="mycarousel-4-1" class="jcarousel-skin-one"><!-- upcoming events more 1 -->
                        <li>
                            <span class="titles-more">Event Title 1 || <a href="javascript:show('section4')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus ac 
                            facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- upcoming events more 1 end --><!-- upcoming events more 2 -->
                        <li>
                            <span class="titles-more">Event Title 2 || <a href="javascript:show('section4')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- upcoming events more 2 end --><!-- upcoming events more 3 -->
                        <li>
                            <span class="titles-more">Event Title 3 || <a href="javascript:show('section4')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- upcoming events more 3 end --><!-- upcoming events more 4 -->
                        <li>
                            <span class="titles-more">Event Title 4 || <a href="javascript:show('section4')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- upcoming events more 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 4-1 end --><!-- content section 5 -->


<div class="slide" id="slide-section5"><span class="close" onclick="closeSlide('section5')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">

                <div class="post-left-top">
                    <h1>Past Events</h1>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet 
                    dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
                    lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="post-left-bottom last">
                    <ul id="mycarousel-5" class="jcarousel-skin-one"><!-- past events 1 -->
                        <li><!-- events date -->
                            <div class="place-date">23</div>
                            <div class="place-month">February</div>
                            <div class="place-year">2013</div><!-- events date end -->
                            <span class="titles">Event Title 1 || <a href="javascript:show('section5-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- past events 1 end --><!-- past events 2 -->
                        <li><!-- events date -->
                            <div class="place-date">24</div>
                            <div class="place-month">February</div>
                            <div class="place-year">2013</div><!-- events date end -->
                            <span class="titles">Event Title 2 || <a href="javascript:show('section5-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- past events 2 end --><!-- past events 3 -->
                        <li><!-- events date -->
                            <div class="place-date">25</div>
                            <div class="place-month">February</div>
                            <div class="place-year">2013</div><!-- events date end -->
                            <span class="titles">Event Title 3 || <a href="javascript:show('section5-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- past events 3 end --><!-- past events 4 -->
                        <li><!-- events date -->
                            <div class="place-date">26</div>
                            <div class="place-month">February</div>
                            <div class="place-year">2013</div><!-- events date end -->
                            <span class="titles">Event Title 4 || <a href="javascript:show('section5-1')">More Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                        </li><!-- past events 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 5 end --><!-- content section 5-1 -->


<div class="slide" id="slide-section5-1"><span class="close" onclick="closeSlide('section5-1')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post-left-top">
                    <h1>Past Events</h1>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet 
                    dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
                    lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>
                <div class="post-left-bottom last">
                    <ul id="mycarousel-5-1" class="jcarousel-skin-one"><!-- past events more 1 -->
                        <li>
                            <span class="titles-more">Event Title 1 || <a href="javascript:show('section5')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- past events more 1 end --><!-- past events more 2 -->
                        <li>
                            <span class="titles-more">Event Title 2 || <a href="javascript:show('section5')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- past events more 2 end --><!-- past events more 3 -->
                        <li>
                            <span class="titles-more">Event Title 3 || <a href="javascript:show('section5')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- past events more 3 end --><!-- past events more 4 -->
                        <li>
                            <span class="titles-more">Event Title 4 || <a href="javascript:show('section5')">Less Info</a></span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum lacinia lacinia. Etiam nulla urna, 
                            laoreet sit amet porttitor quis, blandit eu felis.</p>
                            <p>Proin scelerisque, enim vel bibendum mattis, felis lectus volutpat turpis, sed condimentum libero tortor 
                            vitae ipsum. Duis vestibulum molestie mauris, sed auctor est lobortis nec. Sed tristique scelerisque lectus 
                            ac facilisis. Suspendisse eu laoreet massa. Praesent sed enim magna.</p>
                        </li><!-- past events more 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 5-1 end --><!-- content section 6 -->


<div class="slide" id="slide-section6"><span class="close" onclick="closeSlide('section6')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post">
                	<h1>Legal Info</h1>
                	<p>Common law and statutory marriage have the following characteristics in common:</p>
                    <p>
                    1. Both parties must freely consent to the marriage<br />
                    2. Both parties must be of legal age to contract a marriage or have parental consent to marry<br />
                    3. Neither party may be under a disability that prevents him or her from entering into a valid marriage - e.g. 
                    they must both be of sound mind, neither of them can be currently married (except in Saskatchewan[2]), and some 
                    jurisdictions do not permit prisoners to marry.
                    </p>
                	<p>Otherwise, common law marriage differs from statutory marriage as follows:</p>
                    <p>
                    1. There is no marriage license issued by a government and no marriage certificate filed with a government<br />
                    2. There is no formal ceremony to solemnize the marriage before witnesses<br />
                    3. The parties must hold themselves out to the world as husband and wife (this is not a requirement of statutory marriage) <br />
                    4. Most jurisdictions require the parties to be cohabiting at the time the common law marriage is formed. Some require cohabitation to last a certain length of time (e.g. three years) for the marriage to be valid. But cohabitation alone does not create a marriage.</p> <br />
                	<p>The parties must intend their relationship to be, and to be regarded as, a legally valid marriage.</p>
                	<p>When a marriage is validly contracted, whether according by statutory provision or according to common law, the marriage can be dissolved only by a formal legal proceeding in a court of competent jurisdiction - usually a family or probate court.[1]</p>
                	<p>In the U.S. state of Texas, a new provision was added to the Family Code; either partner in a common law marriage has two years after separation to file an action in order to prove that the common law marriage existed. To use the provision, the separation must have occurred after September 1, 1989.[3]</p>
                	<p>Since the mid-1990s, the term &quot;common law marriage&quot; has been used in parts of Europe and Canada to describe various types of domestic partnership between persons of the same sex as well those of the opposite sex. Although these interpersonal statuses are often, as in Hungary, called &quot;common law marriage&quot; they differ from true common law marriage in that they are not legally recognized as &quot;marriages&quot; but are a parallel interpersonal status, known in most jurisdictions as &quot;domestic partnership&quot;, &quot;registered partnership&quot;, &quot;conjugal union&quot; or &quot;civil union&quot; etc.</p>
                	<p>Not all agreements break statutes. Some are illegal because they break public policy, which is generally &quot;to discourage any interference with the freedom of choice&quot; (Saskatchewan, Canada, excepted). An agreement forbidding a party to  marry or bribing a party to refrain from marriage is considered &quot;Interference with Marriage Relation&quot; or an &quot;Agreement in Restraint of Marriage&quot;; such agreements are typically held to be nonbinding.[4][5]</p>
                </div>
                <div class="post-left-bottom last" style="display: none">
                    <ul id="mycarousel-6" class="jcarousel-skin-one"><!-- photos 1 -->
                        <li><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-1]"><span class="roll-photos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Photo Gallery 1" /></a><span class="titles">Photo Gallery 1</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-1]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-1]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-1]"></a></li><!-- photos 1 end --><!-- photos 2 -->
                        <li><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-2]"><span class="roll-photos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Photo Gallery 2" /></a><span class="titles">Photo Gallery 2</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-2]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-2]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-2]"></a></li><!-- photos 2 end --><!-- photos 3 -->
                        <li><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-3]"><span class="roll-photos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Photo Gallery 3" /></a><span class="titles">Photo Gallery 3</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-3]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-3]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-3]"></a></li><!-- photos 3 end --><!-- photos 4 -->
                        <li><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-4]"><span class="roll-photos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Photo Gallery 4" /></a><span class="titles">Photo Gallery 4</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-4]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-4]"></a><a href="images/rollover/rollover-big.jpg" rel="prettyPhoto[photos-4]"></a></li><!-- photos 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 6 end --><!-- content section 7 -->


<div class="slide" id="slide-section7"><span class="close" onclick="closeSlide('section7')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post">
                	<h1>Legal Info</h1>
                	<h2>Proxy Marriages</h2>
                	<h2>
                	<p>A proxy marriage is a marriage where the parties were not physically present in the presence of each other. Someone stands in for the other party because either the bride or the groom is not physically present for the wedding. During the solemnization of the marriage, based upon a power of attorney, an agent acts on behalf of one of the parties. The marriage is presumed valid if it is valid in the country that performs the ceremony.  Mostly compared and used in military marriages, and maximum security facility. Marriage by proxy, with only one party to the marriage making an appearance, is possible in the United States. It is always necessary for one of the two parties (either husband or wife) to actually appear before the Civil Authorities. </p>
                    </h2>
                	<h2>Written Authorization for Third Person to Act as Proxy</h2>
               	 	<p>If a party to a marriage is unable to be present at the solemnization, that party may authorize in writing a third person to act as his proxy. If the person solemnizing the marriage is satisfied that the absent party is unable to be present and has consented to the marriage, he or she may solemnize the marriage by proxy. If he or she is not satisfied, the parties may petition the court for an order permitting the marriage to be solemnized by proxy.</p>
                	<h2>Proxy Marriages Allowed in Four States</h2>
                	<p>Marriage by proxy has been around a long time. Proxy marriages are most common during wartime. Marriages by proxy are allowed in California, Colorado, Texas, and Montana.</p>
                	<h2>Legal Recognition</h2>
                    <p>Whether a state or country will recognize a marriage by proxy seems to depend on whether or not the law of the locale requires that both parties be present to apply for a license or to give their consent at the ceremony. Some states recognize a proxy marriage that was done in another state. Other states only recognize them as common law marriage. U.S. military personnel may annul a proxy marriage provided there is no consummation, no cohabitation, or no treatment as husband and wife after the marriage ceremony. </p>
                    <h2>Immigration Concerns</h2>
                    <p>Unconsummated proxy marriages are not recognized for immigration purposes in most countries, including the United States. However, a party of an unconsummated proxy marriage may enjoy immigration benefits as a fiance of the opposite party is a U.S. citizen.</p><br />
                    <h2>COMMON LAW </h2>
                    <p>Common law marriage should not be confused with non-marital relationship contracts, which involves two people living together without holding themselves out to the world as spouses and/or without legal recognition as spouses in the jurisdiction where the contract was formed. Non-marital relationship contracts are not necessarily recognized from one jurisdiction to another whereas common law marriages are, by definition, legally valid marriages worldwide - provided the parties complied with the requirements to form a valid marriage while living in a jurisdiction that still allows this irregular form of marriage to be contracted - as was historically the case under the common law of England (hence the name, &quot;common law marriage&quot;).</p>
                </div>
                <div class="post-left-bottom last" style="display: none">
                    <ul id="mycarousel-7" class="jcarousel-skin-one"><!-- videos 1 -->
                        <li><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-1]"><span class="roll-videos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Video Gallery 1" /></a><span class="titles">Video Gallery 1</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-1]"></a><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-1]"></a><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-1]"></a></li><!-- videos 1 end --><!-- videos 2 -->
                        <li><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-2]"><span class="roll-videos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Video Gallery 2" /></a><span class="titles">Video Gallery 2</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-2]"></a><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-2]"></a><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-2]"></a></li><!-- videos 2 end --><!-- videos 3 -->
                        <li><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-3]"><span class="roll-videos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Video Gallery 3" /></a><span class="titles">Video Gallery 3</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-3]"></a><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-3]"></a><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-3]"></a></li><!-- videos 3 end --><!-- videos 4 -->
                        <li><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-4]"><span class="roll-videos"></span><img src="images/rollover/rollover-thumb.jpg" alt="Video Gallery 4" /></a><span class="titles">Video Gallery 4</span>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-4]"></a><a href="http://vimeo.com/15475581" rel="prettyPhoto[videos-4]"></a><a href="http://www.youtube.com/watch?v=fZd9mKJcOR0" rel="prettyPhoto[videos-4]"></a></li><!-- videos 4 end -->
                    </ul>
                </div>
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 7 end --><!-- content section 8 -->


<div class="slide" id="slide-section8"><span class="close" onclick="closeSlide('section8')"></span>
    <div class="wrapper"><!-- wrapper full -->
        <div class="wrapper-full"><!-- container full -->
            <div class="container-full">
                <div class="post">
                	<h1>Legal Info</h1>
                	<h2>SAME SEX MARRIAGES </h2>
                    <p>Same-sex marriage (also known as gay marriage) is marriage between two persons of the same biological sex or gender identity. Supporters of legal recognition for same-sex marriage typically refer to such recognition as marriage equality.[1]</p>
                    <p>Since 2001, eleven countries (Argentina, Belgium, Canada, Denmark, Iceland, Netherlands, Norway, Portugal, Spain, South Africa, Sweden) and some sub-national jurisdictions (parts of Mexico and the United States) have begun to allow same-sex couples to marry. Introduction of same-sex marriage has varied by jurisdiction, resulting from legislative changes to marriage laws or from court challenges based on constitutional guarantees of equality, or a combination of the two. The recognition of same-sex marriage is a political, social, civil-rights and religious issue in many nations, and debates continue to arise over whether same-sex couples should be allowed marriage, be required to hold a different  status (a civil union), or not have any such rights.</p>
                    <p>Same-sex marriages can be performed in a secular civil ceremony or in a religious setting, and are endorsed and practised by various religious groups around the world, including the Quakers, liberal Jews, Wiccans, Druids, Episcopalians,  Unitarian Universalists and Native Americans. </p>
                    <p>As of May 10, 2012, gay marriage has been legalized in the following eight states: Massachusetts (May 17, 2004), Connecticut (Nov. 12, 2008), Iowa (Apr.24.2009), Vermont (Sep. 1, 2009), New Hampshire (Jan. 1, 2010), New York (June 24, 2011), Washington (passed Feb. 14, 2012; effective June 7, 2012), Maryland (passed Mar. 1, 2012; effective Jan. 1, 2013) and the District of Columbia (Mar. 3, 2010). [1] 31 states have constitutional amendments banning gay marriage. [25]</p><br />
                    <p>Offiants;</p>
                    <p>ALL WEDDINGS ARE PERFORMED BY LICENSED, ORDAINED, AND OR CERFTIFIED MINISTERS, CLERGY, AND OR OFFIANTS.</p>
                    <p>1.     http://gaylife.about.com/od/gaymarriagebyregion/ig/Legal-Marriage/</p>
                    <p>2.     http://www.unmarried.org/common-law-marriage-fact-sheet.html</p>
                    <p>3.     http://www.proxymarriagehq.com/proxy-marriage-law/</p>
                    <p>4.     http://www.usmarriagelaws.com/</p>
                    <p>5.     http://www.marriageequality.org/
                </div>       
            </div><!-- container full end -->
        </div><!-- wrapper full end -->
    </div>
</div><!-- content section 8 end --><!-- contents end --><!-- left right shadow -->


<div id="left-shadow"></div>
<div id="right-shadow"></div><!-- left right shadow end --><!-- bottom shadow -->
<div class="bottom-shadow"></div><!-- bottom shadow end -->

<?php include('footer.php'); ?> 