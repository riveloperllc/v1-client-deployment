<?php
session_start();
if(!isset($_SESSION['email'])){
header("location:user_login.php");
}
error_reporting(E_ALL ^ E_NOTICE);
?>
<?php 
require_once('common/connection.php'); 
require_once('common/config.php');
?>
<?php	
$sql="SELECT * FROM wwm_users WHERE email='" . $_SESSION['email'] . "'";
$result=mysql_query($sql);
$row = mysql_fetch_array($result)or die(mysql_error());
$city = $row["city"];
$state = $row["state"];
$country = $row["country"];

if(isset($_POST['submit']))
{
	$address= $_POST['address']; 
	$address = mysql_real_escape_string($address);
	$city= $_POST['city'];
	$city = mysql_real_escape_string($city); 
	$zip= $_POST['zip']; 
	$state= $_POST['state']; 
	$state = mysql_real_escape_string($state); 
	$country= $_POST['country'];
	$country = mysql_real_escape_string($country);  
	$phone= $_POST['phone'];
	$fname= $_POST['fname'];
	$fname = mysql_real_escape_string($fname); 
	$lname= $_POST['lname'];
	$lname = mysql_real_escape_string($lname); 
	$date = strtotime(date("YmdHis"));
	$query = "UPDATE wwm_users SET address='$address',city='$city',zipcode='$zip',state='$state',country='$country',phone='$phone',fname='$fname',lname='$lname',mod_date='$date'"; 
	$data = mysql_query ($query)or die(mysql_error()); 
	header('Location: '.$siteurl.'user_dashboard.php?succ=1');		
}	
?>

<?php  $page_name = basename($_SERVER['PHP_SELF']);?>
<?php include('header.php'); ?>

<div class="form_main">
    <div class="form_main_inside">
        <div class="login payment dashboard">
            <h2>Edit Information</h2>
            <div class="payment_left">
                <form id="editinfo" method="post" enctype="multipart/form-data" action="<?php echo SITEURL; ?>edit_information.php">
                <label>First name</label>
                <input type="text" class="input_text" name="fname" id="fname" value="<?php echo $fname = $row['fname']; ?>" />
                <label>Last name</label>
                <input type="text" class="input_text" name="lname" id="lname" value="<?php echo $lname = $row['lname']; ?>" />
                <label>Telephone</label>
                <input type="text" class="input_text" name="phone" id="phone"  value="<?php echo $phone = $row["phone"]; ?>" />
                <label>Address</label>
                <textarea class="input_text textarea" id="address" name="address"><?php echo $address = $row["address"]; ?></textarea>
                <label>City</label>
                <?php
$sqlcities="SELECT id,city_name FROM wwm_cities";
$resultcities=mysql_query($sqlcities);
if($resultcities) {
echo '<select class="input_text select_option" id="city" name="city"><option value="">Select City</option>';
while ($fetch = mysql_fetch_array($resultcities)) {
$selected = ($fetch['city_name'] == $city) ? ' selected=selected' : '';
echo '<option '.$selected.' id="'.$fetch['id'].'" value="'.$fetch['city_name'].'">'.$fetch['city_name'].'</option>';
}
echo '</select>';
}
?> 

                <label>Zip Code</label>
                <input type="text"  class="input_text" id="zip" name="zip" value="<?php echo $zipcode = $row["zipcode"]; ?>" />
                <label>State / Region</label>
                <?php
$sqlstate="SELECT id,state_name FROM wwm_states";
$resultstate=mysql_query($sqlstate);
if($resultstate) {
echo '<select class="input_text select_option" id="state" name="state"><option value="">Select State</option>';
while ($fetchstate = mysql_fetch_array($resultstate)) {
$selectedstate = ($fetchstate['state_name'] == $state) ? ' selected=selected' : '';
echo '<option '.$selectedstate.' id="'.$fetchstate['id'].'" value="'.$fetchstate['state_name'].'">'.$fetchstate['state_name'].'</option>';
}
echo '</select>';
}
?> 

                <label>Country</label>
  <?php
$sqlcountries="SELECT id,country_name FROM wwm_countries";
$resultcountries=mysql_query($sqlcountries);
if($resultcountries) {
echo '<select class="input_text select_option" id="country" name="country"><option value="">Select Country</option>';
while ($fetchcountries = mysql_fetch_array($resultcountries)) {
$selectedcountries = ($fetchcountries['country_name'] == $country) ? ' selected=selected' : '';
echo '<option '.$selectedcountries.' id="'.$fetchcountries['id'].'" value="'.$fetchcountries['country_name'].'">'.$fetchcountries['country_name'].'</option>';
}
echo '</select>';
}
?> 
                <input type="submit" name="submit" value="Save Information" class="btn" />
                </form>
            </div>
        </div>
    </div>
</div>


<?php include('bottom_link.php'); ?> 
<?php include('footer.php'); ?> 
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>
$(function() {
   $( "#editinfo" ).validate({
           rules: {
                   state_located: {				// compound rule
							required: true
					},
					over_eighteen: {				// compound rule
							required: true
					},
					when_married: {				// compound rule
							required: true
					},
					marriage_counseling: {				// compound rule
							required: true
					},
					government_name: {				// compound rule
							required: true
					},
					people_view_wedding: {				// compound rule
							required: true
					},
					address: {				// compound rule
							required: true
					},
					city: {				// compound rule
							required: true
					},
					zip: {				// compound rule
							required: true,
							number:true,
							minlength:5,
							maxlength:5
					},
					state: {				// compound rule
							required: true
					},
					country: {				// compound rule
							required: true
					},
					phone: {				// compound rule
							required: true,
							number:true,
							minlength:10,
							maxlength:10
					},
					email: {				// compound rule
							required: true,
							email: true,
							remote: "checkEmail.php"  // remote
					},
					password: {				// compound rule
							required: true,
							minlength:5,
				 			maxlength:20
					},
					cpassword: {				// compound rule
							required: true,
							minlength:5,
				 			maxlength:20,
							equalTo:"#password"
					},
					fname: {				// compound rule
							required: true
					},
					mname: {				// compound rule
							required: true
					},
					lname: {				// compound rule
							required: true
					},
					code: {
					required: true
					}
           },
           messages: {
				   state_located: {
                           required: "Please enter location state"
                   },
				   over_eighteen: {
                           required: "Please enter answer"
                   },
				   when_married: {
                           required: "Please enter answer"
                   },
				   marriage_counseling: {
                           required: "Please enter answer"
                   },
				   government_name: {
                           required: "Please enter government name"
                   },
				   people_view_wedding: {
                           required: "Please enter answer"
                   },
				   address: {
                           required: "Please enter address"
                   },
				   city: {
                           required: "Please select city"
                   },
				   zip: {
                           required: "Please enter zip code",
						   number: $.format("Please enter valid number")
                   },
				   state: {
                           required: "Please select state"
                   },
				   country: {
                           required: "Please select country"
                   },
				   phone: {
                           required: "Please enter phone number",
						   minlength: $.format("Keep typing, minimum 10 digits required") 
                   },
				   email: {
                           required: "Please enter email",
						   remote: "Email already exist!"
                   },
				   password: {
                           required: "Please enter password"
                   },
				   cpassword: {
                           required: "Please confirm password",
						   equalTo:"Password and confirm password fields do not match"
                   },
				   fname: {
                           required: "Enter first name"
                   },
				   mname: {
                           required: "Enter middle name"
                   },
				   lname: {
                           required: "Enter last name"
                   },
				   code: {
				   			required: "Enter security code"
					}
           }

   });
});
</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
