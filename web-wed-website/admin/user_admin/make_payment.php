<?php
session_start();
require_once '../common/connection.php';
require_once '../common/config.php';
if(isset($_POST['user_id'])) {
  $_SESSION['user_id'] = $_POST['user_id'];
}

if (!isset($_SESSION['user_id'])) {
  header('Location: index.php');
  exit;
}

if (!isset($_POST['product'])){
  $_POST['product'] = 5;
}

$user_data = getProfile($pdoDB, $_SESSION['user_id'], 1);
// var_dump($user_data);
if (isset($_POST['submit'], $_POST['product_id'], $_POST['stripe_token'], $_POST['billing_name'], $_POST['card_number'], $_POST['card_expiration'], $_POST['card_cvc'], $_POST['discount_amount'])) {

  $product_id = intval($_POST['product_id']);

  if($_POST['discount_amount'] == 'none') {
    $amount = floatval(getProductPricing($pdoDB, intval(@$_POST['product_id']), 0) * 100); //Times by 100 for Stripe. E.g $50 = 5000 pennies.
  } else {
    $amount = floatval((getProductPricing($pdoDB, intval(@$_POST['product_id']), 0) - $_POST['discount_amount']) * 100); //Times by 100 for Stripe. E.g $50 = 5000 pennies.
  }

  if (($amount/100) < 1){
    $amount = ($amount/100);
    $prepared = $pdoDB->prepare("INSERT INTO `payments` VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)");
    $prepared->execute(array($_SESSION['user_id'], 0, $amount, 'NOTXNID', $product_id, strtotime('now'), $_POST['discount_id']));
    $success = '<div class="alert alert-success">Your payment has been successfully processed. You will be redirected in just a moment... <script>setTimeout(function(){ location.href="index.php"; }, 3000)</script></div>';
  }else{
    $stripe_token = $_POST['stripe_token'];
    $billing_name = $_POST['billing_name'];
    // $cardNumber = $_POST['card_number'];
    // $cardExp = @explode(' / ', $_POST['card_expiration']);
    // $cardExpMonth = $cardExp[0];
    // $cardExpYear = $cardExp[1];
    // $cardCVC = intval($_POST['card_cvc']);
    //
    // $myCard = array('number' => '4242424242424242', 'exp_month' => 8, 'exp_year' => 2018);

    $error = '';
    $success = '';

    try{
      $charge = \Stripe\Charge::create(
        array(
          'amount'   => $amount,
          'currency' => 'usd',
          'source'   => $stripe_token,
          'metadata' => array(
            'session_info' => json_encode(@$_SESSION),
            'product_id' => $product_id
          )
        )
      );

      $txn_id = ($charge->balance_transaction);

      $prepared = $pdoDB->prepare("INSERT INTO `payments` VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)");
      $prepared->execute(array($_SESSION['user_id'], 0, ($amount / 100), $txn_id, $product_id, strtotime('now'), $_POST['discount_id']));
      $success = '<div class="alert alert-success">Your payment has been successfully processed. We have sent a copy of your receipt to your email address. You will be redirected in just a moment... <script>setTimeout(function(){ location.href="index.php"; }, 3000)</script></div>';

    }catch(\Stripe\Error\Card $e) {
      // Since it's a decline, \Stripe\Error\Card will be caught
      $body = $e->getJsonBody();
      $err  = $body['error'];

      // print('Status is:' . $e->getHttpStatus() . "\n");
      // print('Type is:' . $err['type'] . "\n");
      // print('Code is:' . $err['code'] . "\n");
      // // param is '' in this case
      // print('Param is:' . $err['param'] . "\n");
      // print('Message is:' . $err['message'] . "\n");
      $error = '<div class="alert alert-danger">'.$err['message'].'</div>';
    } catch (\Stripe\Error\RateLimit $e) {
      // Too many requests made to the API too quickly
      $error = '<div class="alert alert-danger">Please wait a few minutes, and try again.</div>';
    } catch (\Stripe\Error\InvalidRequest $e) {
      // Invalid parameters were supplied to Stripe's API
      $error = '<div class="alert alert-danger">An error seems to have occurred, please try again.</div>';
    } catch (\Stripe\Error\Authentication $e) {
      // Authentication with Stripe's API failed
      // (maybe you changed API keys recently)
      $error = '<div class="alert alert-danger">An error seems to have occurred, please contact team@riveloper.com for assistance.</div>';
    } catch (\Stripe\Error\ApiConnection $e) {
      // Network communication with Stripe failed
      $error = '<div class="alert alert-danger">An error seems to have occurred, please try again.</div>';
    } catch (\Stripe\Error\Base $e) {
      // Display a very generic error to the user, and maybe send
      // yourself an email
      $error = '<div class="alert alert-danger">An error seems to have occurred, please try again.</div>';
    } catch (Exception $e) {
      // Something else happened, completely unrelated to Stripe
      $error = '<div class="alert alert-danger">An error seems to have occurred, please try again.</div>';
    }
  }


}
?>
<?php
$page_title = 'User Payment - ';
include '../header.php';
?>
<style>
.error {
  color: red;
  display:none;
}

.errorField {
  border: 1px solid red;
}
</style>
<div class="form_main" style="margin: 3% auto;">
  <div class="form_main_inside">
    <div class="login">
      <h1>Process payment</h1>
      <?php echo @$error; echo @$success; ?>

      <label style="width: auto;" class="control-label" for="discount">Discount Code</label>
      <input type="text" id="discount" name="discount" class="form-control" onchange="checkCode(this.value);">
      <p id="isValid"></p>

      <form name="make_payment_form" id="make_payment_form" method="post" action="make_payment.php">
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script type="text/javascript">
        Stripe.setPublishableKey('<?php echo STRIPE_PUBLIC_TOKEN; ?>');
        </script>
        <?php if(!isset($_POST['stripe_token'])) { ?>
          <p>You will be charged $<span id="displayed_amount"><?php echo getProductPricing($pdoDB, intval(@$_POST['product']), 0); ?></span> for this service. Please enter your credit card information below.</p><br/>
        <?php } ?>

        <div class="form-wrapper">
          <div class="alert-wrapper"></div>
          <div class="card-wrapper"></div>
          <input type="hidden" name="product_id" value="<?php echo intval(@$_POST['product']); ?>" />
          <input type="hidden" name="stripe_token" value="" />

          <div class="alert alert-danger" id="402error" style="display:none;">The card information provided was invalid. Please double check and try again. Contact <a href="mailto:info@WebWedMobile.com">info@webwedmobile.com</a> if you believe this is a mistake. </div>

          <div class="form-group col-xs-12 col-md-12">
            <span id="name_error" class="error">Incorrect billing name:</span>
            <label style="width: auto;" for="billing_name" class="control-label">Billing Name</label>
            <input type="text" class="form-control" required="required" data-stripe="billing_name" placeholder="Billing Name" onClick="if (!window.edited){ if(confirm('Are you sure you want to edit your billing name?')){ event.preventDefault(); $('#billing_name').val(''); window.edited = true; }else{ $('#billing_name').blur(); window.edited = false; } }" name="billing_name" id="billing_name" value="<?php echo @htmlentities(@getProfile($pdoDB, @$_SESSION['user_id'], 1)['name']); ?>"/>
          </div>

          <div class="form-group col-xs-12 col-md-12">
            <span id="number_error" class="error">Incorrect card number:</span>
            <label style="width: auto;" for="card_number" class="control-label">Card Number</label>
            <input type="text" class="form-control" size="20" max-length="20" required="required" data-stripe="number" placeholder="Card Number" name="card_number" id="card_number"/>
          </div>

          <div class="form-group col-xs-12 col-md-6">
            <label style="width: auto;" for="card_expiration" class="control-label">Expiration Date</label>
            <input type="text" size="9" max-length="9" min-length="9" required="required" class="form-control" placeholder="Expiration Date (MM/YYYY)" name="card_expiration" id="card_expiration"/>
          </div>
          <div class="form-group col-xs-12 col-md-6">
            <label style="width: auto;" for="card_cvc" class="control-label">CCV</label>
            <input type="number" size="4" max-length="4" min-length="4" required="required" data-stripe="cvc" class="form-control" placeholder="CCV" name="card_cvc" id="card_cvc" value="" placeholder=""/>
          </div>
          <input type="hidden" id="discount_amount" name="discount_amount"/>
          <input type="hidden" id="discount_id" name="discount_id"/>
          <input type="hidden" data-stripe="exp-month" id="exp-month" />
          <input type="hidden" data-stripe="exp-year" id="exp-year" />
          <input type="submit" id="submit" name="submit" value="Submit Payment" class="btn btn-primary" />

        </div>
      </form>
    </div>
  </div>
</div>

<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>


$(function() {
  $( "#minister_add_form" ).validate({
    rules: {
      name: {
        required: true
      },
      email: {
        required: true,
        email:true
      },
      username: {
        required: true
      },
      password: {
        required: true
      }
    },
    messages: {
      name: {
        required: "Please enter name."
      },
      email: {
        required: "Please enter email address.",
        email:"Please enter valid email address."
      },
      username: {
        required: "Please enter username."
      },
      password: {
        required: "Please enter password."
      }
    }

  });

  var card = new Card({
    form: 'form', // *required*
    container: '.card-wrapper', // *required*
    formSelectors: {
      numberInput: 'input#card_number', // optional — default input[name="number"]
      expiryInput: 'input#card_expiration', // optional — default input[name="expiry"]
      cvcInput: 'input#card_cvc', // optional — default input[name="cvc"]
      nameInput: 'input#billing_name' // optional - defaults input[name="name"]
    },
    placeholders: {
      number: '•••• •••• •••• ••••',
      name: '<?php echo htmlentities(getProfile($pdoDB, $_SESSION['user_id'], 1)['name']); ?>',
      expiry: '••/••',
      cvc: '•••'
    },
    width: 200, // optional — default 350px
    debug: true // optional - default false
  });
});
window.gottoken = false;
   function checkCode(code) {
     $.ajax({
					url: '../api/v1/api.php?cmd=validate_discount',
					type: 'POST',
					data: {'code': code} ,
					cache: false,
					success:function(data) {
						var data = JSON.parse(data);
						if (!data.success) { //If fails
							if (data.error) { //Returned if any error from api.php
								$('#isValid').removeClass('hidden').text(data.error); //Throw relevant error
                $('#discount_amount').val('none');
							}
						}
						else {
								$('#isValid').removeClass('hidden').text(data.success); //If successful, than throw a success message
                $('#discount_amount').val(data.amount);
                $('#discount_id').val(data.discount_id);
                var currentAmount = $('#displayed_amount').text();
                var newAmount = currentAmount - data.amount;
                $('#displayed_amount').html(newAmount+ "(<strong>Discount of $"+data.amount+" applied</strong>)");
							}
						}
      });
  };

$(document).ready(function(){


  $("#make_payment_form").submit(function(e){

    if (!Stripe.card.validateCardNumber($("#card_number").val())){
      event.preventDefault();
      alert('Please make sure to enter a valid card number and try again.');
      return;
    }

    var expMonth = $("#card_expiration").val().split(' / ')[0];
    var expYear = $("#card_expiration").val().split(' / ')[1];

    if (!Stripe.card.validateExpiry(expMonth, expYear)){
      event.preventDefault();
      alert('Please make sure to enter a valid expiration date and try again.');
      return;
    }

    if (!Stripe.card.validateCVC($("#card_cvc").val())){
      event.preventDefault();
      alert('Please make sure to enter a valid CVC code and try again.');
      return;
    }

    $("#exp-month").val(expMonth);
    $("#exp-year").val(expYear);

    if (!window.gottoken){

      // Grab the form:
      var $form = $(this);

      // Disable the submit button to prevent repeated clicks:
      $('#submit').prop('disabled', true);
      // Request a token from Stripe:
      e.preventDefault();
      Stripe.card.createToken($form, function(status, response){

        if (status == 402){
          $('#402error').show();
        }
        $('#submit').prop('disabled', false);

        if (response.error || status == 402){
          $("#" + response.error.param + "_error").show();
          $("input[data-stripe='" + response.error.param + "']").addClass('errorField');
          $("input[data-stripe='" + response.error.param + "']").click(function(){
            $("#" + response.error.param + "_error").hide();
            $("input[data-stripe='" + response.error.param + "']").removeClass('errorField');
          });
        }else{
          $("input[name='stripe_token']").val(response.id);
          window.gottoken = true;
          $('#submit').click();
          return true;
        }
      });

      //Prevent the form from being submitted:
      event.preventDefault();
    }
  });
});

</script><!-- This function refreshes the security or captcha code when clicked on the refresh link -->
<?php include '../footer.php'; ?>
