<style>
.btn-success {
	width:100%;
}
</style>
<div class="container page-content">
	<div class="row">
		<h1>Sign Up</h1>
		<h2><?php echo @$productName; ?> package for $<?php echo @$productPrice; ?></h2>
			<form id="signUpForm" method="get" action="">

				<div id="rootwizard">
					<ul style="opacity: 0;">
						<li><a href="#tab1" data-toggle="tab">2</a></li>
						<li><a href="#tab2" data-toggle="tab">3</a></li>
						<li><a href="#tab3" data-toggle="tab">4</a></li>
						<li><a href="#tab4" data-toggle="tab">5</a></li>
					</ul>

					<script>
						function checkEmail(){
							var email = $('#email1').val();
							if (email == ''){
								return true;
							}

							$.ajax({
								url: '<?php echo SITEURL; ?>api/v1/api.php?cmd=checkemail&email='+email
							}).done(function(e){
								console.log('Data: ' + e);
								var json = JSON.parse(e);
								if (json.error != undefined){
									window.validator.showErrors({ 'email1': 'This email is already registered. Please follow this link to login: <a href="http://webwedmobile.com/admin/user_admin/login.php">http://webwedmobile.com/admin/user_admin/login.php</a>' });
									return false;
								}
								return true;
							});
						}
					</script>

					<div class="tab-content">
						<input id="p1" type="hidden" name="product_type" value="product<?php echo intval($productID); ?>"/>
						<div class="tab-pane" id="tab1">
							<h2>Personal Information</h2>
						<style>
							.salutation {
								border-radius: 0px !important;
								-webkit-appearance: none;
								margin: 0;
								padding: 0px !important;
								width: 65px !important;
								text-align: center !important;
								border-top-left-radius: 4px !important;
								border-bottom-left-radius: 4px !important;
								border-right-width: 0 !important;
								padding-left: 10px !important;
							}
						</style>
							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="email1">What is your email?</label>
								<input type="text" id="email1" name="email1" onblur="checkEmail();" class="form-control email">
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="name1">What is your name?</label>
								<div class="input-group">
									<div class="input-group-btn">
										<select name="gender1" id="gender1" class="salutation form-control">
											<option value="0">Mr. &#9660;</option>
											<option value="1">Mrs. &#9660;</option>
										</select>
										</div>
										<input type="text" id="name1" name="name1" class="form-control salutation_input">

								</div>
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="email2">What is your fiance's email?</label>
								<input type="text" id="email2" name="email2" class="form-control email">
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="name2">What is your fiance's name?</label>
								<div class="input-group">
									<div class="input-group-btn">
										<select name="gender2" id="gender2" class="salutation form-control">
											<option value="0">Mr. &#9660;</option>
											<option value="1">Mrs. &#9660;</option>
										</select>
										</div>
										<input type="text" id="name2" name="name2" class="form-control salutation_input">

								</div>
							</div>

							<div class="form-group col-xs-4 col-md-4">
								<label for="birthdate_month1" class="control-label">When is your birthday?</label>
								<select class="form-control" id="birthdate_month1" name="birthdate_month1" required="required">
									<option>Month</option>
									<option value="1">January</option>
									<option value="2">Febuary</option>
									<option value="3">March</option>
									<option value="4">April</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">August</option>
									<option value="9">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</div>
							<div class="form-group col-xs-4 col-md-4">
								<label for="birthdate_day1" class="control-label"><span style="opacity:0;">a</span></label>
								<!-- <input type="number" placeholder="Day" max-length="2" min-length="1" class="form-control" id="birthdate_day1" name="birthdate_day1" /> -->
								<select class="form-control" id="birthdate_day1" name="birthdate_day1" required="required">
									<option>Day</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>

								</select>
							</div>
							<div class="form-group col-xs-4 col-md-4">
								<label for="birthdate_year1" class="control-label"><span style="opacity:0;">a</span></label>
								<!-- <input type="number" placeholder="Year" max-length="4" min-length="4" class="form-control" id="birthdate_year1" name="birthdate_year1" /> -->
								<select name="birthdate_year1" class="form-control" required="required">
									<option>Year</option>
									<?php for ($i=1998; $i > 1933; $i--) {
										echo '<option value="'.$i.'">'.$i.'</option>';
									} ?>
								</select>
							</div>

							<div class="form-group col-xs-12 col-md-6">
								<label class="control-label" for="password1">Enter your WebWed password</label>
								<input type="password" name="password1" id="password1" class="form-control">
							</div>

							<div class="form-group col-xs-12 col-md-6">
								<label class="control-label" for="confirmpassword1">Confirm your WebWed password</label>
								<input type="password" name="confirmpassword1" id="confirmpassword1" class="form-control">
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="street1">What is your  address?</label>
								<input type="text" id="street1" name="street1" class="form-control">
							</div>

							<div class="form-group col-xs-12 col-md-4">
								<label class="control-label" for="city1">What is your  city?</label>
								<input type="text" id="city1" name="city1" class="form-control">
							</div>
							<div class="form-group col-xs-12 col-md-4">
								<label class="control-label" for="state1" class="form-control">What is your  state?</label>
								<select class="form-control" name="state1" id="state1" class="form-control">
									<?php
									foreach($us_state_abbrevs_names as $state) { ?>
										<option value="<?php echo $state; ?>" ><?= $state ?></option>
										<?php
									}
									?>
								</select>
							</div>

							<div class="form-group col-xs-12 col-md-4">
								<label class="control-label" for="zip1">What is your  zip code?</label>
								<input type="text" id="zip1" name="zip1" class="form-control">
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="phone1">What is your phone number?</label>
								<input type="text" id="phone1" name="phone1" class="form-control">
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="timezone1" class="form-control">What is your timezone?</label>
								<select class="form-control" name="timezone1" id="timezone" class="form-control">
									<?php
									foreach($tzlist as $name) { ?>
										<option value="<?php echo $name; ?>" <?php if($name == 'America/New_York') { echo 'selected="selected"'; } ?> ><?= $name ?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="tab-pane" id="tab2">
							<h2>Configure Wedding</h2>
							<!-- <div class="form-group col-xs-12 col-md-6">
								<label class="control-label" for="event_name">Event Name</label>
								<input type="text" id="event_name" name="event_name" class="form-control">
							</div> -->

							<!-- <div class="form-group col-xs-4 col-md-4">
								<label for="event_month1" class="control-label">Event Month</label>
								<select class="form-control" id="event_month1" name="event_month1">
									<option value="1">January</option>
									<option value="2">Febuary</option>
									<option value="3">March</option>
									<option value="4">April</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">August</option>
									<option value="9">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</div>
							<div class="form-group col-xs-4 col-md-4">
								<label for="event_day1" class="control-label">Event Day</label>
								<input type="number" placeholder="01" max-length="2" min-length="1" class="form-control" id="event_day1" name="event_day1" />
							</div>
							<div class="form-group col-xs-4 col-md-4">
								<label for="event_year1" class="control-label">Event Year</label>
								<input type="number" placeholder="1980" max-length="4" min-length="4" class="form-control" id="event_year1" name="event_year1" />
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="event_time">Event Time</label>
								<input type="time" name="event_time" id="event_time" placeholder="Event Time" class="form-control">
							</div> -->
							<div class="form-group col-xs-12 col-md-12" id="event_date">
								<label for="event_date1" class="control-label">What day is your wedding?</label>
								<input type="text" class="form-control" id="event_date1" name="event_date1" readonly="readonly" style="background-color: #fff;"/>
							</div>

							<div class="form-group col-xs-12 col-md-12" id="event_time">
								<label class="control-label" for="event_time">What time is your wedding?</label>
								<!-- <input type="time" name="event_time" id="event_time" placeholder="Event Time" class="form-control"> -->
								<select name="event_time" id="event_time" class="form-control">
								<option selected="selected">Pick an Event Time</option>
								<option value="12:00 AM">12:00 AM</option>
								<option value="12:30 AM">12:30 AM</option>
								<option value="1:00 AM">1:00 AM</option>
								<option value="1:30 AM">1:30 AM</option>
								<option value="2:00 AM">2:00 AM</option>
								<option value="2:30 AM">2:30 AM</option>
								<option value="3:00 AM">3:00 AM</option>
								<option value="3:30 AM">3:30 AM</option>
								<option value="4:00 AM">4:00 AM</option>
								<option value="4:30 AM">4:30 AM</option>
								<option value="5:00 AM">5:00 AM</option>
								<option value="5:30 AM">5:30 AM</option>
								<option value="6:00 AM">6:00 AM</option>
								<option value="6:30 AM">6:30 AM</option>
								<option value="7:00 AM">7:00 AM</option>
								<option value="7:30 AM">7:30 AM</option>
								<option value="8:00 AM">8:00 AM</option>
								<option value="8:30 AM">8:30 AM</option>
								<option value="9:00 AM">9:00 AM</option>
								<option value="9:30 AM">9:30 AM</option>
								<option value="10:00 AM">10:00 AM</option>
								<option value="10:30 AM">10:30 AM</option>
								<option value="11:00 AM">11:00 AM</option>
								<option value="11:30 AM">11:30 AM</option>
								<option value="12:00 PM">12:00 PM</option>
								<option value="12:30 PM">12:30 PM</option>
								<option value="1:00 PM">1:00 PM</option>
								<option value="1:30 PM">1:30 PM</option>
								<option value="2:00 PM">2:00 PM</option>
								<option value="2:30 PM">2:30 PM</option>
								<option value="3:00 PM">3:00 PM</option>
								<option value="3:30 PM">3:30 PM</option>
								<option value="4:00 PM">4:00 PM</option>
								<option value="4:30 PM">4:30 PM</option>
								<option value="5:00 PM">5:00 PM</option>
								<option value="5:30 PM">5:30 PM</option>
								<option value="6:00 PM">6:00 PM</option>
								<option value="6:30 PM">6:30 PM</option>
								<option value="7:00 PM">7:00 PM</option>
								<option value="7:30 PM">7:30 PM</option>
								<option value="8:00 PM">8:00 PM</option>
								<option value="8:30 PM">8:30 PM</option>
								<option value="9:00 PM">9:00 PM</option>
								<option value="9:30 PM">9:30 PM</option>
								<option value="10:00 PM">10:00 PM</option>
								<option value="10:30 PM">10:30 PM</option>
								<option value="11:00 PM">11:00 PM</option>
								<option value="11:30 PM">11:30 PM</option>
								</select>
							</div>

							<div class="form-group col-xs-12 col-md-12">
								<label for="religious_affiliation" class="control-label">What is your religious affliation?</label>
								<select name="religious_affiliation" class="form-control">
									<option value="other" selected="seleted">Other – N/A</option>
									<option value="christianity">Christian</option>
									<option value="catholic">Catholic</option>
									<option value="jewish">Jewish</option>
									<option value="hindu">Hindu</option>
									<option value="buddhist">Buddhist</option>
									<option value="religious">Religious</option>
									<option value="nonreligious">Non–Religious</option>
									<option value="interfaith">Interfaith</option>
								</select>
							</div>
							<div class="form-group col-xs-12 col-md-12">
								<label for="event_brief" class="control-label">Enter a description for your wedding</label>
								<textarea class="form-control" id="event_brief" name="event_brief" required></textarea>
							</div>
							<div class="form-group col-xs-12 col-md-6">
								<label class="control-label" for="event_public">Is your wedding public?</label>
								<select class="form-control" name="event_public" id="event_public" class="form-control">
									<option value="yes">Yes</option>
									<option value="no" selected="selected">No</option>
								</select>
							</div>

							<div class="form-group col-xs-12 col-md-6">
								<label class="control-label" for="chat">Are guests allowed to chat?</label>
								<select class="form-control" name="chat" id="chat" class="form-control">
									<option value="yes">Yes</option>
									<option value="no" selected="selected">No</option>
								</select>
							</div>

							<div class="form-group col-xs-12 col-md-6">
								<label class="control-label" for="gifts">Are you accepting gifts?</label>
								<select class="form-control" name="gifts" id="gifts" class="form-control">
									<option value="yes">Yes</option>
									<option value="no" selected="selected">No</option>
								</select>
							</div>

							<div class="form-group col-xs-12 col-md-6">
								<label class="control-label" for="gift_url">What is your registry url?</label>
								<input type="text" id="gift_url" name="gift_url" class="form-control" disabled>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="tab-pane" id="tab3">
							<h2>Configure Wedding Party</h2>
							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="witness1_email">What is your first witnesses email?</label>
								<input type="text" id="witness1_email" name="witness1_email" class="form-control email">
							</div>
							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="witness2_email">What is your second witnesses email?</label>
								<input type="text" id="witness2_email" name="witness2_email" class="form-control email">
							</div>
							<div class="form-group col-xs-12 col-md-12">
								<label class="control-label" for="topofficiants">Select 5 officiants of your choice:</label><br>
								<script>
								function loadOfficiants(){
									if ($('input[name="topofficiants[]"]:checked').length > 0){ return; }

									var name1_prefix = $('#name1_prefix').val();
									var name2_prefix = $('#name2_prefix').val();
									var samesex = 0;

									if (name1_prefix == name2_prefix){
										samesex = 1;
									}

									$.ajax({
										url: 'ajax_officiants.php?samesex=' + samesex
									}).done(function(data){
										data = JSON.parse(data).data;
										$('.officiant_gallery').children().remove();
										for (var i = 0; i < data.length; i++) {
											var officiant = data[i];
											var id = officiant.minister_id;
											var name = officiant.name;
											var bio = officiant.bio;
											var religious_affiliation = officiant.religious_affiliation;
											var gender = officiant.gender;
											var city = officiant.city;
											var state = officiant.state;
											var photo = officiant.photo;

											if (photo == ''){
												photo = 'http://webwedmobile.com/images/default_avatar_male.jpg';
											}else{
												photo = 'http://webwedmobile.com/upload/' + photo;
											}

											$('.officiant_gallery').append('<div class="col-md-4 officiant_gallery_child" onClick="$(\'#minister'+id+'\').click();"><a href="#"><img class="img-responsive" src="'+photo+'" alt="" style="width:100%;"></a><h3><a href="#">'+name+'</a></h3><p>'+religious_affiliation+'</p><input type="checkbox" name="topofficiants[]" id="minister'+id+'" value="'+id+'" /></div>');
										}
									});
								}
								</script>
								<div class="officiant_gallery">

								</div>
							</div>
						</div>

						<div class="tab-pane" id="tab4">
							<h2>Terms and Conditions</h2>
							<iframe width="100%" height="300px" src="http://webwedmobile.com/Legal.html"></iframe><br/>
							<input type="checkbox" name="agree_to_tos" required="required"/> By checking this box and creating a WebWedMobile account you are agreeing to our terms and conditions.<br/>
							<div class="alert alert-danger" id="signuperror" style="display:none;">You already have an existing account. Please login by following this link: <a href="http://webwedmobile.com/admin/user_admin/login.php">http://webwedmobile.com/admin/user_admin/login.php</a></div>
							<input type="submit" value="Create Account and Continue to Payment" class="btn btn-success">
						</div>
						<!-- NAVIGATION -->
						<ul class="pager wizard">
							<li class="previous"><a href="#">Previous</a></li>
							<li class="next"><a href="#" onClick="loadOfficiants()">Next</a></li>
						</ul>


					</div>
				</div>
			</form>
			<p id="success"></p>
			<p class="throw_error"></p>
			</div>
		</div>
			<script>
			$( document ).ready(function(){

				$('#birthdate1').datepicker({
					'format': 'm/d/yyyy',
					'autoclose': true
				});

				$('#event_date1').datepicker({
					'format': 'm/d/yyyy',
					'autoclose': true
				});

				$("#phone1").mask("(999) 999-9999");
				$("#zip1").mask("99999");

				$( "#gifts" ).change(function() {
					var isGift = $(this).val();
					if(isGift == "yes") {
						$("#gift_url").removeAttr('disabled');
					} else {
						$("#gift_url").prop('disabled', 'true');
					}
				});


				$('#signUpForm .product_type').on('change', function() {
					$('.next').css('display', 'block');
					var selectedProduct = $('input[name=product_type]:checked', '#signUpForm').val();

					if (selectedProduct == 'product1') {
						$('#rootwizard').bootstrapWizard('disable', '3');
					} else if (selectedProduct == 'product2' || selectedProduct == 'product3'){
						$('#rootwizard').bootstrapWizard('enable', '3');
					}
				});

				$.validator.addMethod("validDate", function(value, element) {
			        return this.optional(element) || moment(value,"DD/MM/YYYY").isValid();
			    }, "Please enter a valid date in the format DD/MM/YYYY");

				$.validator.addMethod("validBirthday", function(value, element) {
						var month = $('#birthdate_month1').val();
						var day = $('#birthdate_day1').val();
						var year = $('#birthdate_year1').val();
						var age = 18;

						year = Number.parseInt(year);

						var cutOffDate = new Date(year + age, month, day);
						cutOffDate = ((cutOffDate).setFullYear(year+age, month, day));
						if (cutOffDate > Date.now()) {
							return false;
						} else {
							return true;
						}
			    }, "You must be 18 years or older to register.");

				$.validator.addMethod('notBlank', function(value, element){
					if (value == ''){
						return false;
					}else{
						return true;
					}
				}, "You must select an option above to continue.");

				$.validator.addMethod("validEventDate", function(value, element) {
						var month = $('#event_month1').val();
						var day = $('#event_day1').val();
						var year = $('#event_year1').val();

						var cutOffDate = new Date(year, month, day);
						cutOffDate = ((cutOffDate).setFullYear(year, month, day));

						if (cutOffDate > Date.now()) {
							return true;
						} else {
							return false;
						}
			    }, "Your event must be scheduled for after now!");

					$.validator.addMethod("time", function(value, element) {
						return true;
						return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);
					}, "Please enter a valid time. (12:00 PM format)");
				var $validator = $("#signUpForm").validate({
					rules: {
						name1: {
							required: true,
							minlength: 3
						},
						email1: {
							required: true,
							email: true,
							minlength: 3
						},
						password1: {
							required: true,
							minlength: 6
						},
						confirmpassword1 : {
							required: true,
							minlength : 6,
							equalTo : "#password1"
						},
						gender1: {
							required: true
						},
						event_day1: {
							required: true,
							number: true
						},
						event_month1: {
							required: true,
							number: true
						},
						event_year1: {
							required: true,
							validEventDate: true,
							number: true
						},
						birthdate_day1: {
							required: true
						},
						birthdate_month1: {
							required: true
						},
						birthdate_year1: {
							required: true
						},
						event_time: {
							time: true,
							required: true
						},
						street1: {
							required: true
						},
						city1: {
							required: true,
							minlength: 3
						},
						state1: {
							required: true
						},
						zip1: {
							required: true,
							minlength: 4
						},
						phone1: {
							required: true,
							minlength: 10
						},
						timezone1: {
							required: true
						},
						product_type: {
							required: true
						},
						event_name: {
							required: true
						},
						event_date: {
							required: true
						},
						event_time: {
							required: true
						},
						event_public: {
							required: true
						},
						chat: {
							required: true
						},
						gifts: {
							required: true
						},
						topofficiants: {
							required: true
						},
						gift_url: {
							required: function(element) {
								return $("#gifts").val()=='yes'
							},
							url: true
						},
						yours_email: {
							required: function(element) {
								if($("input[name=product_type]:checked").val()=='product2' || $("input[name=product_type]:checked").val()=='product3') {
									return true;
								} else {
									return false;
								}
							}
						},
						witness1_email: {
							required: function(element) {
								if($("input[name=product_type]:checked").val()=='product2' || $("input[name=product_type]:checked").val()=='product3') {
									return true;
								} else {
									return false;
								}
							}
						},
						witness2_email: {
							required: function(element) {
								if($("input[name=product_type]:checked").val()=='product2' || $("input[name=product_type]:checked").val()=='product3') {
									return true;
								} else {
									return false;
								}
							}
						}
					},
					messages: {
						'topofficiants[]': {
							required: "You must check at least 5 box",
							maxlength: "Check no more than {5} boxes",
							minlength: "Please select 5 officiants"
						},
						'birthdate_year1': {
							required: 'Please select one of the options above to continue.',
							minlength: 'Please select one of the options above to continue.'
						}
					},
					errorPlacement: function(error, element) {
						if (element.attr("name") == "product_type" )
						error.insertAfter(".some-class");
						else
						error.insertAfter(element);
					},
					submitHandler: function(form) {

						var formData = $(form).serialize();
						console.log(formData);
						$.ajax({
							url: '../api/v1/api.php?cmd=sign_up',
							type: 'POST',
							data:formData,
							cache: false,
							success:function(data) {
								var data = JSON.parse(data);
								if (!data.success) { //If fails
									if (data.error) { //Returned if any error from api.php
										$('.throw_error').removeClass('hidden').fadeIn(1000).html(data.error); //Throw relevant error
										$('#success').addClass('hidden'); //Throw relevant error
									}
								}
								else {
									$('#success').removeClass('hidden').fadeIn(1000).text(data.success); //If successful, than throw a success message
									$('.throw_error').addClass('hidden'); //Throw relevant error
									var form = $('<form action="make_payment.php" method="post">' +
									'<input type="text" name="user_id" value="' + data.user_id+ '" />' +
									'<input type="text" name="product" value="' + data.product+ '" />' +
									'</form>');
									$('body').append(form);
									form.submit();
								}
							}
						});
						return false;

					}

				});

				window.validator = $validator;

				window.index_ = 1;
				$('#rootwizard').bootstrapWizard({
					'tabClass': 'nav nav-pills',
					'onNext': function(tab, navigation, index) {
						// if(index == '1') {
						// 	if(!$('input[name=product_type]').is(':checked')) {
						// 		$('.next').css('display', 'none');
						// 	}
						// }
						var $valid = $("#signUpForm").valid();
						if(!$valid) {
							$validator.focusInvalid();
							return false;
						}
						// if (window.index_ == 1){
						// 	$('#tab1').removeClass('active');
						// 	$('#tab2').addClass('active');
						// }
						// if (window.index_ == 2){
						// 	$('#tab2').removeClass('active');
						// 	$('#tab3').addClass('active');
						// }
						// if (window.index_ == 3){
						// 	$('#tab3').removeClass('active');
						// 	$('#tab4').addClass('active');
						// }
						// window.index_ += 1;

						if (index == 3 || index == '3'){
							$('.next').css('display', 'none');
						}
					},
					'onPrevious': function(tab, navigation, index) {
						// if(index != '1') {
						// 	$('.next').css('display', 'block');
						// }
					}
				});
			});

			</script>
