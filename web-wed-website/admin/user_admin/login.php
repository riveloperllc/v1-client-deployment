<?php

ini_set('display_errors',1);
ob_start();
session_start();

if(isset($_SESSION['wwm_user_id'])){
	header("location:user_dashboard.php");
	exit;
}

require_once('../common/connection.php');
require_once('../common/config.php');

$msg = '';
if(isset($_POST['email'], $_POST['password']))
{
	$email = $_POST['email'];
	$password = $_POST['password'];
	$password = md5($password);
	$prepared = $pdoDB->prepare("SELECT * FROM www_users_new WHERE email=? AND password=? LIMIT 1");
	$prepared->execute(array($email, $password));
	if ($prepared->rowCount() > 0){
		$row = $prepared->fetch(PDO::FETCH_ASSOC);
		$_SESSION['wwm_user_id'] = $row['id'];
		header("Location: login_success.php");
		exit;
	}else{
		$msg = "<div class='alert alert-danger'>Your username or password is incorrect. Please try again.</div>";
	}
}
?>
<?php $page_name=basename($_SERVER['PHP_SELF']);?>
<?php
$page_title = "Login - ";
include('../header.php');

$email = @$_POST['email'];
$password = @$_POST['password'];
?>

<div class="form_main">
	<div class="login wrapper">
		<h1>User Login</h1>
		<?php echo @$msg; ?>
		<form class="form-horizontal" role="form" name="login" id="login" method="post" action="login.php">
			<input type="text" placeholder="Email" class="input_text" id="email" name="email" value="<?php echo $email; ?>"/><br/><br/>
			<input type="password" placeholder="Password" class="input_text" id="password" name="password" value="<?php echo $password; ?>"/><br/><br/>
			<input  type="submit" name="submit" value="Submit" class="btn btn-primary" /><br/><br/>
			<a href="forgot_password.php" style="color:;">Forgot Password</a><br/><br/>
			<a href="signup.php" style="color:;">Sign up</a><br/><br/>
			<a href="guest_login.php" style="color:;">Attend an event</a><br/><br/>
			<a href="../officiant_admin/login.php" style="color:;">Login as Officiant</a><br/><br/>

		</form>
	</div>
</div>
<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script>


$(function() {


	$( "#login" ).validate({
		rules: {
			email: {
				required: true,
				email: true
			},
			password: {				// compound rule
				required: true
			}
		},
		messages: {
			email: {
				required: "Please enter the email",
				email: "Please enter a valid email address."
			},
			password: {
				required: "Please enter the password"
			}
		}
	});
});
</script>
<?php include('../footer.php'); ?>
