<?php require_once('../header.php'); ?>
<?php if (!isset($_GET['app'])){ ?>
<br/>
<br/>
<br/>
<br/>
<br/>
<?php } ?>
<div class="prices">
  <div class="container">
    <div class="row">

      <div class="price-box col-md-3 col-sm-12 col-xs-12 wow flipInY" data-wow-delay="0.5s" style="visibility: visible; animation: flipInY 0.5s; -webkit-animation: flipInY 0.5s; margin-left: 0;">
        <div class="panel panel-default">
          <div class="panel-heading text-center"> <i class="fa fa-heart fa-2x"></i>
            <h3><?php echo getProductName($pdoDB, 5); ?></h3>
            <p>30 Min Single Feed</p>
          </div>
          <div class="panel-body text-center">
            <p class="lead"><strong>$<?php echo getProductPricing($pdoDB, 5); ?></strong></p>
          </div>
          <ul class="list-group text-center">
            <li class="list-group-item"><em class="text-danger"></em> Use Anytime</li>
            <li class="list-group-item"><em class="text-danger"></em> Sharing</li>
            <li class="list-group-item"><em class="text-danger"></em> Gift Linking</li>
          </ul>
          <div class="panel-footer text-center"> <a class="btn btn-lg btn-dark" href="<?php echo SITEURL; ?>user_admin/signup_wizard.php?id=5<?php if (isset($_GET['app'])){ ?>&app <?php } ?>">Select</a> </div>
        </div>
      </div>

      <div class="price-box price-box-featured col-md-3 col-xs-12 col-sm-12 wow flipInY" data-wow-delay="0.7s" style="visibility: visible; animation: flipInY 0.7s; -webkit-animation: flipInY 0.7s; margin-left: 0;">
        <div class="panel panel-default">
          <div class="panel-heading text-center"> <i class="fa fa-heart fa-2x"></i>
            <h3 class="white"><?php echo getProductName($pdoDB, 3); ?></h3>
            <p>45 Min 5 Feed Promoted</p>
          </div>
          <div class="panel-body text-center">
            <p class="lead"><strong>$<?php echo getProductPricing($pdoDB, 3); ?></strong></p>
          </div>
          <ul class="list-group text-center">
            <li class="list-group-item"><em class="text-danger"></em> Extended Video Stream</li>
            <li class="list-group-item"><em class="text-danger"></em> Ad Sponsored Ceremony</li>
            <li class="list-group-item"><em class="text-danger"></em> Chat, Registry, Sharing</li>
          </ul>
          <div class="panel-footer text-center"> <a class="btn btn-lg btn-main" href="<?php echo SITEURL; ?>user_admin/signup_wizard.php?id=3<?php if (isset($_GET['app'])){ ?>&app <?php } ?>">Select</a> </div>
          <div class="price-box-ribbon"><font color="#3a3a3a" face="Lato">BEST VALUE</font><br>
          </div>
        </div>
      </div>

      <div class="price-box  col-md-3 col-xs-12 col-sm-12  wow flipInY" data-wow-delay="0.5s" style="visibility: visible; animation: flipInY 0.9s; margin-left: 0; -webkit-animation: flipInY 0.9s; margin-left: 0;">
        <div class="panel panel-default">
          <div class="panel-heading text-center"> <i class="fa fa-heart fa-2x"></i>
            <h3><?php echo getProductName($pdoDB, 2) ?></h3>
            <p>30 Min 5 Feed</p>
          </div>
          <div class="panel-body text-center">
            <p class="lead"><strong>$<?php echo getProductPricing($pdoDB, 2); ?></strong></p>
          </div>
          <ul class="list-group text-center">
            <li class="list-group-item"><em class="text-danger"></em> Remote Proxy Ceremony</li>
            <li class="list-group-item"><em class="text-danger"></em> Guest Invites</li>
            <li class="list-group-item"><em class="text-danger"></em> Chat, Sharing, Gift Linking</li>
          </ul>
          <div class="panel-footer text-center"> <a class="btn btn-lg btn-dark" href="<?php echo SITEURL; ?>user_admin/signup_wizard.php?id=2<?php if (isset($_GET['app'])){ ?>&app <?php } ?>">Select</a> </div>

        </div>
      </div>

      <div class="price-box price-box-featured col-md-3 col-xs-12 col-sm-12  wow flipInY" data-wow-delay="0.7s" style="visibility: visible; animation: flipInY 0.9s; margin-left: 0; -webkit-animation: flipInY 0.9s; margin-left: 0;">
        <div class="panel panel-default">
          <div class="panel-heading text-center"><i class="fa fa-heart fa-2x"></i>
            <h3><?php echo getProductName($pdoDB, 1); ?></h3>
            <p>30 Min Single Broadcast</p>
          </div>
          <div class="panel-body text-center">
            <p class="lead"><strong>$<?php echo getProductPricing($pdoDB, 1); ?></strong></p>
          </div>
          <ul class="list-group text-center">
            <li class="list-group-item"><em class="text-danger"></em> Video Sharing</li>
            <li class="list-group-item"><em class="text-danger"></em> Live Chat</li>
            <li class="list-group-item"><em class="text-danger"></em> Registry Linking</li>
          </ul>
          <div class="panel-footer text-center"> <a class="btn btn-lg btn-dark" href="<?php echo SITEURL; ?>user_admin/signup_wizard.php?id=1<?php if (isset($_GET['app'])){ ?>&app <?php } ?>">Select</a> </div>
        </div>
      </div>

    </div>
    <!-- /.row -->
    <?php if (!isset($_GET['app'])){ ?>
    <h2 align="center">Select a WebWed service to register. Already registered? <a href="login.php" target="_self">Login Here.</a></h2>
    <h2 align="center">Are you an officiant? <a href="<?php echo SITEURL; ?>officiant_admin/signup.php" target="_self">Register Here</a> or <a href="<?php echo SITEURL; ?>officiant_admin/login.php" target="_self">Login Here</a></h2>
    <?php } ?>
  </div>
  <!-- /.container -->
</div>
<?php require_once('../footer.php'); ?>
