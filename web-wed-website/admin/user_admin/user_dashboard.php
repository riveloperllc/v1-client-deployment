<?php
ob_start();
session_start();

if(!isset($_SESSION['wwm_user_id'])){
	header("location:login.php");
}
?>
<?php
require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');
require_once('../common/utility_functions.php');
$mostRecentEvent = getMostRecentEventFromUid($_SESSION['wwm_user_id']);

?>
<!-- Middle Start -->
<?php
$succ = @$_GET['succ'];

function findUserById($db, $id){
	$prepared = $db->prepare("SELECT * FROM www_users_new WHERE id = ? LIMIT 1");
	$prepared->execute(array($id));
	if ($prepared->rowCount() > 0){
		$row = $prepared->fetch(PDO::FETCH_ASSOC);
		return $row;
	}else{
		return array('name' => 'Pending Acceptence', 'state' => 'Pending Acceptence', 'city' => 'Pending Acceptence', 'state' => 'Pending Acceptence', 'zip' => 'Pending Acceptence');
	}
}

$user = findUserById($pdoDB, $_SESSION['wwm_user_id']);

function findPaymentByUid($db, $id){
	$prepared = $db->prepare("SELECT * FROM payments WHERE uid = ? ORDER BY id DESC LIMIT 1");
	$prepared->execute(array($id));
	if ($prepared->rowCount() > 0){
		$row = $prepared->fetch(PDO::FETCH_ASSOC);
		return $row;
	}else{
		return array();
	}
}

$payment = findPaymentByUid($pdoDB, $_SESSION['wwm_user_id']);
// if (empty($payment)){
// 	header("Location: make_payment.php");
// 	exit;
// }

// $row = findUserById($pdoDB, $_SESSION['wwm_user_id']);
// $prepared = $pdoDB->prepare("SELECT * FROM `weddings` WHERE `uid` = ? OR `yours_uid` = ? ORDER BY `id` DESC LIMIT 1");
// $prepared->execute(array($_SESSION['wwm_user_id'], $_SESSION['wwm_user_id']));
//
// if ($prepared->rowCount() > 0){
// 	$wedding = $prepared->fetch(PDO::FETCH_ASSOC);
// 	$yours = findUserById($pdoDB, $wedding['yours_uid']);
// 	$witnessOne = findUserById($pdoDB, $wedding['first_witness_uid']);
// 	$witnessTwo = findUserById($pdoDB, $wedding['second_witness_uid']);
// 	$payment = findPaymentByUid($pdoDB, $_SESSION['wwm_user_id']);
// }else{
// 	$wedding = array();
// 	$yours = array();
// 	$witnessOne = array();
// 	$witnessTwo = array();
// 	$payment = array();
// }

?>
<?php  $page_name=basename($_SERVER['PHP_SELF']);?>
<?php include('../header.php'); ?>
<style>
 h3 {
	color: #023A7B;
 }
</style>
<div class="container">
	<div class="form_main_inside">
		<div class="">
			<?php
			if(isset($_SESSION['message']) && $_SESSION['message']!='')
			{
				if($_SESSION['flag'] == 1)
				{
					echo "<div class='alert-box success1'>".$_SESSION['message']."</div>";
				}
				if($_SESSION['flag'] == 2)
				{
					echo "<div class='alert-box error1'><span>error: </span>".$_SESSION['message']."</div>";
				}
				unset($_SESSION['flag']);
				unset($_SESSION['message']);
			}
			?>


			<!-- <a href="edit_information.php" class="btn">Edit Information</a>
			<a href="schedule.php" class="btn">Schedule</a>
			<a href="invitation.php"  class="btn">Invitation</a>-->

			<?php if($succ!='') { echo "<span class='msg_success'>Your profile has been updated successfully.</span>"; } ?>
			<br/>
			<br/>
			<br/>
			<div class="row">
				<div class="col-md-12">

					<h1><span>Hello <?php echo $user['name']; ?> </span></h1>

					<a href="update_event.php" class="btn btn-primary">Update Your Event Details And Settings</a>
					<div class="clear"></div>
					<br>
					<h2>Event Information</h2>


					<div>
						<br/><p class="user_info">Your event is scheduled for the following date and time:</p>
						<input type="text" readonly="readonly" class="form-control" value="<?php echo @$mostRecentEvent['data']['event_date'] . ' ' . @$mostRecentEvent['data']['event_time']; ?>" />
					</div>

					<div>
						<br/><p class="user_info">Are you going to allow the public to join?</p>
						<input type="text" readonly="readonly" class="form-control" value="<?php if (@$mostRecentEvent['data']['public']==1){ echo 'Public is allowed.'; }else{ echo 'Only people I invite are allowed.'; } ?>" />
					</div>

					<div>
						<br/><p class="user_info">Are you going to allow the guests to chat?</p>
						<input type="text" readonly="readonly" class="form-control" value="<?php if (@$mostRecentEvent['data']['chat']==1){ echo 'Chat is allowed.'; }else{ echo 'I do not want guests to chat'; } ?>" />
					</div>

					<div>
						<br/><p class="user_info">Are you accepting gift donations?</p>
						<input type="text" readonly="readonly" class="form-control" value="<?php if (@$mostRecentEvent['data']['gifts']==1){ echo 'Gifts are being accepted using my registry link provided.'; }else{ echo 'I do not want to accept gifts at this time.'; } ?>" />
					</div>

					<?php if ($mostRecentEvent['type'] == 'wedding'){ ?>
					<div>
						<br/><p class="user_info">Have you completed marriage counseling?</p>
						<input type="text" readonly="readonly" class="form-control" value="<?php if (empty($mostRecentEvent['data']['marriage_counseling'])){ echo 'You have not completed any type of marriage counseling.'; }else{ echo 'You have completed some type of marriage counseling.'; } ?>" />
					</div>
					<?php } ?>

					<br/><p class="user_info">&nbsp;</p>
					<br/><p class="user_info">&nbsp;</p>
				</div>
			</div>
			<div class="row">
				<div <?php if (@$mostRecentEvent['type'] == 'wedding'){ ?>class="col-md-6"<?php }else{ ?>class="col-md-12"<?php } ?>>
					<h2>Your Information</h2>

					<br/><p class="user_info">Full Name</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['name']; ?>" />

					<br/><p class="user_info">Phone Number</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['phone']; ?>" />

					<br/><p class="user_info">Email Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['email']; ?>" />

					<br/><p class="user_info">Street Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['street']; ?>" />

					<br/><p class="user_info">City</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['city']; ?>" />

					<br/><p class="user_info">State</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['state']; ?>" />

					<br/><p class="user_info">Zip Code</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['zip']; ?>" />

					<br/><p class="user_info">Birthdate</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$user['birthdate']; ?>" />


					<br/><p class="user_info">&nbsp;</p>
					<br/><p class="user_info">&nbsp;</p>

				</div>
				<?php if ($mostRecentEvent['type'] == 'wedding'){ ?>
				<div class="col-md-6">
					<h2>Yours Information</h2>

					<?php if ($mostRecentEvent['data']['yours_uid'] == 0){ ?>
						<h3>Yours has not accepted your invite yet!</h3>
					<?php }else{ $yours = findUserById($pdoDB, $mostRecentEvent['data']['yours_uid']); ?>
					<br/><p class="user_info">Full Name</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['name']; ?>" />

					<br/><p class="user_info">Phone Number</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['phone']; ?>" />

					<br/><p class="user_info">Email Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['email']; ?>" />

					<br/><p class="user_info">Street Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['street']; ?>" />

					<br/><p class="user_info">City</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['city']; ?>" />

					<br/><p class="user_info">State</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['state']; ?>" />

					<br/><p class="user_info">Zip Code</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['zip']; ?>" />

					<br/><p class="user_info">Birthdate</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$yours['birthdate']; ?>" />
					<?php } ?>


					<br/><p class="user_info">&nbsp;</p>
					<br/><p class="user_info">&nbsp;</p>

				</div>
				<?php } ?>
			</div>
			<?php if ($mostRecentEvent['type'] == 'wedding'){ ?>
			<div class="row">
				<div class="col-md-6">
					<h2>Witness One Information</h2>

					<?php if ($mostRecentEvent['data']['first_witness_uid'] == 0){ ?>
						<h3>Witness One has not accepted your invite yet!</h3>
					<?php }else{ $witnessOne = findUserById($pdoDB, $mostRecentEvent['data']['first_witness_uid']); ?>
					<br/><p class="user_info">Full Name</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['name']; ?>"/>

					<br/><p class="user_info">Phone Number</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['phone']; ?>"/>

					<br/><p class="user_info">Email Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['email']; ?>"/>

					<br/><p class="user_info">Street Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['street']; ?>"/>

					<br/><p class="user_info">City</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['city']; ?>"/>

					<br/><p class="user_info">State</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['state']; ?>"/>

					<br/><p class="user_info">Zip Code</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['zip']; ?>"/>

					<br/><p class="user_info">Birthdate</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessOne['birthdate']; ?>"/>
					<?php } ?>


					<br/><p class="user_info">&nbsp;</p>
					<br/><p class="user_info">&nbsp;</p>

				</div>
				<div class="col-md-6">
					<h2>Witness Two Information</h2>

					<?php if ($mostRecentEvent['data']['second_witness_uid'] == 0){ ?>
						<h3>Witness Two has not accepted your invite yet!</h3>
					<?php }else{ $witnessTwo = findUserById($pdoDB, $mostRecentEvent['data']['second_witness_uid']); ?>
					<br/><p class="user_info">Full Name</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['name']; ?>"/>

					<br/><p class="user_info">Phone Number</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['phone']; ?>"/>

					<br/><p class="user_info">Email Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['email']; ?>"/>

					<br/><p class="user_info">Street Address</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['street']; ?>"/>

					<br/><p class="user_info">City</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['city']; ?>"/>

					<br/><p class="user_info">State</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['state']; ?>"/>

					<br/><p class="user_info">Zip Code</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['zip']; ?>"/>

					<br/><p class="user_info">Birthdate</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$witnessTwo['birthdate']; ?>"/>
					<?php } ?>

					<br/><p class="user_info">&nbsp;</p>
					<br/><p class="user_info">&nbsp;</p>

				</div>
			</div>
			<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<h2>Payment Information</h2>

					<br/><p class="user_info">Transaction ID</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo @$mostRecentEvent['payment']['txn_id']; ?>"/>

					<br/><p class="user_info">Paid Amount</p>
					<input type="text" readonly="readonly" class="form-control" value="$<?php echo @$mostRecentEvent['payment']['payment_amount']; ?>"/>

					<br/><p class="user_info">Date of Payment</p>
					<input type="text" readonly="readonly" class="form-control" value="<?php echo date("m/d/Y h:i A (T)", strtotime(@$mostRecentEvent['payment']["timestamp"]));    ?>"/>

					<br/><p class="user_info">&nbsp;</p>
					<br/><p class="user_info">&nbsp;</p>
					<div class="clear"></div><br>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- pattern -->

<?php include('../footer.php'); ?>
