<?php
ob_start();
session_start();
$page_name=basename($_SERVER['PHP_SELF']);?>
<?php include('../../header.php');
require_once('../../common/connection.php');
require_once('../../common/config.php');

?>

<style>
	iframe {
		width: 100%;
		height: 40vh;
	}
</style>

<br/>
<br/>
<br/>
<div class="container">
	<h1>Marriage Education</h1>
	<div class="mared_tabs">
		<a class="btn btn-primary" href="javascript:void(0);" rel="mared-video">Step1:Watch Video</a>
		<a class="btn" href="javascript:void(0);" rel="dmque" disabled="disabled">Step2:Submit Marriage Questionnaire</a>
		<a class="btn" href="javascript:void(0);" rel="dmcert" disabled="disabled">Step3:Download Marriage Certificate </a>
	</div><br/>
	<div class="mared_info">
		<div id="mared-video" class="homevideowrapper">
			<script src="https://www.youtube.com/iframe_api"></script>
			<div id="marriageeducationvideo"></div>
			<script>

			function onYouTubeIframeAPIReady() {
			    window.player = new YT.Player('marriageeducationvideo', {
			        width: 600,
			        height: 400,
			        videoId: '38jn1bBPCBk',
			        playerVars: {
			            color: 'white'
			        },
			        events: {
									onStateChange: stateChange
			        }
			    });

					setTimeout(function(){
						window.player.playVideo();
					}, 1000)
			}

			function stateChange(e){
				if (e.data == 0){
					window.player.pauseVideo();
					location.href = 'http://webwedmobile.com/admin/user_admin/marriage_education/marriage_education_certification_status.php';
				}
			}

			</script>
			<!-- <iframe src="https://www.youtube.com/embed/38jn1bBPCBk?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&loop=1&playlist=38jn1bBPCBk" frameborder="0" allowfullscreen marginwidth="0" marginheight="0" scrolling="no" align="top" frameborder="0"></iframe> -->
			<br/><br/>
			<p>Please watch the video above in its entireity. Once you are complete you will be automatically moved to the next step of downloading the marriage questionnaire.</p>
		</div>
		<div id="dmque" class="hidden">

		</div>
		<div id="dmcert" class="hidden">
			<div class="video_msg">
				<?php
				$qs = "select * from www_users_new where id = ".$_SESSION['wwm_user_id']."";
				$result = mysql_query($qs);
				$data = mysql_fetch_array($result);

				if(isset($data['marriage_education_certificate']) && $data['marriage_education_certificate'] == 'Request')
				{
					?>
					We are working on your request for Premarital Education Certificate
					<?php

				} if(isset($data['marriage_education_certificate']) && $data['marriage_education_certificate'] == 'Upload')
				{?>
					<a href="marriage_education_completed.php">
						Click here to get "Certificate of Completion of Premarital Education"
					</a>
					<?php }
					else{?>
						<form action="marriage_education_certification_status.php" method="post" name="request_form_prem_cert" id="request_form_prem_cert">
							<input type="submit" value="Click here to APPLY for Certificate of Completion for a Qualified Premarital Education" name="request_for_pre_edu_cert" />
						</form>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>


		<?php include('../../footer.php'); ?>
