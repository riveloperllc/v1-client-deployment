<?php
ob_start();
session_start();
$page_name=basename($_SERVER['PHP_SELF']);?>
<?php include('../../header.php');

require_once('../../common/connection.php');
require_once('../../common/config.php');

if(isset($_POST['request_for_pre_edu_cert']) && $_POST['request_for_pre_edu_cert']!='')
{
	$request_date_cert_pre_marri_edu = date ("Y-m-d");
	$query_string = "UPDATE wwm_users SET marriage_education_certificate='Request', request_date_cert_pre_marri_edu='$request_date_cert_pre_marri_edu' where user_id = ".$_SESSION['wwm_user_id']."";
	mysql_query ($query_string);

	$message = '<html><body>';
	$message .= "<p>Hello Admin,</p>";
	$message .= "<p>One of the couple has been completed the pre education training through watching video.</p>";
	$message .= "<p>He applied for a marriage education certificate of completion.</p>";

	$link = $siteurl.'admin_login.php';
	$message .= "<p>Please <a target=\"_blank\" href=".$link.">Login</a> in your account and see the request.</p>";
	$message .= "<p>Best regards,</p>";
	$message .= "<p>WebWedMobile team &#9786;</p>";
	$message .= "</body></html>";

	$to = $admin_email;
	$subject = 'Applied for Marriage Education Certificate - Notification';

	$host = $_SERVER['HTTP_HOST'];
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= "From: \" WebWedMobile \"<noreply@$host>\n";
	email($to,$subject,$message,$headers);

}
?>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div class="container">
	<h1>Marriage Education</h1>
	<div class="mared_tabs">
		<a class="btn" href="javascript:void(0);" rel="mared-video" disabled="disabled">Step1:Watch Video</a>
		<a class="btn btn-primary" href="javascript:void(0);" rel="dmque">Step2:Submit Marriage Questionnaire</a>
		<a class="btn" href="javascript:void(0);" rel="dmcert" disabled="disabled">Step3:Download Marriage Certificate </a>
	</div><br/>
	<div class="mared_info">

		<div class="form_main">
			<div class="form_main_inside">
				<h1>Download</h1>
				<p>Please download the resources below and reupload them once you have filled them out entirely.</p>
				<a href="http://webwedmobile.com/admin/upload/marriage_education_docs/DateNightInstructions.docx">Date Night Instructions</a><br/>
				<a href="http://webwedmobile.com/admin/upload/marriage_education_docs/LoveLanguageQuiz.pdf">Love Language Quiz</a><br/>
				<a href="http://webwedmobile.com/admin/upload/marriage_education_docs/Pre-MaritalQuestionaire.docx">Pre-Marital Questionaire</a><br/>
				<a href="http://webwedmobile.com/admin/upload/marriage_education_docs/VideoQuestions.docx">Video Questions</a>

				<form action="http://webwedmobile.com/admin/user_admin/marriage_education/marriage_education_completed.php" method="SUBMIT">
					<h1>Upload</h1>
					<p>Please attach the filled out copy of the above documents</p>
					<span>Date Night Questions</span><br/>
					<input type="file" name="datenight" style="width: 100%; box-shadow: none;"/><br/>
					<span>Love Language Questions</span><br/>
					<input type="file" name="lovelanguage" style="width: 100%; box-shadow: none;"/><br/>
					<span>Pre-Marital Questions</span><br/>
					<input type="file" name="premarital" style="width: 100%; box-shadow: none;"/><br/>
					<span>Video Questions</span><br/>
					<input type="file" name="videoquestions" style="width: 100%; box-shadow: none;"/><br/>
					<input type="submit" class="btn btn-primary" value="Upload documents"/>
				</form>
				<?php
				$qs = "select * from weddings where uid = ".$_SESSION['wwm_user_id']."";
				$result = mysql_query($qs);
				$data = mysql_fetch_array($result);

				//&& $data['upload_pre_marriage_education_certificate'] == 'Request'
				if(isset($data['upload_pre_marriage_education_certificate']))
				{
					?>
					<h2>
						You have successfully applied for Certificate of Completion of Qualifying Premarital Education.
					</h2><br/><br/>
					<h2>
						We are now creating certificate for you.
					</h2>
					<?php
				}
				?>
			</div>
		</div>
	</div>
</div>

<?php include('../../footer.php'); ?>
