<?php
@session_start();
if (!defined('BASEPATH')){ exit; }
@include_once('../common/db.php');
@include_once('../../common/db.php');
@include_once('../common/utility_functions.php');
@include_once('../../common/utility_functions.php');
?>
<ul class="nav navbar-nav">
  <li><a href="<?php echo SITEURL; ?>user_admin/user_dashboard.php">Home</a></li>
  <li><a href="<?php echo SITEURL; ?>user_admin/user_profile_edit.php">Manage Account</a></li>
  <?php if (getMostRecentEventFromUid($_SESSION['wwm_user_id'])['type'] == 'wedding'){ ?>
  <li><a href="<?php echo SITEURL; ?>user_admin/manage_invitations.php">Manage Guests</a></li>
  <?php } ?>
  <li class="hidden-xs hidden-sm"><a href="<?php echo SITEURL; ?>user_admin/marriage_education/marriage_education.php">Marriage Education</a></li>
  <li><a href="<?php echo SITEURL; ?>user_admin/logout.php">Logout</a></li>
</ul>
