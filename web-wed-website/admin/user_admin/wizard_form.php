<?php

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');
$page_title = "User Login - ";
include('../header.php');
$tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
// var_dump( $tzlist );
$db = $pdoDB;
$us_state_abbrevs_names = array(
	'AL'=>'ALABAMA',
	'AK'=>'ALASKA',
	'AS'=>'AMERICAN SAMOA',
	'AZ'=>'ARIZONA',
	'AR'=>'ARKANSAS',
	'CA'=>'CALIFORNIA',
	'CO'=>'COLORADO',
	'CT'=>'CONNECTICUT',
	'DE'=>'DELAWARE',
	'DC'=>'DISTRICT OF COLUMBIA',
	'FM'=>'FEDERATED STATES OF MICRONESIA',
	'FL'=>'FLORIDA',
	'GA'=>'GEORGIA',
	'GU'=>'GUAM GU',
	'HI'=>'HAWAII',
	'ID'=>'IDAHO',
	'IL'=>'ILLINOIS',
	'IN'=>'INDIANA',
	'IA'=>'IOWA',
	'KS'=>'KANSAS',
	'KY'=>'KENTUCKY',
	'LA'=>'LOUISIANA',
	'ME'=>'MAINE',
	'MH'=>'MARSHALL ISLANDS',
	'MD'=>'MARYLAND',
	'MA'=>'MASSACHUSETTS',
	'MI'=>'MICHIGAN',
	'MN'=>'MINNESOTA',
	'MS'=>'MISSISSIPPI',
	'MO'=>'MISSOURI',
	'MT'=>'MONTANA',
	'NE'=>'NEBRASKA',
	'NV'=>'NEVADA',
	'NH'=>'NEW HAMPSHIRE',
	'NJ'=>'NEW JERSEY',
	'NM'=>'NEW MEXICO',
	'NY'=>'NEW YORK',
	'NC'=>'NORTH CAROLINA',
	'ND'=>'NORTH DAKOTA',
	'MP'=>'NORTHERN MARIANA ISLANDS',
	'OH'=>'OHIO',
	'OK'=>'OKLAHOMA',
	'OR'=>'OREGON',
	'PW'=>'PALAU',
	'PA'=>'PENNSYLVANIA',
	'PR'=>'PUERTO RICO',
	'RI'=>'RHODE ISLAND',
	'SC'=>'SOUTH CAROLINA',
	'SD'=>'SOUTH DAKOTA',
	'TN'=>'TENNESSEE',
	'TX'=>'TEXAS',
	'UT'=>'UTAH',
	'VT'=>'VERMONT',
	'VI'=>'VIRGIN ISLANDS',
	'VA'=>'VIRGINIA',
	'WA'=>'WASHINGTON',
	'WV'=>'WEST VIRGINIA',
	'WI'=>'WISCONSIN',
	'WY'=>'WYOMING',
	'AE'=>'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST',
	'AA'=>'ARMED FORCES AMERICA (EXCEPT CANADA)',
	'AP'=>'ARMED FORCES PACIFIC'
);

?>
<br/>
<br/>
<br/>
<br/>
<style>
.btn-success {
	width:100%;
}
</style>
	<div class="container page-content">
		<div class="row">
			<h1>Sign Up</h1>

			<?php if(isset($_GET['event_code'], $_GET['user_type'], $_GET['email'])) {
						if ($_GET['user_type'] != 'officiant') { ?>
						<form id="signUpForm" method="get" action="">


									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="email1">Email</label>
										<input type="text" id="email1" name="email1" class="form-control email" value="<?php echo $_GET['email'];?>" readonly>
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="name1">Name</label>
										<input type="text" id="name1" name="name1" class="form-control">
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="password1">Password</label>
										<input type="password" name="password1" id="password1" placeholder="Password" class="form-control">
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="confirmpassword1">Confirm Password</label>
										<input type="password" name="confirmpassword1" id="confirmpassword1" placeholder="Confirm Password" class="form-control">
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="gender1">Gender</label>
										<select class="form-control" name="gender1" id="gender1" class="form-control">
											<option value="male">Male</option>
											<option value="female">Female</option>
										</select>
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="birthdate1">Birth Date</label>
										<input type="date" id="birthdate1" name="birthdate1" class="form-control">
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="street1">Street</label>
										<input type="text" id="street1" name="street1" class="form-control">
									</div>

									<div class="form-group col-xs-12 col-md-6">
											<label class="control-label" for="city1">City</label>
											<input type="text" id="city1" name="city1" class="form-control">
									</div>
									<div class="form-group col-xs-12 col-md-6">
											<label class="control-label" for="state1" class="form-control">State</label>
											<select class="form-control" name="state1" id="state1" class="form-control">
												<?php
														foreach($us_state_abbrevs_names as $state) { ?>
														<option value="<?php echo $state; ?>" ><?= $state ?></option>
												<?php
														}
												?>
											</select>
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="zip1">Zip</label>
										<input type="text" id="zip1" name="zip1" class="form-control">
									</div>

									<div class="form-group col-xs-12 col-md-6">
										<label class="control-label" for="phone1">Phone Number</label>
										<input type="text" id="phone1" name="phone1" class="form-control">
									</div>

									<div class="form-group col-xs-12 col-md-6">
											<label class="control-label" for="timezone1" class="form-control">Timezone</label>
											<select class="form-control" name="timezone1" id="timezone" class="form-control">
													<?php
															foreach($tzlist as $name) { ?>
															<option value="<?php echo $name; ?>" <?php if($name == 'America/New_York') { echo 'selected="selected"'; } ?> ><?= $name ?></option>
													<?php
															}
													?>
											</select>
									</div>
									<input type="text" class="hidden" value="<?php echo $_GET['event_code'];?>" name="event_code" readonly>
									<input type="text" class="hidden" value="<?php echo $_GET['user_type'];?>" name="user_type" readonly>

									<input type="submit" value="Create Account" class="btn btn-success">



						</form>
						<p id="success"></p>
						<p class="throw_error"></p>
						<?php
						} else {
							$weddingData = findWeddingByEventCode($db, $_GET['event_code']);
							$officiantData = getMinisterByEmail($pdoDB, $_GET['email']);
							?>
							<h3>Wedding Details</h3>
							<div class="container">
								<div class="row">
									<p class="col-md-12">
										Date: <?php echo $weddingData["event_date"]; ?>
									</p>
									<p class="col-md-12">
										Time: <?php echo $weddingData["event_time"]; ?>
									</p>
									<p class="col-md-12">
										Location: <?php echo $weddingData["state_located"]; ?>
									</p>
									<form id="assignMinister" method="get" action="">

									<input type="text" class="hidden" value="<?php echo $officiantData['minister_id'];?>" name="officiant_id" readonly>
									<input type="text" class="hidden" value="<?php echo $_GET['event_code'];?>" name="event_code" readonly>

									<input type="submit" value="Approve Invite" class="btn btn-success">
									</form>
									<p id="success"></p>
									<p class="throw_error"></p>
								</div>
							</div>
						<?php }
				} ?>
		</div>
	</div>
	<?php if(!isset($_GET['event_code'])) {
				?>
	<script>
		$( document ).ready(function(){

			$( "#gifts" ).change(function() {
				var isGift = $(this).val();
				if(isGift == "yes") {
					$("#gift_url").removeAttr('disabled');
				} else {
					$("#gift_url").prop('disabled', 'true');
				}
			});


			$('#signUpForm .product_type').on('change', function() {
				$('.next').css('display', 'block');
				var selectedProduct = $('input[name=product_type]:checked', '#signUpForm').val();

				if (selectedProduct == 'product1') {
					$('#rootwizard').bootstrapWizard('disable', '3');
				} else if (selectedProduct == 'product2' || selectedProduct == 'product3'){
					$('#rootwizard').bootstrapWizard('enable', '3');
				}
			});

		var $validator = $("#signUpForm").validate({
		  rules: {
			name1: {
		      required: true,
		      minlength: 3
		    },
		    email1: {
		      required: true,
		      email: true,
		      minlength: 3
		    },
		    password1: {
		      required: true,
		      minlength: 6
		    },
			confirmpassword1 : {
				required: true,
				minlength : 6,
				equalTo : "#password1"
			},
			gender1: {
		      required: true
		    },
			birthdate1: {
		      required: true
		    },
			street1: {
		      required: true
		    },
		    city1: {
		      required: true,
		      minlength: 3
		    },
		    state1: {
		      required: true
		    },
		    zip1: {
		      required: true,
		      minlength: 4
		    },
		    phone1: {
		      required: true,
		      minlength: 10
		    },
		    timezone1: {
		      required: true
		    },
			product_type: {
				required: true
			},
			event_name: {
				required: true
			},
			event_date: {
				required: true
			},
			event_time: {
				required: true
			},
			event_public: {
				required: true
			},
			chat: {
				required: true
			},
			gifts: {
				required: true
			},
			gift_url: {
				required: function(element) {
					return $("#gifts").val()=='yes'
				},
				url: true
			},
			yours_email: {
				required: function(element) {
					if($("input[name=product_type]:checked").val()=='product2' || $("input[name=product_type]:checked").val()=='product3') {
						return true;
					} else {
						return false;
					}
				}
			},
			witness1_email: {
				required: function(element) {
					if($("input[name=product_type]:checked").val()=='product2' || $("input[name=product_type]:checked").val()=='product3') {
						return true;
					} else {
						return false;
					}
				}
			},
			witness2_email: {
				required: function(element) {
					if($("input[name=product_type]:checked").val()=='product2' || $("input[name=product_type]:checked").val()=='product3') {
						return true;
					} else {
						return false;
					}
				}
			},
			'topofficiants[]': {
				required: function(element) {
					if($("input[name=product_type]:checked").val()=='product2' || $("input[name=product_type]:checked").val()=='product3') {
						return true;
					} else {
						return false;
					}
				},
				maxlength: 5,
				minlength: 5
			}
		  },
			messages: {
				'topofficiants[]': {
					required: "You must check at least 5 box",
					maxlength: "Check no more than {5} boxes",
					minlength: "Please select 5 officiants"
				}
			},
		  errorPlacement: function(error, element) {
			if (element.attr("name") == "product_type" )
				error.insertAfter(".some-class");
			else
				error.insertAfter(element);
	      },
		  submitHandler: function(form) {

				var formData = $(form).serialize();
				$.ajax({
					url: '../api/v1/api.php?cmd=sign_up',
					type: 'POST',
					data:formData,
					cache: false,
					success:function(data) {
						var data = JSON.parse(data);
						if (!data.success) { //If fails
							if (data.error) { //Returned if any error from api.php
								$('.throw_error').removeClass('hidden').addClass('alert').addClass('alert-danger').fadeIn(1000).html(data.error); //Throw relevant error
								$('#success').addClass('hidden'); //Throw relevant error
							}
						}
						else {
								$('#success').removeClass('hidden').addClass('alert').addClass('alert-success').fadeIn(1000).text(data.success); //If successful, than throw a success message
								$('.throw_error').addClass('hidden'); //Throw relevant error
								var form = $('<form action="make_payment.php" method="post">' +
								'<input type="text" name="user_id" value="' + data.user_id+ '" />' +
								'<input type="text" name="product" value="' + data.product+ '" />' +
								'</form>');
								$('body').append(form);
								form.submit();
							}
						}
					});
				return false;

           }

		});

	  	$('#rootwizard').bootstrapWizard({
	  		'tabClass': 'nav nav-pills',
	  		'onNext': function(tab, navigation, index) {
				// if(index == '1') {
				// 	if(!$('input[name=product_type]').is(':checked')) {
				// 		$('.next').css('display', 'none');
				// 	}
				// }
	  			var $valid = $("#signUpForm").valid();
	  			if(!$valid) {
	  				$validator.focusInvalid();
	  				return false;
	  			}
	  		},
	  		'onPrevious': function(tab, navigation, index) {
				// if(index != '1') {
				// 	$('.next').css('display', 'block');
				// }
	  		}
	  	});
    });

	</script>
	<?php } else { ?>
		<script>
		$( document ).ready(function(){

			$( "#assignMinister" ).submit(function( event ) {
				var formData = $(this).serialize();
				console.log(formData);
				$.ajax({
					url: '../api/v1/api.php?cmd=assign_minister',
					type: 'POST',
					data:formData,
					cache: false,
					success:function(data) {
						var data = JSON.parse(data);
						if (!data.success) { //If fails
							if (data.error) { //Returned if any error from api.php
								$('.throw_error').removeClass('hidden').addClass('alert').addClass('alert-danger').fadeIn(1000).html(data.error); //Throw relevant error
								$('#success').addClass('hidden'); //Throw relevant error
							}
						}
						else {
								$('#success').removeClass('hidden').addClass('alert').addClass('alert-success').fadeIn(1000).text(data.success); //If successful, than throw a success message
								$('.throw_error').addClass('hidden'); //Throw relevant error
							}
						}
				});
				return false;
			});


		var $validator = $("#signUpForm").validate({
		  rules: {
			name1: {
		      required: true,
		      minlength: 3
		    },
		    email1: {
		      required: true,
		      email: true,
		      minlength: 3
		    },
		    password1: {
		      required: true,
		      minlength: 6
		    },
			confirmpassword1 : {
				required: true,
				minlength : 6,
				equalTo : "#password1"
			},
			gender1: {
		      required: true
		    },
			birthdate1: {
		      required: true
		    },
			street1: {
		      required: true
		    },
		    city1: {
		      required: true,
		      minlength: 3
		    },
		    state1: {
		      required: true
		    },
		    zip1: {
		      required: true,
		      minlength: 4
		    },
		    phone1: {
		      required: true,
		      minlength: 10
		    },
		    timezone1: {
		      required: true
		    }
		  },
		  submitHandler: function(form) {

				var formData = $(form).serialize();
				console.log(formData);
				window.f = formData;
				$.ajax({
					url: '../api/v1/api.php?cmd=sign_up_invite',
					type: 'POST',
					data:formData,
					cache: false,
					success:function(data) {
						var data = JSON.parse(data);
						if (!data.success) { //If fails
							if (data.error) { //Returned if any error from api.php
								$('.throw_error').removeClass('hidden').addClass('alert').addClass('alert-danger').fadeIn(1000).html(data.error); //Throw relevant error
								$('#success').addClass('hidden'); //Throw relevant error
							}
						}
						else {
								$('#success').removeClass('hidden').addClass('alert').addClass('alert-success').fadeIn(1000).text(data.success); //If successful, than throw a success message
								$('.throw_error').addClass('hidden'); //Throw relevant error
							}
						}
					});
				return false;

           }

		});
    });

	</script>
	<?php } ?>
	<script>
	$(document).ready(function(){
		$('#birthdate1').datepicker({
			'format': 'm/d/yyyy',
			'autoclose': true
		});

		$('#event_date1').datepicker({
			'format': 'm/d/yyyy',
			'autoclose': true
		});

		$("#phone1").mask("(999) 999-9999");
		$("#zip1").mask("99999");
	});
	</script>
	<?php include('../footer.php'); ?>
