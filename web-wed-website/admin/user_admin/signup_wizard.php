<?php

require_once('../common/connection.php');
require_once('../common/config.php');
require_once('../common/functions.php');

if (!isset($_GET['id'])){
	header("Location: signup.php");
	exit;
}

$productID = $_GET['id'];

$page_title = "Sign up - ";
include('../header.php');
$tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
// var_dump( $tzlist );
$db = $pdoDB;
$us_state_abbrevs_names = array(
	'AL'=>'ALABAMA',
	'AZ'=>'ARIZONA',
	'AR'=>'ARKANSAS',
	'CA'=>'CALIFORNIA',
	'CO'=>'COLORADO',
	'CT'=>'CONNECTICUT',
	'DE'=>'DELAWARE',
	'FL'=>'FLORIDA',
	'GA'=>'GEORGIA',
	'ID'=>'IDAHO',
	'IL'=>'ILLINOIS',
	'IN'=>'INDIANA',
	'IA'=>'IOWA',
	'KS'=>'KANSAS',
	'KY'=>'KENTUCKY',
	'LA'=>'LOUISIANA',
	'ME'=>'MAINE',
	'MD'=>'MARYLAND',
	'MA'=>'MASSACHUSETTS',
	'MI'=>'MICHIGAN',
	'MN'=>'MINNESOTA',
	'MS'=>'MISSISSIPPI',
	'MO'=>'MISSOURI',
	'MT'=>'MONTANA',
	'NE'=>'NEBRASKA',
	'NV'=>'NEVADA',
	'NH'=>'NEW HAMPSHIRE',
	'NJ'=>'NEW JERSEY',
	'NM'=>'NEW MEXICO',
	'NY'=>'NEW YORK',
	'NC'=>'NORTH CAROLINA',
	'ND'=>'NORTH DAKOTA',
	'OH'=>'OHIO',
	'OK'=>'OKLAHOMA',
	'OR'=>'OREGON',
	'PA'=>'PENNSYLVANIA',
	'RI'=>'RHODE ISLAND',
	'SC'=>'SOUTH CAROLINA',
	'SD'=>'SOUTH DAKOTA',
	'TN'=>'TENNESSEE',
	'TX'=>'TEXAS',
	'UT'=>'UTAH',
	'VT'=>'VERMONT',
	'VI'=>'VIRGIN ISLANDS',
	'VA'=>'VIRGINIA',
	'WA'=>'WASHINGTON',
	'WV'=>'WEST VIRGINIA',
	'WI'=>'WISCONSIN',
	'WY'=>'WYOMING'
);

?>
<br/>
<br/>
<br/>
<br/>
<?php
	$productName = getProductName($pdoDB, $productID);
	$productPrice = getProductPricing($pdoDB, $productID);

	if ($productID == 1 || $productID == 5){
		include_once('_product_special_moment.php');
	}else{
		include_once('_product_wedding.php');
	}
?>
<?php include('../footer.php'); ?>
