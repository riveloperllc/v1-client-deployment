<?php
require_once('../common/db.php');
require_once('../common/config.php');
require_once('../common/connection.php');
require_once('../common/functions.php');
require_once('../common/utility_functions.php');

if (isset($_GET['samesex'])){
  $samesex = intval(boolval($_GET['samesex']));
  $db = $pdoDB;
  $prepared = $db->prepare("SELECT `minister_id`, `name`, `religious_affiliation`, `bio`, `photo`, `gender`, `city`, `state` FROM `ministers` WHERE `perform_willingness` = ? AND `admin_approved` = 1");
  $prepared->execute(array($samesex));
  if ($prepared->rowCount() > 0){
    echo json_encode(array("data" => $prepared->fetchAll()));
  }else{
    echo json_encode(array("data" => array()));
  }
}else{
  echo json_encode(array("data" => array()));
}
?>
