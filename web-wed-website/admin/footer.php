<?php require_once("common/config.php"); ?>
</div>
<!--jQuery-->
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/jquery-migrate-1.3.0.js"></script>

<!--Boostrap-->
<script src="<?php echo ROOTSITEURL; ?>/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo ROOTSITEURL; ?>/js/jquery.bootstrap.wizard.js"></script>

<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/jquery.validate.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo ROOTSITEURL; ?>/js/common.js"></script>

<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/jquery.card.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/inputLabel.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/fileinput.min.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/minister_update.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/jquery.timepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/datepair.js"></script>
<script type="text/javascript" src="<?php echo ROOTSITEURL; ?>/js/jquery.datepair.js"></script>
<script src="<?php echo ROOTSITEURL; ?>/js/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript" src="//momentjs.com/downloads/moment.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

</body>
</html>
