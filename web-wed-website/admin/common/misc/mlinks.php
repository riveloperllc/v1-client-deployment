<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet" media="screen" href="css/style.css" />
<title>Live Wedding</title>


</head>

<body class="mlinks_page">
	<div class="mlink_wrapper">

		<div class="bride">
			<h2>Bride</h2>
			<div class="bride_vid">
				<iframe id="AlertMaintenance" style="border-style: none; border-color: inherit; border-width: 0px;" src="http://webwedmobile.com/ls_php/video.php?n=<?php echo $_GET['wedding_code'];?>_BRIDE"></iframe>
			</div>
		</div>

		<div class="minister">
			<h2>Minister</h2>
			<div class="minister_vid">
				<iframe id="DelayReason" style="border-style: none; border-color: inherit; border-width: 0px;" src="http://webwedmobile.com/ls_php/video.php?n=<?php echo $_GET['wedding_code'];?>_MINISTER"></iframe>
			</div>
		</div>

		<div class="groom">
			<h2>Groom</h2>
			<div class="groom_vid">
				<iframe id="DelayReason" style="border-style: none; border-color: inherit; border-width: 0px;" src="http://webwedmobile.com/ls_php/video.php?n=<?php echo $_GET['wedding_code'];?>_GROOM"></iframe>
			</div>
		</div>
		<div class="clear"></div>
	</div>

</body>
</html>
