<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
require_once('common/connection.php'); 
require_once('common/config.php');
$page_name=basename($_SERVER['PHP_SELF']);?>
<?php include('header.php'); ?>

<div class="form_main">
    <div class="form_main_inside">
        <div class="login">
            <h1>Payment Failure</h1>
             <p style="text-align:center;"> Payment is not successful, Kindly re-try again.</p>                                                           
        </div>
    </div>
</div>

<?php include('bottom_link.php'); ?> 

<?php include('footer.php'); ?> 
