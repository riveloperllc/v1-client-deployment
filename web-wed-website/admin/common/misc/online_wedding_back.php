<?php
session_start();
require_once('common/connection.php'); 
require_once('common/config.php');
?>
<!-- Middle Start -->
<?php include('header.php'); ?>

<div class="form_main">
    <div class="form_main_inside">
        <div class="login payment dashboard">        	
            <div id="dataPost"></div>
        </div>
    </div>
</div>


<!-- pattern -->


<?php include('bottom_link.php'); ?> 
<?php include('footer.php'); ?> 

<script type="text/javascript">
    $(document).ready(function() {
    refreshData();
    });
               function refreshData(){
                   $.ajax({
                     url: 'script.php',
                     type:"POST",
                     data:"wedding_code=<?php echo $_GET['wedding_code'];?>",
                     success: function(data)
					 {
					 	if(data == 'over')
						{
							window.location.href = "thanks_for_watching_wedding.php";
						}
						else
						{
                     	document.getElementById("dataPost").innerHTML = data;//why not use $('#dataPost').html(data) if you're already using jQuery?
                        setTimeout(refreshData, 1000);//will make the next call 3 seconds after the last one has finished.
                       //render the dynamic data into html
					   }
                     }
                   });
               }

               </script>
