<?php
session_start();
require_once('common/connection.php'); 
require_once('common/config.php');
$page_name=basename($_SERVER['PHP_SELF']);?>
<?php include('header.php'); ?>

<div class="form_main">
    <div class="form_main_inside">
        <div class="login">
            <h1>Reset Password</h1>
             <p style="text-align:center;">Thank you for contact with Webwedmobile. </p>
        	<p style="text-align:center;">Your password reset link sent to your email. Click on the link in the email to reset your password.</p>                                          
        </div>
    </div>
</div>

<?php include('bottom_link.php'); ?> 
<?php include('footer.php'); ?> 
