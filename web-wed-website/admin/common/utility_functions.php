<?php
function getWeddingInstanceFromId($id){
  include('db.php');
  $prepared = $pdoDB->prepare("SELECT * FROM `weddings` WHERE `id` = ?");
  $prepared->execute(array($id));
  if ($prepared->rowCount() == 0){
    return array();
  }else{
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }
  return array();
}

function getMostRecentEventFromUid($uid){
  include('db.php');
  $prepared = $pdoDB->prepare("SELECT * FROM `payments` WHERE `uid` = ? AND `usertype` = 0 ORDER BY `id` DESC LIMIT 1");
  $prepared->execute(array($uid));

  if ($prepared->rowCount() > 0){
    $payment = $prepared->fetch(PDO::FETCH_ASSOC);
    if ($payment['product'] == 2 || $payment['product'] == 3){
      $prepared = $pdoDB->prepare("SELECT * FROM `weddings` WHERE `uid` = ? ORDER BY id DESC LIMIT 1");
      $prepared->execute(array($uid));
      if ($prepared->rowCount() > 0){
        $row = $prepared->fetch(PDO::FETCH_ASSOC);
        return array("type" => "wedding", "data" => $row, "payment" => $payment);
      }else{
        return array("type" => "none", "data" => array(), "payment" => array());
      }
    }else{
      $prepared = $pdoDB->prepare("SELECT * FROM `special_moments` WHERE `uid` = ? ORDER BY id DESC LIMIT 1");
      $prepared->execute(array($uid));
      if ($prepared->rowCount() > 0){
        $row = $prepared->fetch(PDO::FETCH_ASSOC);
        return array("type" => "special_moment", "data" => $row, "payment" => $payment);
      }else{
        return array("type" => "none", "data" => array(), "payment" => array());
      }
    }
  }

  return array("type" => "none", "data" => array(), "payment" => array());
}

function searchWeddingsWithUid($uid){

}

function searchSpecialMomentsWithUid($uid){

}

function getUserInstanceFromUid($uid){
  include('db.php');
  $prepared = $pdoDB->prepare("SELECT * FROM `www_users_new` WHERE `id` = ?");
  $prepared->execute(array($uid));
  if ($prepared->rowCount() == 0){
    return array();
  }else{
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }
  return array();
}

function getUsersNameFromUid($uid){
  $user = getUserInstanceFromUid($uid);
  if (empty($user)){
    return 'Pending Acceptence';
  }

  return $user['name'];
}

function getProductInstanceFromId($id){
  include('db.php');
  $prepared = $pdoDB->prepare("SELECT * FROM `ceremony_options` WHERE `co_id` = ?");
  $prepared->execute(array($id));
  if ($prepared->rowCount() == 0){
    return array();
  }else{
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }
  return array();
}

function getProductNameFromId($id){
  $product = getProductInstanceFromId($id);
  if (empty($product)){
    return 'Unknown Product or Service';
  }

  return $product['name'];
}

function isLoggedInAsAdmin($token){
  include('db.php');
  $prepared = $pdoDB->prepare("SELECT `id` FROM `tokens` WHERE `token` = ? AND `type` = 5");
  $prepared->execute(array($token));

  if ($prepared->rowCount() == 0){
    return false;
  }else{
    return true;
  }
}

function getDiscountCodeFromId($id){
  include('db.php');
  $prepared = $pdoDB->prepare("SELECT * FROM `discount_codes` WHERE `id` = ?");
  $prepared->execute(array($id));

  if ($prepared->rowCount() == 0){
    return array();
  }else{
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }
}


?>
