<?php

function paymentIsCurrent($db, $uid, $type = 0){
  $prepared = $db->prepare("SELECT `id`, `timestamp`  FROM `payments` WHERE `uid` = ? AND `usertype` = ? ORDER BY `id` DESC");
  $prepared->execute(array($uid, $type));

  if ($prepared->rowCount() == 0){
    return false;
  }else{
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    $paymentTimestamp = $row['timestamp'];
    $currentTimestamp = strtotime('now');
    $newTimestamp = ($currentTimestamp - $paymentTimestamp);
    if ($paymentTimestamp <= strtotime('-30 days')){
      return false;
    }else{
      return true;
    }
  }
  return true;
}

//Function for get next payment date from database
function getNextPaymentDate($db, $uid, $type = 0){
  // return true;
  $prepared = $db->prepare("SELECT `id`, `timestamp`  FROM `payments` WHERE `uid` = ? AND `usertype` = ? order by `timestamp` LIMIT 1" );
  $prepared->execute(array($uid, $type));
  if ($prepared->rowCount() == 0){
    return 'Due Now!';
  }else{
    //return true;
     $row = $prepared->fetch(PDO::FETCH_ASSOC);
     $Timestamp = $row['timestamp'];
    // $Timestamp=$Timestamp;

     $dueDate=date('m/d/Y ', $Timestamp);
    // return  ($dueDate);
     $dueDate=($dueDate+strtotime("+30 days"));
    return date('m/d/Y ', $dueDate);

  }
}


function getOfficiantList($db){
	$prepared = $db->prepare("SELECT `name`,`minister_id` FROM ministers");
	$prepared->execute();
	$result = $prepared->fetchAll();
	return $result;
}


function minister_pending_approval($db, $uid){
  $prepared = $db->prepare("SELECT admin_approved FROM `ministers` WHERE `minister_id` = ?");
  $prepared->execute(array($uid));
  if ($prepared->rowCount() == 0){
    return false;
  }else{
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row;
  }
  return false;
}

function minister_manage_wedding($db, $weddingid){
  $prepared = $db->prepare("SELECT * FROM `weddings` WHERE `id` = ?");
  $prepared->execute(array($weddingid));

  if ($prepared->rowCount() == 0){
    return array();
  }

  $row = $prepared->fetch(PDO::FETCH_ASSOC);
  return $row;
}

function minister_signup_completed($db, $uid){
  $prepared = $db->prepare("SELECT * FROM `ministers` WHERE `minister_id` = ?");
  $prepared->execute(array($uid));
  if ($prepared->rowCount() == 0){
    return false;
  }else{
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return boolval($row['profile_completed']);
  }
  return false;
}

// function boolval($variable){
//   return $variable;
// }

function getProfile($db, $uid, $type = 0){
  switch($type){
    case 1:
      $prepared = $db->prepare("SELECT * FROM `www_users_new` WHERE `id` = ?");
      $prepared->execute(array($uid));
      if ($prepared->rowCount() > 0){
        return $prepared->fetch(PDO::FETCH_ASSOC);
      }
      break;

    case 2:
      $prepared = $db->prepare("SELECT * FROM `admin` WHERE `admin_id` = ?");
      $prepared->execute(array($uid));
      if ($prepared->rowCount() > 0){
        return $prepared->fetch(PDO::FETCH_ASSOC);
      }
      break;

    default:
      $prepared = $db->prepare("SELECT * FROM `ministers` WHERE `minister_id` = ?");
      $prepared->execute(array($uid));
      if ($prepared->rowCount() > 0){
        return $prepared->fetch(PDO::FETCH_ASSOC);
      }
      break;
  }

  return array();
}

function getProductDescription($db, $productID){
    $prepared = $db->prepare("SELECT * FROM `ceremony_options` WHERE `co_id` = ? ORDER BY `price` ASC");
    $prepared->execute(array($productID));
    $row = $prepared->fetchAll()[0];
    return $row['description'];
}

function getProductName($db, $productID){
    $prepared = $db->prepare("SELECT * FROM `ceremony_options` WHERE `co_id` = ? ORDER BY `price` ASC");
    $prepared->execute(array($productID));
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
    return $row['name'];
}

function getProductPricing($db, $productID = 0, $userType = 0){
  if ($productID != 0){
    $prepared = $db->prepare("SELECT * FROM `ceremony_options` WHERE `co_id` = ? ORDER BY `price` ASC");
    $prepared->execute(array($productID));
  }else{
    $prepared = $db->prepare("SELECT * FROM `ceremony_options` WHERE `usertype` = ? ORDER BY `price` ASC");
    $prepared->execute(array($userType));
  }

  if ($prepared->rowCount() == 0){
    return floatval(0.00);
  }

  if ($prepared->rowCount() > 1){
    $row = $prepared->fetchAll()[0];
  }

  if ($prepared->rowCount() == 1){
    $row = $prepared->fetch(PDO::FETCH_ASSOC);
  }

  return sprintf("%01.2f", floatval($row['price']));
}

function alreadyExists($db, $email, $type = 0){
  $table = 'ministers';
  switch ($type){
    case 0:
      $table = 'ministers';
      break;

    case 1:
      $table = 'wwm_users';
      break;

    case 2:
      $table = 'admin';
      break;

    default:
      $table = 'ministers';
      break;
  }

  $prepared = $db->prepare("SELECT * FROM $table WHERE `email` = ?");
  $prepared->execute(array($email));

  if ($prepared->rowCount() == 0){
    return false;
  }else{
    return true;
  }
}

function getMinisterByEmail($db, $minister_email){
	$prepared = $db->prepare("SELECT * FROM ministers WHERE email = ? LIMIT 1");
	$prepared->execute(array($minister_email));
	if ($prepared->rowCount() > 0){
		$row = $prepared->fetch(PDO::FETCH_ASSOC);
		return $row;
	}else{
		return array();
	}
}

function findWeddingByEventCode($db, $event_code){
	$prepared = $db->prepare("SELECT * FROM weddings WHERE event_code = ? LIMIT 1");
	$prepared->execute(array($event_code));
	if ($prepared->rowCount() > 0){
		$row = $prepared->fetch(PDO::FETCH_ASSOC);
		return $row;
	}else{
		return array();
	}
}

if (!function_exists('boolval')){
  function boolval($b){ return $b; }
}

?>
