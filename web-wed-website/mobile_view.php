<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Web Wed Mobile - RSVP</title>
<!-- Search engines -->
<meta name="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
<!-- Google Plus -->
<meta itemprop="name" content="Web Wed Mobile - Live streaming weddings.">
<meta itemprop="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
<meta itemprop="image" content="images/social_share.png">
<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:400,300|Roboto+Condensed|Clicker+Script" rel="stylesheet" type="text/css">

<!--WebWed Icons and Style Sheets -->

<!--Fav and touch icons-->
<link rel="apple-touch-icon" sizes="57x57" href="images/icon/favicon//apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/icon/favicon//apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/icon/favicon//apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/icon/favicon//apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/icon/favicon//apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/icon/favicon//apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icon/favicon//apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icon/favicon//apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/icon/favicon//apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/icon/favicon//android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon//favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icon/favicon//favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon//favicon-16x16.png">

<!--web wed style-->
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/join.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>
<body>
<div id="wrapper">
<div class="container"><br>
  <h2>Your browser is not compatible.</h2>
<h4>Web Wed's live stream compatiblity relys on certain features that your browser either doesn't support or is blocking. You can download the mobile application on your smart phone or tablet for either iOS and Android, or you can try opening this page in another browser.</h4>
<p>&nbsp;</p>
<p><img src="images/icon/WebWed_logo_white.png" width="198" height="52" alt=""/></p>
</div>
</div>
</body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script> 

<script src="assets/js/script.js"></script> 

<script>
	// app.initialize();
	
	
        $(function () {
            $('.expander').simpleexpand();
            prettyPrint();
        });
	
	
	$(document).ready(function(){
        
   var scroll_start = 0;
   var startchange = $('#rsvpLink');
   var offset = startchange.offset();
    if (startchange.length){
   $(document).scroll(function() { 
      scroll_start = $(this).scrollTop();
      if(scroll_start < offset.top) {
          $(".navbar").css('background-color', '#023a7b)');
       }
   });
    }
});
	
</script> 
<script>
  

var love = setInterval(function() {
    var r_num = Math.floor(Math.random() * 10) + 1;
    var r_size = Math.floor(Math.random() * 35) + 10;
    var r_left = Math.floor(Math.random() * 80) + 1;
    var r_bg = Math.floor(Math.random() * 15) + 100;
    var r_time = Math.floor(Math.random() * 5) + 5;

    $('.bg_heart').append("<div class='heart' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg + ",1);-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

    $('.bg_heart').append("<div class='heart' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg +",1);-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

    $('.heart').each(function() {
        var top = $(this).css("top").replace(/[^-\d\.]/g, '');
        var width = $(this).css("width").replace(/[^-\d\.]/g, '');
        if (top <= -100 || width >= 150) {
            $(this).detach();
        }
    });
}, 1500);
      //@ sourceURL=pen.js
    </script>
</html>