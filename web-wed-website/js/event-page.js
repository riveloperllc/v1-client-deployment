function openModal(){
  $('.cd-section').removeClass('hidden');
  $('.cd-modal-action').removeClass('hidden');
  var actionBtn = 	$('[data-type="modal-trigger"]');
  var scaleValue = retrieveScale(actionBtn.next('.cd-modal-bg'));

  actionBtn.addClass('to-circle');
  actionBtn.next('.cd-modal-bg').addClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
    animateLayer(actionBtn.next('.cd-modal-bg'), scaleValue, true);
  });

  //if browser doesn't support transitions...
  if(actionBtn.parents('.no-csstransitions').length > 0 ) animateLayer(actionBtn.next('.cd-modal-bg'), scaleValue, true);

  //trigger the animation - close modal window
  $('.cd-section .cd-modal-close').on('click', function(){
    closeModal();
  });
  // $(document).keyup(function(event){
  //   if(event.which=='27') closeModal();
  // });

  $(window).on('resize', function(){
    //on window resize - update cover layer dimention and position
    if($('.cd-section.modal-is-visible').length > 0) window.requestAnimationFrame(updateLayer);
  });

  disregardVideoStreams();
  if (window.subscribers){
    for (var i = 0; i < window.subscribers.length; i++){
      if (window.subscribers[i] != undefined){
        if (window.subscribers[i].stream){
          window.subscribers[i].setAudioVolume(0);
        }
      }
    }
  }
}
$(document).ready(function(){
  //trigger the animation - open modal window
  openModal();

});

function retrieveScale(btn) {
  var btnRadius = btn.width()/2,
  left = btn.offset().left + btnRadius,
  top = btn.offset().top + btnRadius - $(window).scrollTop(),
  scale = scaleValue(top, left, btnRadius, $(window).height(), $(window).width());

  btn.css('position', 'fixed').velocity({
    top: top - btnRadius,
    left: left - btnRadius,
    translateX: 0,
  }, 0);

  return scale;
}

function scaleValue( topValue, leftValue, radiusValue, windowW, windowH) {
  var maxDistHor = ( leftValue > windowW/2) ? leftValue : (windowW - leftValue),
  maxDistVert = ( topValue > windowH/2) ? topValue : (windowH - topValue);
  return Math.ceil(Math.sqrt( Math.pow(maxDistHor, 2) + Math.pow(maxDistVert, 2) )/radiusValue);
}

function animateLayer(layer, scaleVal, bool) {
  layer.velocity({ scale: scaleVal }, 400, function(){
    $('body').toggleClass('overflow-hidden', bool);
    (bool)
    ? layer.parents('.cd-section').addClass('modal-is-visible').end().off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend')
    : layer.removeClass('is-visible').removeAttr( 'style' ).siblings('[data-type="modal-trigger"]').removeClass('to-circle');
  });
}

function updateLayer() {
  var layer = $('.cd-section.modal-is-visible').find('.cd-modal-bg'),
  layerRadius = layer.width()/2,
  layerTop = layer.siblings('.btn').offset().top + layerRadius - $(window).scrollTop(),
  layerLeft = layer.siblings('.btn').offset().left + layerRadius,
  scale = scaleValue(layerTop, layerLeft, layerRadius, $(window).height(), $(window).width());

  layer.velocity({
    top: layerTop - layerRadius,
    left: layerLeft - layerRadius,
    scale: scale,
  }, 0);
}

setInterval(function(){
  // showHideVideoContainer();
  if (!$('.cd-modal-action').is(':visible')){
    // $('#video_container').show();
    // $('#video_container').css('display', 'block');

    if (getCookie('active') == 'special_moment'){
      //$('#eventVideoElement').show();
      //$('#eventVideoElement').css('display', 'block');
    }else{
      $('#youVideoElement').show();
      $('#youVideoElement').css('display', 'block');
      $('#yoursVideoElement').show();
      $('#yoursVideoElement').css('display', 'block');
      $('#witness1VideoElement').show();
      $('#witness1VideoElement').css('display', 'block');
      $('#witness2VideoElement').show();
      $('#witness2VideoElement').css('display', 'block');
      $('#officalVideoElement').show();
      $('#officalVideoElement').css('display', 'block');
    }

    return;
  }else{
    // $('#video_container').hide();
    // $('#video_container').css('display', 'none');
  }
}, 200);

function closeModal() {
  if (!$('.cd-modal-action').is(':visible')){
    openModal();
    return;
  }
  var section = $('.cd-section.modal-is-visible');
  section.removeClass('modal-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
    animateLayer(section.find('.cd-modal-bg'), 1, false);
    $('.cd-section').addClass('hidden');
    $('.cd-modal-action').addClass('hidden');
    // $('#video_container').css('opacity', 1);
    // $('video').each(function(e){ $('video')[e].play(); });
    if (window.subscribers){
      for (var i = 0; i < window.subscribers.length; i++){
        if (window.subscribers[i] != undefined){
          if (window.subscribers[i].stream){
            window.subscribers[i].setAudioVolume(50);
          }
        }
      }
    }
  });
  //if browser doesn't support transitions...
  if(section.parents('.no-csstransitions').length > 0 ) animateLayer(section.find('.cd-modal-bg'), 1, false);
}

function loadScript(url, callback) {
  jQuery.ajax({
    url: url,
    dataType: 'script',
    success: callback,
    async: true
  });
};

function sendMessage(message) {
  window.socket.emit('message', message);
};

function sendChatMessage(body) {
  //var body = ''; //Value for textbox element
  $('.messages').append('<li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
  window.session.signal({
   type: 'chat',
   data: body
  }, function(error){
   if (!error){
     body = '';
   }
  });
};

function deleteAllCookies() {
  var cookies = document.cookie.split(";");

  for (var i = 0; i < cookies.length; i++) {
    var cookie = cookies[i];
    var eqPos = cookie.indexOf("=");
    var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
}

function addMinutes(date, minutes) {
  return new Date(date.getTime() + minutes*60000);
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function decode(a){
  var string = decodeURIComponent(a);
  return string.replaceAll('+', ' ');
}

function displayIFrameModal(src){
  document.getElementById('idFrame').src = src;
  hideModals();
  $("#iframeModal").modal({
    show: true
  });

  // addModalBackdropHide();
  var e = document.getElementById('chatWindow');
  e.style.opacity = 0;
  e.style.display = 'none';
};

function showMessage(header, text){
  $('[webwednotice="header"]').text(header);
  $('[webwednotice="paragraph"]').text(text);
  $('#messageDialog').modal({
    show: true
  });
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
};

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
};

function hideModals(){
  $('.modal').modal('hide');
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decode(results[2].replace(/\+/g, " "));
}

function toggle_visibility(id) {
  if ($('[webwedasset="'+id+'"]').is(':visible')){
    $('[webwedasset="'+id+'"]').hide();
    console.log('Hide id: ' + id);
  }else{
    $('[webwedasset="'+id+'"]').show();
    console.log('Show id: ' + id);
  }
}

window.onblur = function() {
  window.blurred = true;
};

window.onfocus = function() {
  window.blurred = false;
};

function messagePosted(from, body) {
  $('.messages').append('<li class="message left appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
};
