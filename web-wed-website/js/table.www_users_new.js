
/*
 * Editor client script for DB table www_users_new
 * Created by http://editor.datatables.net/generator
 */

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		ajax: 'php/table.www_users_new.php',
		table: '#www_users_new',
		fields: [
			{
				"label": "name",
				"name": "name"
			},
			{
				"label": "email",
				"name": "email"
			},
			{
				"label": "password",
				"name": "password",
				"type": "password"
			},
			{
				"label": "gender",
				"name": "gender",
				"type": "hidden"
			},
			{
				"label": "birthdate",
				"name": "birthdate",
				"type": "datetime",
				"format": "MM\/DD\/YY"
			},
			{
				"label": "street",
				"name": "street"
			},
			{
				"label": "city",
				"name": "city"
			},
			{
				"label": "state",
				"name": "state"
			},
			{
				"label": "zip",
				"name": "zip"
			},
			{
				"label": "timezone",
				"name": "timezone"
			},
			{
				"label": "phone",
				"name": "phone"
			}
		]
	} );

	var table = $('#www_users_new').DataTable( {
		dom: 'Bfrtip',
		ajax: 'php/table.www_users_new.php',
		columns: [
			{
				"data": "name"
			},
			{
				"data": "email"
			},
			{
				"data": "password"
			},
			{
				"data": "gender"
			},
			{
				"data": "birthdate"
			},
			{
				"data": "street"
			},
			{
				"data": "city"
			},
			{
				"data": "state"
			},
			{
				"data": "zip"
			},
			{
				"data": "timezone"
			},
			{
				"data": "phone"
			}
		],
		select: true,
		lengthChange: false,
		buttons: [
			{ extend: 'create', editor: editor },
			{ extend: 'edit',   editor: editor },
			{ extend: 'remove', editor: editor }
		]
	} );
} );

}(jQuery));

