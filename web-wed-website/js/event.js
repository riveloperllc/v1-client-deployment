// window.socket = null;
// window.sessionId = null;
// window.streams = {};
window.eventReady = false;

window.controls = {
  chat: $('[webwedcontrol="chat"]'),
  gift: $('[webwedcontrol="gift"]'),
  heart: $('[webwedcontrol="icon-heart"]'),
  calendar: $('[webwedcontrol="event"]'),
  share: $('[webwedcontrol="sharelink"]')
};

window.eventParams = {
  giftlink: 'http://myregistry.com/',
  settings: {
    public: false,
    chat: false,
    gifts: false
  },
  ceremonylink: 'http://webwedmobile.com/join.php?code=Demo',
  ondemand: false,
  date: (new Date()).getMonth()+'/'+(new Date()).getDay()+'/'+(new Date()).getFullYear()+' '+(new Date()).toLocaleTimeString(),
  eventTitle: 'Bride & Groom',
  code: 'Demo'
};

function setEventVariables(){
  if (getParameterByName('demo') == ''){
    $('#eventVideoElement').remove();
    return; /*Use Default Variables*/
  }

  if (getCookie('active') == 'special_moment'){
    var special_moment = JSON.parse(decode(getCookie('special_moment')));
    window.eventParams.giftlink = special_moment.giftlink;
    window.eventParams.eventTitle = special_moment.event_name;
    window.eventParams.ondemand = special_moment.ondemand;
    window.eventParams.date = special_moment.date.computed;
    window.eventParams.settings = special_moment.settings;
    window.eventParams.code = special_moment.code;
    window.eventParams.ceremonylink = special_moment.ceremonylink;

    window.eventParams.session_id = special_moment.session_id;
    window.eventParams.token_id = special_moment.token_id;

    if (window.eventParams.settings.public == '0'){ window.eventParams.settings.public = false; }else{ window.eventParams.settings.public = true; }
    if (window.eventParams.settings.chat == '0'){ window.eventParams.settings.chat = false; }else{ window.eventParams.settings.chat = true; }
    if (window.eventParams.settings.gifts == '0'){ window.eventParams.settings.gifts = false; }else{ window.eventParams.settings.gifts = true; }

    $('#wedding_video_elements_one').remove();
    $('#wedding_video_elements_three').remove();
    $('#officiate').remove();

    return; /*Use Variables Defined Above*/
  }

  if (getCookie('active') == 'wedding'){
    var wedding = JSON.parse(decode(getCookie('wedding')));
    window.eventParams.giftlink = wedding.giftlink;
    window.eventParams.eventTitle = wedding.yours1.name + ' & ' + wedding.yours2.name;
    window.eventParams.date = wedding.date.computed;
    window.eventParams.settings = wedding.settings;
    window.eventParams.code = wedding.code;
    window.eventParams.ceremonylink = wedding.ceremonylink;

    window.eventParams.session_id = wedding.session_id;
    window.eventParams.token_id = wedding.token_id;

    $('#eventVideoElement').remove();

    return; /*Use Variables Defined Above*/
  }
}

function applySettingsAndHandlers(){

  if (window.eventParams.settings.public){
    window.controls.share.each(function(e){
      $(window.controls.share[e]).removeClass('disabled');
    });
  }

  if (window.eventParams.settings.chat){
    window.controls.chat.each(function(e){
      $(window.controls.chat[e]).removeClass('disabled');
      $(window.controls.chat[e]).click(function(){
        toggle_visibility('chatWindow');
      })
    });

    $('#share').click(function(){
      var text = $("#messagetoshare").val();
      $("#messagetoshare").val('');
      sendChatMessage(text);
    });

    $('#messagetoshare').on('keypress', function(e){
      if (e.keyCode == 13){
        $('#share').click();
      }
    });
  }

  if (window.eventParams.settings.gifts && window.eventParams.giftlink != ''){
    window.controls.gift.each(function(e){
      $(window.controls.gift[e]).removeClass('disabled');
    });
  }

  if (!window.eventParams.ondemand){
    if (getParameterByName('demo') != ''){
      window.controls.calendar.each(function(e){
        $(window.controls.calendar[e]).removeClass('disabled');
        $(window.controls.calendar[e]).click(function(){
          addEvent();
        });
      });
    }
  }

  if (window.eventParams.ceremonylink != ''){
    $("#sociallink").attr('data-url', window.eventParams.ceremonylink);
  }

  $('#CoupleName').text(window.eventParams.eventTitle);

  if (!window.eventParams.ondemand){
    $('#WeddingDate').text(window.eventParams.date);
  }else{
    $('#WeddingDate').text('On-Demand Event');
  }

  window.controls.heart.each(function(e){
    $(window.controls.heart[e]).click(function(){
      if (getParameterByName('demo') == ''){
        closeModal();
        return;
      }

      if (!$('.cd-modal-action').is(':visible')){
        openModal();
      }else{
        joinWeddingViaButton();
      }

      if (window.checkIfHeartActive()){
        window.deactivateHeart();
      }else{
        if (window.eventReady){
          window.activateHeart();
        }
      }
    })
  });
}

function showGiftRegistry(){
  displayIFrameModal(window.eventParams.giftlink);
}

var apiEndpoint = 'api/v1/api.php';

function setSettings(){
  setEventVariables(); //Define wedding, special_moment, or demo.
  applySettingsAndHandlers();
  assemble();
  // showHideVideoContainer();
}


function addEvent(){
  var time = new Date(window.eventParams.date);
  var starttime = addMinutes(time, -30);
  var endtime = addMinutes(time, 30);
  var name = 'WebWedMobile Event';
  if (getCookie('active') == 'wedding'){
    var summary = window.eventParams.eventTitle + ' are getting hitched with WebWedMobile.';
    window.open ('ics.php?start=' + starttime + '&end=' + endtime + '&name=' + name + '&summary=' + summary,'_blank',false);
  }else{
    var summary = window.eventParams.eventTitle + ' is a special moment powered by WebWedMobile.';
    window.open ('ics.php?start=' + starttime + '&end=' + endtime + '&name=' + name + '&summary=' + summary,'_blank',false);
  }
}
window.positions = {
  your: false,
  yours: false,
  witness1: false,
  witness2: false,
  officiant: false
};
window.getSubscriberElement = function(event){
  if (event.stream.connection.data == 'yours'){
    //Bride
    var div = 'you';
    window.positions.your = true;
  }else if (event.stream.connection.data == 'yours2'){
    //Groom
    var div = 'yours';
    window.positions.yours = true;
  }else if (event.stream.connection.data == 'witness1'){
    //Witness1
    var div = 'witness1';
    window.positions.witness1 = true;
  }else if (event.stream.connection.data == 'witness2'){
    //Witness2
    var div = 'witness2';
    window.positions.witness2 = true;
  }else if (event.stream.connection.data == 'officiant'){
    //Officiant
    var div = 'officiate';
    window.positions.officiant = true;
  }else{
    //Apple TV
  }

  return div;
};

function assemble(){
  var apikey = getCookie('apikey');
  var session_id = window.eventParams.session_id;
  var token_id = window.eventParams.token_id;

  window.subscribers = [];
  OT.on("exception", function(event) {
    console.log(event.message);
  });
  window.session = OT.initSession(apikey, session_id);

  window.session.on('signal:chat', function(event){
    var body = event.data;
    if (event.from.connectionId != window.session.connection.connectionId){
      messagePosted('Anonymous', body);
    }
  });

  window.session.on('signal:moment_stopped', function(event){
    //Moment has stopped, similar to streamDestroyed event.
    window.eventReady = false;
    window.eventOver = true;
    window.deactivateHeart();
    openModal();
    showMessage('This moment has ended!', 'Thank you for being apart of our very special moment.');
    console.log('signal for moment stopped');
  });

  window.session.on('signal:wedding_started', function(event){
    //Wedding started by Officiant
    console.log('signal for wedding started');
    window.eventReady = true;
    showMessage('This event has started!', 'If you would like to begin watching this wedding please tap the heart below.');
    window.activateHeart();
  });

  window.session.on('signal:weddingCompleted', function(event){
    //Wedding started by Officiant
    console.log('signal for wedding stopped');
    window.eventReady = false;
    window.eventOver = true;
    window.deactivateHeart();
    openModal();
    showMessage('This wedding has concluded.', 'Thank you so much for being apart of our big day!');
  });

  window.session.on('streamCreated', function(event){
    if (getCookie('active') == 'special_moment'){
      window.eventReady = true;
      showMessage('This event has started!', 'If you would like to begin watching this wedding please tap the heart below.');
      window.activateHeart();

      var subscriber = window.session.subscribe(event.stream, 'video_container', {
        insertMode: 'append',
        width: '100vw',
        height: '100vh'
      }, function(error){
        if (!error){
          subscriber.setAudioVolume(0);
        }
      });
      window.subscribers.push(subscriber);
    }else{
      //Wedding stream created.
      var div_id = window.getSubscriberElement(event);

      if (event.stream.connection.data == 'officiant' || (event.stream.connection.data == 'witness1') || (event.stream.connection.data == 'witness2')){
        if(event.stream.connection.data == 'officiant'){
          var subscriber = window.session.subscribe( event.stream, {insertDefaultUI: false, subscribeToAudio: true, insertMode: 'append', height: '100%', width: '100%'} );
        }else{
          var subscriber = window.session.subscribe( event.stream, {insertDefaultUI: false, subscribeToAudio: true, insertMode: 'append', height: '100%', width: '100%'} );
        }
      }else{
        if(event.stream.connection.data == 'yours'){
          var subscriber = window.session.subscribe( event.stream, {insertDefaultUI: false, subscribeToAudio: true, insertMode: 'append', height: '100%', width: '100%'} );
        }else{
          var subscriber = window.session.subscribe( event.stream, {insertDefaultUI: false, subscribeToAudio: true, insertMode: 'append', height: '100%', width: '100%'} );
        }
      }

      /**DEBUG**/
      // window.eventReady = true;
      // showMessage('This event has started!', 'If you would like to begin watching this wedding please tap the heart below.');
      // window.activateHeart();

      subscriber.on('videoElementCreated', function(event) {
        document.getElementById(div_id).appendChild(event.element);
      });

      window.subscribers.push(subscriber);
    }
    console.log('stream was created');
  });

  window.session.on('streamDestroyed', function(event){
    if (getCookie('active') == 'special_moment'){
      window.eventReady = false;
      window.deactivateHeart();
      openModal();
      showMessage('This moment has ended!', 'Thank you for being apart of our very special moment.');
    }else{
      window.eventReady = false;
      window.deactivateHeart();
      openModal();
      if (window.eventOver == false){
        showMessage('A wedding party member disconnected', 'Someone from the wedding party discononected, this event will become available again when they join again.');
      }
      //Wedding stream destroyed.
    }

    for (var i = 0; i < window.subscribers.length; i++){
      if (window.subscribers[i].streamId == null){
        delete window.subscribers[i];
        console.log('deleting subscriber instance');
      }else{
        if (window.subscribers[i].stream == event.stream){
          delete window.subscribers[i];
          console.log('deleting subscriber instance');
        }
      }
    }
    console.log('stream was destroyed');
  });

  if (getParameterByName('demo') != ''){
    window.session.connect(token_id, function(){
      console.log('Connected to Opentok services.');
    });
  }else{
    $('#officiate').append("<video autoplay='autoplay' loop='loop' id='officalVideoElement'  src='demo_videos/officiate.mp4'></video>");
    $('#witness1').append("<video style='height:300px;' autoplay='autoplay' loop='loop' id='witness1VideoElement' src='demo_videos/witness1.mp4'></video>");
    $('#witness2').append("<video style='height:300px;' autoplay='autoplay' loop='loop' id='witness2VideoElement' src='demo_videos/witness2.mp4'></video>");
    $('#yours').append("<video autoplay='autoplay' loop='loop' id='yoursVideoElement' src='demo_videos/bride.mp4'></video>");
    $('#you').append("<video autoplay='autoplay' loop='loop' id='youVideoElement' src='demo_videos/groom.mp4'></video>");
  }

  window.checkIfHeartActive = function(){
    if ($('.yellow').hasClass('active') && $('.blue').hasClass('active') && $('.fav').hasClass('active')){
      return true;
    }else{
      return false;
    }
  }

  window.activateHeart = function(){
    if (!window.checkIfHeartActive()){
      $('.yellow').addClass('active');
      $('.blue').addClass('active');
      $('.fav').removeClass('unlike').addClass('active icon-animation');
    }
  };

  window.deactivateHeart = function(){
    if (window.checkIfHeartActive()){
      $('.yellow').removeClass('active');
      $('.blue').removeClass('active');
      $('.fav').removeClass('active  icon-animation').addClass('unlike');
    }
  }
}

function failureGuestLogin(data) {
  showMessage('Incorrect login', 'Please try again. If you continue to receive this message email info@webwedmobile.com.');
};

function init(callback) {
  // if (getCookie('active') == 'special_moment'){
  //   window.socket = io.connect('http://endpoint.webwedmobile.com:8082/');
  // }else{
  //   window.socket = io.connect('http://endpoint.webwedmobile.com:7999/');
  // }
  //
  // window.onbeforeunload = function() {
  //   window.socket.disconnect();
  // };

  callback();
};

function loadScripts(callback) {
  if (getCookie('active') == 'special_moment'){
    // loadScript('http://endpoint.webwedmobile.com:8082/socket.io/socket.io.js', function() {
      callback();
    // });
  }else{
    // loadScript('http://endpoint.webwedmobile.com:7999/socket.io/socket.io.js', function() {
      callback();
    // });
  }
};

function register() {
  // window.sendMessage({
  //   id: 'register',
  //   wedding: window.eventParams.code,
  //   code: window.eventParams.code,
  //   eventcode: window.eventParams.code,
  //   name: 'Anonymous'
  // });
};

function joinWeddingViaButton() {
  if (window.eventReady) {
    window.renderVideoStreams();
    closeModal();
  } else {
    showMessage('We are not ready yet!', 'This event does not seem to be available just yet, you will be notified when it is available to be joinned.');
  }
};

function disregardVideoStreams() {
  //$('video_container').hide();

  // if (getCookie('active') == 'special_moment'){
  //   document.getElementById('eventVideoElement').src = '';
  // }else{
  //   document.getElementById('youVideoElement').src = '';
  //   document.getElementById('yoursVideoElement').src = '';
  //   document.getElementById('witness1VideoElement').src = '';
  //   document.getElementById('witness2VideoElement').src = '';
  //   document.getElementById('officalVideoElement').src = '';
  // }
};

function renderVideoStreams() {

  var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  // Firefox 1.0+
  var isFirefox = typeof InstallTrigger !== 'undefined';
  // At least Safari 3+: "[object HTMLElementConstructor]"
  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
  // Internet Explorer 6-11
  var isIE = /*@cc_on!@*/ false || !!document.documentMode;
  // Edge 20+
  var isEdge = !isIE && !!window.StyleMedia;
  // Chrome 1+
  var isChrome = !!window.chrome && !!window.chrome.webstore;
  // Blink engine detection
  var isBlink = (isChrome || isOpera) && !!window.CSS;

  var index = 0;
  if (isSafari || isFirefox || isChrome || isIE || isOpera) {
    index = 0;
  } else {
    index = 1;
  }

  if (getCookie('active') == 'special_moment'){

  }else{

  }
};

function showHideVideoContainer() {

  // if (!$('#video_container').is(':visible')){
  //   $('video').each(function(e){
  //     $('video')[e].pause();
  //   });
  // }else{
  //   $('video').each(function(e){
  //     $('video')[e].play();
  //   });
  // }
}

setSettings();
