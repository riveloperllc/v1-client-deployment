<?php
  $url = 'http://olegh.ftp.sh/public-stun.txt';
  $contents = file_get_contents($url);
  $stunServers = explode("\n", $contents);
  $stun = array();

  foreach($stunServers as $stunServer){
      $a = explode(':', $stunServer);
      $ip = $a[0];
      $port = $a[1];
      if (checkIPAddress($ip) == false){
        $ip = getIP($ip);
      }
      $stun = array_merge($stun, array($ip.":".$port));
  }

  echo json_encode(array(
      "stun" => $stunServers,
      "turn" => array()
    )
  );

  function checkIPAddress($ipAddress)
  {
      return preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $ipAddress);
  }

  function getIP($host){
    return file_get_contents('http://api.konvert.me/forward-dns/'.$host);
  }
?>
