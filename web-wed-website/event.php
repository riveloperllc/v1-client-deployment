<?php
ob_start();
session_start();
if (!isset($_SERVER['HTTPS']) || @$_SERVER['HTTPS'] != 'on') {
  if (isset($_GET['demo'])){
    header("Location: https://webwedmobile.com/event.php?demo");
  }else{
    header("Location: https://webwedmobile.com/event.php");
  }
}

$_COOKIE['apikey'] = '45692092'; //Opentok apikey
setcookie('apikey', '45692092'); //Opentok apikey

$_COOKIE['special_moment'] = @$_SESSION['special_moment'];
$_COOKIE['wedding'] = @$_SESSION['wedding'];

if (!isset($_GET['demo'])){
  if ($_COOKIE['wedding'] == '' && $_COOKIE['special_moment'] == '' && !isset($_GET['demo'])){ header("Location: admin/user_admin/guest_login.php"); exit; }
}
$active = 'wedding';

if (@urldecode($_SESSION['wedding']) == '[]') {
  setcookie('active', 'special_moment');
  setcookie('special_moment', @$_SESSION['special_moment'], time() + (86400 * 30), '/');
  $active = 'special_moment';
} else {
  setcookie('active', 'wedding');
  setcookie('wedding', @$_SESSION['wedding'], time() + (86400 * 30), '/');
  $active = 'wedding';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>A WebWed Mobile Event.</title>
  <!-- Search engines -->
  <meta name="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
  <!-- Google Plus -->
  <meta itemprop="name" content="Web Wed Mobile - Live stream weddings.">
  <meta itemprop="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
  <meta itemprop="image" content="images/social_share.png">
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=8" /><![endif]-->
  <?php require_once 'includes/Mobile_Detect.php'; $detect = new Mobile_Detect();?>


  <meta name="apple-itunes-app" content="app-id=1086176524"/>

  <!--WebWed Icons and Style Sheets -->

  <!--Fav and touch icons-->
  <link rel="apple-touch-icon" sizes="57x57" href="images/icon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="images/icon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="images/icon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="images/icon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="images/icon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="images/icon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="images/icon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="images/icon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="images/icon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="images/icon/favicon//android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="images/icon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon-16x16.png">

  <!--web wed style-->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
  <link rel="stylesheet" href="css/main.css">

  <!-- Fonts -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:400,300|Roboto+Condensed|Clicker+Script" rel="stylesheet" type="text/css">
  <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.browser.js"></script>
  <script>
  if ($.browser.msie == true){
    window.location.href = 'unsupported.php';
  }

  if ($.browser.msedge == true){
    window.location.href = 'unsupported.php';
  }
  </script>
  <script src="js/velocity.min.js"></script>
  <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
  <script src="js/event-page.js"></script>
  <link rel="stylesheet" href="css/event-page.css"/>
  <style>

  #videoContainer{width:100vw !important; height:100vh !important; margin:0px !important; left:0 !important; top:0 !important; padding:0px !important;}
  #yourVideoElement2{ background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: url(img/bride_background.jpg) !important;width: 50vw !important; height: 45vh !important; left: 50vw !important; top: 0vh !important; display: block; overflow: hidden !important; position: absolute !important; float: left !important; }
  #yoursVideoElement2{ background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: url(img/groom_background.jpg) !important;width:50vw; height:45vh; top:0; left:0; background-color:#d7d7d7; display:block; overflow:hidden;position:absolute; float:left;}
  #witnessVideoElement2{background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: url(img/witness_male_background.jpg) !important;width:50vw; height:30vh; left:0; /*! bottom:0; */ background-color:#84848d; display:block; overflow:hidden;position:absolute; float:left;top: 45vh;/*! margin: 0px; */background-repeat: no-repeat;}
  #witness2VideoElement2{background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: url(img/witness_female_background.jpg) !important;width:50vw; height:30vh; left:50vw;  background-color:#61619b; display:block; overflow:hidden;position:absolute; float:left;top: 45vh;}
  #officialVideoElement2{ background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: url(img/officient_background.jpg) !important;width: 33vw; height: 33vh; left: 33vw; top:30vh; background-color: black; display: block; overflow: hidden; position: absolute; z-index: 4; float:none;}
  #bottombar2{ top: 50vh; }
  #beatingheart { position: absolute; z-index: 4000; left: 45%; top: 0; }
  </style>
</head>
<body>

  <!-- HEADER END -->
  <?php if ($detect->isMobile()) {
    header("Location: unsupported.php");
    exit;
  } else {
    ?>
    <div id="video_nav_tweet_wrapper" class="wrapper" style="height:100vh;">

      <div class="wrapper"><!-- wrapper start  -->
        <div id="background-carousel" class="">
          <div id="myCarousel" class="carousel carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
              <div class="item item-no1 active"></div>
              <div class="item item-no2"></div>
              <div class="item item-no3"></div>
              <div class="item item-no4"></div>
            </div>
          </div>
        </div>


        <!-- Action bars that only appear on mobile at the right time -->
        <div class="action-bar hidden"> <span class="pull-left">Return Comments</span> <a href="#"> <em id="chatArrow" class="fa fa-arrow-circle-o-up fa-rotate-180 pull-right"></em> </a> </div>

        <!-- /.background-carousel end -->

        <div id="video_container" class="master_video_margin" style="opacity: 1; width: 100%; max-width: 100%; <?php if ($active == 'wedding'){ ?> margin-top: calc(100vh / 3.1) !important; <?php }else{ ?> margin-top: 0 !important; <?php } ?>">
          <!-- <video id="eventVideoElement" autoplay='autoplay' style="height: 100vh; width:100vw; background-color:#000;"></video> -->
          <div class="row row-one" id="wedding_video_elements_one">
            <div id="you" class="vide-cell">
              <!-- <video autoplay='autoplay' loop='loop' id='youVideoElement' src='demo_videos/groom.mp4'></video> -->
            </div>
            <div id="yours" class="vide-cell">
              <!-- <video autoplay='autoplay' loop='loop' id='yoursVideoElement' src='demo_videos/bride.mp4'></video> -->
            </div>
          </div>
          <style>
            #officiate video{
              border-top-left-radius: 60px;
              border-top-right-radius: 60px;
            }

            .chat_window {
              height: 50%;
            }
          </style>
          <div id="officiate" class="vide-cell">
            <!-- <video autoplay='autoplay' loop='loop' id='officalVideoElement'  src='demo_videos/officiate.mp4'></video> -->
          </div>
          <div class="row row-two" id="wedding_video_elements_three">
            <div id="witness1" style="z-index:1;" class="vide-cell">
              <!-- <video autoplay='autoplay' loop='loop' id='witness1VideoElement' src='demo_videos/witness1.mp4'></video> -->
            </div>
            <div id="witness2" style="z-index:1;" class="vide-cell">
              <!-- <video autoplay='autoplay' loop='loop' id='witness2VideoElement' src='demo_videos/witness2.mp4'></video> -->
            </div>
          </div>
        </div>

        <!--WebWed Controls-->
        <div id="webwed_control" style="height: auto !important;">
          <ul class="list-inline">
            <li> <a href="#" webwedcontrol="chat" id="chat" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Chat with other guests."> <img src="images/tab-chat-white.svg" width="35" class="webwed_icon"> </a></li>
            <li> <a href="#" webwedcontrol="gift" id="gift" data-toggle="tooltip" onClick="showGiftRegistry()" class="webwed_icon top disabled" data-placement="top"  data-original-title="Public registry."> <img src="images/tab-gift-white.svg" width="35" class="webwed_icon"> </a></li>
            <li> <a href="#" webwedcontrol="wwHeart" id="wwHeart" class="webwed_icon">
              <div class="icon-heart"  webwedcontrol="icon-heart"> <i  id="forever_heart" class="material-icons blue"> </i> <i  id="together_heart" class="material-icons yellow"> </i> <i  id="whereever_heart" class="material-icons fav disabled"> </i> </div>
            </a></li>
            <li> <a href="#" webwedcontrol="event" id="event" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Download To Calendar"> <img src="images/tab-event-white.svg" width="35" class="webwed_icon"> </a></li>
            <li> <a href="#" webwedcontrol="sharelink" id="sharelink" data-toggle="modal" data-target="#wwSocialShare" class=" disabled"> <img src="images/tab-link-white.svg" width="35" class="webwed_icon"> </a> </li>
          </ul>
        </div>

        <!--Chat Dialog-->

        <div class="chat_window" id="chatWindow" webwedasset="chatWindow">
          <ul class="messages">
          </ul>
          <div class="bottom_wrapper clearfix">
            <div class="message_input_wrapper">
              <input class="message_input" placeholder="Share A Message" id="messagetoshare">
            </div>
            <div class="send_message" id="share">
              <div class="icon"></div>
              <div class="text">Share</div>
            </div>
          </div>
        </div>
        <div class="message_template">
          <li class="message">
            <div class="avatar"></div>
            <div class="text_wrapper">
              <div class="text"></div>
            </div>
          </li>
        </div>

        <!--Terminate Chat-->
      </div>
      <!-- wrapper end -->
    </div>
    <!-- video_nav_tweet_wrapper end -->
    <?php
  } ?>
  <!-- .rsvpLink start -->

  <!-- .rsvpLink end -->
  <section class="cd-section">
    <!-- section content here -->

    <div class="cd-modal-action">
      <a href="#0" class="btn" data-type="modal-trigger" style="opacity:0;"></a>
      <span class="cd-modal-bg"></span>
    </div>

    <div class="cd-modal">
      <div class="cd-modal-content">
        <!-- modal content here -->
        <div class="bg_heart fancy">
          <section id="rsvpLink" class="rsvpLink fancy">
            <div class="color-overlay" style="padding-top:0px !important;">
              <div class="container">
                <div style="display:block; margin-left:auto; margin-right:auto; margin-bottom:20px;">
                  <a class="" href="index.php">
                    <img alt="Web Wed Logo" class="brand" src="images/icon/WebWed_logo_white.png" style="display:block; margin-left:auto; margin-right:auto; width:200px;"/>
                  </a>
                </div>
                <div class=" invitefancy">
                  <div class="row text-center invite_text">
                    <h3 class="color_navy evite">Join In.</h3>
                    <div class="row">
                      <div class="col-sm-12 text-center">
                        <h3 class="color_navy evite_couple" id="CoupleName"><?php if (isset($_GET['demo'])){ ?>John Doe<?php }else{ ?>The Wedding Of<?php } ?></h3>
                        <h3 class="evite_small color_black" id="WeddingDate"><?php if (isset($_GET['demo'])){ ?>Jane Doe<?php }else{ ?>Live Video Multi Person Service<?php } ?></h3>
                        <h3 class="evite_small color_black" id="HideText">Watch the live video by pressing the heart below.</h3>
                        <h3 class="color_navy evite_date" id="HideText"></h3>
                      </div>
                    </div>
                    <!-- row end -->

                    <div class="row">

                      <!-- <div class="user-img col-xs-6 col-sm-6"> <img alt="To be wed" src="images/groom.png"> </div>
                      <div class="user-img col-xs-6 col-sm-6"> <img alt="To be wed" src="images/bride.png"> </div>-->

                      <!-- /.thumb-img -->
                      <!-- /.thumbnail-block -->

                      <div class="col-sm-12 text-center color-black" id="wedding-paragraph">This is your chance to become one of the first to break through the walls and make your special day a special event.<br>
                      </div>

                      <!--<div class="col-sm-12 text-center"> <a href="images/WebWed.ics" class="btn default-btn">RSVP</a> </div>-->
                    </div>
                    <!-- row end -->

                  </div>
                  <!-- row text-center end-->
                </div>
                <div id="webwed_control" style="height: auto !important;">
                  <ul class="list-inline">
                    <li> <a href="#" webwedcontrol="chat" id="chat" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Chat with other guests."> <img src="images/tab-chat-white.svg" width="35" class="webwed_icon"> </a></li>
                    <li> <a href="#" webwedcontrol="gift" id="gift" data-toggle="tooltip" onClick="showGiftRegistry()" class="webwed_icon top disabled" data-placement="top"  data-original-title="Public registry."> <img src="images/tab-gift-white.svg" width="35" class="webwed_icon"> </a></li>
                    <li> <a href="#" webwedcontrol="wwHeart" id="wwHeart" class="webwed_icon">
                      <div class="icon-heart" webwedcontrol="icon-heart"> <i  id="forever_heart" class="material-icons blue"> </i> <i  id="together_heart" class="material-icons yellow"> </i> <i  id="whereever_heart" class="material-icons fav disabled"> </i> </div>
                    </a></li>
                    <li> <a href="#" webwedcontrol="event" id="event" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Download To Calendar"> <img src="images/tab-event-white.svg" width="35" class="webwed_icon"> </a></li>
                    <li> <a href="#" webwedcontrol="sharelink" id="sharelink" data-toggle="modal" data-target="#wwSocialShare" class=" disabled"> <img src="images/tab-link-white.svg" width="35" class="webwed_icon"> </a> </li>
                  </ul>
                </div>
                <!-- border_frame end-->
              </div>

              <!-- /.color-overlay end -->

            </div>
          </section>
        </div>
      </div>
    </div>

    <a href="#0" class="cd-modal-close">Close</a>
  </section>

  <!--Social Modal -->
  <div class="modal fade" id="wwSocialShare" role="dialog">
    <div class="modal-dialog modalwide">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-alt"></i> Share A Link</h4>
        </div>
        <div class="modal-body">
          <div class="ssk-group ssk-lg">
            <div id="sociallink" class="ssk-group" data-url="http://www.webwedmobile.com/" data-text="Live stream wedding ceremony" > <a href="" class="ssk ssk-facebook"></a> <a href="" class="ssk ssk-twitter" data-text="Live stream Twitter. "></a> <a href="" class="ssk ssk-google-plus"></a> <a href="" class="ssk ssk-tumblr" data-media=""></a> </div>
          </div>
        </div>
        <div class="modal-footer"> </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="iframeModal" role="dialog">
    <div class="modal-dialog modalwide">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
          <!-- <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-alt"></i> Marriage Education</h4> -->
        </div>
        <div class="modal-body">
          <iframe width="560" height="315" scrolling="yes" frameborder="0" id="idFrame" allowfullscreen></iframe>
        </div>
        <div class="modal-footer"> </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="messageDialog" role="dialog">
    <div class="modal-dialog modalwide">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        </div>
        <div class="modal-body">
          <h1 webwednotice="header">Warning!</h1>
          <p webwednotice="paragraph">WebWed Notice Message.</p>
        </div>
        <div class="modal-footer"> </div>
      </div>
    </div>
  </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <!-- <script src="js/index.js"></script> -->
  <script src="js/event.js"></script>
  <script src="js/ics.js"></script>
  <script src="js/social-share-kit.min.js"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74133839-1', 'auto');
  ga('send', 'pageview');

  if ($.browser.msie == true){
    window.location.href = 'unsupported.php';
  }

  if ($.browser.msedge == true){
    window.location.href = 'unsupported.php';
  }

  SocialShareKit.init();
  </script>
</body>
</html>
<?php ob_flush(); ?>
<!--This site's Design and Source Code Is Property of Riveloper, LLC | Atlanta, GA until otherwise noted.-->
