<?php
session_start();
require_once 'includes/Mobile_Detect.php';
$detect = new Mobile_Detect;
if($detect->isMobile()){
  $mobile = true;
}else{
  $mobile = false;
}

if (isset($_GET['code']) && !isset($_GET['id'])){
  $code = $_GET['code'];
}

if (isset($_GET['id']) && !$mobile){
  $code = $_GET['id'];
  //header("Location: index.php?id=".$code);
  //exit;
}

if (isset($code) && !$mobile){
  header("Location: http://webwedmobile.com/admin/user_admin/guest_login.php?code=".$code);
  exit;
  echo $mobile;
  exit;
}
?>
<!doctype html>
<html style="height:100vh;">
<head>
  <meta charset="UTF-8">
  <title>Web Wed Mobile - RSVP</title>
  <!-- Search engines -->
  <meta name="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
  <!-- Google Plus -->
  <meta itemprop="name" content="Web Wed Mobile - Live streaming weddings.">
  <meta itemprop="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
  <meta itemprop="image" content="images/social_share.png">
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:400,300|Roboto+Condensed|Clicker+Script" rel="stylesheet" type="text/css">

  <!--WebWed Icons and Style Sheets -->
  <meta name="apple-itunes-app" content="app-id=1086176524"/>
  <link rel="alternate" href="android-app://com.webwed.mobile/https/webwedmobile.com" />
  <link rel="alternate" href="android-app://com.webwed.mobile/http/webwedmobile.com" />

  <!--Fav and touch icons-->
  <link rel="apple-touch-icon" sizes="57x57" href="images/icon/favicon//apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="images/icon/favicon//apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="images/icon/favicon//apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="images/icon/favicon//apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="images/icon/favicon//apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="images/icon/favicon//apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="images/icon/favicon//apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="images/icon/favicon//apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="images/icon/favicon//apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="images/icon/favicon//android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon//favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="images/icon/favicon//favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon//favicon-16x16.png">

  <!--web wed style-->
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/join.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <script type="text/javascript" src="js/deeplink.js"></script>
  <script type="text/javascript">
  // deeplink.setup({
  //   iOS: {
  //     appName: "webwed mobile",
  //     appId: "1086176524"
  //   },
  //   Android: {
  //     appId: "com.webwedmobile.android"
  //   }
  // });
  </script>


</head>
<body>
  <div id="wrapper">
    <div class="container"><br>
      <?php if ($mobile){ ?>
        <h2 style="line-height: 20px;font-size:30px;">Open WebWedMobile</h2><br/>
        <p style="line-height:30px;">Please download WebWedMobile from Google Play or the App Store then revisit this page.</p>
      <?php }else{ ?>
      <h2 style="line-height:20px;">Oops! No Event <br/>Available.</h2>
      <p>But we love the enthusiasm. Check the link you were provided. If you continue getting this message contact the wedding party and notify them that the link has either expired or has been shared wrong. Only an officiate or member of the wedding party has the ability to change this.</p>
      <?php } ?>
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="moda	l-content">
          <div class="modal-body row">
            <div class="col-xs-12">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118.48 171.65"><defs><style>.cls-1{fill:#ededed;}</style></defs><title>Ring_Logo</title><path class="cls-1" d="M560,234.31c13.46-12.48,26.08-15.14,39.82-8.69,11.64,5.61,17.11,15,15.14,27.2-16.68-10.24-34.63-15.7-54.4-15.56a98.69,98.69,0,0,0-54.4,15.84c-4.21-7.29.14-18.37,9.67-24.82C529.84,218.75,545.55,220.85,560,234.31Z" transform="translate(-501.56 -222.23)"/><path class="cls-1" d="M512,265.86c28.6-22,72.63-19.49,96.32-.28-2.94,3.08-5.89,6.31-8.83,9.25-32-16-49.49-15.84-78.24.56C518.21,272.31,515.26,269.23,512,265.86Z" transform="translate(-501.56 -222.23)"/><path class="cls-1" d="M568.82,301.61c-3.37,2.66-5.89,4.77-8.83,7-2.66-2.24-5-4.21-8-6.59C557.75,299.09,562.65,299.51,568.82,301.61Z" transform="translate(-501.56 -222.23)"/><path class="cls-1" d="M596.66,289.79l-6.9,6.82a48.45,48.45,0,1,1-58.59.52l-6.48-5.92a57.52,57.52,0,0,0-23.13,45.91c0,31.23,27.36,56.75,58.59,56.75S620,368.82,620,337.59C620,319,610.46,300.11,596.66,289.79Z" transform="translate(-501.56 -222.23)"/><path class="cls-1" d="M581.38,291.35l7.63-7.54c-17.82-10.28-36.8-12.93-56.43-.11l8.47,7.75A48.41,48.41,0,0,1,581.38,291.35Z" transform="translate(-501.56 -222.23)" style="width: 100px; padding:30px 0px 40px 0px;"/></svg>
            <div class="form-group float-label-control col-xs-12">

            </div>
            <div class="col-xs-12" id="need-account"> <a href="http://webwedmobile.com/admin/user_admin/signup.php">Need to sign up for your event?</a> </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>

<!-- <script src="js/script.js"></script> -->
<?php
if ($mobile){
  ?>
  <script>
  // $(document).ready(function(){
  //   deeplink.open("webwedmobile://?code=<?php echo htmlentities($code); ?>");
  // });
  </script>
  <?php
}
?>
<script>
// app.initialize();


$(function () {
  $('.expander').simpleexpand();
  prettyPrint();
});


$(document).ready(function(){

  var scroll_start = 0;
  var startchange = $('#rsvpLink');
  var offset = startchange.offset();
  if (startchange.length){
    $(document).scroll(function() {
      scroll_start = $(this).scrollTop();
      if(scroll_start < offset.top) {
        $(".navbar").css('background-color', '#023a7b)');
      }
    });
  }
});



var love = setInterval(function() {
  var r_num = Math.floor(Math.random() * 10) + 1;
  var r_size = Math.floor(Math.random() * 35) + 10;
  var r_left = Math.floor(Math.random() * 80) + 1;
  var r_bg = Math.floor(Math.random() * 15) + 100;
  var r_time = Math.floor(Math.random() * 5) + 5;

  $('.bg_heart').append("<div class='heart' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg + ",1);-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

  $('.bg_heart').append("<div class='heart' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:rgba(255," + (r_bg - 25) + "," + r_bg +",1);-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

  $('.heart').each(function() {
    var top = $(this).css("top").replace(/[^-\d\.]/g, '');
    var width = $(this).css("width").replace(/[^-\d\.]/g, '');
    if (top <= -100 || width >= 150) {
      $(this).detach();
    }
  });
}, 1500);
//@ sourceURL=pen.js
</script>
</html>
