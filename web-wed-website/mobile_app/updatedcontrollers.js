setInterval(function(){
  if (window.location.href.indexOf('app\/legal') >= 0){
    var elements = document.getElementsByClassName('left-buttons');
    for (var i in elements){
      var element = elements[i];
      if (element != undefined){
        var children = element.children;
        if (children != undefined){
          var child = children[0];
          if ($(child).hasClass('hide')){
            $(child).removeClass('hide');
          }
        }
      }
    };
  }

  if ((window.location.href.indexOf('login') == -1)){
    $('div.buttons.buttons-left.header-item').css('display', 'block');
    $('.bar.bar-stable .title').css('margin-top', '0px !important');
  }
}, 200);

function loadCSS(href) {

  var cssLink = $("<link>");
  $("head").append(cssLink); //IE hack: append before setting href

  cssLink.attr({
    rel:  "stylesheet",
    type: "text/css",
    href: href
  });

};

if (ionic.Platform.isAndroid()){
  loadCSS("http://webwedmobile.com/mobile_app/android.css");
}
