<?php 
session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Web Wed Mobile - Special Moment Live Broadcast.</title>
<!-- Search engines -->
<meta name="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
<!-- Google Plus -->
<meta itemprop="name" content="Web Wed Mobile - Live stream weddings.">
<meta itemprop="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
<meta itemprop="image" content="images/social_share.png">
<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
.video-container {
	position:relative;
	padding-bottom:56.25%;
	padding-top:30px;
	height:0;
	overflow:hidden;
}

.video-container iframe, .video-container object, .video-container embed {
	position:absolute;
	top:0;
	left:0;
	width:100%;
	height:100%;
}
</style>
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<? require_once 'includes/Mobile_Detect.php'; $detect = new Mobile_Detect;?>

<!--WebWed Icons and Style Sheets -->

<!--Fav and touch icons-->
<link rel="apple-touch-icon" sizes="57x57" href="images/icon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/icon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/icon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/icon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/icon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/icon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/icon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/icon/favicon//android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon-16x16.png">

<!--web wed style-->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
<link rel="stylesheet" href="css/main.css">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:400,300|Roboto+Condensed|Clicker+Script" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script> 
<script src="js/jquery.browser.js"></script> 




</head>
<body>
<div id="barwrap" style="display:none;">
  <div class="bar"> <span id="head-image"><i class="fa fa-heart color_navy"></i></span> <span id="text">
 
    </span> <span id="otherimg"></span> <span id="ok"><a href="#">X</a></span> </div>
</div>
<!-- HEADER START -->
<nav class="navbar navbar-default navbar-fixed-top" id="header" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span> </button >
      <a class="navbar-brand" href="index.php"> <img class="img-responsive" src="images/icon/WebWed_logo_white.png" alt="Web Wed Logo"> </a> </div>
    <div class="collapse navbar-collapse" id="navbar">
      <ul class="nav navbar-nav">
        <li><a href="#story">About</a></li>
        <li><a href="#event">Events</a></li>
        <li><a href="#" data-toggle="modal" data-target="#legal-modal">Resources</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <? if($detect->isMobile()){ } else{?><? } ?>
        <li><a href="https://twitter.com/webwedmobile"><i class="fa fa-twitter"></i></a></li>
        <li><a href="https://www.facebook.com/webwedmobile"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://instagram.com/webwedmobile"><i class="fa fa-instagram"></i></a></li>
        <li></li>
         <? if($detect->isMobile()){ }else{ ?><li id="sign-in-li"><a href="#" id="sign-in-red" onclick="addModalBackdropHide();" data-toggle="modal" data-target="#sign-in-modal"><span class="pull-right"><i class="fa "></i><span id="accountname"></span></span></a></li> <? } ?>
      </ul>
    </div>
    <!--/.nav-collapse --> 
  </div>
</nav>

<!-- HEADER END -->
<? if($detect->isMobile()) { echo ""; } else{ ?>
<div id="video_nav_tweet_wrapper" class="wrapper" style="height:100vh;">
  <div class="wrapper"><!-- wrapper start  -->
    <div id="background-carousel" class="">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="item item-no1 active"></div>
          <div class="item item-no2"></div>
          <div class="item item-no3"></div>
          <div class="item item-no4"></div>
        </div>
      </div>
    </div>
 

    <!-- Action bars that only appear on mobile at the right time -->
    <div class="action-bar hidden"> <span class="pull-left">Return Comments</span> <a href="#"> <em id="chatArrow" class="fa fa-arrow-circle-o-up fa-rotate-180 pull-right"></em> </a> </div>
    
    <!-- /.background-carousel end -->
    <div id="video_container" class="" style="margin-top: 82px;">
      <style>.codegena{position:relative;width:100%;height :0;padding-bottom:56.27198%;}.codegena iframe{position:absolute;top:0;left:0;width:100%;height:100%;}/*Youtube Embed Code : Created with Codegena.com */</style><div class="codegena"><iframe width='500' height='294' src="https://www.youtube.com/embed/mqW0epkw0Lo?&theme=dark&color=white&autoplay=1&autohide=1&modestbranding=1&fs=0&showinfo=0&rel=0"frameborder="0"></iframe></div>
    </div>
    
    <!--WebWed Controls-->
    <div id="webwed_control" style="height: auto !important;">
      <ul class="list-inline">
        <li> <a href="#" id="chat" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Chat with other guests." onClick="toggle_visibility('chatWindow')"> <img src="images/tab-chat-white.svg" width="35" class="webwed_icon"onClick="toggle_visibility('chatWindow')"> </a></li>
        <li> <a href="#" id="gift" data-toggle="tooltip" onClick="showGiftRegistry()" class="webwed_icon top disabled" data-placement="top"  data-original-title="Public registry."> <img src="images/tab-gift-white.svg" width="35" class="webwed_icon"> </a></li>
        <li> <a href="#" id="wwHeart" class="webwed_icon">
          <div class="icon-heart"> <i  id="forever_heart" class="material-icons blue"> </i> <i  id="together_heart" class="material-icons yellow"> </i> <i  id="whereever_heart" class="material-icons fav disabled"> </i> </div>
          </a></li>
        <li> <a href="#" id="event" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Download To Calendar"> <img src="images/tab-event-white.svg" width="35" class="webwed_icon"> </a></li>
        <li> <a href="#" id="sharelink" data-toggle="modal" data-target="#wwSocialShare" class="disabled"> <img src="images/tab-link-white.svg" width="35" class="webwed_icon"> </a> </li>
      </ul>
    </div>
    
    <!--Chat Dialog-->
    
    <div class="chat_window" id="chatWindow"> 
      <!--<div class="top_menu">
        <div class="buttons">
          <div class="button close"></div>
          <div class="button minimize"></div>
          <div class="button maximize"></div>
        </div>
        <div class="title">Public Chat</div>
      </div>-->
      <ul class="messages">
        <!--  <li class="message left appeared hideMe fade">
          <div class="avatar"></div>
          <div class="text_wrapper">
            <div class="text">Hello Jason! :)</div>
          </div>
        </li>
        <li class="message right appeared hideMe fade">
          <div class="avatar"></div>
          <div class="text_wrapper">
            <div class="text">Hi. Are you excited about the launch?</div>
          </div>
        </li>
        <li class="message left appeared hideMe fade">
          <div class="avatar"></div>
          <div class="text_wrapper">
            <div class="text">Couldn't be more excited!</div>
          </div>
        </li>-->
      </ul>
      <div class="bottom_wrapper clearfix">
        <div class="message_input_wrapper">
          <input class="message_input" placeholder="Share a message..." id="messagetoshare">
        </div>
        <div class="send_message" id="share">
          <div class="icon"></div>
          <div class="text">Share</div>
        </div>
      </div>
    </div>
    <div class="message_template">
      <li class="message">
        <div class="avatar"></div>
        <div class="text_wrapper">
          <div class="text"></div>
        </div>
      </li>
    </div>
    
    <!--Terminate Chat--> 
  </div>
  <!-- wrapper end --> 
</div>
<!-- video_nav_tweet_wrapper end --> 
<? } ?> 
<!-- .rsvpLink start -->
<div class="bg_heart">
  <section id="rsvpLink" class="rsvpLink">
    <div class="color-overlay">
      <div class="container">
        <div class="border_frame">
          <div class="row text-center">
            <h3 class="color_navy evite">WATCH IT LIVE</h3>
            <div class="row">
              <div class="col-sm-12 text-center">
                <h3 class="color_white evite_couple" id="CoupleName">SURPRISE ENGAGEMENT</h3>
                <h3 class="evite_small color_black" id="WeddingDate">ATLANTA, GEORGIA</h3>
                <h3 class="evite_small color_black" id="HideText2"><span class="color_navy "> <em class="fa fa-heart"></em></span>Keep up with Web Wed and fill out our contact us form.<span class="color_navy "> <em class="fa fa-heart"></em></span></h3>
                <h3 class="color_white evite_date" id="HideText"></h3>
              </div>
            </div>
            <!-- row end -->
            
            <div class="row"> 
              
              <!-- <div class="user-img col-xs-6 col-sm-6"> <img alt="To be wed" src="images/groom.png"> </div>
              <div class="user-img col-xs-6 col-sm-6"> <img alt="To be wed" src="images/bride.png"> </div>--> 
              
              <!-- /.thumb-img --> 
              <!-- /.thumbnail-block -->
              
              <div class="col-sm-12 text-center"><font color="#ffffff" face="roboto condensed, helvetica, sans-serif">This is your chance to become one of the first couples to break through the walls and make your special day a special event. </font><br>
              </div>
              <!--<div class="col-sm-12 text-center"> <a href="images/WebWed.ics" class="btn default-btn">RSVP</a> </div>--> 
            </div>
            <!-- row end --> 
            
          </div>
          <!-- row text-center end--> 
        </div>
        <!-- border_frame end--> 
      </div>
      
      <!-- /.color-overlay end --> 
      
    </div>
  </section>
</div>
<!-- .rsvpLink end --> 

<!-- banner end --> 

<!-- story start -->
<section id="story" class="story lightsmoke">
  <div class="container">
    <div>
      <div class="col-sm-12 text-center">
        <div class="wow animated fadeInUp text-center">
          <h3 class="color_navy">Together. Wherever. Whenever. Forever.</h3>
        </div>
        <!-- /.heading end --> 
      </div>
    </div>
    <div class="row">
      <h2 class="color_teal text-center" id="story-title">What love brought together, no one can keep apart.</h2>
      <div class="video-container1">
      
      <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/jhw61SUe-cQ?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>
      <div class="col-sm-12">
        <div class="timeline-story">
          <div class="timeline"> <span class="love-sign wow animated pulse"></span>
            <div class="timeline-left pull-left">
              <div class="timeline-block-left">
                <div class="timeline-thumbnail wow animated fadeInLeft" data-wow-delay="0s" data-wow-duration="1s"> <img class="img-thumbnail" src="images/love_story_01.jpg" alt="Love Story"> </div>
              </div>
            </div>
            <!-- /.timeline-left -->
            <div class="timeline-right pull-right">
              <div class="timeline-block-right wow animated fadeInRight" data-wow-delay="0s" data-wow-duration="1s">
                <div class="timeline-head">
                  <h4>Create An Account</h4>
                </div>
                <div class="timeline-body">Sign-up for a Web Wed Account on-line. Select whether you need to schedule a union, are an officiant or are guest. <a href="#" data-toggle="modal" data-target="#sign-in-modal">Get Started.</a></div>
              </div
            </div>
            <!-- /.timeline-right -->
            <div class="clearfix hidden-xs"></div>
          </div>
          <!-- /.timeline end -->
          <div class="timeline"> <span class="love-sign wow animated pulse"></span>
            <div class="timeline-left reverse pull-left">
              <div class="timeline-block-left">
                <div class="timeline-thumbnail wow animated fadeInLeft" data-wow-delay="0.6s" data-wow-duration="1s"> <img class="img-thumbnail" src="images/love_story_02.jpg" alt="Love Story"> </div>
              </div>
            </div>
            <!-- /.timeline-left -->
            <div class="timeline-right reverse pull-right">
              <div class="timeline-block-right wow animated fadeInRight" data-wow-delay="0.6s" data-wow-duration="1s">
                <div class="timeline-head">
                  <h4>Download the Web Wed App</h4>
                </div>
                <div class="timeline-body">Once you have an account, download the Web Wed Mobile application for the best experience. *<i class="fa fa-android"></i>Android or <i class="fa fa-apple"></i>iOS.</div>
              </div>
            </div>
            <!-- /.timeline-right -->
            <div class="clearfix hidden-xs"></div>
          </div>
          <!-- /.timeline end -->
          <div class="timeline"> <span class="love-sign wow animated pulse"></span>
            <div class="timeline-left pull-left">
              <div class="timeline-block-left">
                <div class="timeline-thumbnail wow animated fadeInLeft" data-wow-delay="1" data-wow-duration="1s"> <img class="img-thumbnail" src="images/love_story_03.jpg" alt="Love Story"> </div>
              </div>
            </div>
            <!-- /.timeline-left -->
            <div class="timeline-right pull-right">
              <div class="timeline-block-right wow animated fadeInRight" data-wow-delay="1" data-wow-duration="1s">
                <div class="timeline-head">
                  <h4>Share With Others</h4>
                </div>
                <div class="timeline-body">Make your video broadcast private or share all of your joy publicly through social media and email with the entire world. It is all up to you. If you select a fully public wedding live video stream then your broadcast will be available using a link and can be seen around the world.</div>
              </div>
            </div>
            <!-- /.timeline-right -->
            <div class="clearfix hidden-xs"></div>
          </div>
          <!-- /.timeline end --> 
        </div>
        <!-- /.timeline-story --> 
      </div>
    </div>
  </div>
</section>
<!-- story end --> 

<!-- people start --><!-- people end --> 

<!-- event start -->
<section id="event" class="event lightsmoke">
  <div class="container" style="overflow:visible;">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="wow animated fadeInUp">
          <h3 class="color_white" id="event-title">Web Wed <span class="color_navy wow pulse" data-wow-iteration="5" data-wow-duration="1500ms"><i class="fa fa-heart"></i></span> Highlights</h3>
        </div>
        <!-- /.heading end --> 
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-4">
        <div class="event-block wow animated fadeInUp" data-wow-delay="0s" data-wow-duration="1s">
          <div class="event-img"> <img class="img-responsive" src="images/event_01.png" alt="Event name"> </div>
          <!-- /.event-img end -->
          <div class="event-body">
            <h5><a href="#rsvplink">Sponsorships</a></h5>
            <p>After our premiere couple's soft launch party, Web Wed mobile is actively seeking and accepting sponsorships prior to our official Launch</p>
            <span class="event-date">
            <div class="date-wrap"> <span>Available</span> <span class="event-year">Now</span> </div>
            </span> </div>
          <!-- /.event-body end --> 
        </div>
        <!-- /.event-block end --> 
      </div>
      <!-- <div class="col-sm-6 col-md-4">
        <div class="event-block wow animated fadeInUp" data-wow-delay=".6s" data-wow-duration="1s">
          <div class="event-img"> <img class="img-responsive" src="images/event_02.png" alt="Event name"> </div>
          
          <div class="event-body">
            <h5><a href="#">Sponsorship</a></h5>
            <p>Donations support our mission and help advance our progress. Be a part from the beginning of a world changing service.</p>
            <span class="event-date">
            <div class="date-wrap"> <span>DONATE</span> <span class="event-year">NOW</span> </div>
            </span> </div>
          
        </div>
       
      </div>-->
      <div class="col-sm-6 col-md-4">
        <div class="event-block wow animated fadeInUp" data-wow-delay=".8s" data-wow-duration="1s">
          <div class="event-img"><a href="#" data-toggle="modal" data-target="#youtubeModal"><img class="img-responsive" src="images/event_04.png" alt="Event name"></a> </div>
          <!-- /.event-img end -->
          <div class="event-body">
            <h5><a href="#">FREE Counseling</a></h5>
            <p>Web Wed is happy to provide FREE marriage education and counseling with certificate of completion for all of our couples.</p>
            <span class="event-date">
            <div class="date-wrap"> <span>AVAILABLE</span> <span class="event-year">SOON</span> </div>
            </span> </div>
          <!-- /.event-body end --> 
        </div>
        <!-- /.event-block end --> 
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="event-block wow animated fadeInUp" data-wow-delay=".8s" data-wow-duration="1s">
          <div class="event-img"> <img class="img-responsive" src="images/event_03.png" alt="Event name"> </div>
          <!-- /.event-img end -->
          <div class="event-body">
            <h5>Our <span class="color_red"><i class="fa fa-heart"></i></span>Beliefs</h5>
            <p>Our core belief is that no matter who you are, where you are love defies distance, all cultures, beliefs, races &amp; genders. Join Web Wed Mobile's equality launch event. </p>
            <span class="event-date">
            <div class="date-wrap"> <span>June 26th</span> <span class="event-year">2016</span> </div>
            </span> </div>
          <!-- /.event-body end --> 
        </div>
        <!-- /.event-block end --> 
      </div>
    </div>
  </div>
  </div>
</section>
<!-- event end --> 

<!-- contact start -->
<section id="contact" class="contact">
  <div class="container">
    <div class="row">
      <h3 class="color_navy" id="contact-title" align="center">We'd <span class="color_navy wow pulse" data-wow-iteration="infinite" data-wow-duration="1200"><i class="fa fa-heart"></i></span> To Hear From You</h3>
      <div class="row">
        <div class="col-sm-12">
          <div class="contact-block">
            <h5><strong>Connect With Us</strong><br>
            </h5>
            <form class="contact_form" role="form" onsubmit='event.preventDefault();'>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" class="form-control" id="contactnamefield" name="name" placeholder="Your Full Name" required="required">
                  </div>
                  <div class="form-group">
                    <select name="numberOfGuest" id="contactquestionfield" tabindex="1" class="form-control">
                      <option value="">Select One</option>
                      <option value="How To Register">How To Register</option>
                      <option value="What is the Cost">What is the Cost</option>
                      <option value="Will It Be Legal">Will It Be Legal</option>
                      <option value="Other / Not Listed">Other / Not Listed</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="contactemailfield" name="name" placeholder="Enter your email address" required="required">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <textarea class="form-control" name="message" id="contactmessage" placeholder="Your Message"></textarea>
                  </div>
                  <div class="form-group">
                    <input type="submit" name="submit" onclick="submitcontact();" class="btn btn-lg btn-block" value="Send">
                  </div>
                </div>
                <div class="col-sm-12">
                  <p class="contact-success">Your Message has been successfully sent!</p>
                  <p class="contact-error">Oops! Something went wrong. Please try again later.</p>
                </div>
              </div>
            </form>
          </div>
          <!-- /.contact-block end --> 
        </div>
      </div>
    </div>
  </div>
</section>
<!-- contact end --> 

<!-- footer start -->
<footer>
  <div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center"> <span class="footer-logo"> <img src="images/icon/WebWed_logo_white.svg" alt="WebWed" width="250"> </span> </div>
        <h2 class="color_teal medium">When Love Can't Wait.</h2>
        <div class="col-sm-offset-3 col-sm-6"> <a href="https://www.facebook.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-facebook"></i></a> <a href="https://twitter.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-twitter"></i></a> <a href="https://instagram.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-instagram"></i></a> </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-center">
          <ul class="nav nav-pills">
            <li role="presentation"><a href="#story">About</a></li>
            <li role="presentation"><a href="#rsvplink">RSVP</a></li>
            <li role="presentation"><a href="#" data-toggle="modal" data-target="#legal-modal">Legal Resources</a></li>
          </ul>
          <!-- footer nav end --> 
        </div>
        <div class="col-sm-12 text-center">
          <p class="copyright">&copy;
            2012&#x2011;<? echo date("Y"); ?> Web Wed, LLC. All rights reserved. Provisional Patent Awarded. Service available only within the contiguous United States. A <a href="http://www.marriagelicensenow.com">state issued marriage license</a> is required to use this service. Use of this site and mentioned services constitutes that you agree to our terms of service and privacy policy. Web or mobile application technical related issues can be submitted to <a href="https://www.riveloper.com" >Riveloper, LLC.</a></p>
          <p class="copyright"><a href="#" data-toggle="modal" data-target="#privacy-modal">Privacy Policy</a> | <a href="#" data-toggle="modal" data-target="#terms-modal">Terms of Service</a> | <a href="#" data-toggle="modal" data-target="#disclaimer-modal">Disclaimer</a></p>
          <br>
        </div>
      </div>
    </div>
    <div id="go-to-top"> <a href="#banner"><i class="fa fa-angle-up"></i></a> </div>
  </div>
  <!-- /.color-overlay end --> 
</footer>
<!-- footer end --> 

<!--Account Modal -->
<div id="sign-in-modal" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 118.48 171.65">
          <defs>
            <style>
.cls-1{fill:#ededed;}
</style>
          </defs>
          <title>Ring_Logo</title>
          <path class="cls-1" d="M560,234.31c13.46-12.48,26.08-15.14,39.82-8.69,11.64,5.61,17.11,15,15.14,27.2-16.68-10.24-34.63-15.7-54.4-15.56a98.69,98.69,0,0,0-54.4,15.84c-4.21-7.29.14-18.37,9.67-24.82C529.84,218.75,545.55,220.85,560,234.31Z" transform="translate(-501.56 -222.23)"/>
          <path class="cls-1" d="M512,265.86c28.6-22,72.63-19.49,96.32-.28-2.94,3.08-5.89,6.31-8.83,9.25-32-16-49.49-15.84-78.24.56C518.21,272.31,515.26,269.23,512,265.86Z" transform="translate(-501.56 -222.23)"/>
          <path class="cls-1" d="M568.82,301.61c-3.37,2.66-5.89,4.77-8.83,7-2.66-2.24-5-4.21-8-6.59C557.75,299.09,562.65,299.51,568.82,301.61Z" transform="translate(-501.56 -222.23)"/>
          <path class="cls-1" d="M596.66,289.79l-6.9,6.82a48.45,48.45,0,1,1-58.59.52l-6.48-5.92a57.52,57.52,0,0,0-23.13,45.91c0,31.23,27.36,56.75,58.59,56.75S620,368.82,620,337.59C620,319,610.46,300.11,596.66,289.79Z" transform="translate(-501.56 -222.23)"/>
          <path class="cls-1" d="M581.38,291.35l7.63-7.54c-17.82-10.28-36.8-12.93-56.43-.11l8.47,7.75A48.41,48.41,0,0,1,581.38,291.35Z" transform="translate(-501.56 -222.23)" style="width: 100px; padding:30px 0px 40px 0px;"/>
        </svg>
        <div id="login_initial_screen" class="form-group float-label-control">
          <form role="form" class="col-xs-12 col-sm-8 col-sm-offset-2 text-center" id="modal-form">
            <div class="row">
              <div class="col-xs-12"> <a href="#" class="button button-full button-calm form-control modal_button" data-toggle="modal" data-target="#account-modal" onClick="hideLoginModal();">New Account</a> </div>
            </div>
            <div class="row">
              <div class="col-xs-12"> <a href="#" class="button button-full button-calm form-control modal_button" data-toggle="modal" data-target="#login-modal" onClick="hideLoginModal();">Wedding Party Login</a> </div>
            </div>
            <div class="row">
              <div class="col-xs-12"> <a href="#guest_login" class="button button-full button-assertive form-control form-control toggle modal_button" onClick="toggle_visibility('guest_login')">Invited Guests</a> </div>
            </div>
          </form>
        </div>
        <div id="guest_login" style="display:none;">
          <form name="authorizationForm" onsubmit="event.preventDefault(); loginAsGuest()" novalidate>
            <div class="row">
              <div class="col-xs-12">
                <div class="">
                  <div id="modal"></div>
                  <div id="email"></div>
                  <label class="item item-input">
                    <input type="email" id="emailfield" placeholder="Email address:" class="modal_form_field" name="email" required="">
                  </label>
                </div>
              </div>
              <div class="wedding-code">
                <label class="item item-input">
                  <input type="text" id="passcodefield" class="modal_form_field modal_form_field_email" name="passcode" placeholder='00000' required="">
                </label>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <button class="button button-full button-positive modal_button">We Do.</button>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12"> 
                <!-- <p class="" ng-click="resendMyCode()">Send My Code</p>--> 
              </div>
            </div>
          </form>
        </div>
        <div class="col-xs-12 text-center" id="terms">
          <p>Use of this site constitutes acceptance of our <a href="#" data-toggle="modal" data-target="#terms-modal">Terms of Service</a> &amp; <a href="#" data-toggle="modal" data-target="#privacy-modal">Privacy Policy</a></p>
          
          <!--    <div class="form-group float-label-control col-xs-12">
            <label for="email"></label>
            <input type="email" class="form-control" id="email" placeholder="Email address:">
          </div>
          <div class="form-group float-label-control col-xs-12">
            <label for="password"></label>
            <input type="password" class="form-control" id="password" placeholder="Password:">
          </div>
          <div class="col-xs-12">
            <button type="submit" class="btn btn-default col-xs-12" id="i-do">I Do</button>
          </div>
          <div class="col-xs-12" id="need-account"> <a href="#" data-toggle="modal" data-target="#sign-in-modal">Need to sign-up for your wedding?</a> </div>--> 
        </div>
      </div>
    </div>
  </div>
</div>

<!--New Account Modal -->
<div id="account-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modalwide"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <iframe src="./admin/sign_up.php" width="99.6%" style="height: 100vh;" scrolling="yes" frameborder="0" id="idFrame"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Wedding Party Login Modal -->
<div id="login-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modalwide"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <iframe src="./admin/user_login.php" width="99.6%" style="height: scrolling="yes" 100vh;" frameborder="0"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Legal Modal -->
<div id="legal-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modalwide"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="" id="legal_reosources">
          <h2>Legal Resources</h2>
          <p>While we can't provide you legal advice we can offer some resources to help you understand our service. We do not issue marriage license or offer legal services.</p>
          <p><strong>Proxy Marriages</strong><br>
            A proxy marriage is a marriage where the parties were not physically present in the presence of each other. Someone stands in for the other party because either the bride or the groom is not physically present for the wedding. During the solemnization of the marriage, based upon a power of attorney, an agent acts on behalf of one of the parties. The marriage is presumed valid if it is valid in the country that performs the ceremony. Mostly compared and used in military marriages, and maximum security facility. Marriage by proxy, with only one party to the marriage making an appearance, is possible in the United States. It is always necessary for one of the two parties (either husband or wife) to actually appear before the Civil Authorities.</p>
          <p>Written Authorization for Third Person to Act as Proxy<br>
            If a party to a marriage is unable to be present at the solemnization, that party may authorize in writing a third person to act as his proxy. If the person solemnizing the marriage is satisfied that the absent party is unable to be present and has consented to the marriage, he or she may solemnize the marriage by proxy. If he or she is not satisfied, the parties may petition the court for an order permitting the marriage to be solemnized by proxy.</p>
          <p><strong>Proxy Marriages Allowed in Four States</strong><br>
            Marriage by proxy has been around a long time. Proxy marriages are most common during wartime. Marriages by proxy are allowed in California, Colorado, Texas, and Montana.</p>
          <p><strong>Legal Recognition</strong><br>
            Whether a state or country will recognize a marriage by proxy seems to depend on whether or not the law of the locale requires that both parties be present to apply for a license or to give their consent at the ceremony. Some states recognize a proxy marriage that was done in another state. Other states only recognize them as common law marriage. U.S. military personnel may annul a proxy marriage provided there is no consummation, no cohabitation, or no treatment as husband and wife after the marriage ceremony.</p>
          <p><strong>Immigration Concerns</strong><br>
            Unconsummated proxy marriages are not recognized for immigration purposes in most countries, including the United States. However, a party of an unconsummated proxy marriage may enjoy immigration benefits as a fiancée of the opposite party is a U.S. citizen.</p>
          <p><strong>Commoditized Law</strong><br>
            Common law marriage should not be confused with non-marital relationship contracts, which involves two people living together without holding themselves out to the world as spouses and/or without legal recognition as spouses in the jurisdiction where the contract was formed. Non-marital relationship contracts are not necessarily recognized from one jurisdiction to another whereas common law marriages are, by definition, legally valid marriages worldwide - provided the parties complied with the requirements to form a valid marriage while living in a jurisdiction that still allows this irregular form of marriage to be contracted - as was historically the case under the common law of England (hence the name, &quot;common law marriage&quot;).</p>
          <p>Common law and statutory marriage have the following characteristics in common:<br>
          <ol>
            <li>Both parties must freely consent to the marriage</li>
            <li> Both parties must be of legal age to contract a marriage or have parental consent to marry</li>
            <li> Neither party may be under a disability that prevents him or her from entering into a valid marriage - E.g. they must both be of sound mind, neither of them can be currently married (except in Saskatchewan[2]), and some jurisdictions do not permit prisoners to marry.</li>
          </ol>
          <ol>
            Otherwise, common law marriage differs from statutory marriage as follows:
            <li>There is no marriage license issued by a government and no marriage certificate filed with a government</li>
            <li> There is no formal ceremony to solemnize the marriage before witnesses</li>
            <li> The parties must hold themselves out to the world as husband and wife (this is not a requirement of statutory marriage)</li>
            <li> Most jurisdictions require the parties to be cohabiting at the time the common law marriage is formed. Some require cohabitation to last a certain length of time (E.g. three years) for the marriage to be valid. But cohabitation alone does not create a marriage.</li>
          </ol>
          <p>The parties must intend their relationship to be, and to be regarded as, a legally valid marriage.<br>
            When a marriage is validly contracted, whether according by statutory provision or according to common law, the marriage can be dissolved only by a formal legal proceeding in a court of competent jurisdiction - usually a family or probate court.<br>
            In the U.S. state of Texas, a new provision was added to the Family Code; either partner in a common law marriage has two years after separation to file an action in order to prove that the common law marriage existed. To use the provision, the separation must have occurred after September 1, 1989.<br>
            Since the mid-1990s, the term &quot;common law marriage&quot; has been used in parts of Europe and Canada to describe various types of domestic partnership between persons of the same sex as well those of the opposite sex. Although these interpersonal statuses are often, as in Hungary, called &quot;common law marriage&quot; they differ from true common law marriage in that they are not legally recognized as &quot;marriages&quot; but are a parallel interpersonal status, known in most jurisdictions as &quot;domestic partnership&quot;, &quot;registered partnership&quot;, &quot;conjugal union&quot; or &quot;civil union&quot; etc.<br>
            Not all agreements break statutes. Some are illegal because they break public policy, which is generally &quot;to discourage any interference with the freedom of choice&quot; (Saskatchewan, Canada, excepted). An agreement forbidding a party to marry or bribing a party to refrain from marriage is considered &quot;Interference with Marriage Relation&quot; or an &quot;Agreement in Restraint of Marriage&quot;; such agreements are typically held to be non-bonding.</p>
          <p> <strong>Marriage</strong><br>
            Marriage can be recognized by a state, an organization, a religious authority, a tribal group, a local community or peers. It is often viewed as a contract. Civil marriage, which does not exist in some countries, is marriage without religious content carried out by a government institution in accordance with the marriage laws of the jurisdiction, and recognized as creating the rights and obligations intrinsic to matrimony. Marriages can be performed in a secular civil ceremony or in a religious setting via a wedding ceremony.</p>
          <ol>
            <li><a href="http://gaylife.about.com/od/gaymarriagebyregion/ig/Legal-Marriage" target="_blank">http://gaylife.about.com/od/gaymarriagebyregion/ig/Legal-Marriage</a></li>
            <li><a href="http://www.unmarried.org/common-law-marriage-fact-sheet.html" target="_blank">http://www.unmarried.org/common-law-marriage-fact-sheet.html</a></li>
            <li><a href="http://www.proxymarriagehq.com/proxy-mar" target="_blank">http://www.proxymarriagehq.com/proxy-mar</a>riage-law/</li>
            <li><a href="http://www.usmarriagelaws.com/" target="_blank">http://www.usmarriagelaws.com/</a> </li>
            <li><a href="http://www.marriageequality.org/Same Sex Marriage" target="_blank">http://www.marriageequality.org/Same Sex Marriage</a></li>
            <li><a href="http://www.proxymarriage.com/index.php/double-proxy-marriage" target="_blank">http://www.proxymarriage.com/index.php/double-proxy-marriage</a></li>
          </ol>
          <p>In the United States, same-sex marriage has been legal nationwide since June 26, 2015, when the United States Supreme Court ruled in Obergefell v. Hodges that state-level bans on same-sex marriage are unconstitutional. The court ruled that the denial of marriage licenses to same-sex couples and the refusal to recognize those marriages performed in other jurisdictions violates the Due Process and the Equal Protection clauses of the Fourteenth Amendment of the United States Constitution. The ruling overturned a precedent, Baker v. Nelson.</p>
          <p>While civil rights campaigning took place from the 1970s,[4] the issue became prominent from around 1993, when the Hawaii Supreme Court ruled that the prohibition was unconstitutional. The ruling led to federal actions and actions by several states, to restrict marriage to male-female couples, in particular the Defense of Marriage Act (DOMA). During the period 2003 - 2015, various lower court decisions, state legislation, and popular referendums had already legalized same-sex marriage to some degree in thirty-eight out of fifty U.S. states, in the U.S. territory Guam, and in the District of Columbia. In 2013 the Supreme Court overturned a key provision of DOMA, declaring part of it unconstitutional and in breach of the Fifth Amendment in United States v. Windsor because it &quot;single[d] out a class of persons&quot; for discrimination, by refusing to treat their marriages equally under federal law when state law had created them equally valid. The ruling led to the federal government's recognition of same-sex marriage, with federal benefits for married couples connected to either the state of residence or the state in which the marriage was solemnized. However the ruling focused on the provision of DOMA responsible for the federal government refusing to acknowledge State sanctioned same-sex marriages, leaving the question of state marriage laws itself to the individual States. The Supreme Court addressed that question two years later in 2015, ruling, in Obergefell, that same-sex married couples were to be constitutionally accorded the same recognition as opposite-sex couples at state/territory levels, as well as at federal level.</p>
          <p> <strong>Proxy marriage</strong><br>
            A proxy wedding or (proxy marriage) is a wedding in which one or both of the individuals being united are not physically present, usually being represented instead by other persons. If both partners are absent a double proxy wedding occurs.</p>
          <p>Marriage by proxy is usually resorted to either when a couple wish to marry but one or both partners cannot attend for reasons such as military service, imprisonment, or travel restrictions; or when a couple lives in a jurisdiction in which they cannot legally marry.</p>
          <p>Proxy weddings are not recognized as legally binding in most jurisdictions: both parties must be present. A proxy marriage contracted elsewhere may be recognized where proxy marriage within the jurisdiction is not; for example, Israel recognizes proxy marriages abroad between Israelis who might not have been permitted to marry in Israel.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Terms of Service Modal -->
<div id="terms-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modalwide"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="" id="terms_service">
          <h2>Terms of Service</h2>
          <p> Last updated: December 02, 2015 </p>
          <p> Please read these Terms and Service ("Terms", "Terms and Service") carefully before using the www.webwedmobile.com website and the WEB WED mobile application (together, or individually, the "Service") operated by WEB WED, LLC ("us", "we", or "our"). </p>
          <p> Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service. <br>
            By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service. </p>
          <h5>Communications</h5>
          <p> By creating an Account on our service, you agree to subscribe to newsletters, marketing or promotional materials and other information we may send. However, you may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send. </p>
          <h5>Purchases</h5>
          <p> If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information. </p>
          <p> You represent and warrant that: (i) you have the legal right to use any credit card(s) or other payment method(s) in connection with any Purchase; and that (ii) the information you supply to us is true, correct and complete. </p>
          <p> The service may employ the use of third party services for the purpose of facilitating payment and the completion of Purchases. By submitting your information, you grant us the right to provide the information to these third parties subject to our Privacy Policy. </p>
          <p> We reserve the right to refuse or cancel your order at any time for reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons. <br>
            We reserve the right to refuse or cancel your order if fraud or an unauthorized or illegal transaction is suspected. </p>
          <h5>Availability, Errors and Inaccuracies</h5>
          <p> We are constantly updating product and service offerings on the Service. We may experience delays in updating information on the Service and in our advertising on other web sites. The information found on the Service may contain errors or inaccuracies and may not be complete or current. Products or services may be mis-priced, described inaccurately, or unavailable on the Service and we cannot guarantee the accuracy or completeness of any information found on the Service. </p>
          <p> We therefore reserve the right to change or update information and to correct errors, inaccuracies, or omissions at any time without prior notice. </p>
          <h5>Contests, Sweepstakes and Promotions</h5>
          <p> Any contests, sweepstakes or other promotions (collectively, "Promotions") made available through the Service may be governed by rules that are separate from these Terms & Conditions. If you participate in any Promotions, please review the applicable rules as well as our Privacy Policy. If the rules for a Promotion conflict with these Terms and Conditions, the Promotion rules will apply. </p>
          <h5>Subscriptions</h5>
          <p> Some parts of the Service are billed on a subscription basis ("Subscription(s)"). You will be billed in advance on a recurring and periodic basis ("Billing Cycle"). Billing cycles are set on a monthly basis. </p>
          <p> At the end of each Billing Cycle, your Subscription will automatically renew under the exact same conditions unless you cancel it or WEB WED, LLC cancels it. You may cancel your Subscription renewal either through your on-line account management page or by contacting WEB WED, LLC customer support team. </p>
          <p> A valid payment method, including credit card or PayPal, is required to process the payment for your Subscription. You shall provide WEB WED, LLC with accurate and complete billing information including full name, address, state, zip code, telephone number, and a valid payment method information. By submitting such payment information, you automatically authorize WEB WED, LLC to charge all Subscription fees incurred through your account to any such payment instruments. </p>
          <p> Should automatic billing fail to occur for any reason, WEB WED, LLC will issue an electronic invoice indicating that you must proceed manually, within a certain deadline date, with the full payment corresponding to the billing period as indicated on the invoice. </p>
          <h5>Fee Changes</h5>
          <p> WEB WED, LLC, in its sole discretion and at any time, may modify the Subscription fees for the Subscriptions. Any Subscription fee change will become effective at the end of the then-current Billing Cycle. <br>
            WEB WED, LLC will provide you with a reasonable prior notice of any change in Subscription fees to give you an opportunity to terminate your Subscription before such change becomes effective. </p>
          <p> Your continued use of the Service after the Subscription fee change comes into effect constitutes your agreement to pay the modified Subscription fee amount. </p>
          <h5>Refunds</h5>
          <p> Except when required by law, paid Subscription fees are non-refundable. </p>
          <h5>Content</h5>
          <p> Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You are responsible for the Content that you post on or through the Service, including its legality, reliability, and appropriateness. </p>
          <p> By posting Content on or through the Service, You represent and warrant that: (i) the Content is yours (you own it) and/or you have the right to use it and the right to grant us the rights and license as provided in these Terms, and (ii) that the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person or entity. We reserve the right to terminate the account of anyone found to be infringing on a copyright. </p>
          <p> You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights. We take no responsibility and assume no liability for Content you or any third party posts on or through the Service. However, by posting Content using the Service you grant us the right and license to use, modify, publicly perform, publicly display, reproduce, and distribute such Content on and through the Service. You agree that this license includes the right for us to make your Content available to other users of the Service, who may also use your Content subject to these Terms. </p>
          <p> WEB WED, LLC has the right but not the obligation to monitor and edit all Content provided by users. </p>
          <p> In addition, Content found on or through this Service are the property of WEB WED, LLC or used with permission. You may not distribute, modify, transmit, reuse, download, repost, copy, or use said Content, whether in whole or in part, for commercial purposes or for personal gain, without express advance written permission from us. </p>
          <h5>Accounts</h5>
          <p> When you create an account with us, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service. </p>
          <p> You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account. </p>
          <p> You may not use as a user-name the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a user-name any name that is offensive, vulgar or obscene. <br>
            We reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in our sole discretion. </p>
          <h5>Copyright Policy</h5>
          <p> We respect the intellectual property rights of others. It is our policy to respond to any claim that Content posted on the Service infringes on the copyright or other intellectual property rights ("Infringement") of any person or entity.
            If you are a copyright owner, or authorized on behalf of one, and you believe that the copyrighted work has been copied in a way that constitutes copyright infringement, please submit your claim via email to info@webwedmobile.com, with the subject line: "Copyright Infringement" and include in your claim a detailed description of the alleged Infringement as detailed below, under "DMCA Notice and Procedure for Copyright Infringement Claims" </p>
          <p> You may be held accountable for damages (including costs and attorneys' fees) for misrepresentation or bad-faith claims on the infringement of any Content found on and/or through the Service on your copyright. </p>
          <h5>DMCA Notice and Procedure for Copyright Infringement Claims</h5>
          <p> You may submit a notification pursuant to the Digital Millennium Copyright Act (DMCA) by providing our Copyright Agent with the following information in writing (see 17 U.S.C 512(c)(3) for further detail): </p>
          <ul>
            <li>an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright's interest; </li>
            <li>a description of the copyrighted work that you claim has been infringed, including the URL (i.e., web page address) of the location where the copyrighted work exists or a copy of the copyrighted work; </li>
            <li>identification of the URL or other specific location on the Service where the material that you claim is infringing is located; </li>
            <li>your address, telephone number, and email address; </li>
            <li>a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; </li>
            <li>a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf. </li>
          </ul>
          <p> You can contact our Copyright Agent via email at <a href="mailto:info@webwedmobile.com">info@webwedmobile.com</a> </p>
          <h5>Intellectual Property</h5>
          <p> The Service and its original content (excluding Content provided by users), features and functionality are and will remain the exclusive property of WEB WED, LLC and its licensors. The Service is protected by copyright, trademark, and other laws of both the United States and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of WEB WED, LLC. </p>
          <h5>Links To Other Web Sites</h5>
          <p> Our Service may contain links to third party web sites or services that are not owned or controlled by WEB WED, LLC. <br>
            WEB WED, LLC has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites. <br>
            You acknowledge and agree that WEB WED, LLC shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services. <br>
            We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit. </p>
          <h5>Termination</h5>
          <p> We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms. </p>
          <p> If you wish to terminate your account, you may simply discontinue using the Service. </p>
          <p> All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability. </p>
          <h5>Indemnification</h5>
          <p> You agree to defend, indemnify and hold harmless WEB WED, LLC and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password; b) a breach of these Terms, or c) Content posted on the Service. </p>
          <h5>Limitation Of Liability</h5>
          <p> In no event shall WEB WED, LLC, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose. </p>
          <h5>Disclaimer</h5>
          <p> Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance. </p>
          <p> Web Wed, LLC its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements. </p>
          <h5>Exclusions</h5>
          <p> Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you. </p>
          <h5>Governing Law</h5>
          <p> These Terms shall be governed and construed in accordance with the laws of Nevada, United States, without regard to its conflict of law provisions.
            Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service. </p>
          <h5>Changes</h5>
          <p> We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.
            By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service. </p>
          <h5>Contact Us</h5>
          <p> If you have any questions about these Terms, please contact us. </p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Privacy Modal -->
<div id="privacy-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modalwide"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="" id="privacy_body">
          <h2>Privacy Policy</h2>
          <p> Last updated: December 02, 2015 </p>
          <p> Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the www.webwedmobile.com website and the WEB WED mobile application (together, or individually, the "Service") operated by WEB WED, LLC ("us", "we", or "our"). </p>
          <p> Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service. <br>
            By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service. </p>
          <h5>Communications</h5>
          <p> By creating an Account on our service, you agree to subscribe to newsletters, marketing or promotional materials and other information we may send. However, you may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send. </p>
          <h5>Purchases</h5>
          <p> If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information. </p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Disclaimer Modal -->
<div id="disclaimer-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modalwide"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="" id="disclaimer-body">
          <h2>Disclaimer</h2>
          <p>No warranties</p>
          <p>This site and applications are provided &ldquo;as-is&rdquo; without any representations or warranties, express or implied. Web Wed, LLC makes no representations or warranties in relation to this website or the information and materials provided. </p>
          <p>Without prejudice to the generality of the aforementioned paragraph, Web Wed, LLC does not warrant that:</p>
          <p>this website will be constantly available, or available at all; or the information on this website is complete, true, accurate or non-misleading.</p>
          <p>Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you require advice in relation to any [legal, financial or medical matter you should consult a appropriate professional service.</p>
          <p>Limitations of liability</p>
          <p>Web Wed, LLC will not be liable to you in relation to the contents of, or use of, or otherwise in connection with, this website:</p>
          <p>to the extent that the website is provided free-of-charge, for any direct loss; for any indirect, special or consequential loss; or for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.</p>
          <p>These limitations of liability still apply even if Web Wed, LLC has been expressly advised of the potential loss.</p>
          <p>Exceptions</p>
          <p>Nothing in this website disclaimer will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or limit Web Wed's liability in respect of any:</p>
          <p>a death or personal injury caused by Web Wed's negligence; fraud or fraudulent misrepresentation on the part of Web Wed, LLC; or matter which it would be illegal or unlawful for Web Wed, LLC to exclude or limit, or to attempt or purport to exclude or limit, its liability. </p>
          <p>Other parties</p>
          <p>You accept that, as a limited liability entity, Web Wed, LLC has an interest in limiting the personal liability of its officers and employees. You agree that you will not bring any claim personally against Web Wed's officers or employees in respect of any losses you suffer in connection with the website</p>
          <p>Without prejudice to the foregoing paragraph, you agree that the limitations of warranties and liability set out in this website disclaimer will protect Web Wed's officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as Web Wed, LLC. </p>
          <p>Unenforceable provisions</p>
          <p>If any provision of this website disclaimer is, or is found to be, unenforceable under applicable law, that will not affect the enforceability of the other provisions of this website disclaimer. </p>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Live Stream Modal--> 
<!--<div id="livestream-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modalwide"> 
    
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-xs-12">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="" id="livestream-body">
          <iframe src="" style="border: 0 none transparent;"  webkitallowfullscreen allowfullscreen frameborder="no" width="99.6hw" height="90vh"></iframe>
          <br />
          <a href="http://www.ustream.tv/" style="padding: 2px 0px 4px; width: 400px; background: #ffffff; display: block; color: #000000; font-weight: normal; font-size: 10px; text-decoration: underline; text-align: center;" target="_blank">Live streaming video by Ustream</a> </div>
      </div>
    </div>
  </div>
</div>--> 

<!--Social Modal -->
<div class="modal fade" id="wwSocialShare" role="dialog">
  <div class="modal-dialog modalwide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-alt"></i> Share Ceremony Link</h4>
      </div>
      <div class="modal-body">
        <div class="ssk-group ssk-lg">
          <div id="sociallink" class="ssk-group" data-url="http://www.webwedmobile.com/" data-text="Live stream wedding ceremony" > <a href="" class="ssk ssk-facebook"></a> <a href="" class="ssk ssk-twitter" data-text="Live stream Twitter. "></a> <a href="" class="ssk ssk-google-plus"></a> <a href="" class="ssk ssk-tumblr" data-media=""></a> </div>
        </div>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>

<div class="modal fade" id="youtubeModal" role="dialog">
  <div class="modal-dialog modalwide">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-alt"></i> Marriage Education</h4>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/j6E2FHVcawE" scrolling="yes" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>

<!--Register Modal -->
<div id="wwRegister" class="modal hide fade" tabindex="-1" role="dialog">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
  </div>
  <div class="modal-body">
    <iframe src="" width="99.6%" style="height: 100vh;" frameborder="0" scrolling="yes"></iframe>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal">OK</button>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script> 
<script src="js/modernizr.js"></script><script src="js/background.js"></script><script src="js/index.js"></script><script src="assets/js/smoothscroll.js"></script><script src="assets/js/jquery.fitvids.js"></script><script src="assets/js/jquery.nav.js"></script> 
<script src="assets/js/modal-center.js"></script> 
<script src="assets/js/jquery.sticky.js"></script> 
<script type="text/javascript" src="https://rawgithub.com/nwcell/ics.js/master/ics.deps.min.js"></script> 
<script src="js/webwed_webview.js"></script> 
<script type="text/javascript" src="js/social-share-kit.min.js"></script> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74133839-1', 'auto');
  ga('send', 'pageview');

SocialShareKit.init();


</script>
</body>
</html>

<!--This site's Design and Source Code Is Property of Riveloper, LLC | Atlanta, GA until otherwise noted.-->