<?php
if (isset($_GET['code'])){
  header("Location: join.php?code=".htmlentities($_GET['code']));
  exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Live Video Broadcast Weddings - Web Wed Mobile</title>
  <!-- Search engines -->
  <meta content=
  "From a iPhone or Android device you can share a live video feed of your wedding and even get hitched via a live video chat. Whenever, wherever, together, forever."
  name="description">
  <!-- Google Plus -->
  <meta content="WebWed Mobile - Live stream weddings.">
  <meta content=
  "When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
  <meta content="images/social_share.png">
  <!-- Mobile Specific Meta -->
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <? require_once 'includes/Mobile_Detect.php'; $detect = new Mobile_Detect;?>
  <meta content="app-id=1086176524" name="apple-itunes-app">
  <meta name="google-play-app" content="app-id=com.webwed.android">
  <link rel="stylesheet" href="css/smartbanner.css" type="text/css" media="screen">
  <link rel="android-touch-icon" href="images/icon/apple-icon-72x72.png" />
  <!--Fav and touch icons-->
  <link href="images/icon/apple-icon-57x57.png" rel="apple-touch-icon" sizes=
  "57x57">
  <link href="images/icon/apple-icon-60x60.png" rel="apple-touch-icon" sizes=
  "60x60">
  <link href="images/icon/apple-icon-72x72.png" rel="apple-touch-icon" sizes=
  "72x72">
  <link href="images/icon/apple-icon-76x76.png" rel="apple-touch-icon" sizes=
  "76x76">
  <link href="images/icon/apple-icon-114x114.png" rel="apple-touch-icon"
  sizes="114x114">
  <link href="images/icon/apple-icon-120x120.png" rel="apple-touch-icon"
  sizes="120x120">
  <link href="images/icon/apple-icon-144x144.png" rel="apple-touch-icon"
  sizes="144x144">
  <link href="images/icon/apple-icon-152x152.png" rel="apple-touch-icon"
  sizes="152x152">
  <link href="images/icon/apple-icon-180x180.png" rel="apple-touch-icon"
  sizes="180x180">
  <link href="images/icon/favicon//android-icon-192x192.png" rel="icon"
  sizes="192x192" type="image/png">
  <link href="images/icon/favicon-32x32.png" rel="icon" sizes="32x32" type=
  "image/png">
  <link href="images/icon/favicon-96x96.png" rel="icon" sizes="96x96" type=
  "image/png">
  <link href="images/icon/favicon-16x16.png" rel="icon" sizes="16x16" type=
  "image/png">
  <!--web wed preloader-->
  <link crossorigin="anonymous" href=
  "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
  integrity=
  "sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw=="
  rel="stylesheet">
  <link crossorigin="anonymous" href=
  "https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
  integrity=
  "sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1"
  rel="stylesheet">
  <link href=
  "https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:400,300|Roboto+Condensed|Clicker+Script"
  rel="stylesheet" type="text/css">
  <link href="css/webwed.css" rel="stylesheet" type="text/css">
  <script src="https://code.jquery.com/jquery-2.2.0.min.js">
  </script>
  <style>
  .carousel {
    position: relative;
  }
  .carousel-inner {
    position: relative;
    width: 100%;
    overflow: hidden;
  }
  .carousel-inner>.item {
    position: relative;
    display: none;
    -webkit-transition: .6s ease-in-out left;
    -o-transition: .6s ease-in-out left;
    transition: .6s ease-in-out left;
  }
  @media all and (transform-3d), (-webkit-transform-3d) {
    .carousel-inner>.item {
      -webkit-transition: -webkit-transform .3s ease-in-out;
      -o-transition: -o-transform .3s ease-in-out;
      transition: transform .3s ease-in-out;
      -webkit-backface-visibility: hidden;
      backface-visibility: hidden;
      -webkit-perspective: 1000px;
      perspective: 1000px;
    }
    .carousel-inner>.item.active {
      left: 0;
      -webkit-transform: translate3d(0, 0, 0);
      transform: translate3d(0, 0, 0);
    }
  }
  .carousel-inner>.active {
    display: block;
  }
  .carousel-inner>.active {
    left: 0;
  }
  #background-carousel {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  #background-carousel .carousel, #background-carousel .carousel-inner {
    width: 100%;
    height: 100%;
    z-index: 0;
    overflow: hidden;
  }
  #background-carousel .item {
    width: 100%;
    height: 100%;
    z-index: 0;
    -webkit-transition: opacity .6s;
    -moz-transition: opacity .6s;
    -ms-transition: opacity .6s;
    -o-transition: opacity .6s;
    transition: opacity .6s;
  }
  #background-carousel .item-no1 {
    background: url(../images/slider/slide-1.jpg) no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position: center;
  }
  #background-carousel .item-no3 {
    background: url(../images/slider/slide-2.jpg) no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position: center;
  }
  #background-carousel .item-no2 {
    background: url(../images/slider/slide-3.jpg) no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position: center;
  }
  #background-carousel .item-no4 {
    background: url(../images/slider/slide-4.jpg) no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    background-position: center;
  }
  #background-carousel .carousel .active.left {
    left: 0;
    opacity: 0;
    z-index: 2;
    -webkit-transition: .3s ease;
    -moz-transition: .3s ease;
    -o-transition: .3s ease;
    -ms-transition: .3s ease;
    transition: .3s ease;
  }
  #background-carousel .carousel-inner > .item.next, #background-carousel .carousel-inner > .item.active.right {
    -webkit-transform: none;
    transform: none;
  }
  #background-carousel .carousel-inner > .item.prev, #background-carousel .carousel-inner > .item.active.left {
    -webkit-transform: none;
    transform: none;
  }

  .carousel-fade .carousel-inner .item {
    opacity: 0;
    -webkit-transition-property: opacity;
    transition-property: opacity;
  }
  .carousel-fade .carousel-inner .active {
    opacity: 1;
  }
  .carousel-fade .carousel-inner .active.left, .carousel-fade .carousel-inner .active.right {
    left: 0;
    opacity: 0;
    z-index: 1;
  }
  .carousel-fade .carousel-inner .next.left, .carousel-fade .carousel-inner .prev.right {
    opacity: 1;
  }
  .carousel-fade .carousel-control {
    z-index: 2;
  }
  @media all and (transform-3d), (-webkit-transform-3d) {
    .carousel-inner > .item.next, .carousel-inner > .item.active.right {
      -webkit-transform: translate3d( 0, 0, 0);
      transform: translate3d( 0, 0, 0);
    }
    .carousel-inner > .item.prev, .carousel-inner > .item.active.left {
      -webkit-transform: translate3d( 0, 0, 0);
      transform: translate3d( 0, 0, 0);
    }
    .carousel-inner > .item.next.left, .carousel-inner > .item.prev.right, .carousel-inner > .item.active {
      -webkit-transform: translate3d(0, 0, 0);
      transform: translate3d(0, 0, 0);
    }
  }

  </style>
</head>
<body>
  <nav class="nav navbar navbar-default" role="navigation">
    <!-- WebWed Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button class="navbar-toggle" data-target="#navbar-collapse-1"
      data-toggle="collapse" type="button"><span class="sr-only">Toggle
        navigation</span> <span class="icon-bar"></span> <span class=
        "icon-bar"></span> <span class="icon-bar"></span></button>
        <div class="navbar-brand navbar-brand-centered"> <a class="hidden-xs" href="index.php"><img alt="Web Wed Logo" class=
          "brand" src="images/icon/WebWed_logo_white.png"></a> </div>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-1">
          <ul class="nav navbar-nav navbar-left">
            <!-- <li>
            <a href="#about">About</a>
          </li> -->
          <li class="hidden-xs hidden-sm"> <a href="//webwedmobile.com/event.php?demo">View The
            Demo</a> </li>
            <li> <a data-target="#legal-modal" data-toggle="modal" href="#">Legal Resources</a> </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <!-- <li class="hidden-sm">
            <a href="tel:8559329333"><i class="fa fa-phone"></i> (855)-932-9333</a>
          </li> -->

          <li class="hidden-sm hidden-md hidden-lg"> <a href="//webwedmobile.com/admin/user_admin/signup.php">Create An Event</a> </li>
          <li class="hidden-sm hidden-md hidden-lg"> <a href="//webwedmobile.com/admin/user_admin/login.php">Manage An Event</a> </li>
          <li class="hidden-sm hidden-md hidden-lg"> <a href="//webwedmobile.com/admin/officiant_admin/login.php">Officiants</a> </li>
          <li class="hidden-sm hidden-md hidden-lg"> <a href="//webwedmobile.com/admin/user_admin/guest_login.php">Attend An Event</a> </li>
          <li class="hidden-sm hidden-md hidden-lg"> <a href="//webwedmobile.com/admin/user_admin/resend_wedding_code.php">Resend Event Code</a> </li>
          <li class="dropdown hidden-xs"> <a class="dropdown-toggle" data-toggle="dropdown" href=
            "//webwedmobile.com/admin/user_admin/login.php">Account <span class=
            "fa fa-heart fa-fw pull-right color-red"> </span></a>
            <ul class="dropdown-menu">
              <li> <a href=
                "//webwedmobile.com/admin/user_admin/signup.php">Create an
                Event<span class=
                "glyphicon glyphicon-user pull-left"></span></a> </li>
                <li> <a href=
                  "//webwedmobile.com/admin/user_admin/login.php">Manage
                  An Event</a> </li>
                  <li class="divider"></li>
                  <li> <a href=
                    "//webwedmobile.com/admin/officiant_admin/login.php"> Officiants ></a> </li>
                  </ul>
                </li>
                <li class="dropdown hidden-xs"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Attend an event<b class="caret"></b></a>
                  <ul class="dropdown-menu" style=
                  "padding: 15px;min-width: 250px;">
                  <li>
                    <div class="row">
                      <div class="col-md-12">
                        <form accept-charset="UTF-8" action=
                        "guest_login.php" class="form" id=
                        "login-nav" method="post" name="login-nav"
                        role="form">
                        <div class="form-group">
                          <input class="form-control" id=
                          "email" placeholder="Email address"
                          required="" type="email">
                        </div>
                        <div class="form-group">
                          <input class="form-control" id=
                          "eventid" placeholder="Enter Event Code"
                          required="" type="text">
                        </div>
                        <div class="form-group">
                          <button class=
                          "btn btn-success btn-block" type=
                          "submit">Join Now></button>
                        </div>
                      </form>
                    </div>
                  </div>
                </li>
                <li> <a href=
                  "//webwedmobile.com/admin/user_admin/resend_wedding_code.php"> Forgot Event Code?</a> </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
        <!-- story start -->
        <section class="story lightsmoke" id="story">
          <div class="container wrapper">
            <div id="couple_back">

              <div class="row">
                <!-- <h2 class="color_teal text-center" id="story-title">Live Video Feed Of
                Your Wedding &amp; Special Moments.</h2> -->
                <div class="" style="width: 100%;
                /* position: fixed; */
                height: 100%;">
                <!-- <h2 class="color_teal text-center" id="story-title">Live Video Feed Of
                Your Wedding &amp; Special Moments.</h2> -->
                <div style="width: 100%;
                height: 100%;
                float: left;
                margin-left: auto;
                margin-right: auto;
                overflow: hidden;
                padding: 0;
                margin: 0;">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/N3eo6p-Cs4w?rel=0&amp;controls=0&amp;showinfo=0;&amp;showinfo=0;&amp;autoplay=true;&amp;loop=true;" frameborder="0"> </iframe>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="timeline-story">
              <div class="timeline"> <span class="love-sign wow animated pulse"></span>
                <div class="timeline-left pull-left">
                  <div class="timeline-block-left">
                    <div class=
                    "timeline-thumbnail wow animated fadeInLeft"
                    data-wow-delay="0s" data-wow-duration="0.5s"> <img alt="Love Story" class="img-thumbnail"
                    src="images/love_story_01.jpg"></div>
                  </div>
                </div>
                <!-- /.timeline-left -->
                <div class="timeline-right pull-right">
                  <div class=
                  "timeline-block-right wow animated fadeInRight"
                  data-wow-delay="0s" data-wow-duration="1s">
                  <div class="timeline-head">
                    <h4>Start An Event</h4>
                  </div>
                  <div class="timeline-body"> Sign up for a WebWed Account on-line.
                    Select whether you need to schedule a union, are an officiant or are guest.
                    <!-- <a data-target="#sign-in-modal"
                    data-toggle="modal" href="#">Get
                    Started.</a> -->
                  </div>
                </div>
              </div>
              <!-- /.timeline-right -->
              <div class="clearfix hidden-xs"></div>
            </div>
            <!-- /.timeline end -->
            <div class="timeline"> <span class="love-sign wow animated pulse"></span>
              <div class="timeline-left reverse pull-left">
                <div class="timeline-block-left">
                  <div class=
                  "timeline-thumbnail wow animated fadeInLeft"
                  data-wow-delay="0.6s" data-wow-duration=
                  "1s"><img alt="Love Story" class=
                  "img-thumbnail" src=
                  "images/love_story_02.jpg"></div>
                </div>
              </div>
              <!-- /.timeline-left -->
              <div class="timeline-right reverse pull-right">
                <div class=
                "timeline-block-right wow animated fadeInRight"
                data-wow-delay="0.6s" data-wow-duration="1s">
                <div class="timeline-head">
                  <h4>Download the WebWed App</h4>
                </div>
                <div class="timeline-body"> Once you have an account, download the
                  WebWed Mobile app and login to begin your video feed. <i class=
                  "fa fa-fw fa-android"></i><a href="https://play.google.com/store/apps/details?id=com.webwed.android">Android</a> or <i class="fa fa-fw fa-apple"></i><a href="https://itunes.apple.com/us/app/webwed/id1086176524?mt=8">iOS</a> </div>
                </div>
              </div>
              <!-- /.timeline-right -->
              <div class="clearfix hidden-xs"></div>
            </div>
            <!-- /.timeline end -->
            <div class="timeline"> <span class="love-sign wow animated pulse"></span>
              <div class="timeline-left pull-left">
                <div class="timeline-block-left">
                  <div class=
                  "timeline-thumbnail wow animated fadeInLeft"
                  data-wow-delay="1" data-wow-duration="1s"> <img alt="Love Story" class="img-thumbnail"
                  src="images/love_story_03.jpg"></div>
                </div>
              </div>
              <!-- /.timeline-left -->
              <div class="timeline-right pull-right">
                <div class=
                "timeline-block-right wow animated fadeInRight"
                data-wow-delay="1" data-wow-duration="1s">
                <div class="timeline-head">
                  <h4>H-app-y Ever After</h4>
                </div>
                <div class="timeline-body"> Make your video broadcast private or
                  share publicly through social media and email with the entire
                  world. Add your registry for gifting. And select if you want your guests to chat. </div>
                </div>
              </div>
              <!-- /.timeline-right -->
              <div class="clearfix hidden-xs"></div>
            </div>
            <!-- /.timeline end -->
          </div>
          <!-- /.timeline-story -->
        </div>
      </div>
      <div class="heart x1"></div>
      <div class="heart x2"></div>
      <div class="heart x3"></div>
      <div class="heart x4"></div>
      <div class="heart x5"> </div>
      <div class="heart x1"></div>
      <div class="heart x2"></div>
      <div class="heart x3"></div>
      <div class="heart x4"></div>
      <div class="heart x5"> </div>
      <!-- <div class="altheart x6"></div> -->
    </div></div>
  </section>
  <!-- story end -->
  <div class="bg_heart">
    <section class="rsvpLink <?php if ($detect->isMobile()){ ?> hidden <?php } ?>" id="rsvpLink">
      <div class="color-overlay">
        <div class="container">
          <div class="embed-responsive embed-responsive-16by9" id="border_frame_spacer">
            <iframe class="embed-responsive-item" src=
            "https://www.youtube-nocookie.com/embed/jhw61SUe-cQ?rel=0&amp;controls=0&amp;showinfo=0"> </iframe>
          </div>
          <div class="border_frame">
            <div class="row text-center">
              <h3 class="color_navy evite">You Are
                E&#x2011;vited To A</h3>
                <div class="row">
                  <div class="col-sm-12 text-center">
                    <h3 class="color_white evite_couple" id=
                    "CoupleName">Live Feed Video<br>
                    of Your Wedding</h3>
                    <!-- <h3 class="evite_small color_black" id="WeddingDate">&nbsp;</h3> -->
                    <p align="center"><img src="images/border.svg" style="width:50%; margin-top: 10px; margin-bottom:10px;"></p>
                    <a href=
                    "http://webwedmobile.com/event.php?demo">
                    <h3 class="evite_small color_black" id=
                    "HideText">View a preview
                    ceremony</h3>
                  </a>
                  <h5 class="color_black"> Register online or call <a href="tel:+1855WEBWEDD" class="color_white">(855)-WEB-WEDD</a></h5>
                  <h3 class="color_white evite_date" id=
                  "HideText"></h3>
                </div>
              </div>
              <!-- row end -->
              <div class="row">
                <!-- <div class="user-img col-xs-6 col-sm-6"> <img alt="To be wed" src="images/groom.png"> </div>
                <div class="user-img col-xs-6 col-sm-6"> <img alt="To be wed" src="images/bride.png"> </div>-->
                <!-- /.thumb-img -->
                <!-- /.thumbnail-block -->
                <div class="col-sm-12 text-center" id=
                "wedding-paragraph black"> <font color="white" face=
                "roboto condensed, helvetica, sans-serif">This
                is your chance to become one of the first
                couples to break through the walls and make
                your special day a special
                event.</font><br>
              </div>
              <!--<div class="col-sm-12 text-center"> <a href="images/WebWed.ics" class="btn default-btn">RSVP</a> </div>-->
            </div>
            <!-- row end -->
          </div>
          <!-- row text-center end-->
        </div>
        <!-- border_frame end-->
      </div>
      <!-- /.color-overlay end -->
    </div>
  </section>
</div>
<!-- promotons start -->
<section class="event lightsmoke" id="event">
  <div class="container" style="overflow:visible;">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="wow animated fadeInUp">
          <h3 class="color_white" id="event-title">WebWed <span class="color_navy wow pulse" data-wow-duration=
            "1500ms" data-wow-iteration="5"><i class=
            "fa fa-heart"></i></span> Highlights</h3>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-4">
          <div class="event-block wow animated fadeInUp"
          data-wow-delay="0s" data-wow-duration="1s">
          <div class="event-img"><img alt="Event name" class=
            "img-responsive" src="images/event_01.png"></div>
            <!-- /.event-img end -->
            <div class="event-body">
              <h5>Love Agents</h5>
              <p>Help us spread love to the world through sponsorship, endorsements, vendors and love agents. Are you looking to donate to a GREAT cause? Please visit our gofundme page at gofund.me/www-webwedmobile-c to learn more about WebWed and to provide a love offering of your choosing. Also, we are looking for sponsors and endorsements to help us pay love forward, take a stand for equality, and to support women in technology and minority businesses. Are you a Vendor looking to add an exclusive touch to your business? You can do just that by adding WebWed as a premier service.</p>
              <!-- <span class="event-date"></span>
              <div class="date-wrap">
              <span class="event-date"><span>Available</span>
              <span class="event-year">Now</span></span>
            </div> -->
          </div>
          <!-- /.event-body end -->
        </div>
        <!-- /.event-block end -->
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="event-block wow animated fadeInUp"
        data-wow-delay=".8s" data-wow-duration="1s">
        <div class="event-img"> <a data-target="#youtubeModal" data-toggle="modal"
          href="#"><img alt="Event name" class=
          "img-responsive" src="images/event_04.png"></a> </div>
          <!-- /.event-img end -->
          <div class="event-body">
            <h5>FREE Counseling</h5>
            <p>Web Wed is happy to provide FREE marriage
              education and counseling with certificate of
              completion for all of our couples.</p>
              <!-- <span class=
              "event-date"></span>
              <div class="date-wrap">
              <span class="event-date"><span>AVAILABLE</span>
              <span class="event-year">now</span></span>
            </div> -->
          </div>
          <!-- /.event-body end -->
        </div>
        <!-- /.event-block end -->
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="event-block wow animated fadeInUp"
        data-wow-delay=".8s" data-wow-duration="1s">
        <div class="event-img"><img alt="Event name" class=
          "img-responsive" src="images/event_03.png"></div>
          <!-- /.event-img end -->
          <div class="event-body">
            <h5>Our <span class="color_red"><i class=
              "fa fa-heart"></i></span>Beliefs</h5>
              <p>Our core belief is no matter who you are or your demographic location love defies distance, culture, beliefs, race & gender. If you share in our belief join our team and become a love agent. Help us break the internet. Download the WebWed Mobile App. Follow us on social media. Look for us at an upcoming event in a city and a mobile device near you Email us for a free shirt, and post/share your experience with the world. </p>
              <!-- <span class=
              "event-date"></span>
              <div class="date-wrap">
              <span class="event-date"><span>June 26th</span>
              <span class="event-year">2016</span></span>
            </div> -->
          </div>
          <!-- /.event-body end -->
        </div>
        <!-- /.event-block end -->
      </div>
    </div>
  </div>
</section>
<!-- event end -->
<!-- contact start -->
<section class="contact" id="contact">
  <div class="container">
    <div class="row">
      <h3 align="center" class="color_white" id="contact-title">We'd <span class="color_navy wow pulse" data-wow-duration="1200"
        data-wow-iteration="infinite"><i class=
        "fa fa-heart"></i></span> To Hear From You</h3>
        <div class="row">
          <div class="col-sm-12">
            <div class="contact-block">
              <!-- <h5><strong>Connect With Us</strong><br></h5> -->
              <form class="contact_form" onSubmit=
              'event.preventDefault();' role="form">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input class="form-control" id=
                    "contactnamefield" name="name"
                    placeholder="Your Full Name"
                    required="required" type="text">
                  </div>
                  <div class="form-group">
                    <select class="form-control" id=
                    "contactquestionfield" name=
                    "numberOfGuest" tabindex="1">
                    <option value=""> [Select One] </option>
                    <option value=
                    "How To Register"> How To Register </option>
                    <option value=
                    "What is the Cost"> What is the Cost? </option>
                    <option value=
                    "Will It Be Legal"> Will It Be Legal? </option>
                    <option value=
                    "Other / Not Listed"> Other / Not Listed </option>
                  </select>
                </div>
                <div class="form-group">
                  <input class="form-control" id=
                  "contactemailfield" name="name"
                  placeholder=
                  "Enter your email address"
                  required="required" type="text">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <textarea class="form-control" id=
                  "contactmessage" name="message"
                  placeholder=
                  "Your Message"></textarea>
                </div>
                <div class="form-group">
                  <input class="btn btn-lg btn-block"
                  name="submit" onClick=
                  "submitcontact();" type="submit"
                  value="Send">
                </div>
              </div>
              <div class="col-sm-12">
                <p class="contact-success">Your Message
                  has been successfully sent!</p>
                  <p class="contact-error">Sorry.
                    Something went wrong. Please try again
                    later.</p>
                    <h1 align="center" class="small color_white">Or
                      Call <a href="tel:+1855WEBWEDD">1-855-WEB-WEDD</a> and Speak Directly With A WebWed Mobile Agent.</h1>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.contact-block end -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- contact end -->
    <!-- footer start -->
    <footer>
      <div>
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center"> <span class="footer-logo"><img alt="WebWed" src=
              "images/icon/WebWed_logo_white.svg" width="250"></span> </div>
              <h2 class="color_teal medium text-center">When Love Can't
                Wait.</h2>

              </div>
              <div class="row">
                <div class="col-sm-12 text-center">
                  <ul class="nav nav-pills">
                    <li role="presentation"> <a data-target="#about-modal" data-toggle="modal"
                      href="#">About Us</a> </li>
                      <li role="presentation" class="hidden-xs hidden-sm"> <a href="//webwedmobile.com/event.php?demo">See A Preview</a> </li>
                      <li role="presentation"> <a data-target="#legal-modal" data-toggle=
                        "modal" href="#">Legal Resources</a> </li>
                      </ul>
                      <!-- footer nav end -->
                    </div>
                    <div class="col-sm-offset-2 col-sm-8"> <a class="wow animated rollIn" href=
                      "https://www.facebook.com/webwedmobile"><i class=
                      "fa fa-facebook"></i></a> <a class=
                      "wow animated rollIn" href=
                      "https://twitter.com/webwedmobile"><i class=
                      "fa fa-twitter"></i></a> <a class="wow animated rollIn"
                      href="https://instagram.com/webwedmobile"><i class=
                      "fa fa-instagram"></i></a> </div>
                      <div class="col-sm-12 text-center">

                        <p class="copyright">&copy; 2012&#x2011; <? echo date("Y"); ?> WebWed, LLC. All rights reserved. Provisional Patent
                          Awarded. Service available only within the contiguous
                          United States. A <a href=
                          "http://www.marriagelicensenow.com">state issued
                          marriage license</a> is required to use this service.
                          Use of this site and mentioned services constitutes
                          that you agree to our terms of service and privacy
                          policy. Web or App related
                          technical issues can be sent to <a href=
                          "https://www.riveloper.com">Riveloper
                          Development.</a></p>
                          <p class="copyright"><a data-target="#privacy-modal"
                            data-toggle="modal" href="#">Privacy Policy</a> | <a data-target="#terms-modal" data-toggle="modal" href=
                            "#">Terms of Service</a> | <a data-target=
                            "#disclaimer-modal" data-toggle="modal" href=
                            "#">Disclaimer</a></p>
                            <br>
                          </div>
                        </div>
                      </div>
                      <div id="go-to-top"> <a href="#banner"><i class="fa fa-angle-up"></i></a> </div>
                    </div>
                    <!-- /.color-overlay end -->
                  </footer>
                  <!-- footer end -->
                  <!-- Legal Modal -->
                  <div class="modal fade" id="legal-modal" role="dialog">
                    <div class="modal-dialog modalwide">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-body row">
                          <div class="col-xs-12">
                            <button class="close" data-dismiss="modal" type=
                            "button">&times;</button>
                          </div>
                          <div class="" id="modal-indent">
                            <h2>Legal Resources</h2>
                            <p>While we can't provide you legal advice we can offer
                              some resources to help you understand our service. We
                              do not issue marriage license or offer legal
                              services.</p>
                              <p><strong>Proxy Marriages</strong><br>
                                A proxy marriage is a marriage where the parties were
                                not physically present in the presence of each other.
                                Someone stands in for the other party because either
                                the bride or the groom is not physically present for
                                the wedding. During the solemnization of the marriage,
                                based upon a power of attorney, an agent acts on behalf
                                of one of the parties. The marriage is presumed valid
                                if it is valid in the country that performs the
                                ceremony. Mostly compared and used in military
                                marriages, and maximum security facility. Marriage by
                                proxy, with only one party to the marriage making an
                                appearance, is possible in the United States. It is
                                always necessary for one of the two parties (either
                                husband or wife) to actually appear before the Civil
                                Authorities.</p>
                                <p>Written Authorization for Third Person to Act as
                                  Proxy<br>
                                  If a party to a marriage is unable to be present at the
                                  solemnization, that party may authorize in writing a
                                  third person to act as his proxy. If the person
                                  solemnizing the marriage is satisfied that the absent
                                  party is unable to be present and has consented to the
                                  marriage, he or she may solemnize the marriage by
                                  proxy. If he or she is not satisfied, the parties may
                                  petition the court for an order permitting the marriage
                                  to be solemnized by proxy.</p>
                                  <p><strong>Proxy Marriages Allowed in Four
                                    States</strong><br>
                                    Marriage by proxy has been around a long time. Proxy
                                    marriages are most common during wartime. Marriages by
                                    proxy are allowed in California, Colorado, Texas, and
                                    Montana.</p>
                                    <p><strong>Legal Recognition</strong><br>
                                      Whether a state or country will recognize a marriage by
                                      proxy seems to depend on whether or not the law of the
                                      locale requires that both parties be present to apply
                                      for a license or to give their consent at the ceremony.
                                      Some states recognize a proxy marriage that was done in
                                      another state. Other states only recognize them as
                                      common law marriage. U.S. military personnel may annul
                                      a proxy marriage provided there is no consummation, no
                                      cohabitation, or no treatment as husband and wife after
                                      the marriage ceremony.</p>
                                      <p><strong>Immigration Concerns</strong><br>
                                        Unconsummated proxy marriages are not recognized for
                                        immigration purposes in most countries, including the
                                        United States. However, a party of an unconsummated
                                        proxy marriage may enjoy immigration benefits as a
                                        fiancée of the opposite party is a U.S. citizen.</p>
                                        <p><strong>Commoditized Law</strong><br>
                                          Common law marriage should not be confused with
                                          non-marital relationship contracts, which involves two
                                          people living together without holding themselves out
                                          to the world as spouses and/or without legal
                                          recognition as spouses in the jurisdiction where the
                                          contract was formed. Non-marital relationship contracts
                                          are not necessarily recognized from one jurisdiction to
                                          another whereas common law marriages are, by
                                          definition, legally valid marriages worldwide -
                                          provided the parties complied with the requirements to
                                          form a valid marriage while living in a jurisdiction
                                          that still allows this irregular form of marriage to be
                                          contracted - as was historically the case under the
                                          common law of England (hence the name, &quot;common law
                                          marriage&quot;).</p>
                                          <p>Common law and statutory marriage have the following
                                            characteristics in common:<br>
                                          </p>
                                          <ol>
                                            <li>Both parties must freely consent to the
                                              marriage</li>
                                              <li>Both parties must be of legal age to contract a
                                                marriage or have parental consent to marry</li>
                                                <li>Neither party may be under a disability that
                                                  prevents him or her from entering into a valid
                                                  marriage - E.g. they must both be of sound mind,
                                                  neither of them can be currently married (except in
                                                  Saskatchewan[2]), and some jurisdictions do not
                                                  permit prisoners to marry.</li>
                                                </ol>
                                                <ol>
                                                  <li style="list-style: none">Otherwise, common law
                                                    marriage differs from statutory marriage as
                                                    follows:</li>
                                                    <li>There is no marriage license issued by a
                                                      government and no marriage certificate filed with a
                                                      government</li>
                                                      <li>There is no formal ceremony to solemnize the
                                                        marriage before witnesses</li>
                                                        <li>The parties must hold themselves out to the
                                                          world as husband and wife (this is not a
                                                          requirement of statutory marriage)</li>
                                                          <li>Most jurisdictions require the parties to be
                                                            cohabiting at the time the common law marriage is
                                                            formed. Some require cohabitation to last a certain
                                                            length of time (E.g. three years) for the marriage
                                                            to be valid. But cohabitation alone does not create
                                                            a marriage.</li>
                                                          </ol>
                                                          <p>The parties must intend their relationship to be,
                                                            and to be regarded as, a legally valid marriage.<br>
                                                            When a marriage is validly contracted, whether
                                                            according by statutory provision or according to common
                                                            law, the marriage can be dissolved only by a formal
                                                            legal proceeding in a court of competent jurisdiction -
                                                            usually a family or probate court.<br>
                                                            In the U.S. state of Texas, a new provision was added
                                                            to the Family Code; either partner in a common law
                                                            marriage has two years after separation to file an
                                                            action in order to prove that the common law marriage
                                                            existed. To use the provision, the separation must have
                                                            occurred after September 1, 1989.<br>
                                                            Since the mid-1990s, the term &quot;common law
                                                            marriage&quot; has been used in parts of Europe and
                                                            Canada to describe various types of domestic
                                                            partnership between persons of the same sex as well
                                                            those of the opposite sex. Although these interpersonal
                                                            statuses are often, as in Hungary, called &quot;common
                                                            law marriage&quot; they differ from true common law
                                                            marriage in that they are not legally recognized as
                                                            &quot;marriages&quot; but are a parallel interpersonal
                                                            status, known in most jurisdictions as &quot;domestic
                                                            partnership&quot;, &quot;registered partnership&quot;,
                                                            &quot;conjugal union&quot; or &quot;civil union&quot;
                                                            etc.<br>
                                                            Not all agreements break statutes. Some are illegal
                                                            because they break public policy, which is generally
                                                            &quot;to discourage any interference with the freedom
                                                            of choice&quot; (Saskatchewan, Canada, excepted). An
                                                            agreement forbidding a party to marry or bribing a
                                                            party to refrain from marriage is considered
                                                            &quot;Interference with Marriage Relation&quot; or an
                                                            &quot;Agreement in Restraint of Marriage&quot;; such
                                                            agreements are typically held to be non-bonding.</p>
                                                            <p><strong>Marriage</strong><br>
                                                              Marriage can be recognized by a state, an organization,
                                                              a religious authority, a tribal group, a local
                                                              community or peers. It is often viewed as a contract.
                                                              Civil marriage, which does not exist in some countries,
                                                              is marriage without religious content carried out by a
                                                              government institution in accordance with the marriage
                                                              laws of the jurisdiction, and recognized as creating
                                                              the rights and obligations intrinsic to matrimony.
                                                              Marriages can be performed in a secular civil ceremony
                                                              or in a religious setting via a wedding ceremony.</p>
                                                              <ol>
                                                                <li> <a href=
                                                                  "http://gaylife.about.com/od/gaymarriagebyregion/ig/Legal-Marriage"
                                                                  target=
                                                                  "_blank">http://gaylife.about.com/od/gaymarriagebyregion/ig/Legal-Marriage</a> </li>
                                                                  <li> <a href=
                                                                    "http://www.marriagelicensenow.com"
                                                                    target=
                                                                    "_blank">http://www.marriagelicensenow.com</a> </li>
                                                                    <li> <a href=
                                                                      "http://www.imarriedanalien.com"
                                                                      target=
                                                                      "_blank">http://www.imarriedanalien.com</a> </li>
                                                                      <li> <a href=
                                                                        "https://www.missnowmrs.com"
                                                                        target=
                                                                        "_blank">https://www.missnowmrs.com</a> </li>
                                                                        <li> <a href=
                                                                          "http://www.unmarried.org/common-law-marriage-fact-sheet.html"
                                                                          target=
                                                                          "_blank">http://www.unmarried.org/common-law-marriage-fact-sheet.html</a> </li>
                                                                          <li> <a href=
                                                                            "http://www.proxymarriagehq.com/proxy-mar"
                                                                            target=
                                                                            "_blank">http://www.proxymarriagehq.com/proxy-marriage-law/</a> </li>
                                                                            <li> <a href="http://www.usmarriagelaws.com/"
                                                                              target="_blank">http://www.usmarriagelaws.com/</a> </li>
                                                                              <li> <a href=
                                                                                "http://www.marriageequality.org/Same%20Sex%20Marriage"
                                                                                target=
                                                                                "_blank">http://www.marriageequality.org/Same Sex Marriage</a> </li>
                                                                                <li> <a href=
                                                                                  "http://www.proxymarriage.com/index.php/double-proxy-marriage"
                                                                                  target=
                                                                                  "_blank">http://www.proxymarriage.com/index.php/double-proxy-marriage</a> </li>
                                                                                </ol>
                                                                                <p>In the United States, same-sex marriage has been
                                                                                  legal nationwide since June 26, 2015, when the United
                                                                                  States Supreme Court ruled in Obergefell v. Hodges that
                                                                                  state-level bans on same-sex marriage are
                                                                                  unconstitutional. The court ruled that the denial of
                                                                                  marriage licenses to same-sex couples and the refusal
                                                                                  to recognize those marriages performed in other
                                                                                  jurisdictions violates the Due Process and the Equal
                                                                                  Protection clauses of the Fourteenth Amendment of the
                                                                                  United States Constitution. The ruling overturned a
                                                                                  precedent, Baker v. Nelson.</p>
                                                                                  <p>While civil rights campaigning took place from the
                                                                                    1970s,[4] the issue became prominent from around 1993,
                                                                                    when the Hawaii Supreme Court ruled that the
                                                                                    prohibition was unconstitutional. The ruling led to
                                                                                    federal actions and actions by several states, to
                                                                                    restrict marriage to male-female couples, in particular
                                                                                    the Defense of Marriage Act (DOMA). During the period
                                                                                    2003 - 2015, various lower court decisions, state
                                                                                    legislation, and popular referendums had already
                                                                                    legalized same-sex marriage to some degree in
                                                                                    thirty-eight out of fifty U.S. states, in the U.S.
                                                                                    territory Guam, and in the District of Columbia. In
                                                                                    2013 the Supreme Court overturned a key provision of
                                                                                    DOMA, declaring part of it unconstitutional and in
                                                                                    breach of the Fifth Amendment in United States v.
                                                                                    Windsor because it &quot;single[d] out a class of
                                                                                    persons&quot; for discrimination, by refusing to treat
                                                                                    their marriages equally under federal law when state
                                                                                    law had created them equally valid. The ruling led to
                                                                                    the federal government's recognition of same-sex
                                                                                    marriage, with federal benefits for married couples
                                                                                    connected to either the state of residence or the state
                                                                                    in which the marriage was solemnized. However the
                                                                                    ruling focused on the provision of DOMA responsible for
                                                                                    the federal government refusing to acknowledge State
                                                                                    sanctioned same-sex marriages, leaving the question of
                                                                                    state marriage laws itself to the individual States.
                                                                                    The Supreme Court addressed that question two years
                                                                                    later in 2015, ruling, in Obergefell, that same-sex
                                                                                    married couples were to be constitutionally accorded
                                                                                    the same recognition as opposite-sex couples at
                                                                                    state/territory levels, as well as at federal
                                                                                    level.</p>
                                                                                    <p><strong>Proxy marriage</strong><br>
                                                                                      A proxy wedding or (proxy marriage) is a wedding in
                                                                                      which one or both of the individuals being united are
                                                                                      not physically present, usually being represented
                                                                                      instead by other persons. If both partners are absent a
                                                                                      double proxy wedding occurs.</p>
                                                                                      <p>Marriage by proxy is usually resorted to either when
                                                                                        a couple wish to marry but one or both partners cannot
                                                                                        attend for reasons such as military service,
                                                                                        imprisonment, or travel restrictions; or when a couple
                                                                                        lives in a jurisdiction in which they cannot legally
                                                                                        marry.</p>
                                                                                        <p>Proxy weddings are not recognized as legally binding
                                                                                          in most jurisdictions: both parties must be present. A
                                                                                          proxy marriage contracted elsewhere may be recognized
                                                                                          where proxy marriage within the jurisdiction is not;
                                                                                          for example, Israel recognizes proxy marriages abroad
                                                                                          between Israelis who might not have been permitted to
                                                                                          marry in Israel.</p>
                                                                                        </div>
                                                                                      </div>
                                                                                    </div>
                                                                                  </div>
                                                                                </div>
                                                                                <!-- Terms of Service Modal -->
                                                                                <div class="modal fade" id="terms-modal" role="dialog">
                                                                                  <div class="modal-dialog modalwide">
                                                                                    <!-- Modal content-->
                                                                                    <div class="modal-content">
                                                                                      <div class="modal-body row">
                                                                                        <div class="col-xs-12">
                                                                                          <button class="close" data-dismiss="modal" type=
                                                                                          "button">&times;</button>
                                                                                        </div>
                                                                                        <div class="" id="modal-indent">
                                                                                          <h2>Terms of Service</h2>
                                                                                          <p>Last updated: May 2016</p>
                                                                                          <p>Please read these Terms and Service ("Terms", "Terms
                                                                                            and Service") carefully before using the
                                                                                            www.webwedmobile.com website and the WEB WED mobile
                                                                                            application (together, or individually, the "Service")
                                                                                            operated by WEB WED, LLC ("us", "we", or "our").</p>
                                                                                            <p>Your access to and use of the Service is conditioned
                                                                                              upon your acceptance of and compliance with these
                                                                                              Terms. These Terms apply to all visitors, users and
                                                                                              others who wish to access or use the Service.<br>
                                                                                              By accessing or using the Service you agree to be bound
                                                                                              by these Terms. If you disagree with any part of the
                                                                                              terms then you do not have permission to access the
                                                                                              Service.</p>
                                                                                              <h5>Communications</h5>
                                                                                              <p>By creating an Account on our service, you agree to
                                                                                                subscribe to newsletters, marketing or promotional
                                                                                                materials and other information we may send. However,
                                                                                                you may opt out of receiving any, or all, of these
                                                                                                communications from us by following the unsubscribe
                                                                                                link or instructions provided in any email we send.</p>
                                                                                                <h5>Purchases</h5>
                                                                                                <p>If you wish to purchase any product or service made
                                                                                                  available through the Service ("Purchase"), you may be
                                                                                                  asked to supply certain information relevant to your
                                                                                                  Purchase including, without limitation, your credit
                                                                                                  card number, the expiration date of your credit card,
                                                                                                  your billing address, and your shipping
                                                                                                  information.</p>
                                                                                                  <p>You represent and warrant that: (i) you have the
                                                                                                    legal right to use any credit card(s) or other payment
                                                                                                    method(s) in connection with any Purchase; and that
                                                                                                    (ii) the information you supply to us is true, correct
                                                                                                    and complete.</p>
                                                                                                    <p>The service may employ the use of third party
                                                                                                      services for the purpose of facilitating payment and
                                                                                                      the completion of Purchases. By submitting your
                                                                                                      information, you grant us the right to provide the
                                                                                                      information to these third parties subject to our
                                                                                                      Privacy Policy.</p>
                                                                                                      <p>We reserve the right to refuse or cancel your order
                                                                                                        at any time for reasons including but not limited to:
                                                                                                        product or service availability, errors in the
                                                                                                        description or price of the product or service, error
                                                                                                        in your order or other reasons.<br>
                                                                                                        We reserve the right to refuse or cancel your order if
                                                                                                        fraud or an unauthorized or illegal transaction is
                                                                                                        suspected.</p>
                                                                                                        <h5>Availability, Errors and Inaccuracies</h5>
                                                                                                        <p>We are constantly updating product and service
                                                                                                          offerings on the Service. We may experience delays in
                                                                                                          updating information on the Service and in our
                                                                                                          advertising on other web sites. The information found
                                                                                                          on the Service may contain errors or inaccuracies and
                                                                                                          may not be complete or current. Products or services
                                                                                                          may be mis-priced, described inaccurately, or
                                                                                                          unavailable on the Service and we cannot guarantee the
                                                                                                          accuracy or completeness of any information found on
                                                                                                          the Service.</p>
                                                                                                          <p>We therefore reserve the right to change or update
                                                                                                            information and to correct errors, inaccuracies, or
                                                                                                            omissions at any time without prior notice.</p>
                                                                                                            <h5>Contests, Sweepstakes and Promotions</h5>
                                                                                                            <p>Any contests, sweepstakes or other promotions
                                                                                                              (collectively, "Promotions") made available through the
                                                                                                              Service may be governed by rules that are separate from
                                                                                                              these Terms & Conditions. If you participate in any
                                                                                                              Promotions, please review the applicable rules as well
                                                                                                              as our Privacy Policy. If the rules for a Promotion
                                                                                                              conflict with these Terms and Conditions, the Promotion
                                                                                                              rules will apply.</p>
                                                                                                              <h5>Subscriptions</h5>
                                                                                                              <p>Some parts of the Service are billed on a
                                                                                                                subscription basis ("Subscription(s)"). You will be
                                                                                                                billed in advance on a recurring and periodic basis
                                                                                                                ("Billing Cycle"). Billing cycles are set on a monthly
                                                                                                                basis.</p>
                                                                                                                <p>At the end of each Billing Cycle, your Subscription
                                                                                                                  will automatically renew under the exact same
                                                                                                                  conditions unless you cancel it or WEB WED, LLC cancels
                                                                                                                  it. You may cancel your Subscription renewal either
                                                                                                                  through your on-line account management page or by
                                                                                                                  contacting WEB WED, LLC customer support team.</p>
                                                                                                                  <p>A valid payment method, including credit card or
                                                                                                                    PayPal, is required to process the payment for your
                                                                                                                    Subscription. You shall provide WEB WED, LLC with
                                                                                                                    accurate and complete billing information including
                                                                                                                    full name, address, state, zip code, telephone number,
                                                                                                                    and a valid payment method information. By submitting
                                                                                                                    such payment information, you automatically authorize
                                                                                                                    WEB WED, LLC to charge all Subscription fees incurred
                                                                                                                    through your account to any such payment
                                                                                                                    instruments.</p>
                                                                                                                    <p>Should automatic billing fail to occur for any
                                                                                                                      reason, WEB WED, LLC will issue an electronic invoice
                                                                                                                      indicating that you must proceed manually, within a
                                                                                                                      certain deadline date, with the full payment
                                                                                                                      corresponding to the billing period as indicated on the
                                                                                                                      invoice.</p>
                                                                                                                      <h5>Fee Changes</h5>
                                                                                                                      <p>WEB WED, LLC, in its sole discretion and at any
                                                                                                                        time, may modify the Subscription fees for the
                                                                                                                        Subscriptions. Any Subscription fee change will become
                                                                                                                        effective at the end of the then-current Billing
                                                                                                                        Cycle.<br>
                                                                                                                        WEB WED, LLC will provide you with a reasonable prior
                                                                                                                        notice of any change in Subscription fees to give you
                                                                                                                        an opportunity to terminate your Subscription before
                                                                                                                        such change becomes effective.</p>
                                                                                                                        <p>Your continued use of the Service after the
                                                                                                                          Subscription fee change comes into effect constitutes
                                                                                                                          your agreement to pay the modified Subscription fee
                                                                                                                          amount.</p>
                                                                                                                          <h5>Refunds</h5>
                                                                                                                          <p>Except when required by law, paid Subscription fees
                                                                                                                            are non-refundable.</p>
                                                                                                                            <h5>Content</h5>
                                                                                                                            <p>Our Service allows you to post, link, store, share
                                                                                                                              and otherwise make available certain information, text,
                                                                                                                              graphics, videos, or other material ("Content"). You
                                                                                                                              are responsible for the Content that you post on or
                                                                                                                              through the Service, including its legality,
                                                                                                                              reliability, and appropriateness.</p>
                                                                                                                              <p>By posting Content on or through the Service, You
                                                                                                                                represent and warrant that: (i) the Content is yours
                                                                                                                                (you own it) and/or you have the right to use it and
                                                                                                                                the right to grant us the rights and license as
                                                                                                                                provided in these Terms, and (ii) that the posting of
                                                                                                                                your Content on or through the Service does not violate
                                                                                                                                the privacy rights, publicity rights, copyrights,
                                                                                                                                contract rights or any other rights of any person or
                                                                                                                                entity. We reserve the right to terminate the account
                                                                                                                                of anyone found to be infringing on a copyright.</p>
                                                                                                                                <p>You retain any and all of your rights to any Content
                                                                                                                                  you submit, post or display on or through the Service
                                                                                                                                  and you are responsible for protecting those rights. We
                                                                                                                                  take no responsibility and assume no liability for
                                                                                                                                  Content you or any third party posts on or through the
                                                                                                                                  Service. However, by posting Content using the Service
                                                                                                                                  you grant us the right and license to use, modify,
                                                                                                                                  publicly perform, publicly display, reproduce, and
                                                                                                                                  distribute such Content on and through the Service. You
                                                                                                                                  agree that this license includes the right for us to
                                                                                                                                  make your Content available to other users of the
                                                                                                                                  Service, who may also use your Content subject to these
                                                                                                                                  Terms.</p>
                                                                                                                                  <p>WEB WED, LLC has the right but not the obligation to
                                                                                                                                    monitor and edit all Content provided by users.</p>
                                                                                                                                    <p>In addition, Content found on or through this
                                                                                                                                      Service are the property of WEB WED, LLC or used with
                                                                                                                                      permission. You may not distribute, modify, transmit,
                                                                                                                                      reuse, download, repost, copy, or use said Content,
                                                                                                                                      whether in whole or in part, for commercial purposes or
                                                                                                                                      for personal gain, without express advance written
                                                                                                                                      permission from us.</p>
                                                                                                                                      <h5>Accounts</h5>
                                                                                                                                      <p>When you create an account with us, you guarantee
                                                                                                                                        that you are above the age of 18, and that the
                                                                                                                                        information you provide us is accurate, complete, and
                                                                                                                                        current at all times. Inaccurate, incomplete, or
                                                                                                                                        obsolete information may result in the immediate
                                                                                                                                        termination of your account on the Service.</p>
                                                                                                                                        <p>You are responsible for maintaining the
                                                                                                                                          confidentiality of your account and password, including
                                                                                                                                          but not limited to the restriction of access to your
                                                                                                                                          computer and/or account. You agree to accept
                                                                                                                                          responsibility for any and all activities or actions
                                                                                                                                          that occur under your account and/or password, whether
                                                                                                                                          your password is with our Service or a third-party
                                                                                                                                          service. You must notify us immediately upon becoming
                                                                                                                                          aware of any breach of security or unauthorized use of
                                                                                                                                          your account.</p>
                                                                                                                                          <p>You may not use as a user-name the name of another
                                                                                                                                            person or entity or that is not lawfully available for
                                                                                                                                            use, a name or trademark that is subject to any rights
                                                                                                                                            of another person or entity other than you, without
                                                                                                                                            appropriate authorization. You may not use as a
                                                                                                                                            user-name any name that is offensive, vulgar or
                                                                                                                                            obscene.<br>
                                                                                                                                            We reserve the right to refuse service, terminate
                                                                                                                                            accounts, remove or edit content, or cancel orders in
                                                                                                                                            our sole discretion.</p>
                                                                                                                                            <h5>Copyright Policy</h5>
                                                                                                                                            <p>We respect the intellectual property rights of
                                                                                                                                              others. It is our policy to respond to any claim that
                                                                                                                                              Content posted on the Service infringes on the
                                                                                                                                              copyright or other intellectual property rights
                                                                                                                                              ("Infringement") of any person or entity. If you are a
                                                                                                                                              copyright owner, or authorized on behalf of one, and
                                                                                                                                              you believe that the copyrighted work has been copied
                                                                                                                                              in a way that constitutes copyright infringement,
                                                                                                                                              please submit your claim via email to
                                                                                                                                              info@webwedmobile.com, with the subject line:
                                                                                                                                              "Copyright Infringement" and include in your claim a
                                                                                                                                              detailed description of the alleged Infringement as
                                                                                                                                              detailed below, under "DMCA Notice and Procedure for
                                                                                                                                              Copyright Infringement Claims"</p>
                                                                                                                                              <p>You may be held accountable for damages (including
                                                                                                                                                costs and attorneys' fees) for misrepresentation or
                                                                                                                                                bad-faith claims on the infringement of any Content
                                                                                                                                                found on and/or through the Service on your
                                                                                                                                                copyright.</p>
                                                                                                                                                <h5>DMCA Notice and Procedure for Copyright
                                                                                                                                                  Infringement Claims</h5>
                                                                                                                                                  <p>You may submit a notification pursuant to the
                                                                                                                                                    Digital Millennium Copyright Act (DMCA) by providing
                                                                                                                                                    our Copyright Agent with the following information in
                                                                                                                                                    writing (see 17 U.S.C 512(c)(3) for further
                                                                                                                                                    detail):</p>
                                                                                                                                                    <ul>
                                                                                                                                                      <li>an electronic or physical signature of the
                                                                                                                                                        person authorized to act on behalf of the owner of
                                                                                                                                                        the copyright's interest;</li>
                                                                                                                                                        <li>a description of the copyrighted work that you
                                                                                                                                                          claim has been infringed, including the URL (i.e.,
                                                                                                                                                          web page address) of the location where the
                                                                                                                                                          copyrighted work exists or a copy of the
                                                                                                                                                          copyrighted work;</li>
                                                                                                                                                          <li>identification of the URL or other specific
                                                                                                                                                            location on the Service where the material that you
                                                                                                                                                            claim is infringing is located;</li>
                                                                                                                                                            <li>your address, telephone number, and email
                                                                                                                                                              address;</li>
                                                                                                                                                              <li>a statement by you that you have a good faith
                                                                                                                                                                belief that the disputed use is not authorized by
                                                                                                                                                                the copyright owner, its agent, or the law;</li>
                                                                                                                                                                <li>a statement by you, made under penalty of
                                                                                                                                                                  perjury, that the above information in your notice
                                                                                                                                                                  is accurate and that you are the copyright owner or
                                                                                                                                                                  authorized to act on the copyright owner's
                                                                                                                                                                  behalf.</li>
                                                                                                                                                                </ul>
                                                                                                                                                                <p>You can contact our Copyright Agent via email at <a href=
                                                                                                                                                                  "mailto:info@webwedmobile.com">info@webwedmobile.com</a></p>
                                                                                                                                                                  <h5>Intellectual Property</h5>
                                                                                                                                                                  <p>The Service and its original content (excluding
                                                                                                                                                                    Content provided by users), features and functionality
                                                                                                                                                                    are and will remain the exclusive property of WEB WED,
                                                                                                                                                                    LLC and its licensors. The Service is protected by
                                                                                                                                                                    copyright, trademark, and other laws of both the United
                                                                                                                                                                    States and foreign countries. Our trademarks and trade
                                                                                                                                                                    dress may not be used in connection with any product or
                                                                                                                                                                    service without the prior written consent of WEB WED,
                                                                                                                                                                    LLC.</p>
                                                                                                                                                                    <h5>Links To Other Web Sites</h5>
                                                                                                                                                                    <p>Our Service may contain links to third party web
                                                                                                                                                                      sites or services that are not owned or controlled by
                                                                                                                                                                      WEB WED, LLC.<br>
                                                                                                                                                                      WEB WED, LLC has no control over, and assumes no
                                                                                                                                                                      responsibility for the content, privacy policies, or
                                                                                                                                                                      practices of any third party web sites or services. We
                                                                                                                                                                      do not warrant the offerings of any of these
                                                                                                                                                                      entities/individuals or their websites.<br>
                                                                                                                                                                      You acknowledge and agree that WEB WED, LLC shall not
                                                                                                                                                                      be responsible or liable, directly or indirectly, for
                                                                                                                                                                      any damage or loss caused or alleged to be caused by or
                                                                                                                                                                      in connection with use of or reliance on any such
                                                                                                                                                                      content, goods or services available on or through any
                                                                                                                                                                      such third party web sites or services.<br>
                                                                                                                                                                      We strongly advise you to read the terms and conditions
                                                                                                                                                                      and privacy policies of any third party web sites or
                                                                                                                                                                      services that you visit.</p>
                                                                                                                                                                      <h5>Termination</h5>
                                                                                                                                                                      <p>We may terminate or suspend your account and bar
                                                                                                                                                                        access to the Service immediately, without prior notice
                                                                                                                                                                        or liability, under our sole discretion, for any reason
                                                                                                                                                                        whatsoever and without limitation, including but not
                                                                                                                                                                        limited to a breach of the Terms.</p>
                                                                                                                                                                        <p>If you wish to terminate your account, you may
                                                                                                                                                                          simply discontinue using the Service.</p>
                                                                                                                                                                          <p>All provisions of the Terms which by their nature
                                                                                                                                                                            should survive termination shall survive termination,
                                                                                                                                                                            including, without limitation, ownership provisions,
                                                                                                                                                                            warranty disclaimers, indemnity and limitations of
                                                                                                                                                                            liability.</p>
                                                                                                                                                                            <h5>Indemnification</h5>
                                                                                                                                                                            <p>You agree to defend, indemnify and hold harmless WEB
                                                                                                                                                                              WED, LLC and its licensee and licensors, and their
                                                                                                                                                                              employees, contractors, agents, officers and directors,
                                                                                                                                                                              from and against any and all claims, damages,
                                                                                                                                                                              obligations, losses, liabilities, costs or debt, and
                                                                                                                                                                              expenses (including but not limited to attorney's
                                                                                                                                                                              fees), resulting from or arising out of a) your use and
                                                                                                                                                                              access of the Service, by you or any person using your
                                                                                                                                                                              account and password; b) a breach of these Terms, or c)
                                                                                                                                                                              Content posted on the Service.</p>
                                                                                                                                                                              <h5>Limitation Of Liability</h5>
                                                                                                                                                                              <p>In no event shall WEB WED, LLC, nor its directors,
                                                                                                                                                                                employees, partners, agents, suppliers, or affiliates,
                                                                                                                                                                                be liable for any indirect, incidental, special,
                                                                                                                                                                                consequential or punitive damages, including without
                                                                                                                                                                                limitation, loss of profits, data, use, goodwill, or
                                                                                                                                                                                other intangible losses, resulting from (i) your access
                                                                                                                                                                                to or use of or inability to access or use the Service;
                                                                                                                                                                                (ii) any conduct or content of any third party on the
                                                                                                                                                                                Service; (iii) any content obtained from the Service;
                                                                                                                                                                                and (iv) unauthorized access, use or alteration of your
                                                                                                                                                                                transmissions or content, whether based on warranty,
                                                                                                                                                                                contract, tort (including negligence) or any other
                                                                                                                                                                                legal theory, whether or not we have been informed of
                                                                                                                                                                                the possibility of such damage, and even if a remedy
                                                                                                                                                                                set forth herein is found to have failed of its
                                                                                                                                                                                essential purpose.</p>
                                                                                                                                                                                <h5>Disclaimer</h5>
                                                                                                                                                                                <p>Your use of the Service is at your sole risk. The
                                                                                                                                                                                  Service is provided on an "AS IS" and "AS AVAILABLE"
                                                                                                                                                                                  basis. The Service is provided without warranties of
                                                                                                                                                                                  any kind, whether express or implied, including, but
                                                                                                                                                                                  not limited to, implied warranties of merchantability,
                                                                                                                                                                                  fitness for a particular purpose, non-infringement or
                                                                                                                                                                                  course of performance.</p>
                                                                                                                                                                                  <p>Web Wed, LLC its subsidiaries, affiliates, and its
                                                                                                                                                                                    licensors do not warrant that a) the Service will
                                                                                                                                                                                    function uninterrupted, secure or available at any
                                                                                                                                                                                    particular time or location; b) any errors or defects
                                                                                                                                                                                    will be corrected; c) the Service is free of viruses or
                                                                                                                                                                                    other harmful components; or d) the results of using
                                                                                                                                                                                    the Service will meet your requirements.</p>
                                                                                                                                                                                    <h5>Exclusions</h5>
                                                                                                                                                                                    <p>Some jurisdictions do not allow the exclusion of
                                                                                                                                                                                      certain warranties or the exclusion or limitation of
                                                                                                                                                                                      liability for consequential or incidental damages, so
                                                                                                                                                                                      the limitations above may not apply to you.</p>
                                                                                                                                                                                      <h5>Governing Law</h5>
                                                                                                                                                                                      <p>These Terms shall be governed and construed in
                                                                                                                                                                                        accordance with the laws of Nevada, United States,
                                                                                                                                                                                        without regard to its conflict of law provisions. Our
                                                                                                                                                                                        failure to enforce any right or provision of these
                                                                                                                                                                                        Terms will not be considered a waiver of those rights.
                                                                                                                                                                                        If any provision of these Terms is held to be invalid
                                                                                                                                                                                        or unenforceable by a court, the remaining provisions
                                                                                                                                                                                        of these Terms will remain in effect. These Terms
                                                                                                                                                                                        constitute the entire agreement between us regarding
                                                                                                                                                                                        our Service, and supersede and replace any prior
                                                                                                                                                                                        agreements we might have had between us regarding the
                                                                                                                                                                                        Service.</p>
                                                                                                                                                                                        <h5>Changes</h5>
                                                                                                                                                                                        <p>We reserve the right, at our sole discretion, to
                                                                                                                                                                                          modify or replace these Terms at any time. If a
                                                                                                                                                                                          revision is material we will provide at least 30 days
                                                                                                                                                                                          notice prior to any new terms taking effect. What
                                                                                                                                                                                          constitutes a material change will be determined at our
                                                                                                                                                                                          sole discretion. By continuing to access or use our
                                                                                                                                                                                          Service after any revisions become effective, you agree
                                                                                                                                                                                          to be bound by the revised terms. If you do not agree
                                                                                                                                                                                          to the new terms, you are no longer authorized to use
                                                                                                                                                                                          the Service.</p>
                                                                                                                                                                                          <h5>USER CONDUCT</h5>
                                                                                                                                                                                          <p>Website and App users (“Users”) shall use the
                                                                                                                                                                                            Website and App for lawful, noncommercial (with the
                                                                                                                                                                                            exception of co-branding services discussed in Section
                                                                                                                                                                                            ) purposes only. You agree and acknowledge that User
                                                                                                                                                                                            comments, photographs, videos, and other user materials
                                                                                                                                                                                            (“User Content”) posted on the Website and App
                                                                                                                                                                                            constitute public and not private communications. We
                                                                                                                                                                                            reserve the right in our sole discretion and without
                                                                                                                                                                                            notice to you (i) to refuse to post any User Content,
                                                                                                                                                                                            and (ii) to delete or move User Content. You may not
                                                                                                                                                                                            use or allow others to use your WebWed account to: If
                                                                                                                                                                                            WebWed finds that you and or other parties, views,
                                                                                                                                                                                            guest violates A-H user conduct section. WebWed has the
                                                                                                                                                                                            right to shut down event, disable your account, and no
                                                                                                                                                                                            refunds will be given.<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Post or stream any content that is abusive,
                                                                                                                                                                                            vulgar, obscene, hateful, fraudulent, threatening,
                                                                                                                                                                                            harassing, defamatory, libelous or which discloses
                                                                                                                                                                                            private or personal matters concerning any
                                                                                                                                                                                            person;<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Post or stream any material that you do not have
                                                                                                                                                                                            the right to transmit under law (such as third-party
                                                                                                                                                                                            copyrights, trade secrets, trademarks, securities, or
                                                                                                                                                                                            other proprietary rights) or under contractual or
                                                                                                                                                                                            fiduciary relationships (such as nondisclosure
                                                                                                                                                                                            agreements);<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Post, stream, or link to sexually explicit
                                                                                                                                                                                            material or other material that is hateful or incites
                                                                                                                                                                                            violence;<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Impersonate any person, or falsely state or
                                                                                                                                                                                            otherwise misrepresent your affiliation with a person
                                                                                                                                                                                            or entity;<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Post or stream any advertising, promotional
                                                                                                                                                                                            materials, or other forms of solicitation including
                                                                                                                                                                                            chain letters and pyramid schemes;<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Intentionally violate any other applicable law or
                                                                                                                                                                                            regulation while accessing and using the Website and
                                                                                                                                                                                            APP;<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Deliberately disrupt discussions with repetitive
                                                                                                                                                                                            messages, meaningless messages, or “spam”; or<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>a. Post or stream any file that contains viruses,
                                                                                                                                                                                            malicious code, corrupted files, “Trojan Horses,” or
                                                                                                                                                                                            any other contaminating or destructive features that
                                                                                                                                                                                            may damage someone else’s computer.<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>You further acknowledge and agree that any
                                                                                                                                                                                            statements, actions or opinions expressed in User
                                                                                                                                                                                            Content are those of the User posting such content, and
                                                                                                                                                                                            do not necessarily represent the view or opinions of
                                                                                                                                                                                            WebWed We reserve the right to refuse, delete, or move
                                                                                                                                                                                            any User Content if in our sole discretion it is
                                                                                                                                                                                            inaccurate, abusive, vulgar, obscene, hateful,
                                                                                                                                                                                            fraudulent, threatening, harassing, defamatory,
                                                                                                                                                                                            libelous or is<br>
                                                                                                                                                                                            otherwise in violation of these Terms. We reserve the
                                                                                                                                                                                            right to block any User who violates these Terms from
                                                                                                                                                                                            posting User Content to the Website.&nbsp;<br>
                                                                                                                                                                                          </p>
                                                                                                                                                                                          <p>As stated in terms of service, you agree to
                                                                                                                                                                                            indemnify WebWed and hold us harmless from any actions,
                                                                                                                                                                                            claims, proceedings, or liabilities arising out of your
                                                                                                                                                                                            violation of the Agreements. You are responsible for
                                                                                                                                                                                            all statements made and acts that occur through the use
                                                                                                                                                                                            of your user name and password. Please do not disclose
                                                                                                                                                                                            your password to anybody. If it has been lost or
                                                                                                                                                                                            stolen, contact us as soon as possible by the methods
                                                                                                                                                                                            set forth in terms of service. If notified by a User
                                                                                                                                                                                            that specific User Content does not conform to the
                                                                                                                                                                                            Agreements, WebWed may investigate the allegation and
                                                                                                                                                                                            determine in its sole discretion whether to remove or
                                                                                                                                                                                            request the removal of the User Content. WebWed has no
                                                                                                                                                                                            liability or responsibility to Users for performance or
                                                                                                                                                                                            nonperformance of such actions.</p>
                                                                                                                                                                                          </div>
                                                                                                                                                                                        </div>
                                                                                                                                                                                      </div>
                                                                                                                                                                                    </div>
                                                                                                                                                                                  </div>
                                                                                                                                                                                  <!-- Privacy Modal -->
                                                                                                                                                                                  <div class="modal fade" id="privacy-modal" role="dialog">
                                                                                                                                                                                    <div class="modal-dialog modalwide">
                                                                                                                                                                                      <!-- Modal content-->
                                                                                                                                                                                      <div class="modal-content">
                                                                                                                                                                                        <div class="modal-body row">
                                                                                                                                                                                          <div class="col-xs-12">
                                                                                                                                                                                            <button class="close" data-dismiss="modal" type=
                                                                                                                                                                                            "button">&times;</button>
                                                                                                                                                                                          </div>
                                                                                                                                                                                          <div class="" id="modal-indent">
                                                                                                                                                                                            <h2>Privacy Policy</h2>
                                                                                                                                                                                            <p>Last updated: December 02, 2015</p>
                                                                                                                                                                                            <p>Please read these Terms and Conditions ("Terms",
                                                                                                                                                                                              "Terms and Conditions") carefully before using the
                                                                                                                                                                                              www.webwedmobile.com website and the WEB WED mobile
                                                                                                                                                                                              application (together, or individually, the "Service")
                                                                                                                                                                                              operated by WEB WED, LLC ("us", "we", or "our").</p>
                                                                                                                                                                                              <p>Your access to and use of the Service is conditioned
                                                                                                                                                                                                upon your acceptance of and compliance with these
                                                                                                                                                                                                Terms. These Terms apply to all visitors, users and
                                                                                                                                                                                                others who wish to access or use the Service.<br>
                                                                                                                                                                                                By accessing or using the Service you agree to be bound
                                                                                                                                                                                                by these Terms. If you disagree with any part of the
                                                                                                                                                                                                terms then you do not have permission to access the
                                                                                                                                                                                                Service.</p>
                                                                                                                                                                                                <h5>Communications</h5>
                                                                                                                                                                                                <p>By creating an Account on our service, you agree to
                                                                                                                                                                                                  subscribe to newsletters, marketing or promotional
                                                                                                                                                                                                  materials and other information we may send. However,
                                                                                                                                                                                                  you may opt out of receiving any, or all, of these
                                                                                                                                                                                                  communications from us by following the unsubscribe
                                                                                                                                                                                                  link or instructions provided in any email we send.</p>
                                                                                                                                                                                                  <h5>Purchases</h5>
                                                                                                                                                                                                  <p>If you wish to purchase any product or service made
                                                                                                                                                                                                    available through the Service ("Purchase"), you may be
                                                                                                                                                                                                    asked to supply certain information relevant to your
                                                                                                                                                                                                    Purchase including, without limitation, your credit
                                                                                                                                                                                                    card number, the expiration date of your credit card,
                                                                                                                                                                                                    your billing address, and your shipping
                                                                                                                                                                                                    information.</p>
                                                                                                                                                                                                  </div>
                                                                                                                                                                                                </div>
                                                                                                                                                                                              </div>
                                                                                                                                                                                            </div>
                                                                                                                                                                                          </div>
                                                                                                                                                                                          <!-- Disclaimer Modal -->
                                                                                                                                                                                          <div class="modal fade" id="disclaimer-modal" role="dialog">
                                                                                                                                                                                            <div class="modal-dialog modalwide">
                                                                                                                                                                                              <!-- Modal content-->
                                                                                                                                                                                              <div class="modal-content">
                                                                                                                                                                                                <div class="modal-body row">
                                                                                                                                                                                                  <div class="col-xs-12">
                                                                                                                                                                                                    <button class="close" data-dismiss="modal" type=
                                                                                                                                                                                                    "button">&times;</button>
                                                                                                                                                                                                  </div>
                                                                                                                                                                                                  <div class="" id="modal-indent">
                                                                                                                                                                                                    <h2>Disclaimer</h2>
                                                                                                                                                                                                    <p>No warranties</p>
                                                                                                                                                                                                    <p>This site and applications are provided
                                                                                                                                                                                                      &ldquo;as-is&rdquo; without any representations or
                                                                                                                                                                                                      warranties, express or implied. Web Wed, LLC makes no
                                                                                                                                                                                                      representations or warranties in relation to this
                                                                                                                                                                                                      website or the information and materials provided.</p>
                                                                                                                                                                                                      <p>Without prejudice to the generality of the
                                                                                                                                                                                                        aforementioned paragraph, Web Wed, LLC does not warrant
                                                                                                                                                                                                        that:</p>
                                                                                                                                                                                                        <p>this website will be constantly available, or
                                                                                                                                                                                                          available at all; or the information on this website is
                                                                                                                                                                                                          complete, true, accurate or non-misleading.</p>
                                                                                                                                                                                                          <p>Nothing on this website constitutes, or is meant to
                                                                                                                                                                                                            constitute, advice of any kind. If you require advice
                                                                                                                                                                                                            in relation to any [legal, financial or medical matter
                                                                                                                                                                                                            you should consult a appropriate professional
                                                                                                                                                                                                            service.</p>
                                                                                                                                                                                                            <p>Limitations of liability</p>
                                                                                                                                                                                                            <p>Web Wed, LLC will not be liable to you in relation
                                                                                                                                                                                                              to the contents of, or use of, or otherwise in
                                                                                                                                                                                                              connection with, this website:</p>
                                                                                                                                                                                                              <p>to the extent that the website is provided
                                                                                                                                                                                                                free-of-charge, for any direct loss; for any indirect,
                                                                                                                                                                                                                special or consequential loss; or for any business
                                                                                                                                                                                                                losses, loss of revenue, income, profits or anticipated
                                                                                                                                                                                                                savings, loss of contracts or business relationships,
                                                                                                                                                                                                                loss of reputation or goodwill, or loss or corruption
                                                                                                                                                                                                                of information or data.</p>
                                                                                                                                                                                                                <p>These limitations of liability still apply even if
                                                                                                                                                                                                                  Web Wed, LLC has been expressly advised of the
                                                                                                                                                                                                                  potential loss.</p>
                                                                                                                                                                                                                  <p>Exceptions</p>
                                                                                                                                                                                                                  <p>Nothing in this website disclaimer will exclude or
                                                                                                                                                                                                                    limit any warranty implied by law that it would be
                                                                                                                                                                                                                    unlawful to exclude or limit; and nothing in this
                                                                                                                                                                                                                    website disclaimer will exclude or limit Web Wed's
                                                                                                                                                                                                                    liability in respect of any:</p>
                                                                                                                                                                                                                    <p>a death or personal injury caused by Web Wed's
                                                                                                                                                                                                                      negligence; fraud or fraudulent misrepresentation on
                                                                                                                                                                                                                      the part of Web Wed, LLC; or matter which it would be
                                                                                                                                                                                                                      illegal or unlawful for Web Wed, LLC to exclude or
                                                                                                                                                                                                                      limit, or to attempt or purport to exclude or limit,
                                                                                                                                                                                                                      its liability.</p>
                                                                                                                                                                                                                      <p>Other parties</p>
                                                                                                                                                                                                                      <p>You accept that, as a limited liability entity, Web
                                                                                                                                                                                                                        Wed, LLC has an interest in limiting the personal
                                                                                                                                                                                                                        liability of its officers and employees. You agree that
                                                                                                                                                                                                                        you will not bring any claim personally against Web
                                                                                                                                                                                                                        Wed's officers or employees in respect of any losses
                                                                                                                                                                                                                        you suffer in connection with the website</p>
                                                                                                                                                                                                                        <p>Without prejudice to the foregoing paragraph, you
                                                                                                                                                                                                                          agree that the limitations of warranties and liability
                                                                                                                                                                                                                          set out in this website disclaimer will protect Web
                                                                                                                                                                                                                          Wed's officers, employees, agents, subsidiaries,
                                                                                                                                                                                                                          successors, assigns and sub-contractors as well as Web
                                                                                                                                                                                                                          Wed, LLC.</p>
                                                                                                                                                                                                                          <p>Unenforceable provisions</p>
                                                                                                                                                                                                                          <p>If any provision of this website disclaimer is, or
                                                                                                                                                                                                                            is found to be, unenforceable under applicable law,
                                                                                                                                                                                                                            that will not affect the enforceability of the other
                                                                                                                                                                                                                            provisions of this website disclaimer.</p>
                                                                                                                                                                                                                          </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                      </div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                  </div>
                                                                                                                                                                                                                  <div class="modal fade" id="about-modal" role="dialog">
                                                                                                                                                                                                                    <div class="modal-dialog modalwide">
                                                                                                                                                                                                                      <!-- Modal content-->
                                                                                                                                                                                                                      <div class="modal-content">
                                                                                                                                                                                                                        <div class="modal-body row">
                                                                                                                                                                                                                          <div class="col-xs-12">
                                                                                                                                                                                                                            <button class="close" data-dismiss="modal" type=
                                                                                                                                                                                                                            "button">&times;</button>
                                                                                                                                                                                                                          </div>
                                                                                                                                                                                                                          <div class="" id="modal-indent">
                                                                                                                                                                                                                            <h2>About WebWedMobile</h2>
                                                                                                                                                                                                                            <p>WebWed Mobile is a cross-platform mobile application that will revolutionize the wedding industry, by providing an optical indirect human experience. Our objective is to merge the three most powerful elements of the world: Love, Law, and Technology, to afford individuals from all walks of life and corners of the world, the opportunity to wed on an affordable global platform. Uniquely, the development of our raw komu tequenology has unlocked the doors to a new way of sharing special moments with family and friends. Therefore, WebWed Mobile is dedicated to ushering couples of all socio-economic backgrounds into the new marital digital world, on their time schedule, through a virtual stage. As a direct result, the judicial system will be eternally altered as the evolution of technology and law fuse to accommodate the new normal, where roaming-o meets emolji-et.

                                                                                                                                                                                                                              In an age where divorce rates are skyrocketing due to debt obligations from the wedding ceremony, WebWed mobile offers a necessary avenue to diminish debt and redirect the economic focus back onto love and family. Providing marital freedom, financial freedom, marriage equality and a resolution to marriage discriminations across the country, are major fundamentals that WebWed Mobile stands for. Our ultimate aim is to encourage the local and global communities to move from the mindset of the “impossible” to a statement of “I’m possible”. With love as the primary foundation, law as the legally binding agent and technology as the human way of life, WebWeb Mobile is proven to be an asset to the human experience in all communities. </p>
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                          </div>
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                      </div>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    <!-- Disclaimer Modal -->
                                                                                                                                                                                                                    <div class="modal fade" id="disclaimer-modal" role="dialog">
                                                                                                                                                                                                                      <div class="modal-dialog modalwide">
                                                                                                                                                                                                                        <!-- Modal content-->
                                                                                                                                                                                                                        <div class="modal-content">
                                                                                                                                                                                                                          <div class="modal-body row">
                                                                                                                                                                                                                            <div class="col-xs-12">
                                                                                                                                                                                                                              <button class="close" data-dismiss="modal" type=
                                                                                                                                                                                                                              "button">&times;</button>
                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                            <div class="" id="modal-indent">
                                                                                                                                                                                                                              <h2>Disclaimer</h2>
                                                                                                                                                                                                                              <p>No warranties</p>
                                                                                                                                                                                                                              <p>This site and applications are provided
                                                                                                                                                                                                                                &ldquo;as-is&rdquo; without any representations or
                                                                                                                                                                                                                                warranties, express or implied. Web Wed, LLC makes no
                                                                                                                                                                                                                                representations or warranties in relation to this
                                                                                                                                                                                                                                website or the information and materials provided.</p>
                                                                                                                                                                                                                                <p>Without prejudice to the generality of the
                                                                                                                                                                                                                                  aforementioned paragraph, Web Wed, LLC does not warrant
                                                                                                                                                                                                                                  that:</p>
                                                                                                                                                                                                                                  <p>this website will be constantly available, or
                                                                                                                                                                                                                                    available at all; or the information on this website is
                                                                                                                                                                                                                                    complete, true, accurate or non-misleading.</p>
                                                                                                                                                                                                                                    <p>Nothing on this website constitutes, or is meant to
                                                                                                                                                                                                                                      constitute, advice of any kind. If you require advice
                                                                                                                                                                                                                                      in relation to any [legal, financial or medical matter
                                                                                                                                                                                                                                      you should consult a appropriate professional
                                                                                                                                                                                                                                      service.</p>
                                                                                                                                                                                                                                      <p>Limitations of liability</p>
                                                                                                                                                                                                                                      <p>Web Wed, LLC will not be liable to you in relation
                                                                                                                                                                                                                                        to the contents of, or use of, or otherwise in
                                                                                                                                                                                                                                        connection with, this website:</p>
                                                                                                                                                                                                                                        <p>to the extent that the website is provided
                                                                                                                                                                                                                                          free-of-charge, for any direct loss; for any indirect,
                                                                                                                                                                                                                                          special or consequential loss; or for any business
                                                                                                                                                                                                                                          losses, loss of revenue, income, profits or anticipated
                                                                                                                                                                                                                                          savings, loss of contracts or business relationships,
                                                                                                                                                                                                                                          loss of reputation or goodwill, or loss or corruption
                                                                                                                                                                                                                                          of information or data.</p>
                                                                                                                                                                                                                                          <p>These limitations of liability still apply even if
                                                                                                                                                                                                                                            Web Wed, LLC has been expressly advised of the
                                                                                                                                                                                                                                            potential loss.</p>
                                                                                                                                                                                                                                            <p>Exceptions</p>
                                                                                                                                                                                                                                            <p>Nothing in this website disclaimer will exclude or
                                                                                                                                                                                                                                              limit any warranty implied by law that it would be
                                                                                                                                                                                                                                              unlawful to exclude or limit; and nothing in this
                                                                                                                                                                                                                                              website disclaimer will exclude or limit Web Wed's
                                                                                                                                                                                                                                              liability in respect of any:</p>
                                                                                                                                                                                                                                              <p>a death or personal injury caused by Web Wed's
                                                                                                                                                                                                                                                negligence; fraud or fraudulent misrepresentation on
                                                                                                                                                                                                                                                the part of Web Wed, LLC; or matter which it would be
                                                                                                                                                                                                                                                illegal or unlawful for Web Wed, LLC to exclude or
                                                                                                                                                                                                                                                limit, or to attempt or purport to exclude or limit,
                                                                                                                                                                                                                                                its liability.</p>
                                                                                                                                                                                                                                                <p>Other parties</p>
                                                                                                                                                                                                                                                <p>You accept that, as a limited liability entity, Web
                                                                                                                                                                                                                                                  Wed, LLC has an interest in limiting the personal
                                                                                                                                                                                                                                                  liability of its officers and employees. You agree that
                                                                                                                                                                                                                                                  you will not bring any claim personally against Web
                                                                                                                                                                                                                                                  Wed's officers or employees in respect of any losses
                                                                                                                                                                                                                                                  you suffer in connection with the website</p>
                                                                                                                                                                                                                                                  <p>Without prejudice to the foregoing paragraph, you
                                                                                                                                                                                                                                                    agree that the limitations of warranties and liability
                                                                                                                                                                                                                                                    set out in this website disclaimer will protect Web
                                                                                                                                                                                                                                                    Wed's officers, employees, agents, subsidiaries,
                                                                                                                                                                                                                                                    successors, assigns and sub-contractors as well as Web
                                                                                                                                                                                                                                                    Wed, LLC.</p>
                                                                                                                                                                                                                                                    <p>Unenforceable provisions</p>
                                                                                                                                                                                                                                                    <p>If any provision of this website disclaimer is, or
                                                                                                                                                                                                                                                      is found to be, unenforceable under applicable law,
                                                                                                                                                                                                                                                      that will not affect the enforceability of the other
                                                                                                                                                                                                                                                      provisions of this website disclaimer.</p>
                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                  </div>
                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                              </div>
                                                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                                            <div class="modal fade" id="youtubeModal" role="dialog">
                                                                                                                                                                                                                                              <div class="modal-dialog modalwide">
                                                                                                                                                                                                                                                <div class="modal-content">
                                                                                                                                                                                                                                                  <div class="modal-header">
                                                                                                                                                                                                                                                    <button class="close" data-dismiss="modal" type=
                                                                                                                                                                                                                                                    "button"><span aria-hidden="true">×</span><span class=
                                                                                                                                                                                                                                                    "sr-only">Close</span></button>
                                                                                                                                                                                                                                                    <h4 class="modal-title" id="myModalLabel"><i class=
                                                                                                                                                                                                                                                      "fa fa-share-alt"></i> Marriage Education</h4>
                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                    <div class="modal-body">
                                                                                                                                                                                                                                                      <iframe class="embed-responsive-item" allowfullscreen="" frameborder="0" height="315"
                                                                                                                                                                                                                                                      scrolling="yes" src=
                                                                                                                                                                                                                                                      "https://www.youtube.com/embed/j6E2FHVcawE"></iframe>
                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                    <div class="modal-footer"></div>
                                                                                                                                                                                                                                                  </div>
                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                              </div>
                                                                                                                                                                                                                                              <div class="modal fade" id="iframeModal" role="dialog">
                                                                                                                                                                                                                                                <div class="modal-dialog modalwide">
                                                                                                                                                                                                                                                  <div class="modal-content">
                                                                                                                                                                                                                                                    <div class="modal-header">
                                                                                                                                                                                                                                                      <button class="close" data-dismiss="modal" type=
                                                                                                                                                                                                                                                      "button"><span aria-hidden="true">×</span><span class=
                                                                                                                                                                                                                                                      "sr-only">Close</span></button>
                                                                                                                                                                                                                                                      <!-- <h4 class="modal-title" id="myModalLabel"><i class="fa fa-share-alt"></i> Marriage Education</h4> -->
                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                    <div class="modal-body">
                                                                                                                                                                                                                                                      <iframe class="embed-responsive-item" allowfullscreen="" frameborder="0" height="315" id=
                                                                                                                                                                                                                                                      "idFrame" name="idFrame" scrolling="yes" width=
                                                                                                                                                                                                                                                      "560"></iframe>
                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                    <div class="modal-footer"></div>
                                                                                                                                                                                                                                                  </div>
                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                              </div>
                                                                                                                                                                                                                                              <!--Register Modal -->
                                                                                                                                                                                                                                              <div class="modal hide fade" id="wwRegister" role="dialog" tabindex="-1">
                                                                                                                                                                                                                                                <div class="modal-header">
                                                                                                                                                                                                                                                  <button class="close" data-dismiss="modal" type="button">×</button>
                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                                <div class="modal-body">
                                                                                                                                                                                                                                                  <iframe frameborder="0" scrolling="yes" src="" style=
                                                                                                                                                                                                                                                  "height: 100vh;" width="99.6%"></iframe>
                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                                <div class="modal-footer">
                                                                                                                                                                                                                                                  <button class="btn" data-dismiss="modal">OK</button>
                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                              </div>
                                                                                                                                                                                                                                              <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                                                                                                                                                                                                                              <script src="js/bootstrap.min.js"></script>
                                                                                                                                                                                                                                              <script src="js/modernizr.js"></script>
                                                                                                                                                                                                                                              <script>
                                                                                                                                                                                                                                              function submitcontact(){
                                                                                                                                                                                                                                                var name = $("#contactnamefield").val();
                                                                                                                                                                                                                                                var email = $("#contactemailfield").val();
                                                                                                                                                                                                                                                var question = $("#contactquestionfield").val();
                                                                                                                                                                                                                                                var message = $("#contactmessage").val();

                                                                                                                                                                                                                                                var messageComposed = '';

                                                                                                                                                                                                                                                $.ajax({
                                                                                                                                                                                                                                                  url: apiEndpoint + "?cmd=contactus",
                                                                                                                                                                                                                                                  contentType: "application/x-www-form-urlencoded",
                                                                                                                                                                                                                                                  dataType: "json",
                                                                                                                                                                                                                                                  headers: {
                                                                                                                                                                                                                                                    'Content-Type': 'application/x-www-form-urlencoded',
                                                                                                                                                                                                                                                    'Accept': 'application/json'
                                                                                                                                                                                                                                                  },
                                                                                                                                                                                                                                                  method: 'POST',
                                                                                                                                                                                                                                                  data: {
                                                                                                                                                                                                                                                    name: name,
                                                                                                                                                                                                                                                    email: email,
                                                                                                                                                                                                                                                    message: messageComposed
                                                                                                                                                                                                                                                  }
                                                                                                                                                                                                                                                }).done(function(data) {
                                                                                                                                                                                                                                                  $('.contact-success').show();
                                                                                                                                                                                                                                                  $('.contact-error').hide();
                                                                                                                                                                                                                                                  $("#contactnamefield").val('');
                                                                                                                                                                                                                                                  $("#contactemailfield").val('');
                                                                                                                                                                                                                                                  $("#contactquestionfield").val('');
                                                                                                                                                                                                                                                  $("#contactmessage").val('');

                                                                                                                                                                                                                                                });
                                                                                                                                                                                                                                              }
                                                                                                                                                                                                                                              </script> <!-- <script src="js/social-share-kit.min.js"></script> -->

                                                                                                                                                                                                                                              <script src="js/index.js"></script>
                                                                                                                                                                                                                                              <script src="js/smartbanner.js"></script>
                                                                                                                                                                                                                                              <script type="text/javascript">
                                                                                                                                                                                                                                              $(function() { $.smartbanner() } )
                                                                                                                                                                                                                                              </script>
                                                                                                                                                                                                                                              <!-- <link href="css/webwed.css" rel="stylesheet" type="text/css" > -->
                                                                                                                                                                                                                                              <!--This site's Design and Source Code Is Property of Riveloper, LLC | Atlanta, GA until otherwise noted.-->
                                                                                                                                                                                                                                            </body>
                                                                                                                                                                                                                                            </html>
