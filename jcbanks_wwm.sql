-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2016 at 09:38 AM
-- Server version: 5.5.52-cll
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jcbanks_wwm`
--

-- --------------------------------------------------------

--
-- Table structure for table `ceremony_options`
--

CREATE TABLE IF NOT EXISTS `ceremony_options` (
  `co_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float(10,2) NOT NULL,
  `usertype` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`co_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ceremony_options`
--

INSERT INTO `ceremony_options` (`co_id`, `name`, `description`, `price`, `usertype`) VALUES
(1, 'Stream Wedding Ceremony', 'Single video stream centered around your wedding experience for consecutive 30 minutes, and can be shared publicly or privately.', 249.99, 1),
(2, 'Traditional Wedding / Vow Renewal', 'Five live video broadcasts for consecutive 30 minutes that can be publicly or privately shared.', 399.99, 1),
(3, 'Proxy Wedding', 'Sponsored five live video broadcasts (wedding) for consecutive 30 minutes that can be shared publicly or privately.', 599.99, 1),
(4, 'Officiant Package', 'Monthly subscription to conduct WebWed Ceremonies.', 9.99, 0),
(5, 'Special Moments', 'Single video stream for consecutive 30 minutes, and can be shared publicly or privately.', 149.99, 1);

-- --------------------------------------------------------

--
-- Table structure for table `discount_codes`
--

CREATE TABLE IF NOT EXISTS `discount_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `discount_codes`
--

INSERT INTO `discount_codes` (`id`, `code`, `amount`) VALUES
(8, 'FRIENDS AND FAMILY', 25),
(9, 'PAY IT FORWARD', 50),
(10, 'STREAM WEDDING', 249),
(11, 'PROXY', 599),
(12, 'ONLINE WEDDING', 399),
(13, 'SPECIAL MOMENTS', 149),
(14, 'OFFICIANT ', 9);

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE IF NOT EXISTS `guests` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_email` varchar(255) NOT NULL,
  `guest_name` varchar(255) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `event_code` varchar(255) NOT NULL,
  PRIMARY KEY (`guest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ministers`
--

CREATE TABLE IF NOT EXISTS `ministers` (
  `minister_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `religious_affiliation` varchar(255) NOT NULL,
  `bio` varchar(255) NOT NULL DEFAULT '',
  `gender` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(6) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `performing_states` varchar(255) NOT NULL,
  `perform_willingness` int(11) NOT NULL,
  `available_times` varchar(255) NOT NULL DEFAULT '',
  `available_days` varchar(255) NOT NULL DEFAULT '',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `profile_completed` int(11) NOT NULL DEFAULT '0',
  `admin_approved` int(11) NOT NULL DEFAULT '0',
  `membership_expiry` date NOT NULL,
  `subscription_id` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`minister_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ministers`
--

INSERT INTO `ministers` (`minister_id`, `name`, `email`, `password`, `religious_affiliation`, `bio`, `gender`, `phone`, `photo`, `address`, `city`, `state`, `zip`, `birthdate`, `payment_date`, `performing_states`, `perform_willingness`, `available_times`, `available_days`, `created_date`, `profile_completed`, `admin_approved`, `membership_expiry`, `subscription_id`) VALUES
(6, 'Deborah Cooper', 'dkcooper10@gmail.com', '7d96c87e7df13632fb939628e79494cd', '', '', 0, '', '', '', '', '', '', '', '0000-00-00', '', 0, '', '', '2016-10-06 03:22:13', 0, 0, '0000-00-00', '0'),
(7, 'Linda Clay', 'chaplainclay@yahoo.com', '04a36e43715f361a98b20c741c51a799', 'Other', 'SSBsb3ZlIHBlb3BsZSBhbmQgbG92ZSB0byBzaG93IHRoZSBMb3ZlIG9mIEdvZC4g', 1, '(317) 681-2818', '', '1500 Southland Circle', 'Atlanta', 'GEORGIA', '30318', '8/4/1964', '0000-00-00', '["IN"]', 0, '{"start":"8:00 AM","end":"8:00 PM"}', '', '2016-10-26 15:21:24', 1, 0, '0000-00-00', '0'),
(9, 'Cabrina Jeffers', 'christiancompanions@yahoo.com', 'd41d8cd98f00b204e9800998ecf8427e', 'Christian', 'SSBhbSBhIE9yZGFpbmVkIChNaW5pc3RlcikgL0VsZGVyDQpJIGFtIHRoZSBDby1BdXRob3Igb2YgdGhlIGJvb2sgUHJpbmNpcGxlcyBvZiBSb21hbmNlDQpJIGhhdmUgb3ZlciAyMCsgWWVhciBFeHAgYXMgQSBNaXN0cmVzcyBvZiBDZXJlbW9uaWVzL0Fubm91bmNlciBAIFdlZGRpbmcgUmVjZXB0aW9ucw==', 1, '(678) 490-4867', '147881519920160911_140531.jpg', '321 Bowling Avenue Columbia Sc 29203', 'Atlanta', 'GEORGIA', '30088', '1/28/1967', '0000-00-00', '["AL","AZ","AR","CA","CO","CT","DE","FL","GA","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]', 0, '{"start":"9:30 AM","end":"8:00 PM"}', '', '2016-11-10 21:59:59', 1, 0, '0000-00-00', 'cus_9XUii38Gl1wEfp'),
(10, 'Austin Sweat', 'sweat.austin@riveloper.com', '96d24f4feb7794cb1544cec404dfe4b2', 'Other', 'SGVsbG8sIFdvcmxkLg==', 0, '(678) 467-8435', '', '203 Old Zion Cemetery Rd', 'Loganville', 'GEORGIA', '30052', '12/23/1997', '0000-00-00', '["AL","AZ","AR","CA","CO","CT","DE","FL","GA","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"]', 1, '{"start":"8:00 AM","end":"8:00 PM"}', '', '2016-11-12 00:41:59', 1, 0, '0000-00-00', 'cus_9XvRnsAJrLVWxN'),
(11, 'Nathan slaton', 'Weddingsbynate@gmail.com', 'f8b69c161eabde8d8eb88a46abce7cc5', 'Christian', 'SSB3b3VsZCBsb3ZlIHRvIGRvIHlvdXIgd2VkZGluZyEgSSBsb3ZlIGpvaW5pbmcgcGVvcGxlIHRoYXQgbG92ZSBlYWNoIG90aGVyIGluIG1hcnJpYWdlLiA=', 0, '(404) 729-0527', '1479257162IMG_6296.JPG', '8050 Johnson rd', 'Palmetto', 'GEORGIA', '30268', '1/26/1979', '0000-00-00', '["GA"]', 1, '{"start":"8:00 AM","end":"11:00 PM"}', '', '2016-11-16 14:04:03', 1, 0, '0000-00-00', 'cus_9ZQUYjvWlzktQw'),
(12, 'Kenji clark', 'kenkalclak@yahoo.com', 'd41d8cd98f00b204e9800998ecf8427e', 'Other', 'TWluaXN0ZXIuIEhhcHBpbHkgbWFycmllZCB3LyA2IGtpZHM=', 0, '(901) 679-2885', '', '1544 Gold', 'Memphis', 'TENNESSEE', '38106', '4/1/1979', '0000-00-00', '["MS","TN"]', 0, '{"start":"10:00 AM","end":"7:00 PM"}', '', '2016-11-16 18:54:39', 1, 0, '0000-00-00', 'cus_9ZhxIRLaezfW1c');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `usertype` int(11) NOT NULL DEFAULT '0',
  `payment_amount` varchar(255) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `product` int(11) NOT NULL,
  `timestamp` varchar(255) NOT NULL,
  `discount_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `uid`, `usertype`, `payment_amount`, `txn_id`, `product`, `timestamp`, `discount_id`) VALUES
(53, 99, 0, '0.99', 'NOTXNID', 1, '1474574684', 10),
(54, 1, 0, '0.0999', 'txn_18xwXhKzznlrMvESo2n6Vazq', 4, '1474886461', 0),
(55, 2, 0, '0.0999', 'txn_18xwZ7KzznlrMvEStX6qBgFD', 4, '1474886549', 0),
(56, 3, 0, '0.0999', 'txn_18xwa7KzznlrMvESh2NBektc', 4, '1474886611', 0),
(57, 4, 0, '0.0999', 'txn_18xwbCKzznlrMvESUVIJ5CPh', 4, '1474886678', 0),
(58, 5, 0, '0.0999', 'txn_18xwcQKzznlrMvESBXzWWWC0', 4, '1474886754', 0),
(59, 101, 0, '599.99', 'txn_18xwgxKzznlrMvESTidwAksn', 3, '1474887035', 0),
(60, 105, 0, '0.99', 'NOTXNID', 1, '1475089401', 10),
(61, 107, 0, '0.99', 'NOTXNID', 5, '1475255442', 13),
(62, 108, 0, '0.99', 'NOTXNID', 1, '1475685179', 10),
(63, 109, 0, '0.99', 'NOTXNID', 5, '1475687649', 13),
(64, 110, 0, '0.99', 'NOTXNID', 5, '1475689514', 13),
(65, 9, 0, '0.0999', 'txn_19EPieKzznlrMvES1r7vItGQ', 0, '1478811864', 0),
(66, 10, 0, '9.99', 'TRIAL PAYMENT', 0, '1478911319', 0),
(67, 11, 0, '9.99', 'TRIAL PAYMENT', 4, '1479257464', 0),
(68, 12, 0, '9.99', 'TRIAL PAYMENT', 4, '1479322453', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reset_tokens`
--

CREATE TABLE IF NOT EXISTS `reset_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `reset_tokens`
--

INSERT INTO `reset_tokens` (`id`, `uid`, `token`) VALUES
(3, 99, 'a709909b1ea5c2bee24248203b1728a5');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `event_code`, `session_id`) VALUES
(1, 'SRLZZ', '1_MX40NTY5MjA5Mn5-MTQ3NDg0OTA2MzMzN35pMzFFV09ka3Zhano1UEUwNkpvYWNidGV-fg'),
(2, 'JHF8V', '1_MX40NTY5MjA5Mn5-MTQ3NDg4Nzg5OTg2OX5hUlpjK0Y0ZWg3Vzd6VWx4a3hmcHN1WTd-fg'),
(3, 'VQVRL', '1_MX40NTY5MjA5Mn5-MTQ3NTg3NzA0MDI4OH5lQU5zZWRmazM4YzVxSjdXYnZlMVNMbWp-QX4'),
(4, 'N4ZXS', '1_MX40NTY5MjA5Mn5-MTQ3NTI0NTY4MDE0Nn40bXQyaCt6TTJtQ2cyb0I1VFdCd2twNkt-fg'),
(5, 'QLGNK', '2_MX40NTY5MjA5Mn5-MTQ3NTg3NzEyNTExMn5ZYzFZSzltK2NDM1gxcWpYTlJGYXRzVyt-QX4'),
(6, 'KXV66', '1_MX40NTY5MjA5Mn5-MTQ3NTg3NzI5NzgxMn5pVllFd2Nqa05BREl2djZubGtmZ2hCcVh-QX4'),
(7, 'ZXOTO', '2_MX40NTY5MjA5Mn5-MTQ3NjAyMTkzMTA2MX42cmQ1cjQ4eFc5Q2c1SWJwK2xudk93TUl-fg');

-- --------------------------------------------------------

--
-- Table structure for table `special_moments`
--

CREATE TABLE IF NOT EXISTS `special_moments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `event_code` varchar(255) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_date` varchar(255) NOT NULL,
  `event_time` varchar(255) NOT NULL,
  `giftlink` varchar(255) NOT NULL,
  `gifts` int(11) NOT NULL,
  `public` int(11) NOT NULL,
  `chat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `special_moments`
--

INSERT INTO `special_moments` (`id`, `uid`, `event_code`, `event_name`, `event_date`, `event_time`, `giftlink`, `gifts`, `public`, `chat`) VALUES
(42, 99, 'SRLZZ', 'GREG AND MIKE WEDDING', '9/27/2016', '4:20 PM', 'https://www.honeyfund.com/wedding/MichaelandGreg4ever', 1, 1, 1),
(43, 105, 'N4ZXS', 'MOOK AND CHECHE  WEDDING', '', '', 'https://www.honeyfund.com/wedding/livelaughlovechat', 1, 1, 1),
(44, 106, 'VQVRL', 'Test moment on firewall', '', '', 'Http://Www.target.com', 0, 1, 0),
(45, 107, 'VSCEM', 'AFRICA', '', '', '', 0, 0, 0),
(46, 108, 'QLGNK', 'EdholmlsWhereTheHuckls', '', '', 'https://www.honeyfund.com/wedding/EdholmIsWhereTheHuckIs', 1, 1, 1),
(47, 109, 'KXV66', 'GBLC Annual Conference', '', '', '', 0, 1, 1),
(48, 110, 'ZXOTO', 'ATLANTA PRIDE', '', '', '', 0, 1, 1),
(49, 111, 'JBF4W', 'THE FULLER AFFAIR', '7/23/2017', '12:00 PM', 'https://www.honeyfund.com/OurRegistryNew#dashboard', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3111 ;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `token`, `uid`, `type`) VALUES
(3072, '843084f64c63d49053da3c333a2225e7', 110, 2),
(2986, '988cc9eca004bb6efa3d460e13a85a15', 109, 2),
(3060, 'ecf12f732a75447d987957291c9d4bc8', 108, 2),
(2822, '1c78b7d95d17ba0b9c69d82920aa22f7', 105, 2),
(3098, '9c625b025edf0efd29cd1dbdfdf191d9', 106, 2),
(2665, '0be494887635ca3067717d1749d34cef', 2, 1),
(2565, '59b9eff422cbc5d623eed62d8d43675e', 104, 2),
(2562, 'e46f1763591ab377c4435b0975ba146b', 102, 2),
(2561, 'e287b87a1790ff62da4534a85f136eab', 103, 2),
(2611, 'b6c505039e3a1e8bf9f622ea6440ecb2', 101, 2),
(2412, '604e1bb085e218207c14d15eb0aeda53', 4, 3),
(2669, '63e49b8c4f3fb6910d6b04b7ffee5149', 99, 2),
(3097, '4735df961a69ab0ee3fd5a25dd868d11', 0, 3),
(3110, 'a1ac3e488def8067ce4ca5ded5fa9f041614641477453f69be03040cb123ea29', 100, 5);

-- --------------------------------------------------------

--
-- Table structure for table `weddings`
--

CREATE TABLE IF NOT EXISTS `weddings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `type_of_wedding` int(11) NOT NULL,
  `state_located` varchar(255) NOT NULL,
  `event_date` varchar(255) NOT NULL,
  `event_time` varchar(255) NOT NULL,
  `marriage_counseling` varchar(255) NOT NULL,
  `yours_uid` int(11) NOT NULL DEFAULT '0',
  `first_witness_uid` int(11) NOT NULL DEFAULT '0',
  `second_witness_uid` int(11) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) NOT NULL,
  `activation_status` varchar(255) NOT NULL DEFAULT 'inactive',
  `marriage_education_certificate` varchar(255) NOT NULL,
  `pre_marriage_request_date` date NOT NULL,
  `upload_pre_marriage_education_certificate` varchar(255) NOT NULL,
  `brief_description` text NOT NULL,
  `officiant_picks` varchar(255) NOT NULL,
  `religious_affliation` varchar(255) NOT NULL,
  `assigned_officiant_uid` int(11) NOT NULL,
  `event_code` varchar(255) NOT NULL,
  `marriage_certificate` varchar(255) NOT NULL,
  `giftlink` varchar(255) NOT NULL,
  `public` int(11) NOT NULL,
  `chat` int(11) NOT NULL,
  `gifts` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `weddings`
--

INSERT INTO `weddings` (`id`, `uid`, `type_of_wedding`, `state_located`, `event_date`, `event_time`, `marriage_counseling`, `yours_uid`, `first_witness_uid`, `second_witness_uid`, `activation_code`, `activation_status`, `marriage_education_certificate`, `pre_marriage_request_date`, `upload_pre_marriage_education_certificate`, `brief_description`, `officiant_picks`, `religious_affliation`, `assigned_officiant_uid`, `event_code`, `marriage_certificate`, `giftlink`, `public`, `chat`, `gifts`) VALUES
(1, 101, 3, 'GEORGIA', '9/29/2016', '2:30 PM', '', 103, 102, 104, 'activationcode', 'inactive', '', '0000-00-00', '', 'testing ', '1, 2, 3, 4, 5', 'other', 2, 'JHF8V', '', 'http://google.com', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `www_users_new`
--

CREATE TABLE IF NOT EXISTS `www_users_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` int(11) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `timezone` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `permission_level` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `www_users_new`
--

INSERT INTO `www_users_new` (`id`, `name`, `email`, `password`, `gender`, `birthdate`, `street`, `city`, `state`, `zip`, `timezone`, `phone`, `permission_level`) VALUES
(99, 'Michael Renzulli ', 'mgrg7870@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, '6/12/1961', '510 East Lake Drive ', 'Marietta', 'GEORGIA', '30062', 'America/New_York', '(404) 845-6008', 1),
(100, 'Junikia Banks', 'info@webwedmobile.com', '82d2e1eb0db0e1a8ace8118b67f32b9c', 0, '1/1/1980', '555 Mary Lane', 'Symrna', 'GEORGIA', '30062', 'America/New_York', '(555) 123-1234', 5),
(101, 'Austin Sweat', 'yours@riveloper.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, '12/23/1997', '123 bradmore ct', 'loganville', 'GEORGIA', '30052', 'America/New_York', '(678) 123-1234', 1),
(102, 'John Doe', 'witness1@riveloper.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, '10/25/1994', '123 main st', 'loganville', 'GEORGIA', '30052', 'America/New_York', '(123) 123-4567', 1),
(103, 'Jane Doe', 'yours2@riveloper.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, '2/6/1990', '123 main st', 'loganville', 'GEORGIA', '30052', 'America/New_York', '(123) 412-4123', 1),
(104, 'Josh Blane', 'witness2@riveloper.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, '6/19/1990', '123 main st', 'loganville', 'GEORGIA', '30052', 'America/New_York', '(123) 678-1244', 1),
(105, 'Marvin Chatman', 'sunshinesims84@gmail.com', '9bf3cff84721dcfff3586ac6fc449d64', 0, '1/1/Year', '1290 Jackson Ponce Cricle', 'Cordova', 'TENNESSEE', '38018', 'America/Costa_Rica', '(901) 338-0282', 1),
(106, 'Test', 'test@test.com', 'e10adc3949ba59abbe56e057f20f883e', 0, '1/1/1980', '123 Main Street', 'duluth', 'GEORGIA', '30097', 'America/New_York', '(855) 272-2777', 1),
(107, 'ERICA THOMAS', 'erica@electericathomas.com', '3089bf186430b7cad43fb7e2460e88b5', 1, '6/9/1984', '1500 Southland Circle Suite C ', 'Atlanta', 'GEORGIA', '30317', 'Africa/Abidjan', '(404) 621-0496', 1),
(108, 'Nicole Edholm', 'ncedholm@gmail.com', 'ef91dca3580adc58b5e6c728aa3f627b', 1, '8/14/1978', '606 Columbero Drive', 'mccloud', 'CALIFORNIA', '96057', 'America/Los_Angeles', '(530) 251-3118', 1),
(109, 'DOREE HENRY', 'doree@galbc.org', '9b0a142000a85e3c586f10877567b2dd', 1, '11/16/1980', '18 Capitol Square SW suite 602', 'Atlanta', 'GEORGIA', '30334', 'America/New_York', '(470) 321-3419', 1),
(110, 'WEBWED', 'webwedmobile@gmail.com', '420a45727ade493f69018446daa5ad40', 1, '10/14/1961', '1500 Southland Circle Suite C', 'atlanta', 'GEORGIA', '30318', 'America/New_York', '(678) 431-8668', 1),
(111, 'Sheryl Mason', 'sherylmason32@yahoo.com', '223b1413c475684c9a5ecde39fd7b658', 1, '1/14/1967', '1624 Liberty Valley', 'Decatur', 'GEORGIA', '30032', 'America/New_York', '(404) 438-3233', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
